const std = @import("std");

const modify = @import("modify_state.zig");

const core = @import("core.zig");
const internal = core.internal;
const utils = core.utils;

const file_io = core.file_io;

const state = core.internal.state;

pub const buffer = struct {
    pub fn openByPath(path: []const u8, open_opts: core.OpenOptions) !core.Pair {
        const result = try internal.getAbsolutePath(path);
        const abs_path = result.buf[0..result.len];
        const lasting_path = try state().path_bt_storage.getAndPut(abs_path);

        const handle = createAbsolute(abs_path) catch |err| switch (err) {
            error.BufferAlreadyExists => (core.buffer.getHandleByPathAbsolute(abs_path)).?,

            error.BufferOpenByAnotherProcess => |e| {
                try core.dialogs.startForceOpenBuffer(lasting_path, open_opts);
                return e;
            },
            else => |e| return e,
        };

        return try openByHandle(handle, open_opts);
    }

    pub fn openByHandle(bhandle: core.BufferHandle, open_opts: core.OpenOptions) !core.Pair {
        if (state().tabs.items.len == 0 and open_opts.where != .new_tab) {
            const new_opts = utils.override(open_opts, .{ .where = .new_tab });
            return try openByHandle(bhandle, new_opts);
        }

        const window = switch (open_opts.where) {
            .new_tab, .dir => try buffer_window.create(bhandle),
            .here => blk: {
                var tab = core.tabs.focusedTab().tab;
                var window = tab.focused_window;
                if (tab.hasWindow(window) == .float) {
                    const root = tab.panes.tree.root.?;
                    window = (root.leftMost() orelse root).data.window;
                }
                break :blk window;
            },
        };

        return try openByWindow(window, bhandle, open_opts);
    }

    pub fn openByWindow(window: core.BufferWindow.ID, bhandle: core.BufferHandle, open_opts: core.OpenOptions) !core.Pair {
        if (state().tabs.items.len == 0 and open_opts.where != .new_tab) {
            const new_opts = utils.override(open_opts, .{ .where = .new_tab });
            return try openByWindow(window, bhandle, new_opts);
        }

        switch (open_opts.where) {
            .new_tab => {
                try tabs.create(window);
            },
            .dir => |dir| {
                const tab = core.tabs.focusedTab();
                try tabs.split(tab.index, tab.tab.focused_window, window, .{ .size = open_opts.size, .dir = dir });
            },
            .here => {
                var tab = core.tabs.focusedTab().tab;
                if (tab.hasWindow(window) == .float) {
                    const root = tab.panes.tree.root.?;
                    const pane = (root.leftMost() orelse root).data.window;
                    try buffer_window.setBufferHandle(pane, bhandle);
                } else {
                    try buffer_window.setBufferHandle(window, bhandle);
                }
            },
        }

        // TODO: Defer this call by using addTimedCallback
        if (core.buffer.bufferExists(bhandle)) {
            const buff = internal.getBufferPtr(bhandle) catch unreachable;
            try jump_list.pushJumpPoint(window, buff.path.items, core.buffer_window.cursor(window).?);

            std.log.info("Opened buffer [{?}] {s}", .{ buff.handle, buff.path.items });
        }

        try tabs.setFocusedWindowAndTab(window);

        return .{ .window = window, .buffer = bhandle };
    }

    /// Returns a handle to a buffer
    /// Creates a Buffer and returns a BufferHandle to it
    pub fn create(path: []const u8) !core.BufferHandle {
        const result = try core.getAbsolutePath(path);
        const abs_path = result.buf[0..result.len];
        return try createAbsolute(abs_path);
    }

    pub fn createAbsolute(path: []const u8) !core.BufferHandle {
        switch (try core.buffer.bufferWithPathExistsAbsolute(path)) {
            .in_current_proc => return error.BufferAlreadyExists,
            .in_another_proc => return error.BufferOpenByAnotherProcess,
            .false => {},
        }

        return internal.deferCreateBuffer(state(), path);
    }

    pub fn createPathless() error{OutOfMemory}!core.BufferHandle {
        return internal.deferCreateBuffer(state(), "") catch |err| switch (err) {
            error.OutOfMemory => |e| return e,
        };
    }

    pub fn openScratchBuffer() !void {
        const handle = try createPathless();
        try setBufferType(handle, core.scratch_buffer_type);
        _ = try openByHandle(handle, .{ .where = .new_tab });
    }

    /// Constructs a new path from the given path relative to the file's
    /// directory if the `new_path` is not absolute
    /// Returns an error when:
    /// 1. The new path already exists
    /// 2. New path is a directory
    pub fn renameFromDir(bhandle: core.BufferHandle, new_path: []const u8) !void {
        const data = try core.buffer.getBufferData(bhandle);
        const dirname = std.fs.path.dirname(data.path) orelse return;

        const joined = try std.fs.path.join(core.getAllocator(), &.{ dirname, new_path });
        defer core.getAllocator().free(joined);
        const joined_resolved = try std.fs.path.resolve(core.getAllocator(), &.{joined});
        defer core.getAllocator().free(joined_resolved);

        if (!std.mem.eql(u8, joined_resolved, data.path)) {
            try setBufferPathAbsolute(bhandle, joined_resolved);
        }
    }

    /// Constructs a new path from the given path relative to the `cwd` if the `new_path`
    /// is not absolute
    /// Returns an error when:
    /// 1. The new path already exists
    /// 2. New path is a directory
    pub fn renameFromCwd(bhandle: core.BufferHandle, new_path: []const u8) !void {
        var buf: [std.fs.MAX_PATH_BYTES]u8 = undefined;
        const path = try file_io.getAbsolutePath(state().allocator, new_path, &buf);
        const data = try core.getData(bhandle);

        if (!std.mem.eql(u8, path, data.path)) {
            try setBufferPathAbsolute(bhandle, path);
        }
    }

    pub fn setBufferType(bhandle: core.BufferHandle, new_bt: []const u8) !void {
        try state().deferred_calls.add(.set_buffer_type, .{
            .handle = bhandle,
            .buffer_type = new_bt,
        });
    }

    pub fn setBufferPathAbsolute(handle: core.BufferHandle, new_path: []const u8) !void {
        try modify.canSetBufferPathAbsolute(state(), new_path);
        try state().deferred_calls.add(.set_buffer_path, .{
            .handle = handle,
            .new_path = new_path,
        });
    }

    pub fn save(handle: core.BufferHandle, options: core.SaveOptions) !void {
        try modify.canSaveBuffer(state(), handle, options);
        try state().deferred_calls.add(.save_buffer, .{
            .handle = handle,
            .options = options,
        });
    }

    pub fn kill(handle: core.BufferHandle, options: core.KillOptions) !void {
        try modify.canKillBuffer(state(), handle, options);
        try state().deferred_calls.add(.kill_buffer, .{
            .handle = handle,
            .options = options,
        });
    }

    pub fn setSelection(handle: core.BufferHandle, kind: core.SetSelectionType) !void {
        try state().deferred_calls.add(.set_buffer_selection, .{
            .handle = handle,
            .kind = kind,
        });
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Editing functions
    //
    //

    fn replaceBytes(handle: core.BufferHandle, start_point: core.Buffer.Point, end_point: core.Buffer.Point, string: []const u8) !void {
        try modify.canReplaceBytes(state(), handle, string);
        try state().deferred_calls.add(.replace_bytes, .{
            .handle = handle,
            .start_point = start_point,
            .end_point = end_point,
            .string = string,
        });
    }

    pub fn replaceRange(handle: core.BufferHandle, prange: core.Buffer.PointRange, string: []const u8) !void {
        try replaceBytes(handle, prange.start, prange.end, string);
    }

    pub fn deleteRange(handle: core.BufferHandle, prange: core.Buffer.PointRange) !void {
        try replaceRange(handle, prange, "");
    }

    pub fn insertAt(handle: core.BufferHandle, point: core.Buffer.Point, string: []const u8) !void {
        try replaceBytes(handle, point, point, string);
    }

    pub fn deleteAfter(handle: core.BufferHandle, point: core.Buffer.Point, chars_to_delete: u32) !void {
        var buff = try internal.getBufferPtr(handle);

        const start_index = buff.getIndex(point);
        const end_index = buff.moveIndex(start_index, @intCast(chars_to_delete));
        const end_point = buff.getPoint(end_index);
        try replaceBytes(handle, point, end_point, "");
    }

    pub fn deleteBefore(handle: core.BufferHandle, point: core.Buffer.Point, chars_to_delete: u32) !void {
        var buff = try internal.getBufferPtr(handle);

        const start = buff.moveIndex(buff.getIndex(point), -@as(isize, @intCast(chars_to_delete)));
        const start_point = buff.getPoint(start);
        try replaceBytes(handle, start_point, point, "");
    }

    pub fn setContent(handle: core.BufferHandle, content: []const u8) !void {
        const data = try core.buffer.getBufferData(handle);
        const end_point = core.Buffer.Point{ .row = data.line_count, .col = 0 };
        try replaceBytes(handle, .{}, end_point, content);
    }

    pub fn setLine(handle: core.BufferHandle, row: u32, content: []const u8) !void {
        const buff = try internal.getBufferPtr(handle);

        if (buff.size() == 1 and buff.lineCount() == 1) {
            try insertAt(handle, .{ .row = 0 }, content);
        } else if (row >= buff.lineCount()) {
            try append(handle, content);
        } else {
            const end_point = buff.getPoint(buff.indexOfLastByteAtRow(row));
            try replaceBytes(handle, .{ .row = row }, end_point, content);
        }
    }

    pub fn append(handle: core.BufferHandle, string: []const u8) !void {
        var buff = try internal.getBufferPtr(handle);
        const point = core.Buffer.Point{ .row = buff.lineCount() };
        try replaceBytes(handle, point, point, string);
    }

    //
    //
    // Editing functions
    //
    ////////////////////////////////////////////////////////////////////////////

};

pub const history = struct {
    pub fn pushHistory(handle: core.BufferHandle) !void {
        try modify.canModifyHistory(state(), handle, .push_snapshot);
        try state().deferred_calls.add(.modify_history, .{
            .handle = handle,
            .operation = .push_snapshot,
        });
    }

    pub fn undo(handle: core.BufferHandle) !void {
        try modify.canModifyHistory(state(), handle, .undo);
        try state().deferred_calls.add(.modify_history, .{
            .handle = handle,
            .operation = .undo,
        });
    }

    pub fn redo(handle: core.BufferHandle) !void {
        try modify.canModifyHistory(state(), handle, .redo);
        try state().deferred_calls.add(.modify_history, .{
            .handle = handle,
            .operation = .redo,
        });
    }
};

pub const buffer_window = struct {
    pub fn create(bhandle: core.BufferHandle) !core.BufferWindow.ID {
        return try internal.deferCreateWindow(state(), bhandle);
    }

    pub fn setCursor(id: core.BufferWindow.ID, point: core.Buffer.Point) error{OutOfMemory}!void {
        try state().deferred_calls.add(.set_cursor, .{
            .window = id,
            .point = point,
        });
    }

    pub fn scroll(window: core.BufferWindow.ID, dir: core.ScrollDir, scroll_by: u32) !void {
        try state().deferred_calls.add(.scroll_window, .{
            .window = window,
            .dir = dir,
            .scroll_by = scroll_by,
        });
    }

    pub fn centerWindow(window: core.BufferWindow.ID) !void {
        var bw = internal.getBufferWindow(window) orelse return;
        const diff = bw.centerScreen(core.buffer_window.cursor(window).?);
        const dir: core.ScrollDir = if (diff < 0) .up else .down;

        try scroll(window, dir, @truncate(@abs(diff)));
    }

    pub fn swapCursorAndAnchor(id: core.BufferWindow.ID) void {
        const bw = internal.getBufferWindow(id) orelse return;

        const selection = core.buffer.getSelection(bw.bhandle) catch return;
        const cur = core.buffer_window.cursor(id).?;
        const anchor = selection.anchor orelse return;

        buffer.setSelection(bw.bhandle, .{ .set_anchor = cur }) catch return;
        buffer_window.setCursor(id, anchor) catch return;
    }

    pub fn close(window: core.BufferWindow.ID) !void {
        try state().deferred_calls.add(.kill_window, .{ .window = window });
    }

    pub fn setBufferHandle(window: core.BufferWindow.ID, new_bhandle: core.BufferHandle) !void {
        try state().deferred_calls.add(.set_buffer_handle, .{
            .window = window,
            .new_handle = new_bhandle,
        });
    }
};

pub const tabs = struct {
    pub fn create(window: core.BufferWindow.ID) !void {
        try state().deferred_calls.add(.create_tab_with_window, .{ .window = window });
    }

    pub fn split(tab_index: usize, other_window: core.BufferWindow.ID, new_window: core.BufferWindow.ID, opts: core.WindowSplitOptions) !void {
        try state().deferred_calls.add(.open_window, .{
            .window = new_window,
            .tab_index = tab_index,
            .kind = .{ .split = .{
                .other_window = other_window,
                .opts = opts,
            } },
        });
    }

    pub fn float(tab_index: usize, window: core.BufferWindow.ID, rect: core.draw.Rect) !void {
        try state().deferred_calls.add(.open_window, .{
            .window = window,
            .tab_index = tab_index,
            .kind = .{ .float = rect },
        });
    }

    pub fn repositionFloat(window: core.BufferWindow.ID, rect: core.draw.Rect) !void {
        const tab_index = (core.tabs.tabOfWindow(window) orelse return).index;
        try float(tab_index, window, rect);
    }

    pub fn setFocusedWindowIn(tab_index: usize, window: core.BufferWindow.ID) !void {
        try state().deferred_calls.add(.set_focused_window, .{
            .tab_index = tab_index,
            .window = window,
            .focus_tab = false,
        });
    }

    /// Looks through tabs and selects the window and the tab it's in.
    /// If the window is not found the function returns silently
    pub fn setFocusedWindowAndTab(window: core.BufferWindow.ID) !void {
        const result = internal.tabOfWindow(window) orelse return;
        try state().deferred_calls.add(.set_focused_window, .{
            .tab_index = result.index,
            .window = window,
            .focus_tab = true,
        });
    }
};

pub const jump_list = struct {
    pub fn pushOldAndCurrent(old: core.JumpData) !void {
        try pushJumpPoint(old.id, old.path, old.point);
        try pushCurrentPoint();
    }

    pub fn pushCurrentPoint() !void {
        const data = core.jump_list.currentJumpData();
        try pushJumpPoint(data.id, data.path, data.point);
    }

    pub fn pushJumpPoint(window: core.BufferWindow.ID, file_path: []const u8, point: core.Buffer.Point) !void {
        if (!std.fs.path.isAbsolute(file_path)) return;

        try state().deferred_calls.add(.push_jump_point, .{
            .window = window,
            .path = file_path,
            .point = point,
        });
    }

    pub fn jumpNext(window: core.BufferWindow.ID) !void {
        try state().deferred_calls.add(.jump_list_jump, .{
            .window = window,
            .kind = .next,
        });
    }

    pub fn jumpPrev(window: core.BufferWindow.ID) !void {
        try state().deferred_calls.add(.jump_list_jump, .{
            .window = window,
            .kind = .prev,
        });
    }
};

pub const completion = struct {
    pub fn accept() void {
        state().ac_engine.acceptSelected() catch return;
    }
};

pub const registers = struct {
    pub fn pasteAtFrom(register: []const u8, bhandle: core.BufferHandle, point: core.Buffer.Point) !void {
        const content = core.registers.getFrom(register);
        try buffer.insertAt(bhandle, point, content);
    }
};
