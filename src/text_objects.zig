const std = @import("std");
const unicode = std.unicode;

const Buffer = @import("buffer.zig");
const motions = @import("motions.zig");

const core = @import("core.zig");

const Point = Buffer.Point;
const PointRange = Buffer.PointRange;
const getSize = core.buffer.getSize;

const BufferHandle = core.BufferHandle;

const inList = motions.inList;
const isWhiteSpace = motions.isWhiteSpace;
const isEndOfRow = motions.isEndOfRow;

pub const pairs = struct {
    pub const parens = [2]u21{ '(', ')' };
    pub const braces = [2]u21{ '{', '}' };
    pub const brackets = [2]u21{ '[', ']' };
    pub const angle_brackets = [2]u21{ '<', '>' };
};

comptime {
    for (std.meta.fields(TextObjectKind)) |field| {
        // if (std.mem.indexOf(u8, field.name, "inner_") != null)
        // _ = @as(TextObjectKind, @enumFromInt(std.meta.activeTag(field.value))).aroundEquivalent();

        if (std.mem.indexOf(u8, field.name, "around_") == null and std.mem.indexOf(u8, field.name, "inner_") == null)
            @compileError("All text objects must start with `around_` or `inner_`");
    }
}

pub const TextObjectKind = union(enum) {
    /// Doesn't care about delimiters
    inner_full_word,
    inner_word,
    /// Whatever is between two newline bytes
    inner_row,
    inner_pair: [2]u21,
    inner_cp: u21,

    /// Doesn't care about delimiters
    around_full_word,
    around_word,
    /// Whatever is between two newline bytes including the last newline bytes
    around_row,
    around_pair: [2]u21,
    around_cp: u21,

    pub fn getRange(text_object: TextObjectKind, allocator: std.mem.Allocator, handle: BufferHandle, point: Point, delimiters: []const u21) ?Buffer.PointRange {
        return switch (text_object) {
            .inner_full_word => innerWord(allocator, handle, point, &.{}),
            .inner_word => innerWord(allocator, handle, point, delimiters),
            .inner_row => innerRow(handle, point),

            .inner_pair => |pair| innerPair(allocator, handle, point, pair),

            .inner_cp => |cp| innerCP(allocator, handle, point, cp),

            // //
            // //
            // //

            .around_full_word => aroundWord(allocator, handle, point, &.{}),
            .around_word => aroundWord(allocator, handle, point, delimiters),

            .around_row => aroundRow(point),

            .around_pair => |pair| aroundPair(allocator, handle, point, pair),

            .around_cp => |cp| aroundCP(allocator, handle, point, cp),
        };
    }

    pub fn aroundEquivalent(kind: TextObjectKind) TextObjectKind {
        std.debug.assert(std.mem.indexOf(u8, @tagName(kind), "inner_") != null); // only accept inner objects

        return switch (kind) {
            .inner_full_word => .around_full_word,
            .inner_word => .around_word,
            .inner_row => .around_word,
            .inner_pair => |pair| .{ .around_pair = pair },
            .inner_cp => |cp| .{ .around_cp = cp },

            .around_word,
            .around_row,
            .around_pair,
            .around_cp,
            .around_full_word,
            => unreachable,
        };
    }
};

pub fn innerWord(allocator: std.mem.Allocator, handle: BufferHandle, point: Point, delimiters: []const u21) ?PointRange {
    var line = (core.buffer.getLine(allocator, handle, point.row) catch null) orelse return null;
    defer allocator.free(line);

    const left_side = line[0..core.utils.getIndexOfCol(line, point.col +| 1)];
    const right_side = line[core.utils.getIndexOfCol(line, point.col)..];

    const started_on_white_space = blk: {
        const last = if (left_side.len > 0) isWhiteSpace(left_side[left_side.len -| 1]) else false;
        const first = isWhiteSpace(right_side[0]);
        break :blk first and last;
    };

    if (started_on_white_space) {
        const start_col = startOfWhiteSpace(left_side, point.col) orelse return null;
        const end_col = endOfWhiteSpace(right_side, point.col) orelse return null;
        return .{ .start = point.setCol(start_col), .end = point.setCol(end_col) };
    } else {
        const start_col = startOfWordBackward(left_side, delimiters, point.col) orelse return null;
        const end_col = endOfWord(right_side, delimiters, point.col) orelse return null;
        return .{ .start = point.setCol(start_col), .end = point.setCol(end_col) };
    }
}

pub fn aroundWord(allocator: std.mem.Allocator, handle: BufferHandle, point: Point, delimiters: []const u21) ?PointRange {
    var line = (core.buffer.getLine(allocator, handle, point.row) catch null) orelse return null;
    defer allocator.free(line);

    const left_side = line[0..core.utils.getIndexOfCol(line, point.col +| 1)];
    const right_side = line[core.utils.getIndexOfCol(line, point.col)..];

    const started_on_white_space = blk: {
        const last = if (left_side.len > 0) isWhiteSpace(left_side[left_side.len -| 1]) else false;
        const first = isWhiteSpace(right_side[0]);
        break :blk first and last;
    };

    if (started_on_white_space) {
        const start_col = startOfWhiteSpace(left_side, point.col) orelse return null;
        const end_col = endOfWhiteSpace(right_side, point.col) orelse return null;
        return .{ .start = point.setCol(start_col), .end = point.setCol(end_col) };
    } else {
        const start_col = startOfWordBackward(left_side, delimiters, point.col) orelse return null;
        const end_col = startOfWordForward(right_side, delimiters) orelse return null;
        return .{ .start = point.setCol(start_col), .end = point.addCol(end_col) };
    }
}

pub fn innerCP(allocator: std.mem.Allocator, handle: BufferHandle, point: Point, cp: u21) ?PointRange {
    const start = motions.backTill(allocator, handle, point, cp, std.math.maxInt(u32)) orelse return null;
    const end = motions.find(allocator, handle, point, cp, std.math.maxInt(u32)) orelse return null;

    const cp_at_cursor = core.buffer.getCodepointAt(handle, point) catch return null;
    if (cp_at_cursor == cp) {
        const c = closest(point, start, end);
        if (std.meta.eql(start, c)) {
            return .{ .start = start, .end = point };
        } else {
            return .{ .start = point.addCol(1), .end = end };
        }
    } else {
        return .{ .start = start, .end = end };
    }
}

pub fn aroundCP(allocator: std.mem.Allocator, handle: BufferHandle, point: Point, cp: u21) ?PointRange {
    const start = motions.backFind(allocator, handle, point, cp, std.math.maxInt(u32)) orelse return null;
    const end = motions.find(allocator, handle, point, cp, std.math.maxInt(u32)) orelse return null;

    const cp_at_cursor = core.buffer.getCodepointAt(handle, point) catch return null;
    if (cp_at_cursor == cp) {
        const c = closest(point, start, end);
        if (std.meta.eql(start, c)) {
            return .{ .start = start, .end = point.addCol(1) };
        } else {
            return .{ .start = point, .end = end.addCol(1) };
        }
    } else {
        return .{ .start = start, .end = end };
    }
}

pub fn innerPair(allocator: std.mem.Allocator, handle: BufferHandle, point: Point, pair: [2]u21) ?PointRange {
    const cp_at_cursor = core.buffer.getCodepointAt(handle, point) catch return null;
    if (cp_at_cursor == pair[0]) {
        const end = findPairForward(allocator, handle, point.addCol(1), pair) orelse return null;
        return .{ .start = point.addCol(1), .end = end };
    } else if (cp_at_cursor == pair[1]) {
        const start = findPairBackward(allocator, handle, point.subCol(1), pair) orelse return null;
        return .{ .start = start.addCol(1), .end = point };
    } else {
        const start = findPairBackward(allocator, handle, point, pair) orelse return null;
        const end = findPairForward(allocator, handle, point, pair) orelse return null;
        return .{ .start = start.addCol(1), .end = end };
    }
}

pub fn aroundPair(allocator: std.mem.Allocator, handle: BufferHandle, point: Point, pair: [2]u21) ?PointRange {
    const cp_at_cursor = core.buffer.getCodepointAt(handle, point) catch return null;
    if (cp_at_cursor == pair[0]) {
        const end = findPairForward(allocator, handle, point.addCol(1), pair) orelse return null;
        return .{ .start = point, .end = end.addCol(1) };
    } else if (cp_at_cursor == pair[1]) {
        const start = findPairBackward(allocator, handle, point.subCol(1), pair) orelse return null;
        return .{ .start = start, .end = point.addCol(1) };
    } else {
        const start = findPairBackward(allocator, handle, point, pair) orelse return null;
        const end = findPairForward(allocator, handle, point, pair) orelse return null;
        return .{ .start = start, .end = end.addCol(1) };
    }
}

pub fn innerRow(handle: BufferHandle, point: Point) ?Buffer.PointRange {
    const max_cols = core.buffer.lastColAtRow(handle, point.row) catch return null;
    return .{ .start = point.setCol(0), .end = point.setCol(max_cols) };
}

pub fn aroundRow(point: Point) Buffer.PointRange {
    return .{ .start = point.setCol(0), .end = .{ .row = point.row +| 1 } };
}

pub fn findPairForward(allocator: std.mem.Allocator, handle: BufferHandle, point: Point, pair: [2]u21) ?Point {
    const left = pair[0];
    const right = pair[1];

    var right_to_ignore: usize = 0;

    var iter = core.buffer.LineIterator.init(allocator, handle, point, .{ .row = std.math.maxInt(u32) }) catch return null;
    defer iter.deinit();

    var col = point.col;
    while (iter.next() catch null) |result| {
        defer col = 0;

        var view = core.utf8.Utf8Iterator.init(result.line);
        while (view.nextCodepointSlice()) |slice| {
            defer col += 1;
            const cp = unicode.utf8Decode(slice) catch continue;
            if (cp == left) {
                right_to_ignore += 1;
            } else if (cp == right and right_to_ignore != 0) {
                right_to_ignore -= 1;
            } else if (cp == right and right_to_ignore == 0) {
                return .{ .row = result.number, .col = col };
            }
        }
    }

    return null;
}

pub fn findPairBackward(allocator: std.mem.Allocator, handle: BufferHandle, point: Point, pair: [2]u21) ?Point {
    const left = pair[0];
    const right = pair[1];

    var left_to_ignore: usize = 0;

    var iter = core.buffer.ReverseLineIterator.init(allocator, handle, .{}, point.addCol(1)) catch return null;
    defer iter.deinit();

    while (iter.next() catch null) |result| {
        var view = core.utf8.ReverseUtf8View.init(result.line);
        var i: usize = result.line.len -| 1;
        while (view.nextSlice()) |slice| {
            const cp = unicode.utf8Decode(slice) catch continue;

            defer i -|= slice.len;
            if (cp == right) {
                left_to_ignore += 1;
            } else if (cp == left and left_to_ignore != 0) {
                left_to_ignore -= 1;
            } else if (cp == left and left_to_ignore == 0) {
                return .{
                    .row = result.number,
                    .col = core.utils.getColOfIndex(result.line, i),
                };
            }
        }
    }

    return null;
}

// a word end is:
// non-white-space - end-of-line
// non-white-space - white-space
// none-delimiter - delimiter
// delimiter - none-delimiter
pub fn endOfWord(string: []const u8, delimiters: []const u21, start_col: u32) ?u32 {
    var iter = core.utf8.Utf8Iterator.init(string);
    var col = start_col;
    while (iter.nextCodepointSlice()) |slice| {
        defer col += 1;
        const cp = unicode.utf8Decode(slice) catch continue;
        const next_cp = iter.peekCodePoint() catch continue;

        if (!isWhiteSpace(cp) and isEndOfRow(next_cp))
            return col +| 1;

        if (!isWhiteSpace(cp) and isWhiteSpace(next_cp))
            return col +| 1;

        if ((!inList(next_cp, delimiters) and inList(cp, delimiters)) or
            (inList(next_cp, delimiters) and !inList(cp, delimiters)))
            return col +| 1;
    }

    return null;
}

// a word start is:
// start-of-line - non-white-space
// white-space - non-white-space
// none-delimiter - delimiter
// delimiter - none-delimiter
pub fn startOfWordBackward(string: []const u8, delimiters: []const u21, start_col: u32) ?u32 {
    var iter = core.utf8.ReverseUtf8View.init(string);
    var col = start_col;
    while (iter.nextSlice()) |slice| {
        defer col -|= 1;
        const cp = unicode.utf8Decode(slice) catch continue;
        const prev_cp = iter.peekCodePoint() catch continue;

        if (prev_cp == null and !isWhiteSpace(cp))
            return 0;

        if (isWhiteSpace(prev_cp) and !isWhiteSpace(cp))
            return col;

        if ((!inList(prev_cp, delimiters) and inList(cp, delimiters)) or
            (inList(prev_cp, delimiters) and !inList(cp, delimiters)))
            return col;
    }

    return null;
}

// a word is:
// non-white-space - end-of-line
// white-space - non-white-space
// none-delimiter - delimiter
// delimiter - none-delimiter
pub fn startOfWordForward(string: []const u8, delimiters: []const u21) ?u32 {
    var iter = core.utf8.Utf8Iterator{ .bytes = string };
    var col: u32 = 0;
    while (iter.nextCodepointSlice()) |slice| {
        defer col += 1;
        const cp = unicode.utf8Decode(slice) catch continue;
        const next_cp = (iter.peekCodePoint() catch continue) orelse return null;

        if (!isWhiteSpace(cp) and isEndOfRow(next_cp))
            return col +| 1;

        if (isWhiteSpace(cp) and !isWhiteSpace(next_cp))
            return col +| 1;

        if (!inList(cp, delimiters) and inList(next_cp, delimiters))
            return col +| 1;

        if (inList(cp, delimiters) and !inList(next_cp, delimiters))
            return col +| 1;
    }

    return null;
}

pub fn startOfWhiteSpace(string: []const u8, start_col: u32) ?u32 {
    var iter = core.utf8.ReverseUtf8View.init(string);
    var col = start_col;
    while (iter.nextSlice()) |slice| {
        defer col -|= 1;
        const cp = unicode.utf8Decode(slice) catch continue;
        const prev_cp = iter.peekCodePoint() catch continue;

        if (prev_cp == null and isWhiteSpace(cp))
            return 0;

        if (!isWhiteSpace(prev_cp) and isWhiteSpace(cp))
            return col;
    }

    return null;
}

pub fn endOfWhiteSpace(string: []const u8, start_col: u32) ?u32 {
    var iter = core.utf8.Utf8Iterator.init(string);
    var col = start_col;
    while (iter.nextCodepointSlice()) |slice| {
        defer col += 1;
        const cp = unicode.utf8Decode(slice) catch continue;
        const next_cp = iter.peekCodePoint() catch continue;

        if (isWhiteSpace(cp) and !isWhiteSpace(next_cp))
            return col +| 1;
    }

    return null;
}

fn closest(p: Point, x: Point, y: Point) Point {
    const pxr = core.utils.diff(p.row, x.row);
    const pyr = core.utils.diff(p.row, y.row);
    if (pxr < pyr) {
        return x;
    } else if (pxr > pyr) {
        return y;
    } else {
        const pxc = core.utils.diff(p.col, x.col);
        const pyc = core.utils.diff(p.col, y.col);

        if (pxc < pyc) {
            return x;
        } else {
            return y;
        }
    }
}
