const std = @import("std");
const print = std.debug.print;
const fs = std.fs;
const ArrayListUnmanaged = std.ArrayListUnmanaged;
const eql = std.mem.eql;
const assert = std.debug.assert;
const file_io = @import("file_io.zig");

const modify = @import("modify_state.zig");
const State = @import("state.zig");
const core = @import("core.zig");

const BufferWindow = @import("buffer_window.zig").BufferWindow;
const Dir = BufferWindow.Dir;
const WindowTree = @import("buffer_window.zig").WindowTree;
const command_line = @import("command_line.zig");

const utils = @import("utils.zig");

const log = std.log.scoped(.core);

pub var __state: ?State = null;
pub const thabit_options = @import("thabit_options");
pub const input_layer = @import("input_layer");
pub const user = if (thabit_options.init_user) @import("user") else struct {
    pub fn init() !void {}
    pub fn deinit() void {}
};

////////////////////////////////////////////////////////////////////////////////
// Section: Error and Struct definitions

pub const key_queue_size = 1024;
pub const char_queue_size = 1024;
pub const KeyQueue = std.BoundedArray(input.Key, key_queue_size);
pub const CharQueue = std.BoundedArray(u21, char_queue_size);

pub const CommandLine = command_line.CommandLine;
pub const Buffer = @import("buffer.zig");
pub const input = @import("input.zig");
pub const hooks = @import("hooks.zig");
pub const Marks = @import("marks.zig");
pub usingnamespace @import("buffer_window.zig");

pub const BufferHandle = struct { handle: u32 };

pub fn state() *State {
    return &(__state.?);
}

var initialized = false;
pub fn initThabit(allocator: std.mem.Allocator) !void {
    if (initialized) return;

    __state = try State.init(allocator);
    errdefer __state.?.deinit();

    if (try createDataPath(allocator, file_io.data_dir_name)) |path| {
        defer allocator.free(path);
        state().data_dir_path = try state().path_bt_storage.getAndPut(path);
    }

    { // init builtins
        try core.builtin.auto_complete.init(allocator);
    }

    const default_displayers = [_]core.BufferDisplayer{
        core.SymbolHighlight.interface(),
        core.SearchResultsDisplayer.interface(),
    };
    try state().buffer_displayers.appendSlice(&default_displayers);

    try input_layer.init();
    errdefer input_layer.deinit();

    try user.init();
    errdefer user.deinit();

    if (state().tabs.items.len == 0) {
        try core.dmodify.buffer.openScratchBuffer();
    }

    initialized = true;
}

pub fn deinitThabit() void {
    if (!initialized) return;

    state().dialogs.deinitAllDialogs();
    state().deferred_calls.event_listeners.dispatch(.thabit_shutdown);

    user.deinit();

    input_layer.deinit();

    { // deinit builtins
        core.builtin.auto_complete.deinit();
    }

    var iter = state().buffers.iterator();
    while (iter.next()) |buff| {
        _ = modify.killBuffer(state(), buff.handle, .{ .force_kill = true }) catch continue;
    }

    __state.?.deinit();
    __state = null;

    initialized = false;
}

pub fn eventLoopTick(key_queue: *KeyQueue, char_queue: *CharQueue) !void {
    state().extra_frames -|= 1;
    var timer = try std.time.Timer.start();

    defer {
        state().notifications.clearDone(timer.read());
    }

    switch (state().input_mode) {
        .input_layer => core.input_layer.handleInput(key_queue, char_queue),
        .dialog => handleOpenDialog(key_queue, char_queue),
        .ignore => {},
    }

    try handleTimedCallbacks();

    const deferred_calls = &state().deferred_calls;
    const len = deferred_calls.calls.items.len;

    for (deferred_calls.observers.items) |observer| {
        observer.callback(deferred_calls.calls.items, .before);
    }

    for (deferred_calls.calls.items) |mod| {
        const result = mod.call(state()) catch |err| {
            std.log.err("{}: {}", .{ mod, err });
            continue;
        };

        for (result.events[0..result.len]) |event| {
            state().deferred_calls.event_listeners.dispatch(event);
            std.debug.assert(len == deferred_calls.calls.items.len); // event listeners are not allowed to defer modify the state
        }
    }

    for (deferred_calls.observers.items) |observer| {
        observer.callback(deferred_calls.calls.items, .after);
    }

    deferred_calls.clearRetainingCapacity();
}

pub fn deferCreateBuffer(s: *State, path: []const u8) !BufferHandle {
    std.debug.assert(std.fs.path.isAbsolute(path) or path.len == 0);

    try s.buffers.growCapacity(1);
    try s.path_bt_storage.ensureUnusedCapacity(2);

    s.path_bt_storage.put(path) catch unreachable;

    const handle = BufferHandle{ .handle = s.nextUniqueId() };
    try s.deferred_calls.add(.create_buffer, .{
        .handle = handle,
        .path = path,
    });

    return handle;
}

pub fn deferCreateWindow(s: *State, bhandle: BufferHandle) !BufferWindow.ID {
    const id = BufferWindow.ID{ .id = s.nextUniqueId() };
    try s.deferred_calls.add(.create_window, .{
        .handle = bhandle,
        .window = id,
    });

    return id;
}

pub fn getWindowsWithBuffer(alloctor: std.mem.Allocator, bhandle: BufferHandle) ![]const BufferWindow.ID {
    var list = std.ArrayList(BufferWindow.ID).init(alloctor);
    defer list.deinit();

    var iter = state().buffer_windows.iterator();
    while (iter.next()) |window| {
        if (window.bhandle.handle == bhandle.handle) {
            try list.append(window.id);
        }
    }

    return try list.toOwnedSlice();
}

pub fn getFirstBufferWithPath() ?BufferHandle {
    var iter = state().buffers.iterator();
    while (iter.next()) |buffer| {
        if (buffer.handle == core.command_line.handle()) continue;
        if (buffer.handle == core.floating_window.handle()) continue;
        if (buffer.path.items.len != 0)
            return buffer.handle;
    }

    return null;
}

pub fn getPathlessBuffer() ?BufferHandle {
    var iter = state().buffers.iterator();
    while (iter.next()) |buffer| {
        if (buffer.handle == core.command_line.handle()) continue;
        if (buffer.handle == core.floating_window.handle()) continue;
        if (buffer.path.items.len == 0)
            return buffer.handle;
    }

    return null;
}

pub fn getPathlessTypelessBuffer() ?BufferHandle {
    var iter = state().buffers.iterator();
    while (iter.next()) |buffer| {
        if (buffer.handle.handle == core.command_line.handle().handle) continue;
        if (buffer.path.items.len == 0 and buffer.buffer_type.items.len == 0)
            return buffer.handle;
    }

    return null;
}

pub fn handleOpenDialog(keys: *KeyQueue, chars: *CharQueue) void {
    std.debug.assert(state().input_mode == .dialog);
    const dialog = state().dialogs.peek().?;

    switch (dialog.kind) {
        .key => if (keys.slice().len == 0) return,
        .text => if (chars.slice().len == 0) return,
    }

    const contains_shift_escape = blk: {
        for (keys.slice()) |key| {
            if (key.kind == .escape) {
                break :blk key.mod.contains(.shift);
            }
        }
        break :blk false;
    };

    const contains_shift_enter = blk: {
        for (keys.slice()) |key| {
            if (key.kind == .enter) {
                break :blk key.mod.contains(.shift);
            }
        }
        break :blk false;
    };
    _ = contains_shift_enter;

    const callback_result = if (contains_shift_escape) .done else switch (dialog.kind) {
        .key => |k| blk: {
            defer keys.resize(0) catch unreachable;
            break :blk k.callback(dialog.ptr, keys.slice()[0]);
        },
        .text => |t| blk: {
            defer keys.resize(0) catch unreachable;
            for (keys.slice()) |key| {
                if (key.kind == .backspace) _ = chars.popOrNull();
            }

            var buf: [char_queue_size * 4]u8 = undefined;
            const string = charQueueToString(chars, &buf) catch unreachable;
            break :blk t.callback(dialog.ptr, string);
        },
    };

    switch (callback_result) {
        .not_done => {},
        .done => {
            var removed = state().dialogs.pop().?;
            removed.deinit();

            if (state().dialogs.queue.items.len == 0) {
                state().input_mode = .input_layer;
                _ = state().dialogs.ptrs_arena.reset(.free_all);
            }

            keys.resize(0) catch unreachable;
            chars.resize(0) catch unreachable;

            if (contains_shift_escape) {
                state().dialogs.deinitAllDialogs();
                state().input_mode = .input_layer;
                keys.resize(0) catch unreachable;
                chars.resize(0) catch unreachable;
            }
        },
    }
}

pub fn handleTimedCallbacks() !void {
    var to_call = std.ArrayList(struct { cb: TimedCallback, i: usize }).init(state().allocator);
    defer to_call.deinit();

    var i: usize = 0;
    while (i < state().timed_callbacks.items.len) {
        var cb = state().timed_callbacks.items[i];
        if (cb.timer.read() / std.time.ns_per_ms >= cb.requested_time) {
            _ = state().timed_callbacks.orderedRemove(i);
            try to_call.append(.{ .cb = cb, .i = i });
        } else {
            i += 1;
        }
    }

    for (to_call.items) |tc| {
        tc.cb.call() catch continue;
    }
}

pub fn nextTimedCallbackTime() ?u64 {
    var next_callback_time: ?u64 = null;
    for (state().timed_callbacks.items) |cb| {
        next_callback_time = if (next_callback_time) |nt|
            @min(nt, cb.requested_time)
        else
            cb.requested_time;
    }

    return next_callback_time;
}

////////////////////////////////////////////////////////////////////////////////
// Section: Default status line hooks

pub const default_status_line = struct {
    pub const cursor_point = "cursor point";
    pub const buffer_name = "buffer name";
    pub const buffer_type = "buffer type";
    pub const buffer_size = "buffer size";

    pub fn initDefualtStatusLine(gs: *State) !void {
        // default status line
        try gs.status_line.add(cursor_point, .mid);
        try gs.status_line.add(buffer_name, .left);
        try gs.status_line.add(buffer_type, .left);
        try gs.status_line.add(buffer_size, .left);

        try gs.deferred_calls.event_listeners.attach(undefined, hookCallback);
    }

    pub fn hookCallback(_: *anyopaque, hook: core.Event) anyerror!void {
        switch (hook) {
            .after_cursor_move, .buffer_window_focused, .after_change => {
                const id = selectedTab().focused_window;
                const handle = core.buffer_window.bufferHandle(id).?;

                updateBufferInfo(handle, id);
            },
            else => return,
        }
    }

    pub fn updateBufferInfo(handle: BufferHandle, wid: BufferWindow.ID) void {
        {
            const point = core.buffer_window.cursor(wid) orelse return;
            const size = comptime std.fmt.count("{}", .{std.math.maxInt(u64)}) * 2 + 1;
            var buf: [size]u8 = undefined;
            const value = std.fmt.bufPrint(&buf, "{}:{}", .{ point.row + 1, point.col + 1 }) catch return;
            state().status_line.setValue(cursor_point, value);
        }

        const data = core.buffer.getBufferData(handle) catch return;

        state().status_line.setValue(buffer_name, fs.path.basename(data.path));
        state().status_line.setValue(buffer_type, data.buffer_type);

        var buf: [std.fmt.count("{}", .{std.math.maxInt(u64)}) + 1]u8 = undefined;
        const size = if (data.size < 1024)
            std.fmt.bufPrint(&buf, "{}b", .{data.size}) catch return
        else
            std.fmt.bufPrint(&buf, "{d:.1}k", .{@as(f32, @floatFromInt(data.size)) / 1024}) catch return;

        state().status_line.setValue(buffer_size, size);
    }
};

pub const TimedCallback = struct {
    pub const Callback = *const fn (*anyopaque) anyerror!void;
    ptr: *anyopaque,
    requested_time: u64,
    timer: std.time.Timer,
    callback: Callback,

    pub fn call(self: TimedCallback) anyerror!void {
        try self.callback(self.ptr);
    }
};

pub fn createDataPath(allocator: std.mem.Allocator, data_dir_name: []const u8) !?[]const u8 {
    const path = try std.fs.getAppDataDir(allocator, data_dir_name);

    std.fs.makeDirAbsolute(path) catch |err| switch (err) {
        else => {},
    };

    return path;
}

pub fn charQueueToString(chars: *CharQueue, buf: []u8) ![]u8 {
    var i: usize = 0;
    for (chars.slice()) |cp| {
        i += try std.unicode.utf8Encode(cp, buf[i..]);
    }

    return buf[0..i];
}

pub const BufferWindowCycle = std.BoundedArray(BufferWindow.ID, 50);
pub const Tab = struct {
    pub const WindowKind = enum { float, pane };

    const FloatingWindow = struct {
        window: BufferWindow.ID,
        /// The values of this rect must be between 0 to 1 to represent
        /// position and size in percentages of the total tab size
        rect: core.draw.Rect,
    };

    panes: WindowTree,
    floats: std.ArrayList(FloatingWindow),
    focused_window: BufferWindow.ID,

    pub fn init(allocator: std.mem.Allocator, focused_window: BufferWindow.ID) !Tab {
        var tab = Tab{
            .panes = WindowTree.init(allocator),
            .focused_window = focused_window,
            .floats = std.ArrayList(FloatingWindow).init(allocator),
        };
        const node = try tab.panes.createWindow(focused_window);

        tab.panes.setRoot(node);
        return tab;
    }

    pub fn deinit(tab: *Tab) void {
        tab.panes.deinit();
        tab.floats.deinit();
    }

    pub fn split(tab: *Tab, window: BufferWindow.ID, new_window: BufferWindow.ID, size: f32, dir: WindowTree.Dir) !bool {
        std.debug.assert(tab.hasWindow(window) == .pane);
        std.debug.assert(tab.hasWindow(new_window) == null);

        const result = try tab.panes.split(window, new_window, size, dir);
        if (result) {
            tab.focused_window = new_window;
        }

        return result;
    }

    pub fn addFloating(tab: *Tab, window: BufferWindow.ID, rect: core.draw.Rect) !void {
        std.debug.assert(tab.hasWindow(window) == null);

        try tab.floats.append(.{ .window = window, .rect = rect });
    }

    pub fn close(tab: *Tab, window: BufferWindow.ID) void {
        const kind = tab.hasWindow(window).?; // window must exist

        switch (kind) {
            .pane => {
                const replacement_window = tab.getSibling(window);
                tab.panes.remove(window);

                if (replacement_window) |win| {
                    tab.focused_window = win;
                }
            },
            .float => {
                const index = tab.getFloat(window).?;
                _ = tab.floats.orderedRemove(index);

                if (window.id == tab.focused_window.id and tab.panes.tree.root != null) {
                    tab.focused_window = tab.panes.tree.root.?.leftMost().?.data.window;
                }
            },
        }
    }

    pub fn hasWindow(tab: Tab, window: BufferWindow.ID) ?WindowKind {
        if (tab.panes.getWindowNode(window)) |_| return .pane;
        if (tab.getFloat(window)) |_| return .float;
        return null;
    }

    fn getSibling(tab: *Tab, window: BufferWindow.ID) ?BufferWindow.ID {
        const root = tab.panes.tree.root orelse return null;
        if (root.data == .split) {
            const node = tab.panes.getWindowNode(window) orelse return null;
            const parent = node.parent.?;

            var is_left_child = false;
            const sibling = blk: {
                if (node == parent.left) {
                    is_left_child = true;
                    break :blk parent.right.?;
                } else {
                    is_left_child = false;
                    break :blk parent.left.?;
                }
            };

            switch (sibling.data) {
                .window => |id| return id,
                .split => {
                    // All nodes of kind .window are leaf nodes
                    const leaf = if (is_left_child) sibling.rightMost().? else sibling.leftMost().?;
                    return leaf.data.window;
                },
            }
        }

        return null;
    }

    pub fn emptyFloats(tab: *Tab) bool {
        return tab.floats.items.len == 0;
    }

    pub fn emptyPanes(tab: Tab) bool {
        return tab.panes.tree.root == null;
    }

    fn getFloat(tab: Tab, window: BufferWindow.ID) ?usize {
        for (tab.floats.items, 0..) |float, i| {
            if (float.window.id == window.id) return i;
        }

        return null;
    }
};

pub fn selectedTab() *Tab {
    const i = state().tabs_view.selected_index.?;
    return &state().tabs.items[i];
}

pub fn selectedTabIndex() usize {
    return state().tabs_view.selected_index.?;
}

pub fn tabOfWindow(window_id: BufferWindow.ID) ?struct { tab: *Tab, index: usize } {
    for (state().tabs.items, 0..) |*tab, i| {
        if (tab.panes.getWindowNode(window_id)) |_| {
            return .{ .tab = tab, .index = i };
        }

        if (tab.getFloat(window_id)) |_| {
            return .{ .tab = tab, .index = i };
        }
    }

    return null;
}

pub const CustomDrawing = struct {
    pub const DrawFn = fn (ptr: *anyopaque, ctx: core.draw.UIContext) anyerror!DrawResult;
    pub const DrawResult = struct {
        draw_list: core.draw.DrawList,
        /// Only respected when drawing pane windows
        cursor_rect: core.draw.Rect = .{},
    };

    windows: []const BufferWindow.ID,
    ptr: *anyopaque,
    draw: *const DrawFn,
};

pub fn windowHasCustomDrawing(window: BufferWindow.ID) bool {
    return getCustomDrawingFor(window) != null;
}

pub fn getCustomDrawingFor(window: BufferWindow.ID) ?CustomDrawing {
    for (state().custom_drawing.items) |item| {
        if (utils.anyEql(BufferWindow.ID, item.windows, window)) {
            return item;
        }
    }

    return null;
}

pub fn getBufferPtr(handle: BufferHandle) !*Buffer {
    return state().buffers.getPtrBy(handle) orelse error.BufferDoesNotExist;
}

pub fn getAbsolutePath(arg_path: []const u8) !struct { buf: [std.fs.MAX_PATH_BYTES]u8, len: usize } {
    var out: [std.fs.MAX_PATH_BYTES]u8 = undefined;
    const slice = try file_io.getAbsolutePath(state().allocator, arg_path, &out);
    return .{ .buf = out, .len = slice.len };
}

pub fn getBufferWindow(id: BufferWindow.ID) ?*BufferWindow {
    return state().buffer_windows.getPtrBy(id);
}
