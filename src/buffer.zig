const std = @import("std");
const builtin = @import("builtin");
const print = std.debug.print;
const fs = std.fs;
const ArrayList = std.ArrayList;
const Stack = std.ArrayList;
const unicode = std.unicode;
const assert = std.debug.assert;

pub const PieceTable = @import("piece_table.zig");
const core = @import("core.zig");
const globals = core.globals;
const utf8 = @import("utf8.zig");
const NaryTree = @import("nary.zig").NaryTree;
const utils = @import("utils.zig");
const file_io = @import("file_io.zig");

const Buffer = @This();
pub const Handle = core.BufferHandle;

pub const Change = struct {
    delete_len: usize,
    inserted_len: usize,
    start_point: Point,
    start_index: usize,
    delete_end_point: Point,

    pub fn kind(self: Change) enum { insert, delete, replace } {
        return if (self.inserted_len != 0 and self.delete_len == 0)
            .insert
        else if (self.inserted_len == 0 and self.delete_len != 0)
            .delete
        else
            .replace;
    }
};

pub const Size = struct {
    size: u64,
    line_count: u32,
};

pub const Range = struct {
    start: usize,
    end: usize,

    pub fn overlaps(a: Range, b: Range) bool {
        return a.start <= b.end and a.end >= b.start;
    }

    pub fn len(range: Range) u64 {
        return utils.diff(range.start, range.end);
    }
};

pub const PointRange = struct {
    start: Point,
    end: Point,
};

pub const Point = struct {
    row: u32 = 0,
    col: u32 = 0,

    pub fn min(a: Point, b: Point) Point {
        if (a.row == b.row) {
            if (a.col <= b.col) return a else return b;
        }
        if (a.row < b.row) return a else return b;
    }

    pub fn max(a: Point, b: Point) Point {
        if (a.row == b.row) {
            if (a.col >= b.col) return a else return b;
        }
        if (a.row > b.row) return a else return b;
    }

    pub fn addRow(point: Point, offset: u32) Point {
        return .{ .row = point.row +| offset, .col = point.col };
    }

    pub fn addCol(point: Point, offset: u32) Point {
        return .{ .row = point.row, .col = point.col +| offset };
    }

    pub fn subRow(point: Point, offset: u32) Point {
        return .{ .row = point.row -| offset, .col = point.col };
    }

    pub fn subCol(point: Point, offset: u32) Point {
        return .{ .row = point.row, .col = point.col -| offset };
    }

    pub fn setRow(point: Point, row: u32) Point {
        return .{ .row = row, .col = point.col };
    }

    pub fn setCol(point: Point, col: u32) Point {
        return .{ .row = point.row, .col = col };
    }
};

pub const Selection = struct {
    pub const Kind = enum { regular, block, line };
    anchor: ?Point = null,
    kind: Selection.Kind = .regular,

    pub fn get(selection: Selection, cursor: Point) PointRange {
        const anchor = selection.anchor orelse return .{ .start = .{}, .end = .{} };
        var start = anchor.min(cursor);
        var end = anchor.max(cursor);
        start.col = start.col;
        end.col = end.col;

        if (selection.kind == .block and end.col < start.col) {
            const temp = start.col;
            start.col = end.col;
            end.col = temp;
        }

        end.col +|= 1; // end exclusive

        return switch (selection.kind) {
            .regular, .block => .{ .start = start, .end = end },
            .line => .{ .start = .{ .row = start.row, .col = 0 }, .end = .{ .row = end.row + 1, .col = 0 } },
        };
    }

    pub fn selected(selection: Selection) bool {
        return selection.anchor != null;
    }

    pub fn reset(selection: *Selection) void {
        selection.anchor = null;
    }
};

/// The data structure holding every line in the buffer
lines: PieceTable,
allocator: std.mem.Allocator,

selection: Selection = .{},
/// With every change to the buffer this value will increment.
version: u32 = 0,

file_last_mod_time: i128 = 0,
dirty: bool = false,
read_only: bool = false,

handle: Handle,
path: std.ArrayListUnmanaged(u8),
buffer_type: std.ArrayListUnmanaged(u8),

pub fn init(allocator: std.mem.Allocator, handle: Handle, buf: []const u8, path: []const u8) !Buffer {
    var path_list = std.ArrayListUnmanaged(u8){};
    try path_list.appendSlice(allocator, path);
    errdefer path_list.deinit(allocator);

    const buffer_type = blk: {
        var list = std.ArrayListUnmanaged(u8){};
        const extension = fs.path.extension(path);
        try list.appendSlice(allocator, if (extension.len > 0) extension[1..] else "");
        break :blk list;
    };

    var buffer = Buffer{
        .lines = try PieceTable.init(allocator, buf),
        .allocator = allocator,
        .file_last_mod_time = 0,
        .dirty = false,
        .path = path_list,
        .buffer_type = buffer_type,
        .handle = handle,
    };

    try buffer.insureLastByteIsNewline();

    return buffer;
}

pub fn deinit(buffer: *Buffer) void {
    buffer.path.deinit(buffer.allocator);
    buffer.buffer_type.deinit(buffer.allocator);
    buffer.lines.deinit(buffer.allocator);
}

pub fn canReplace(buffer: *Buffer, string: []const u8) !void {
    if (buffer.read_only) return error.ModifyingReadOnlyBuffer;
    if (!std.unicode.utf8ValidateSlice(string)) return error.InvalidUTF8;
}

pub fn canReplaceWithRange(buffer: *Buffer, index: usize, delete_len: u64, string: []const u8) !void {
    try buffer.canReplace(string);

    try buffer.validateInsertionIndex(index);
    if (delete_len > 0)
        try buffer.validateRange(.{ .start = index, .end = index + delete_len });
}

pub fn replace(buffer: *Buffer, index: usize, delete_len: u64, string: []const u8) !void {
    try buffer.canReplaceWithRange(index, delete_len, string);

    try buffer.lines.change(buffer.allocator, index, delete_len, string);

    buffer.setDirty();
    try buffer.insureLastByteIsNewline();

    buffer.version += 1;
}

////////////////////////////////////////////////////////////////////////////////
// Convenience insertion and deletion functions
////////////////////////////////////////////////////////////////////////////////

pub fn insertAt(buffer: *Buffer, index: usize, string: []const u8) !void {
    try buffer.replace(index, 0, string);
}

/// End exclusive
pub fn deleteRange(buffer: *Buffer, range: Range) !void {
    try buffer.replace(range.start, range.len(), "");
}

pub fn replaceRange(buffer: *Buffer, range: Range, string: []const u8) !void {
    try buffer.replace(range.start, range.len(), string);
}

pub fn replaceAllWith(buffer: *Buffer, string: []const u8) !void {
    try buffer.replace(0, buffer.size(), string);
}

pub fn insertAtPoint(buffer: *Buffer, rc: Point, string: []const u8) !void {
    const index = buffer.getIndex(rc);
    try buffer.insertAt(index, string);
}

pub fn deleteBefore(buffer: *Buffer, index: usize) !void {
    if (index == 0) return;
    const i = buffer.moveIndex(index, -1);
    try buffer.deleteRange(.{ .start = i, .end = index });
}

pub fn deleteAfterCursor(buffer: *Buffer, index: usize) !void {
    const end = buffer.moveIndex(index, 1);
    try buffer.deleteRange(.{ .start = index, .end = end });
}

pub fn deleteRows(buffer: *Buffer, start_row: u32, end_row: u32) !void {
    assert(start_row <= end_row);
    assert(end_row < buffer.lineCount());

    const start_index = buffer.indexOfFirstByteAtRow(start_row);
    const end_index = buffer.indexOfLastByteAtRow(end_row) + 1;

    try buffer.deleteRange(.{ .start = start_index, .end = end_index });
}

pub fn deletePointRange(buffer: *Buffer, range: PointRange) !void {
    if (range.start.row > range.end.row)
        return error.InvalidRange;

    const start_index = buffer.getIndex(.{ .row = range.start.row, .col = range.start.col });
    const end_index = if (range.end.row > buffer.lineCount())
        buffer.size()
    else
        buffer.getIndex(.{ .row = range.end.row, .col = range.end.col });

    try buffer.deleteRange(.{ .start = start_index, .end = end_index });
}

pub fn clear(buffer: *Buffer) !void {
    try buffer.replaceAllWith("\n");
}

// TODO: this
// pub fn replaceRange(buffer: *Buffer, string: []const u8, start_row: i32, start_col: i32, end_row: i32, end_col: i32) !void {
// }

pub fn validateInsertionIndex(buffer: *Buffer, index: usize) !void {
    const i = @min(index, buffer.size() -| 1);

    if (i == 0 or
        i == buffer.size() - 1 or
        utf8.byteType(buffer.lines.byteAt(i)) == .start_byte)
        return;

    return error.InvalidInsertionIndex;
}

pub fn validateRange(buffer: *Buffer, range: Range) !void {
    const s = @min(range.start, buffer.size() -| 1);
    const e = @min(range.end, buffer.size() -| 1);

    if (e == buffer.size() - 1 or
        utf8.byteType(buffer.lines.byteAt(s)) == .start_byte or
        utf8.byteType(buffer.lines.byteAt(e)) == .start_byte)
        return;

    return error.InvalidRange;
}

pub fn countCodePointsAtRow(buffer: *Buffer, arg_row: u32) u32 {
    const row = @min(arg_row, buffer.lineCount());
    var count: u64 = 0;
    var iter = LineIterator.initLines(buffer, row, row);
    while (iter.next()) |slice|
        count += utf8.countCodepoints(slice);

    return @truncate(count);
}

pub fn insureLastByteIsNewline(buffer: *Buffer) !void {
    if (buffer.size() == 0 or buffer.lines.byteAt(buffer.size() - 1) != '\n') {
        try buffer.lines.change(buffer.allocator, buffer.size(), 0, "\n");
    }
}

pub fn lineSize(buffer: *Buffer, line: u32) u64 {
    return buffer.indexOfLastByteAtRow(line) - buffer.indexOfFirstByteAtRow(line) + 1;
}

pub fn lineRangeSize(buffer: *Buffer, start_line: u32, end_line: u32) u64 {
    return buffer.indexOfLastByteAtRow(end_line) - buffer.indexOfFirstByteAtRow(start_line) + 1;
}

pub fn maxLineRangeSize(buffer: *Buffer, start_row: u32, end_row: u32) u64 {
    var s: u64 = 0;
    for (start_row..end_row + 1) |row| s = @max(s, buffer.lineSize(@truncate(row)));
    return s;
}

pub fn codePointAt(buffer: *Buffer, index: u64) !u21 {
    return unicode.utf8Decode(buffer.codePointSliceAt(index));
}

pub fn codePointSliceAt(buffer: *Buffer, const_index: u64) []const u8 {
    var index = const_index;
    var byte = buffer.lines.byteAt(index);

    while (utf8.byteType(byte) != .start_byte) {
        // if we're here then the buffer contains invalid utf8 or the index is in
        // an invalid position, either way just move forward until we find a valid position
        index += 1;
        byte = buffer.lines.byteAt(index);
    }

    const count = unicode.utf8ByteSequenceLength(byte) catch 1;
    var piece_info = buffer.lines.tree.findNode(index);
    const i = piece_info.relative_index;
    const slice = piece_info.piece.content(&buffer.lines)[i .. i + count];

    return slice;
}

/// Searches the buffer for *string* and returns a slice of indices of all occurrences
/// within the given range
pub fn search(buffer: *Buffer, allocator: std.mem.Allocator, string: []const u8, start_row: u32, end_row: u32) !?[]usize {
    if (string.len == 0) return null;

    const max_line_size = buffer.maxLineRangeSize(start_row, end_row);
    const line_buf = try buffer.allocator.alloc(u8, max_line_size);
    defer buffer.allocator.free(line_buf);

    var indices = std.ArrayListUnmanaged(usize){};
    errdefer indices.deinit(allocator);

    for (start_row..end_row + 1) |row| {
        const full_line = buffer.getLinesBuf(line_buf, @truncate(row), @truncate(row));
        var slicer: u64 = 0;
        while (slicer < full_line.len) { // get all matches of string in the same line
            const line = full_line[slicer..];
            if (line.len < string.len) break;

            const index = std.mem.indexOf(u8, line, string);
            if (index) |i| {
                try indices.append(allocator, slicer + i + buffer.indexOfFirstByteAtRow(@truncate(row)));
                slicer += string.len + i;
            } else break;
        }
    }

    return if (indices.items.len == 0) null else indices.items;
}

pub fn getStringRange(buffer: *Buffer, allocator: std.mem.Allocator, range: Range) ![]u8 {
    var buf = try allocator.alloc(u8, range.end - range.start);

    var iter = BufferIterator.init(buffer, range.start, range.end);
    var i: usize = 0;
    while (iter.next()) |string| : (i += string.len)
        std.mem.copyForwards(u8, buf[i..], string);

    return buf;
}

pub fn getLinesBuf(buffer: *Buffer, buf: []u8, arg_first_line: u32, arg_last_line: u32) []u8 {
    const first_line = @min(buffer.lineCount(), @min(arg_first_line, arg_last_line));
    const last_line = @min(buffer.lineCount(), @max(arg_first_line, arg_last_line));

    utils.assert(buf.len >= buffer.lineRangeSize(first_line, last_line), "");

    const start = buffer.indexOfFirstByteAtRow(first_line);
    const end = buffer.indexOfLastByteAtRow(last_line) + 1;
    var iter = BufferIterator.init(buffer, start, end);
    var i: u64 = 0;
    while (iter.next()) |slice| {
        std.mem.copyForwards(u8, buf[i..], slice);
        i += slice.len;
    }

    return buf[0..i];
}

pub fn getLines(buffer: *Buffer, allocator: std.mem.Allocator, first_line: u32, last_line: u32) ![]u8 {
    const lines = try allocator.alloc(u8, buffer.lineRangeSize(first_line, last_line));
    return buffer.getLinesBuf(lines, first_line, last_line);
}

/// Returns a copy of the entire buffer.
/// Caller owns memory.
pub fn getAllLines(buffer: *Buffer, allocator: std.mem.Allocator) ![]u8 {
    const lines = try allocator.alloc(u8, buffer.size());
    return buffer.getLinesBuf(lines, 0, buffer.lineCount());
}

pub fn getIndex(buffer: *Buffer, rc: Point) usize {
    if (rc.row >= buffer.lineCount()) return buffer.size() - 1;

    const row = rc.row;

    var i: u64 = 0;
    var char_count: u64 = 0;

    var iter = BufferIterator.initLines(buffer, row, row);
    outer: while (iter.next()) |string| {
        var uiter = utf8.Utf8Iterator{ .bytes = string };
        while (uiter.nextCodepointSlice()) |utf8_seq| {
            if (char_count >= rc.col) break :outer;

            char_count += 1;
            i += utf8_seq.len;
        }
    }

    const start_index = buffer.indexOfFirstByteAtRow(row);
    const end_index = buffer.indexOfLastByteAtRow(row);
    return @min(start_index + i, end_index);
}

pub fn indexOfFirstByteAtRow(buffer: *Buffer, arg_row: u32) usize {
    const row = @min(arg_row, buffer.lineCount() -| 1);

    return if (row == 0)
        0
    else
        buffer.lines.tree.findNodeWithLine(&buffer.lines, row - 1).newline_index + 1;
}

pub const RowOfIndex = struct { row: u32, start: u64, end: u64 };
pub fn rowOfIndex(buffer: *Buffer, index: u64) RowOfIndex {
    if (index >= buffer.size()) return .{
        .row = @truncate(buffer.lineCount()),
        .start = buffer.indexOfFirstByteAtRow(buffer.lineCount()),
        .end = buffer.indexOfLastByteAtRow(buffer.lineCount()),
    };

    var lower_bound: u32 = 0;
    var upper_bound: u32 = buffer.lineCount() -| 1;
    var row = (upper_bound + lower_bound) / 2;

    var start: u64 = 0;
    var end: u64 = 0;
    while (true) {
        start = buffer.indexOfFirstByteAtRow(row);
        end = buffer.indexOfLastByteAtRow(row);

        if ((index >= start and index <= end) or upper_bound == lower_bound)
            break;

        if (index < start)
            upper_bound = row - 1
        else
            lower_bound = row + 1;

        row = (upper_bound + lower_bound) / 2;
    }

    return .{ .row = @truncate(row), .start = start, .end = end };
}

pub fn indexOfLastByteAtRow(buffer: *Buffer, row: u32) usize {
    const r = @min(row, buffer.lineCount() -| 1);
    return buffer.lines.tree.findNodeWithLine(&buffer.lines, r).newline_index;
}

pub fn getLineLength(buffer: *Buffer, row: u32) usize {
    return buffer.getLinesLength(row, row);
}

pub fn getLinesLength(buffer: *Buffer, first_row: u32, last_row: u32) usize {
    const i = buffer.indexOfFirstByteAtRow(first_row);
    const j = buffer.indexOfFirstByteAtRow(last_row + 1);
    return utils.diff(i, j);
}

pub fn getPoint(buffer: *Buffer, index_: usize) Point {
    const index = @min(index_, buffer.size() - 1);
    const roi = buffer.rowOfIndex(index);

    var col: u32 = 0;
    var i: u64 = roi.start;
    while (i < index) {
        const byte = buffer.lines.byteAt(i);
        const len = unicode.utf8ByteSequenceLength(byte) catch 1;
        col += 1;
        i += len;
    }

    return .{ .row = roi.row, .col = col };
}

pub fn getColIndex(buffer: *Buffer, point: Point) usize {
    const index = buffer.getIndex(point);
    const line_start = buffer.indexOfFirstByteAtRow(point.row);
    return index - line_start;
}

/// Offsets the index to be relative to the line it is in
pub fn offsetIndexToLine(buffer: *Buffer, index: u64) u64 {
    const roi = buffer.rowOfIndex(index);
    return index - roi.start;
}

pub fn pointRangeToRange(buffer: *Buffer, range: PointRange) Range {
    const start = range.start.min(range.end);
    const end = range.start.max(range.end);

    const start_index = buffer.getIndex(start);
    const end_index = buffer.getIndex(end);

    return .{ .start = start_index, .end = end_index };
}

pub const AllocLineIterator = struct {
    const Self = @This();

    inner_iter: LineIterator,
    buf: []u8,
    current_line: u64,

    pub fn initPoint(buffer: *Buffer, start: Point, end: Point) !AllocLineIterator {
        const buf = try buffer.allocator.alloc(u8, buffer.maxLineRangeSize(start.row, end.row));
        return .{
            .inner_iter = LineIterator.initPoint(buffer, .{ .start = start, .end = end }),
            .buf = buf,
            .current_line = start.row -| 1,
        };
    }

    pub fn initLines(buffer: *Buffer, start_row: u64, end_row: u64) !AllocLineIterator {
        const buf = try buffer.allocator.alloc(u8, buffer.maxLineRangeSize(start_row, end_row));
        return .{
            .inner_iter = LineIterator.initLines(buffer, start_row, end_row),
            .buf = buf,
            .current_line = start_row -| 1,
        };
    }

    pub fn deinit(self: *AllocLineIterator) void {
        self.inner_iter.buffer.allocator.free(self.buf);
    }

    pub fn next(self: *AllocLineIterator) ?[]u8 {
        self.current_line += 1;
        var i: u64 = 0;
        while (self.inner_iter.next()) |string| {
            std.mem.copy(u8, self.buf[i..], string);
            i += string.len;

            if (string[string.len - 1] == '\n') break;
        }

        if (i > 0) return self.buf[0..i];

        return null;
    }
};

pub const LineIterator = struct {
    const Self = @This();

    buffer: *Buffer,
    start: u64,
    end: u64,
    current_line: u32,

    pub fn initPoint(buffer: *Buffer, range: PointRange) LineIterator {
        const r = buffer.pointRangeToRange(range);

        return .{
            .buffer = buffer,
            .start = r.start,
            .end = r.end,
            .current_line = range.start.min(range.end).row,
        };
    }

    pub fn initLines(buffer: *Buffer, first_line: u32, last_line: u32) LineIterator {
        utils.assert(first_line <= last_line, "first_line must be <= last_line");
        utils.assert(last_line <= buffer.lineCount(), "last_line cannot be greater than the total rows in the buffer");

        const start = buffer.indexOfFirstByteAtRow(first_line);
        const end = buffer.indexOfLastByteAtRow(last_line) + 1;
        return .{
            .buffer = buffer,
            .start = start,
            .end = end,
            .current_line = first_line,
        };
    }

    pub fn next(self: *Self) ?[]const u8 {
        if (self.start >= self.end) {
            return null;
        }
        const current_line_end = @min(self.buffer.indexOfLastByteAtRow(self.current_line) + 1, self.end);

        const piece_info = self.buffer.lines.tree.findNode(self.start);
        const slice = piece_info.piece.content(&self.buffer.lines)[piece_info.relative_index..];

        const end = @min(slice.len, current_line_end -| self.start);

        const relevant_content = slice[0..end];
        self.start += relevant_content.len;
        if (self.start >= current_line_end) self.current_line += 1;

        return relevant_content;
    }
};

/// BufferIterator is end exclusive
pub const BufferIterator = struct {
    const Self = @This();

    pt: *PieceTable,
    start: u64,
    end: u64,

    pub fn init(buffer: *Buffer, start: usize, end: usize) BufferIterator {
        return .{
            .pt = &buffer.lines,
            .start = start,
            .end = end,
        };
    }

    pub fn initLines(buffer: *Buffer, first_line: u32, last_line: u32) BufferIterator {
        utils.assert(first_line <= last_line, "first_line must be <= last_line");
        utils.assert(last_line < buffer.lineCount(), "last_line cannot be greater than the total rows in the buffer");

        const start = buffer.indexOfFirstByteAtRow(first_line);
        const end = buffer.indexOfLastByteAtRow(last_line) + 1;
        return .{
            .pt = &buffer.lines,
            .start = start,
            .end = end,
        };
    }

    pub fn next(self: *Self) ?[]const u8 {
        if (self.start >= self.end) return null;
        const piece_info = self.pt.tree.findNode(self.start);
        const string = piece_info.piece.content(self.pt)[piece_info.relative_index..];
        defer self.start += string.len;

        const end = @min(string.len, self.end - self.start);
        return string[0..end];
    }
};

pub const ReverseBufferIterator = struct {
    const Self = @This();

    pt: *PieceTable,
    start: u64,
    end: u64,
    done: bool = false,

    pub fn init(buffer: *Buffer, start: u64, end: u64) ReverseBufferIterator {
        return .{
            .pt = &buffer.lines,
            .start = start,
            .end = @min(buffer.size() - 1, end),
        };
    }

    pub fn initLines(buffer: *Buffer, first_line: u64, last_line: u64) ReverseBufferIterator {
        utils.assert(first_line <= last_line, "first_line must be <= last_line");
        utils.assert(last_line <= buffer.lineCount(), "last_line cannot be greater than the total rows in the buffer");

        const start = buffer.indexOfFirstByteAtRow(first_line);
        const end = buffer.indexOfLastByteAtRow(last_line);
        return .{
            .pt = &buffer.lines,
            .start = start,
            .end = @min(buffer.size() - 1, end),
        };
    }

    pub fn next(self: *Self) ?[]const u8 {
        if (self.done) return null;
        if (self.end <= self.start or self.end == 0) self.done = true;

        const piece_info = self.pt.tree.findNode(self.end);
        const content = piece_info.piece.content(self.pt);

        const node_start_abs_index = self.end - content[0..piece_info.relative_index].len;
        const end = piece_info.relative_index + 1;
        const start = if (utils.inRange(node_start_abs_index, self.start, self.end))
            0
        else
            self.start - node_start_abs_index;

        if (self.start == self.end) {
            self.done = true;
            return content[end - 1 .. end];
        } else {
            const string = content[start..end];

            self.end -|= string.len;
            return string;
        }
    }
};

pub fn size(buffer: *const Buffer) u64 {
    return buffer.lines.tree.size;
}

pub fn lineCount(buffer: *const Buffer) u32 {
    return @truncate(buffer.lines.tree.newline_count);
}

////////////////////////////////////////////////////////////////////////////////
// Cursor
////////////////////////////////////////////////////////////////////////////////

// TODO: Faster move code
pub fn moveRelativeColumn(buffer: *Buffer, index: usize, col_offset: i32) usize {
    if (col_offset == 0) return index;

    const abs_offset: u32 = @truncate(std.math.absCast(col_offset));
    const point = buffer.getPoint(index);

    const col: u32 = blk: {
        if (col_offset > 0) {
            const row_size = buffer.countCodePointsAtRow(point.row);
            break :blk @min(row_size -| 1, point.col +| abs_offset);
        } else {
            break :blk point.col -| abs_offset;
        }
    };

    return buffer.getIndex(point.setCol(col));
}

// TODO: Faster move code
pub fn moveRelativeRow(buffer: *Buffer, index: usize, row_offset: i32) usize {
    if (row_offset == 0) return index;

    const point = buffer.getPoint(index);
    const roi = buffer.rowOfIndex(index);
    const abs_offset: u32 = @truncate(std.math.absCast(row_offset));
    var row = if (row_offset > 0) roi.row +| abs_offset else roi.row -| abs_offset;
    row = @min(row, buffer.lineCount() -| 1);

    return buffer.getIndex(.{ .row = row, .col = point.col });
}

pub fn moveIndex(buffer: *Buffer, arg_index: usize, arg_offset: isize) usize {
    if (arg_offset == 0) return arg_index;

    var index = arg_index;
    var offset = arg_offset;
    if (arg_offset > 0 and index < buffer.size()) {
        while (offset != 0) {
            const len = unicode.utf8ByteSequenceLength(buffer.lines.byteAt(index)) catch 1;
            index += len;
            offset -|= 1;
        }
    } else {
        while (offset != 0 and index > 0) {
            if (utf8.byteType(buffer.lines.byteAt(index)) == .start_byte)
                index -|= 1;

            while (utf8.byteType(buffer.lines.byteAt(index)) == .continue_byte)
                index -|= 1
            else
                offset += 1;
        }
    }

    return @min(index, buffer.size() -| 1);
}

pub fn setDirty(buffer: *Buffer) void {
    buffer.dirty = true;
}
