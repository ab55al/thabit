const std = @import("std");

const StatusLine = @This();
pub const Alignment = enum { left, mid, right };

pub const Widget = struct {
    name: std.ArrayListUnmanaged(u8) = .{},
    value: std.ArrayListUnmanaged(u8) = .{},

    pub fn deinit(widget: *Widget, allocator: std.mem.Allocator) void {
        widget.name.deinit(allocator);
        widget.value.deinit(allocator);
    }
};

allocator: std.mem.Allocator,
left: std.ArrayListUnmanaged(Widget) = .{},
mid: std.ArrayListUnmanaged(Widget) = .{},
right: std.ArrayListUnmanaged(Widget) = .{},

pub fn init(allocator: std.mem.Allocator) StatusLine {
    return .{ .allocator = allocator };
}

pub fn deinit(self: *StatusLine) void {
    const lists = &.{ &self.left, &self.mid, &self.right };

    inline for (lists) |list| {
        for (list.items) |*widget|
            widget.deinit(self.allocator);

        list.deinit(self.allocator);
    }
}

pub fn add(self: *StatusLine, name: []const u8, alignment: Alignment) !void {
    var widget = Widget{};

    errdefer widget.deinit(self.allocator);

    try widget.name.appendSlice(self.allocator, name);
    try widget.value.appendSlice(self.allocator, "");

    switch (alignment) {
        .left => try self.left.append(self.allocator, widget),
        .mid => try self.mid.append(self.allocator, widget),
        .right => try self.right.append(self.allocator, widget),
    }
}

pub fn remove(self: *StatusLine, name: []const u8) void {
    const location = self.getLocation(name) orelse return;

    return switch (location.alignment) {
        .left => self.left.orderedRemove(location.i),
        .mid => self.mid.orderedRemove(location.i),
        .right => self.right.orderedRemove(location.i),
    };
}

pub fn setValue(self: *StatusLine, name: []const u8, value: []const u8) void {
    var widget = self.get(name) orelse return;

    widget.value.ensureTotalCapacity(self.allocator, value.len) catch return;
    widget.value.clearRetainingCapacity();
    widget.value.appendSliceAssumeCapacity(value);
}

fn get(self: *StatusLine, name: []const u8) ?*Widget {
    const location = self.getLocation(name) orelse return null;

    return switch (location.alignment) {
        .left => &self.left.items[location.index],
        .mid => &self.mid.items[location.index],
        .right => &self.right.items[location.index],
    };
}

fn getLocation(self: *StatusLine, name: []const u8) ?struct { index: usize, alignment: Alignment } {
    for (self.left.items, 0..) |widget, i|
        if (std.mem.eql(u8, name, widget.name.items))
            return .{ .index = i, .alignment = .left };

    for (self.mid.items, 0..) |widget, i|
        if (std.mem.eql(u8, name, widget.name.items))
            return .{ .index = i, .alignment = .mid };

    for (self.right.items, 0..) |widget, i|
        if (std.mem.eql(u8, name, widget.name.items))
            return .{ .index = i, .alignment = .right };

    return null;
}
