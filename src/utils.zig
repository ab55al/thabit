const std = @import("std");
const print = std.debug.print;

const utf8 = @import("utf8.zig");

pub fn assert(ok: bool, comptime message: []const u8) void {
    if (!ok) {
        print("{s}\n", .{message});
        unreachable;
    }
}

pub fn passert(ok: bool, comptime fmt: []const u8, args: anytype) void {
    if (!ok) {
        print(fmt ++ "\n", args);
        unreachable;
    }
}

/// Takes three numbers and returns true if the first number is in the range
/// of the second and third numbers
pub fn inRange(a: anytype, b: @TypeOf(a), c: @TypeOf(a)) bool {
    return a >= b and a <= c;
}

pub fn abs(val: f32) f32 {
    return if (val < 0) -val else val;
}

pub fn here(comptime location: std.builtin.SourceLocation) []const u8 {
    return location.file ++ " " ++ location.fn_name ++ "()";
}

pub fn atLeastOneIsEqual(comptime T: type, slice: []const T, value: T) bool {
    for (slice) |v| if (v == value) return true;
    return false;
}

pub fn diff(a: anytype, b: @TypeOf(a)) @TypeOf(a) {
    return if (a >= b) a - b else b - a;
}

pub fn bound(value: anytype, at_least: @TypeOf(value), at_most: @TypeOf(value)) @TypeOf(value) {
    return if (value <= at_least) at_least else if (value >= at_most) at_most else value;
}

pub fn getIndexOfCol(string: []const u8, target_col: u32) usize {
    var col: u32 = 0;
    var iter = utf8.Utf8Iterator{ .bytes = string };
    while (iter.nextCodepointSlice()) |slice| {
        if (col == target_col) return iter.i -| slice.len;
        col += 1;
    }
    return string.len -| 1;
}

pub fn getColOfIndex(string: []const u8, target_index: usize) u32 {
    var col: u32 = 0;
    var iter = utf8.Utf8Iterator.init(string);
    while (iter.nextCodepointSlice()) |slice| {
        if (iter.i -| slice.len == target_index) return col;
        col += 1;
    }
    return @truncate(utf8.countCodepoints(string));
}

pub const StringTableContext = struct {
    pub const Key = [2][]const u8;

    pub fn hash(self: @This(), keys: Key) u64 {
        _ = self;
        var wh = std.hash.Wyhash.init(0);

        wh.update(keys[0]);
        wh.update(keys[1]);

        return wh.final();
    }

    pub fn eql(self: @This(), a: Key, b: Key) bool {
        _ = self;
        for (a, b) |a_array, b_array| if (!std.mem.eql(u8, a_array, b_array)) return false;
        return true;
    }
};

test "StringTable() context only" {
    const allocator = std.testing.allocator;
    var table = std.HashMap(StringTableContext.Key, usize, StringTableContext, std.hash_map.default_max_load_percentage).init(allocator);
    defer table.deinit();

    try table.put(.{ "A", "a" }, 0);
    try table.put(.{ "A", "b" }, 1);
    try table.put(.{ "A", "c" }, 2);
    try table.put(.{ "A", "c" }, 3);

    try table.put(.{ "B", "a" }, 100);
    try table.put(.{ "B", "b" }, 200);
    try table.put(.{ "B", "c" }, 300);
    try table.put(.{ "B", "c" }, 300);

    try std.testing.expect(table.get(.{ "A", "a" }) == 0);
    try std.testing.expect(table.get(.{ "A", "b" }) == 1);
    try std.testing.expect(table.get(.{ "A", "c" }) == 3);
}

pub fn structFromTuple(comptime T: type, tuple: anytype) T {
    var the_struct: T = undefined;
    const struct_fields = std.meta.fields(T);
    inline for (std.meta.fields(@TypeOf(tuple)), 0..) |tuple_field, i| {
        @field(the_struct, struct_fields[i].name) = @field(@TypeOf(tuple), tuple_field.name);
    }

    return the_struct;
}

pub fn TupleFromStruct(comptime Struct: type) type {
    const StructField = std.builtin.Type.StructField;
    var fields: []const StructField = &[_]StructField{};

    const name_size = std.fmt.count("{}", .{std.math.maxInt(usize)}) + 1;
    var buf: [name_size]u8 = undefined;
    for (std.meta.fields(Struct), 0..) |field, i| {
        const numeric_name = std.fmt.bufPrintZ(&buf, "{}", .{i}) catch unreachable;
        fields = fields ++ &[_]StructField{.{
            .name = numeric_name,
            .type = field.type,
            .default_value = field.default_value,
            .is_comptime = field.is_comptime,
            .alignment = field.alignment,
        }};
    }

    return @Type(.{ .Struct = .{
        .layout = .Auto,
        .fields = fields,
        .decls = &.{},
        .is_tuple = true,
    } });
}

pub fn naiveAsciifuzzySearch(text: []const u8, chars: []const u8) ?u32 {
    var score: u32 = 0;

    var chars_i: usize = 0;
    var previous_i: usize = 0;
    for (text, 0..) |b, i| {
        if (chars_i == chars.len) break;
        const char = chars[chars_i];
        if (std.ascii.toLower(char) == std.ascii.toLower(b)) {
            var extra_score: usize = 0;
            if ((std.ascii.isLower(char) and std.ascii.isUpper(b)) or
                (std.ascii.isUpper(char) and std.ascii.isLower(b))) extra_score += 1;

            score += @truncate((i - previous_i) + extra_score);
            previous_i = i;
            chars_i += 1;
        }
    }

    return if (chars_i != chars.len) null else score;
}

test "fuzzy" {
    const chars = "hot";
    const text1 = "hello there";
    const text2 = "heyo there";
    const text3 = "nope there";

    const text1_score = naiveAsciifuzzySearch(text1, chars);
    const text2_score = naiveAsciifuzzySearch(text2, chars);
    const text3_score = naiveAsciifuzzySearch(text3, chars);

    try std.testing.expect(text1_score == 6);
    try std.testing.expect(text2_score == 5);
    try std.testing.expect(text3_score == null);
}

test "fuzzy mixed case" {
    const chars = "heL";
    const text1 = "hello there";
    const text2 = "heLlo there";

    const text1_score = naiveAsciifuzzySearch(text1, chars).?;
    const text2_score = naiveAsciifuzzySearch(text2, chars).?;

    try std.testing.expect(text1_score > text2_score);
}

pub fn UniqueOptionalSegmentedList(comptime T: type, comptime key_enum_literal: anytype, comptime prealloc_item_count: usize) type {
    comptime {
        if (@typeInfo(T) != .Struct) {
            @compileError("Provided Type must be a struct");
        }

        if (@typeInfo(@TypeOf(key_enum_literal)) != .EnumLiteral) {
            @compileError("Key must be an EnumLiteral and it's value name must exist in `" ++ @typeName(T) ++ "`");
        }

        var found = false;
        for (std.meta.fields(T)) |field| {
            if (std.mem.eql(u8, field.name, @tagName(key_enum_literal)))
                found = true;
        }
        if (!found) @compileError("EnumLiteral `" ++ @tagName(key_enum_literal) ++ "` Doesn't exist in `" ++ @typeName(T) ++ "`");
    }

    return struct {
        const Self = @This();

        pub const List = std.SegmentedList(?T, prealloc_item_count);
        pub const key_name = @tagName(key_enum_literal);
        pub const KeyType = blk: {
            for (std.meta.fields(T)) |field| {
                if (std.mem.eql(u8, field.name, @tagName(key_enum_literal)))
                    break :blk field.type;
            }
            @compileError("Well this is awkward. Seems the previous check failed!");
        };

        allocator: std.mem.Allocator,
        list: List = .{},

        pub fn init(allocator: std.mem.Allocator) Self {
            return .{ .allocator = allocator };
        }

        pub fn deinit(self: *Self) void {
            self.list.deinit(self.allocator);
        }

        pub fn add(self: *Self, new_value: T) !void {
            if (self.getPtrBy(@field(new_value, key_name)) != null) {
                return error.AddingNonUniqueValue;
            }

            const value_ptr: *?T = blk: {
                const list_count = self.list.count();
                for (0..list_count) |i| { // find vacant spots
                    const value: *?T = self.list.uncheckedAt(i);
                    if (value.* == null)
                        break :blk value;
                } else {
                    break :blk try self.list.addOne(self.allocator);
                }
            };

            value_ptr.* = new_value;
        }

        pub fn deleteBy(self: *Self, key: KeyType) void {
            const list_count = self.list.count();
            for (0..list_count) |i| {
                const value: *?T = self.list.uncheckedAt(i);
                const v = value.* orelse continue;
                if (std.meta.eql(@field(v, key_name), key))
                    value.* = null;
            }
        }

        pub fn getBy(self: *Self, key: KeyType) ?T {
            const value = self.getPtrBy(key) orelse return null;
            return value.*;
        }

        pub fn getPtrBy(self: *Self, key: KeyType) ?*T {
            const list_count = self.list.count();
            for (0..list_count) |i| {
                const value: *T = &(self.list.uncheckedAt(i).* orelse continue);
                if (std.meta.eql(@field(value, key_name), key)) return value;
            }

            return null;
        }

        pub fn count(self: *Self) usize {
            var c: usize = 0;
            var iter = self.iterator();
            while (iter.next()) |_| c += 1;

            return c;
        }

        pub fn growCapacity(self: *Self, grow_by: usize) !void {
            if (self.list.count() + grow_by <= self.list.prealloc_segment.len) {
                return;
            }

            try self.list.growCapacity(self.allocator, self.list.count() + grow_by);
        }

        pub fn iterator(self: *Self) Iterator {
            return .{ .index = 0, .unique_list = self };
        }

        pub const Iterator = struct {
            index: usize,
            unique_list: *Self,

            pub fn next(iter: *Iterator) ?*T {
                const max_count = iter.unique_list.list.count();
                if (iter.index >= max_count) {
                    return null;
                }

                const start_index = iter.index;
                for (start_index..max_count) |index| {
                    iter.index = index + 1;
                    const value_ptr: *?T = iter.unique_list.list.uncheckedAt(index);
                    const value: *T = &(value_ptr.* orelse continue);
                    return value;
                }

                iter.index = max_count;
                return null;
            }
        };
    };
}

test "UniqueSegmentedList" {
    const Foo = struct { the_key: u32, string: []const u8 };
    var ulist = UniqueOptionalSegmentedList(Foo, .the_key, 0).init(std.testing.allocator);
    defer ulist.deinit();

    try ulist.add(.{ .the_key = 10, .string = "hello there" });
    try ulist.add(.{ .the_key = 20, .string = "this is 20" });
    try ulist.add(.{ .the_key = 50, .string = "this is 50" });
    try ulist.add(.{ .the_key = 40, .string = "this is 40" });

    // modify
    const well = "Well Well Well";
    ulist.getPtrBy(40).?.string = well;
    try std.testing.expectEqualStrings(well, ulist.getPtrBy(40).?.string);

    // delete
    ulist.deleteBy(50);
    try std.testing.expect(ulist.getPtrBy(50) == null);
    try std.testing.expect(ulist.list.at(2).* == null);

    // check count
    try std.testing.expect(ulist.count() == 3);
    try std.testing.expect(ulist.list.count() == 4);

    // add and take place of vacant spots
    try ulist.add(.{ .the_key = 100, .string = "This should take the place of 50" });
    try std.testing.expect(ulist.getPtrBy(100) != null);
    try std.testing.expect(ulist.list.at(2).* != null);
}

pub fn stringToEnum(comptime E: type, string: []const u8) ?E {
    inline for (std.meta.fields(E)) |field| {
        if (std.mem.eql(u8, string, field.name)) {
            return @enumFromInt(field.value);
        }
    }

    return null;
}

test "stringToEnum" {
    const Enum = enum { hello, there };
    try std.testing.expect(stringToEnum(Enum, "hello") == .hello);
    try std.testing.expect(stringToEnum(Enum, "not here") == null);
}

pub const ListView = struct {
    pub const ViewRange = struct { start: usize, end: usize };

    offset: usize = 0,
    len: usize = 5,
    /// The value is absolute and within offset..offsetEnd()
    selected_index: ?usize = null,

    pub fn adjustView(self: *ListView, entries_len: usize) void {
        if (self.offset >= entries_len) {
            self.offset = 0;
        }
        if (self.selected_index != null and self.selected_index.? >= self.offsetEnd(entries_len)) {
            self.selected_index = null;
        }
    }

    pub fn adjustViewSelect(self: *ListView, entries_len: usize) void {
        self.adjustView(entries_len);

        self.selected_index = self.selected_index orelse 0;
    }

    pub fn selectNext(self: *ListView, list_len: usize, at_end: enum { wrap, stop, unselect }) void {
        self.adjustView(list_len);

        if (list_len == 0) return;

        const index = self.selected_index orelse {
            self.selected_index = 0;
            self.offset = 0;
            return;
        };

        if (index + 1 >= list_len) {
            self.selected_index = switch (at_end) {
                .wrap => 0,
                .stop => @min(index, list_len - 1),
                .unselect => null,
            };
        } else {
            self.selected_index = index + 1;
        }

        self.viewFollowIndex(list_len);
    }

    pub fn viewFollowIndex(self: *ListView, list_len: usize) void {
        const index = self.selected_index orelse {
            self.offset = 0;
            return;
        };

        const result = moveStart(self.offset, self.len, list_len, index);
        self.offset = result.start;
    }

    pub fn indexFollowView(self: *ListView, list_len: usize) void {
        if (list_len == 0) return;

        const index = self.selected_index orelse {
            self.selected_index = self.offset;
            return;
        };

        const end = self.offsetEnd(list_len) -| 1;
        if (index > end) {
            self.selected_index = end;
        } else if (index < self.offset) {
            self.selected_index = self.offset;
        }
    }

    pub fn selectPrev(self: *ListView, list_len: usize, at_end: enum { wrap, stop, unselect }) void {
        self.adjustView(list_len);
        if (list_len == 0) return;

        const index = self.selected_index orelse {
            self.selected_index = list_len - 1;
            self.offset = list_len - @min(self.len, list_len);
            return;
        };

        if (self.offset == 0 and index == 0) {
            self.selected_index = switch (at_end) {
                .wrap => list_len - 1,
                .stop => 0,
                .unselect => null,
            };
        } else {
            self.selected_index = index - 1;
        }

        self.viewFollowIndex(list_len);
    }

    pub fn reset(self: *ListView) void {
        self.selected_index = null;
        self.offset = 0;
    }

    pub fn viewRange(self: *ListView, list_len: usize) ViewRange {
        self.adjustView(list_len);
        return .{ .start = self.offset, .end = self.offset + @min(list_len, self.len) };
    }

    pub fn offsetEnd(self: *ListView, list_len: usize) usize {
        const len = @min(self.len, list_len);
        return self.offset + len;
    }

    /// Given `start`, `len`, `total_elements` and `index`. If `index` is outside the range of `start..start + len`,
    /// modifies start such that index will equal to `start or `start` + `len` - 1. Which ever is closer.
    /// The returned `start` + `len` will never be > `total_elements.
    ///Before:
    /// e0 e1 e2 e3 e4 e5 e6 e7 e8
    ///          |------|    I
    /// start = e3, len 3, index e7
    ///
    ///After:
    /// e0 e1 e2 e3 e4 e5 e6 e7 e8
    ///                      |I--|
    /// start = e7, len 2, index e7
    ///
    pub fn moveStart(arg_start: usize, arg_len: usize, total_elements: usize, index: usize) struct { start: usize, len: usize } {
        var start = arg_start;
        const end = start + arg_len -| 1;

        if (index < start)
            start = index
        else if (index > end)
            start += index - end;

        start = @min(total_elements -| 1, start);
        const len = @min(total_elements - start, arg_len);

        return .{ .start = start, .len = len };
    }
};

/// Useful for when there are multiple strings with the same life time
pub const MultiStrings = struct {
    const Self = @This();

    pub const Slice = struct { offset: u32, len: u32 };
    bytes: std.ArrayListUnmanaged(u8) = .{},

    pub fn deinit(self: *Self, allocator: std.mem.Allocator) void {
        self.bytes.deinit(allocator);
    }

    pub fn clear(self: *Self) void {
        self.bytes.clearRetainingCapacity();
    }

    pub fn append(self: *Self, allocator: std.mem.Allocator, string: []const u8) !Slice {
        try self.bytes.ensureUnusedCapacity(allocator, string.len);

        const slice = Slice{ .offset = @truncate(self.bytes.items.len), .len = @truncate(string.len) };
        self.bytes.appendSliceAssumeCapacity(string);
        return slice;
    }

    /// Tries to reuse the same memory for new strings.
    pub fn appendReuse(self: *Self, allocator: std.mem.Allocator, string: []const u8) !Slice {
        try self.bytes.ensureUnusedCapacity(allocator, string.len);

        if (std.mem.indexOf(u8, self.bytes.items, string)) |offset| {
            return .{ .offset = @truncate(offset), .len = @truncate(string.len) };
        } else {
            const slice = Slice{ .offset = @truncate(self.bytes.items.len), .len = @truncate(string.len) };
            self.bytes.appendSliceAssumeCapacity(string);
            return slice;
        }
    }

    pub fn get(self: *const Self, index: usize) []const u8 {
        std.debug.assert(index < self.strings.items.len);
        const slice = self.strings.items[index];
        return self.bytes.items[slice.offset .. slice.offset + slice.len];
    }

    pub fn getBySlice(self: *const Self, slice: Slice) []const u8 {
        return self.bytes.items[slice.offset .. slice.offset + slice.len];
    }
};

pub fn override(source: anytype, values: anytype) @TypeOf(source) {
    var new = source;
    inline for (std.meta.fields(@TypeOf(values))) |field| {
        @field(new, field.name) = @field(values, field.name);
    }
    return new;
}

pub fn FilterableList(comptime T: type) type {
    return struct {
        const Self = @This();

        const NonFiltered = struct {
            offset: u32,
            score: u32,
        };
        allocator: std.mem.Allocator,
        entries: std.ArrayListUnmanaged(T) = .{},
        non_filtered: std.ArrayListUnmanaged(NonFiltered) = .{},
        filtered: std.ArrayListUnmanaged(u32) = .{},

        pub fn init(allocator: std.mem.Allocator) Self {
            return .{ .allocator = allocator };
        }

        pub fn deinit(self: *Self) void {
            self.entries.deinit(self.allocator);
            self.non_filtered.deinit(self.allocator);
            self.filtered.deinit(self.allocator);
        }

        pub fn add(self: *Self, item: T) !void {
            try self.entries.append(self.allocator, item);
        }

        pub fn showAll(self: *Self) !void {
            if (self.non_filtered.items.len == self.entries.items.len) return;

            self.non_filtered.clearRetainingCapacity();
            self.filtered.clearRetainingCapacity();
            try self.non_filtered.ensureTotalCapacity(self.allocator, self.entries.items.len);
            for (self.entries.items, 0..) |_, offset| {
                self.non_filtered.appendAssumeCapacity(.{ .score = 0, .offset = @truncate(offset) });
            }
        }

        pub fn clearRetainingCapacity(self: *Self) void {
            self.entries.clearRetainingCapacity();
            self.non_filtered.clearRetainingCapacity();
            self.filtered.clearRetainingCapacity();
        }

        pub fn clearAndFree(self: *Self) void {
            self.entries.clearAndFree(self.allocator);
            self.non_filtered.clearAndFree(self.allocator);
            self.filtered.clearAndFree(self.allocator);
        }

        pub fn getNonFiltered(self: *const Self, index: usize) T {
            const offset = self.non_filtered.items[index].offset;
            return self.entries.items[offset];
        }

        pub fn getFiltered(self: *const Self, index: usize) T {
            const offset = self.filtered.items[index];
            return self.entries.items[offset];
        }

        pub fn filterSort(self: *Self, ctx: anytype, comptime scoreFn: fn (context: @TypeOf(ctx), item: T) ?u32) !void {
            self.filtered.clearRetainingCapacity();
            self.non_filtered.clearRetainingCapacity();

            for (self.entries.items, 0..) |item, usize_i| {
                const score = scoreFn(ctx, item);
                const i: u32 = @truncate(usize_i);
                if (score) |s| {
                    try self.non_filtered.append(self.allocator, .{ .score = s, .offset = i });
                } else {
                    try self.filtered.append(self.allocator, i);
                }
            }

            const Context = struct {
                pub fn lessThan(_: void, lhs: NonFiltered, rhs: NonFiltered) bool {
                    return lhs.score < rhs.score;
                }
            };

            std.sort.pdq(NonFiltered, self.non_filtered.items, {}, Context.lessThan);
        }

        pub fn filter(self: *Self, ctx: anytype, comptime filterFn: fn (context: @TypeOf(ctx), item: T) bool) !void {
            self.filtered.clearRetainingCapacity();
            self.non_filtered.clearRetainingCapacity();

            for (self.entries.items, 0..) |item, i| {
                if (filterFn(ctx, item)) {
                    try self.filtered.append(self.allocator, i);
                } else {
                    try self.non_filtered.append(self.allocator, i);
                }
            }
        }

        pub fn sortNonFiltered(self: *Self, context: anytype, comptime lessThanFn: fn (@TypeOf(context), lhs: T, rhs: T) bool) void {
            const Context = SortContext(@TypeOf(context), lessThanFn);
            const ctx = Context{ .list = self, .user_context = context };
            std.sort.block(usize, self.non_filtered.items, ctx, Context.lessThan);
        }

        pub fn sortFiltered(self: *Self, context: anytype, comptime lessThanFn: fn (@TypeOf(context), lhs: T, rhs: T) bool) void {
            const Context = SortContext(@TypeOf(context), lessThanFn);
            const ctx = Context{ .list = self, .user_context = context };
            std.sort.block(usize, self.filtered.items, ctx, Context.lessThan);
        }

        fn SortContext(comptime Context: type, comptime lessThanFn: fn (Context, T, T) bool) type {
            return struct {
                list: *const Self,
                user_context: Context,

                pub fn lessThan(ctx: @This(), lhs_offset: usize, rhs_offset: usize) bool {
                    const left_entry = ctx.list.entries.items[lhs_offset];
                    const right_entry = ctx.list.entries.items[rhs_offset];

                    return lessThanFn(ctx.user_context, left_entry, right_entry);
                }
            };
        }

        fn FilterSortContext(comptime Context: type, comptime scoreFn: fn (context: Context, item: T) ?usize) type {
            return struct {
                list: *const Self,
                inner_context: Context,

                pub fn lessThan(filter_sort_ctx: @This(), lhs_offset: usize, rhs_offset: usize) bool {
                    const left_entry = filter_sort_ctx.list.entries.items[lhs_offset];
                    const right_entry = filter_sort_ctx.list.entries.items[rhs_offset];

                    // result of scoreFn is never null because we are going through the non_filtered list
                    const lhs_score = scoreFn(filter_sort_ctx.inner_context, left_entry).?;
                    const rhs_score = scoreFn(filter_sort_ctx.inner_context, right_entry).?;

                    return lhs_score < rhs_score;
                }
            };
        }
    };
}

pub fn vtableFromContainer(comptime VTable: type, comptime Container: type) VTable {
    if (!@inComptime()) @compileError("vtableFromContainer() Must be called at compile time");
    comptime {
        const fields = std.meta.fields(VTable);
        for (fields) |field| {
            if (isFunction(field.type)) {
                const has_decl = @hasDecl(Container, field.name);
                const is_optional = isOptional(field.type);
                const has_default = field.default_value != null;

                const container_function_full_name = @typeName(Container) ++ "." ++ field.name;
                if (!has_decl and !is_optional and !has_default) {
                    @compileError("Function `" ++ field.name ++ "` Doesn't exist in container `" ++ container_function_full_name ++ "`");
                } else if (has_decl and !is_optional) {
                    const in_container_field = @field(Container, field.name);
                    const interface_function = field.type;
                    const container_function = @TypeOf(in_container_field);
                    validateDefinition(container_function_full_name, interface_function, container_function);
                }
            }
        }

        var vtable: VTable = undefined;
        for (fields) |field| {
            if (isFunction(field.type)) {
                @field(vtable, field.name) = @ptrFromInt(0xDEADBEEF); // just in case
            }
        }

        for (fields) |field| {
            if (isFunction(field.type)) {
                @field(vtable, field.name) =
                    if (@hasDecl(Container, field.name))
                    @field(Container, field.name)
                else if (isOptional(field.type))
                    null
                else if (field.default_value) |default|
                    @as(*const field.type, @ptrCast(@alignCast(default))).*
                else
                    unreachable; // validation above failed somehow
            }
        }

        return vtable;
    }
}

/// Given two function definitions validates that they are the same
pub fn validateDefinition(comptime container_name: []const u8, comptime interface_function: type, comptime container_function: type) void {
    const interface = getFn(interface_function).?;
    const container = getFn(container_function).?;

    if (interface.params.len != container.params.len) {
        const pre_msg = "Interface and Container `" ++ container_name ++ "` have different argument count.";
        const int = "Interface: `" ++ @typeName(interface_function) ++ "`";
        const con = "Container: `" ++ @typeName(container_function) ++ "`";
        @compileError(pre_msg ++ "\n" ++ int ++ "\n" ++ con);
    }

    inline for (interface.params, container.params, 0..) |iface, cont, i| {
        if (iface.type != cont.type) {
            const interface_param = @typeName(iface.type.?);
            const container_param = @typeName(cont.type.?);

            var buf: [64]u8 = undefined;
            const ith = comptime std.fmt.bufPrint(&buf, "{}", .{i + 1}) catch unreachable;

            const int = "Interface: `" ++ @typeName(interface_function) ++ "`";
            const con = "Container: `" ++ @typeName(container_function) ++ "`";
            const differ = "Diff: Argument number " ++ ith ++ " `" ++ interface_param ++ "` and `" ++ container_param ++ "`";
            @compileError(int ++ "\n" ++ con ++ "\n" ++ differ);
        }
    }

    if (container.return_type != interface.return_type) {
        const pre_msg = "`" ++ container_name ++ "` return type does not match that of the interface";
        const int = "Interface: `" ++ @typeName(interface.return_type.?) ++ "`";
        const con = "Container: `" ++ @typeName(container.return_type.?) ++ "`";
        @compileError(pre_msg ++ "\n" ++ int ++ "\n" ++ con);
    }
}

pub fn isFunction(comptime T: type) bool {
    return getFn(T) != null;
}

pub fn isOptional(comptime T: type) bool {
    return switch (@typeInfo(T)) {
        .Optional => true,
        else => false,
    };
}

pub fn getFn(comptime T: type) ?std.builtin.Type.Fn {
    return switch (@typeInfo(T)) {
        .Fn => |f| f,
        .Pointer => |ptr| getFn(ptr.child),
        .Optional => |opt| getFn(opt.child),
        else => null,
    };
}

pub fn anyEql(comptime T: type, a: []const T, b: T) bool {
    for (a) |a_item| {
        if (std.meta.eql(b, a_item)) return true;
    }

    return false;
}
