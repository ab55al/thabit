const std = @import("std");

const core = @import("core.zig");

const Buffer = core.Buffer;
const BufferWindow = core.BufferWindow;

pub const BufferDisplayer = struct {
    pub const GetType = fn (ptr: *anyopaque, arena: std.mem.Allocator, bhandle: core.BufferHandle, row: u32) anyerror!RowInfo;

    ptr: *anyopaque,
    getFn: *const GetType,

    pub fn get(self: BufferDisplayer, arena: std.mem.Allocator, bhandle: core.BufferHandle, row: u32) anyerror!RowInfo {
        return self.getFn(self.ptr, arena, bhandle, row);
    }
};

pub const RowInfo = struct {
    pub const RangeKind = enum { col, byte };
    pub const VirtualText = struct {
        text: []const u8,
        color: u32,
    };

    virtual_text: VirtualText = .{ .text = "", .color = 0 },
    text: []const ColorRange = &.{},
    underline: []const ColorRange = &.{},
    row_size: f32 = 0,
    range_kind: RangeKind,

    pub const empty = RowInfo{ .range_kind = .col };
};

pub const SymbolHighlight = struct {
    pub fn interface() BufferDisplayer {
        return .{ .ptr = undefined, .getFn = get };
    }

    pub fn get(self: *anyopaque, arena: std.mem.Allocator, bhandle: core.BufferHandle, row: u32) anyerror!RowInfo {
        _ = self;

        const bt = (core.buffer.getBufferData(bhandle) catch return RowInfo.empty).buffer_type;
        if (bt.len == 0) return RowInfo.empty;

        var ranges = try std.ArrayList(ColorRange).initCapacity(arena, 64);
        const line = core.buffer.getLine(arena, bhandle, row) catch |err| switch (err) {
            error.OutOfMemory => return std.mem.Allocator.Error.OutOfMemory,
            else => return RowInfo.empty,
        };

        const color = 0xCC00CC_FF;

        outer: for (symbols) |symbol| {
            var slicer: usize = 0;
            while (slicer < line.len) {
                const slice = line[slicer..];

                const index = std.mem.indexOf(u8, slice, symbol) orelse continue :outer;
                const abs_index = slicer + index;
                try ranges.append(.{
                    .start = abs_index,
                    .end = abs_index + symbol.len,
                    .color = color,
                });

                slicer += index + symbol.len;
            }
        }

        return RowInfo{ .text = try ranges.toOwnedSlice(), .range_kind = .byte };
    }

    pub const symbols = [_][]const u8{
        "(", ")",  "[", "]", "{",
        "}", "\"", "'", "<", ">",
        "#", "@",  "*", "&",
    };
};

pub const SearchResultsDisplayer = struct {
    pub fn interface() BufferDisplayer {
        return .{ .ptr = undefined, .getFn = get };
    }

    pub fn get(self: *anyopaque, arena: std.mem.Allocator, bhandle: core.BufferHandle, row: u32) anyerror!RowInfo {
        _ = self;
        const search_results = core.internal.state().search_keeper.rangesForBuffer(bhandle) orelse return RowInfo.empty;

        var ranges = try std.ArrayList(ColorRange).initCapacity(arena, 64);
        const color = 0x00FF00FF;
        for (search_results) |range| {
            if (range.start.row < row) continue;
            if (range.start.row > row) break;

            try ranges.append(.{
                .start = range.start.col,
                .end = range.end.col,
                .color = color,
            });
        }

        return .{ .underline = try ranges.toOwnedSlice(), .range_kind = .col };
    }
};

pub const ColorRange = struct {
    start: usize,
    end: usize,
    color: u32,

    // pub const ColorRangeIterator = struct {
    //     start: u64,
    //     end: u64,
    //     line_len: u64,
    //     i: u64,
    //     color_ranges: []ColorRange,
    //     defualt_color: u32,

    //     pub fn init(defualt_color: u32, line_len: u64, color_ranges: []ColorRange) ColorRangeIterator {
    //         var end: u64 = if (color_ranges.len == 0)
    //             line_len
    //         else if (color_ranges[0].start == 0)
    //             color_ranges[0].end
    //         else
    //             color_ranges[0].start;

    //         return .{
    //             .start = 0,
    //             .end = @min(line_len, end),
    //             .line_len = line_len,
    //             .i = 0,
    //             .color_ranges = color_ranges,
    //             .defualt_color = defualt_color,
    //         };
    //     }

    //     pub fn next(self: *ColorRangeIterator) ?ColorRange {
    //         if (self.start >= self.line_len) return null;
    //         if (self.color_ranges.len == 0 or self.i >= self.color_ranges.len) {
    //             const start = self.start;
    //             self.start = self.line_len;
    //             return .{ .start = start, .end = self.line_len, .color = self.defualt_color };
    //         }

    //         const is_color_range = self.color_ranges[self.i].start == self.start and self.color_ranges[self.i].end == self.end;
    //         const next_color_range: ?ColorRange =
    //             if (self.i + 1 < self.color_ranges.len)
    //             self.color_ranges[self.i + 1]
    //         else
    //             null;

    //         const new_start = self.end;
    //         const new_end = blk: {
    //             if (is_color_range) {
    //                 if (next_color_range) |n| break :blk n.start;
    //                 break :blk self.line_len;
    //             } else {
    //                 break :blk self.color_ranges[self.i].end;
    //             }
    //         };

    //         const color = if (is_color_range)
    //             self.color_ranges[self.i].color
    //         else
    //             self.defualt_color;

    //         const result = ColorRange{ .start = self.start, .end = self.end, .color = color };

    //         self.start = @min(self.line_len, new_start);
    //         self.end = @min(self.line_len, new_end);
    //         if (is_color_range) self.i += 1;

    //         // the next range is invalid so ignore the rest of the color_ranges
    //         if (new_end < result.end) {
    //             self.i = self.color_ranges.len;
    //         }

    //         return result;
    //     }
    // };
};
