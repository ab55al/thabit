const std = @import("std");
const fs = std.fs;

const core = @import("../../core.zig");

const completion = @import("completion.zig");
const Engine = completion.Engine;
const SourceInterface = completion.SourceInterface;
const Source = completion.Source;
const Entry = completion.Entry;
const InputContext = completion.InputContext;

pub const path_source = struct {
    pub const name = "path";

    pub fn source() Source {
        return .{
            .name = name,
            .trigger_characters = &.{std.fs.path.sep},
            .iface = interface(),
        };
    }

    pub fn interface() SourceInterface {
        return .{
            .ptr = undefined,
            .vtable = &.{
                .updateEntries = updateEntries,
            },
        };
    }

    var old_relavent_path_hash: u64 = 0;
    pub fn updateEntries(_: *anyopaque, entries: *Engine.Entries, input_context: InputContext) anyerror!void {
        const allocator = entries.allocator;

        const relavent_path = findRelaventSlice(input_context.beforeCursor()) orelse return error.Ignore;
        const new_relavent_path_hash = std.hash.Wyhash.hash(0, relavent_path);
        if (entries.filterable_list.non_filtered.items.len > 0 and new_relavent_path_hash == old_relavent_path_hash) return;
        old_relavent_path_hash = new_relavent_path_hash;

        const abs_path = try fs.cwd().realpathAlloc(allocator, relavent_path);
        defer allocator.free(abs_path);

        const files = try core.file_io.filesInDir(allocator, abs_path);
        defer {
            for (files) |p| allocator.free(p.path);
            allocator.free(files);
        }

        entries.clear();
        for (files) |file| {
            const text = file.path;
            try entries.add(.{
                .source = name,
                .label = text,
                .text = text,
                .kind = @tagName(file.kind),
                .edit = .delete_insert,
            });
        }
    }

    pub fn findRelaventSlice(string: []const u8) ?[]const u8 {
        // find an indicator of a path (`../`, `./` or `/`) and make sure
        // there's no white space between it and the end of the string

        var index: usize = 0;
        outer: while (index < string.len) {
            if (std.mem.indexOf(u8, string[index..], &.{fs.path.sep})) |relative_sep_index| {
                const abs_sep_index = index + relative_sep_index;

                for (string[abs_sep_index..]) |c| {
                    if (std.ascii.isWhitespace(c)) {
                        index += relative_sep_index + 1;
                        continue :outer;
                    }
                }

                const relavent = if (peek(string, abs_sep_index, -2) == '.' and peek(string, abs_sep_index, -1) == '.')
                    string[abs_sep_index - 2 ..]
                else if (peek(string, abs_sep_index, -1) == '.')
                    string[abs_sep_index - 1 ..]
                else
                    string[abs_sep_index..];

                // ignore everything after the last separator
                // This will ignore user text that may or may not be part
                // of a valid path
                if (relavent[relavent.len - 1] != fs.path.sep) {
                    const last_index = std.mem.lastIndexOf(u8, relavent, fs.path.sep_str) orelse return relavent;
                    return relavent[0 .. last_index + 1];
                } else {
                    return relavent;
                }
            } else {
                return null;
            }
        }

        return null;
    }

    fn peek(string: []const u8, index: usize, offset: isize) ?u8 {
        const i = @as(isize, @intCast(index)) + offset;
        if (i < 0 or i >= string.len) return null;

        return string[@intCast(i)];
    }
};

test "findRelaventSlice" {
    const target_path = "./";

    const string = "hello ./haha there " ++ target_path;
    try std.testing.expectEqualStrings(target_path, path_source.findRelaventSlice(string).?);
}
