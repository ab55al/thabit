const std = @import("std");

const core = @import("../../core.zig");
const utils = core.utils;

pub const InputContext = struct {
    line: []const u8,
    cursor_index: usize,
    cursor_point: core.Buffer.Point,
    trigger_character: ?u21 = null,

    pub fn atCursor(self: InputContext) []const u8 {
        const len = std.unicode.utf8ByteSequenceLength(self.line[self.cursor_index]) catch 1;
        return self.line[self.cursor_index .. self.cursor_index + len];
    }

    pub fn beforeCursor(self: InputContext) []const u8 {
        return self.line[0..self.cursor_index];
    }

    pub fn afterCursor(self: InputContext) []const u8 {
        const len = std.unicode.utf8ByteSequenceLength(self.line[self.cursor_index]) catch 1;
        return self.line[self.cursor_index + len ..];
    }
};

pub const Entry = struct {
    pub const EditStrategy = union(enum) {
        replace: core.Buffer.PointRange,
        insert,
        delete_insert,
    };

    source: []const u8,
    /// Used for filtering entries and for display on the UI
    label: []const u8,
    /// Used for the text edits on entry acceptance
    text: []const u8,
    kind: []const u8,

    edit: EditStrategy,
};

pub const Source = struct {
    name: []const u8,
    trigger_characters: []const u21 = &.{},
    iface: SourceInterface,
};

pub const SourceInterface = struct {
    ptr: *anyopaque,
    vtable: *const VTable,

    pub const VTable = struct {
        updateEntries: *const fn (ptr: *anyopaque, entries: *Engine.Entries, input_context: InputContext) anyerror!void,
    };

    pub fn updateEntries(self: SourceInterface, entries: *Engine.Entries, input_context: InputContext) anyerror!void {
        try self.vtable.updateEntries(self.ptr, entries, input_context);
    }
};

pub const Engine = struct {
    allocator: std.mem.Allocator,
    string_storage: core.slice_storage.StringStorage,

    sources: std.ArrayListUnmanaged(Source) = .{},
    current_entries: std.StringArrayHashMapUnmanaged(Entries) = .{},
    view: utils.ListView = .{},

    pub fn init(allocator: std.mem.Allocator) Engine {
        return .{
            .allocator = allocator,
            .string_storage = core.slice_storage.StringStorage.init(allocator),
        };
    }

    pub fn deinit(self: *Engine) void {
        self.string_storage.deinit();

        for (self.sources.items) |source| {
            if (source.trigger_characters.len > 0)
                self.allocator.free(source.trigger_characters);
        }

        self.sources.deinit(self.allocator);

        for (self.current_entries.values()) |*entries| {
            entries.deinit();
        }
        self.current_entries.deinit(self.allocator);
    }

    pub fn addSource(self: *Engine, source: Source) !void {
        const source_name = try self.string_storage.getAndPut(source.name);
        const trigger_characters = try self.allocator.dupe(u21, source.trigger_characters);
        try self.sources.append(self.allocator, .{
            .name = source_name,
            .trigger_characters = trigger_characters,
            .iface = source.iface,
        });
    }

    pub fn getSource(self: *Engine, source_name: []const u8) ?*Source {
        for (self.sources.items) |*source| {
            if (std.mem.eql(u8, source.name, source_name)) return source;
        }

        return null;
    }

    pub fn getTriggerChars(self: *Engine, source_name: []const u8) []const u21 {
        const source = self.getSource(source_name) orelse return &.{};
        return source.trigger_characters;
    }

    pub fn addTriggerCharacters(self: *Engine, source_name: []const u8, chars: []const u21) !void {
        var source = self.getSource(source_name) orelse return;
        var new_slice = try self.allocator.alloc(u21, source.trigger_characters.len + chars.len);
        std.mem.copyForwards(u21, new_slice, source.trigger_characters);
        std.mem.copyForwards(u21, new_slice[source.trigger_characters.len..], chars);
        self.allocator.free(source.trigger_characters);
        source.trigger_characters = new_slice;
    }

    pub fn updateEntriesFromAll(self: *Engine, ic: InputContext) !void {
        defer self.view.reset();
        for (self.sources.items) |source| {
            self.updateEntriesFrom(source, ic) catch continue;
        }
    }

    pub fn doesNotStartWith(comptime T: type, haystack: []const T, needle: []const T) bool {
        return !std.mem.startsWith(T, haystack, needle);
    }

    pub fn updateEntriesFrom(self: *Engine, source: Source, ic: InputContext) !void {
        const gop = try self.current_entries.getOrPut(self.allocator, source.name);
        var entries: *Entries = gop.value_ptr;
        if (!gop.found_existing) {
            entries.* = Entries.init(self.allocator, source);
        }

        source.iface.updateEntries(entries, ic) catch {
            entries.clear();
        };

        const search_prompt = getSearchPrompt(ic, source) orelse "";

        const FilterSortContext = struct {
            filter_by: []const u8,
            entries: *const Entries,

            pub fn score(ctx: @This(), item: Entries.StringEntry) ?u32 {
                const label = ctx.entries.toEntry(item).label;
                return utils.naiveAsciifuzzySearch(label, ctx.filter_by);
            }
        };

        const ctx = FilterSortContext{ .filter_by = search_prompt, .entries = entries };
        try entries.filterable_list.filterSort(ctx, FilterSortContext.score);
    }

    pub fn resetEntries(self: *Engine) void {
        for (self.current_entries.values()) |*entries| {
            entries.clear();
        }
        self.view.reset();
    }

    pub fn selectNext(self: *Engine) void {
        self.view.selectNext(self.nonFilteredEntriesCount(), .unselect);
    }

    pub fn selectPrev(self: *Engine) void {
        self.view.selectPrev(self.nonFilteredEntriesCount(), .unselect);
    }

    pub fn viewRange(self: *Engine) utils.ListView.ViewRange {
        return self.view.viewRange(self.nonFilteredEntriesCount());
    }

    pub fn getSelectedEntry(self: *Engine) ?Entry {
        const entries_count = self.nonFilteredEntriesCount();
        const index = self.view.selected_index orelse return null;
        if (entries_count == 0 or index >= entries_count) return null;

        return self.getNonFilteredEntry(index);
    }

    pub fn getNonFilteredEntry(self: *Engine, index: usize) Entry {
        std.debug.assert(index < self.nonFilteredEntriesCount());

        var i = index;
        for (self.current_entries.values()) |current| {
            if (i < current.filterable_list.non_filtered.items.len) {
                return current.toEntry(current.filterable_list.getNonFiltered(i));
            }

            i -= current.filterable_list.non_filtered.items.len;
        }

        unreachable;
    }

    pub fn getFilteredEntry(self: *Engine, index: usize) Entry {
        std.debug.assert(index < self.filteredEntriesCount());

        var i = index;
        for (self.current_entries.values()) |current| {
            if (i < current.filtered_entries.items.len) {
                return current.toEntry(current.filtered_entries.items[i]);
            }

            i -= current.filtered_entries.items.len;
        }

        unreachable;
    }

    pub fn entriesCount(self: *Engine) usize {
        var len: usize = 0;
        for (self.current_entries.values()) |current| {
            len += current.filterable_list.entries.items.len;
        }

        return len;
    }

    pub fn filteredEntriesCount(self: *Engine) usize {
        var len: usize = 0;
        for (self.current_entries.values()) |current| {
            len += current.filterable_list.filtered.items.len;
        }

        return len;
    }

    pub fn nonFilteredEntriesCount(self: *Engine) usize {
        var len: usize = 0;
        for (self.current_entries.values()) |current| {
            len += current.filterable_list.non_filtered.items.len;
        }

        return len;
    }

    pub fn clearEntries(self: *Engine) void {
        for (self.current_entries.values()) |*entries| {
            entries.clear();
        }
    }

    pub fn updateEntriesByTriggersCharacter(self: *Engine, ic: InputContext, cp: u21) void {
        outer: for (self.sources.items) |source| {
            for (source.trigger_characters) |trig| {
                if (trig == cp) {
                    self.updateEntriesFrom(source, ic) catch continue :outer;
                }
            }
        }
    }

    pub fn isTriggerCharacterForAny(self: *Engine, cp: u21) bool {
        for (self.sources.items) |source| {
            for (source.trigger_characters) |trig| {
                if (trig == cp) return true;
            }
        }

        return false;
    }

    pub const Entries = struct {
        const String = utils.MultiStrings.Slice;
        const StringEntry = struct {
            source: String,
            label: String,
            text: String,
            kind: String,
            edit: Entry.EditStrategy,
        };

        allocator: std.mem.Allocator,
        source: Source,
        strings: utils.MultiStrings = .{},
        filterable_list: utils.FilterableList(StringEntry),

        fn init(allocator: std.mem.Allocator, source: Source) Entries {
            return .{
                .allocator = allocator,
                .source = source,
                .filterable_list = utils.FilterableList(StringEntry).init(allocator),
            };
        }

        fn deinit(self: *Entries) void {
            self.filterable_list.deinit();
            self.strings.deinit(self.allocator);
        }

        pub fn add(self: *Entries, entry: Entry) !void {
            const source = try self.strings.appendReuse(self.allocator, entry.source);
            const kind = try self.strings.appendReuse(self.allocator, entry.kind);
            const text = try self.strings.append(self.allocator, entry.text);
            const label = try self.strings.append(self.allocator, entry.label);

            try self.filterable_list.add(.{
                .source = source,
                .kind = kind,
                .text = text,
                .label = label,
                .edit = entry.edit,
            });
        }

        pub fn clear(self: *Entries) void {
            self.filterable_list.clearRetainingCapacity();
            self.strings.clear();
        }

        fn toEntry(self: Entries, e: StringEntry) Entry {
            return .{
                .source = self.strings.getBySlice(e.source),
                .label = self.strings.getBySlice(e.label),
                .text = self.strings.getBySlice(e.text),
                .kind = self.strings.getBySlice(e.kind),
                .edit = e.edit,
            };
        }
    };
};

pub fn findPartOfTextInLine(line: []const u8, text: []const u8, trigger_chars: []const u21) ?usize {
    // get index based on trigger chars
    if (findIndexAfterLastChar(line, trigger_chars)) |i|
        return i;

    // get index based on string matching
    var i = text.len;
    while (i > 0) : (i -= 1) {
        if (std.mem.lastIndexOf(u8, line, text[0..i])) |index| {
            return index;
        }
    }

    return null;
}

pub fn findIndexAfterLastChar(line: []const u8, chars: []const u21) ?usize {
    if (chars.len == 0) return null;
    var trigger_index: ?usize = null;
    for (chars) |char| {
        var buf: [4]u8 = undefined;
        const len = std.unicode.utf8Encode(char, &buf) catch continue;
        const index = std.mem.lastIndexOf(u8, line, buf[0..len]);

        if (trigger_index != null and index != null) {
            trigger_index = @max(trigger_index.?, index.? + len);
        } else if (index) |i| {
            trigger_index = i + len;
        }
    }

    return trigger_index;
}

pub fn findLastWhiteSpace(line: []const u8) ?usize {
    var i = line.len;
    while (i > 0) {
        i -= 1;
        if (std.ascii.isWhitespace(line[i])) return i;
    }

    return null;
}

pub fn getSearchPrompt(ic: InputContext, source: Source) ?[]const u8 {
    const search_prompt_index =
        findIndexAfterLastChar(
        ic.beforeCursor(),
        source.trigger_characters,
    ) orelse 1 + (findLastWhiteSpace(ic.beforeCursor()) orelse return null);
    if (search_prompt_index >= ic.beforeCursor().len) return null;

    return ic.line[search_prompt_index..ic.cursor_index];
}
