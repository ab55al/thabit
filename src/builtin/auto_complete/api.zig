const std = @import("std");

const core = @import("../../core.zig");
const dmodify = core.dmodify;
const completion = @import("completion.zig");

pub const sources = @import("sources.zig");
pub const Source = completion.Source;
pub const SourceInterface = completion.SourceInterface;
pub const Entry = completion.Entry;
pub const Engine = completion.Engine;
pub const InputContext = completion.InputContext;

var inited = false;
pub var engine_state: Engine = undefined;

pub fn init(allocator: std.mem.Allocator) !void {
    engine_state = Engine.init(allocator);
    errdefer engine_state.deinit();

    try core.event_listener.attach(&engine_state, hooksCallback);
    errdefer core.event_listener.detachByPtr(&engine_state);

    try engine_state.addSource(sources.path_source.source());

    inited = true;
}

pub fn deinit() void {
    engine_state.deinit();
    core.event_listener.detachByPtr(&engine_state);

    inited = false;
}

pub fn addSource(source: Source) !void {
    try engine_state.addSource(source);
}

pub fn addTriggerChars(source_name: []const u8, chars: []const u21) !void {
    try engine_state.addTriggerCharacters(source_name, chars);
}

pub fn selectNext() void {
    engine_state.selectNext();
}

pub fn selectPrev() void {
    engine_state.selectPrev();
}

pub fn updateEntries() void {
    const ic = getInputContext(&engine_state, core.focused.cursor(), null) catch return;
    defer engine_state.allocator.free(ic.line);
    engine_state.updateEntriesFromAll(ic) catch return;
}

pub fn acceptSelected() !void {
    const entry = engine_state.getSelectedEntry() orelse return;
    const cursor = core.focused.cursor();

    const col_offset: u32 = @truncate(core.utf8.countCodepoints(entry.text));
    switch (entry.edit) {
        .replace => |range| {
            try dmodify.buffer.replaceRange(core.focused.handle(), range, entry.text);
            if (range.start.row == range.end.row) {
                try dmodify.buffer_window.setCursor(core.focused.bufferWindow(), range.end.addCol(col_offset));
            }
        },
        .insert => {
            try dmodify.buffer.insertAt(core.focused.handle(), cursor, entry.text);
            try dmodify.buffer_window.setCursor(core.focused.bufferWindow(), cursor.addCol(col_offset));
        },
        .delete_insert => {
            const ic = try getInputContext(&engine_state, cursor, null);
            defer engine_state.allocator.free(ic.line);
            const trigger_chars = engine_state.getTriggerChars(entry.source);

            var point = cursor;
            if (completion.findPartOfTextInLine(ic.beforeCursor(), entry.label, trigger_chars)) |i| {
                const line_start = core.focused.lineStartIndex(cursor.row);
                point = core.focused.getPoint(i + line_start);
            }

            try dmodify.buffer.replaceRange(core.focused.handle(), .{ .start = point, .end = cursor }, entry.text);
            try dmodify.buffer_window.setCursor(core.focused.bufferWindow(), point.addCol(col_offset));
        },
    }
    engine_state.clearEntries();
}

pub fn getSelectedEntry() ?core.auto_complete.Entry {
    return engine_state.getSelectedEntry();
}

fn hooksCallback(_: *anyopaque, hook: core.Event) anyerror!void {
    switch (hook) {
        .after_change => |v| {
            var cp: u21 = 0;
            if (v.change.inserted_len >= 1 and v.change.inserted_len <= 4) {
                cp = core.buffer.getCodepointAt(v.bhandle, v.change.start_point) catch 0;
            }

            const callback = struct {
                var char: u21 = 0;
                pub fn call(_: *anyopaque) anyerror!void {
                    const ic = try getInputContext(&engine_state, core.focused.cursor(), char);
                    defer engine_state.allocator.free(ic.line);
                    if (engine_state.isTriggerCharacterForAny(char)) {
                        engine_state.updateEntriesByTriggersCharacter(ic, char);
                    } else if (engine_state.entriesCount() > 0) {
                        try engine_state.updateEntriesFromAll(ic);
                    }
                }
            };

            // this will ensure that updating will only happen after
            // all hooks have been handled.
            // This gives LSP servers enough time to sync their document state with the client's
            callback.char = cp;
            core.addTimedCallback(undefined, callback.call, 0) catch return;
        },
        .after_cursor_move => |v| {
            if (v.old_point.row != v.new_point.row) {
                engine_state.clearEntries();
            }
        },
        .buffer_window_focused => {
            engine_state.clearEntries();
        },
        else => return,
    }
}

pub fn getInputContext(self: *Engine, point: core.Buffer.Point, trigger_character: ?u21) !InputContext {
    const cursor = point;
    const line = try core.focused.getLine(self.allocator, cursor.row);
    const index = core.utils.getIndexOfCol(line, cursor.col);
    return .{
        .line = line,
        .cursor_index = index,
        .cursor_point = cursor,
        .trigger_character = trigger_character,
    };
}
