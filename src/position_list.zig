const std = @import("std");

const StringStorageUnmanaged = @import("slice_storage.zig").StringStorageUnmanaged;
const Buffer = @import("core.zig").Buffer;
const Point = Buffer.Point;
const MarkKey = @import("core.zig").Marks.MarkKey;

pub const PositionList = @This();

list: std.ArrayListUnmanaged(Position) = .{},
string_storage: StringStorageUnmanaged = .{},
allocator: std.mem.Allocator,
index: usize = 0,

pub fn init(allocator: std.mem.Allocator) PositionList {
    return .{ .allocator = allocator };
}

pub fn deinit(self: *PositionList) void {
    self.list.deinit(self.allocator);
    self.string_storage.deinit(self.allocator);
}

pub fn push(self: *PositionList, file_path: []const u8, mark: MarkKey) !void {
    std.debug.assert(std.fs.path.isAbsolute(file_path));
    try self.list.ensureUnusedCapacity(self.allocator, 1);
    const ft = try self.string_storage.getAndPut(self.allocator, file_path);
    self.list.appendAssumeCapacity(.{ .file_path = ft, .mark = mark });
}

pub fn moveFirst(self: *PositionList) void {
    self.index = 0;
}

pub fn moveLast(self: *PositionList) void {
    self.index = self.list.items.len;
}

pub fn moveNext(self: *PositionList) void {
    self.index = @min(self.index + 1, self.list.items.len);
}

pub fn movePrev(self: *PositionList) void {
    self.index -|= 1;
}

pub fn getPosition(self: *PositionList) ?Position {
    if (self.list.items.len == 0) return null;

    if (self.index >= self.list.items.len) {
        self.index = self.list.items.len -| 1;
    }

    return self.list.items[self.index];
}

pub const Position = struct {
    file_path: []const u8,
    mark: MarkKey,
};
