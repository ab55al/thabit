const std = @import("std");
const print = std.debug.print;
const unicode = std.unicode;
const assert = std.debug.assert;

const utils = @import("utils.zig");

pub const ByteType = enum {
    start_byte,
    continue_byte,
};

pub fn byteType(byte: u8) ByteType {
    const result = unicode.utf8ByteSequenceLength(byte);
    return if (result == error.Utf8InvalidStartByte) .continue_byte else .start_byte;
}

/// Counts the number of code points in a string. When an invalid byte is encountered
/// it is counted as one code point.
pub fn countCodepoints(string: []const u8) usize {
    var count: u64 = 0;
    var i: u64 = 0;
    while (i < string.len) {
        const byte = string[i];

        const len = unicode.utf8ByteSequenceLength(byte) catch 1;
        i += len;
        count += 1;
    }

    return count;
}

/// Finds the index of the *nth* code point in a string. When an invalid byte is encountered
/// it is counted as one code point.
pub fn indexOfCP(string: []const u8, cp: u64) u64 {
    assert(cp > 0);

    var count: u64 = 0;
    var i: u64 = 0;
    while (i < string.len) {
        const byte = string[i];
        const len = unicode.utf8ByteSequenceLength(byte) catch 1;
        count += 1;
        if (cp == count) return i;
        i += len;
    }

    return i;
}

pub const Utf8Iterator = struct {
    const Self = @This();

    bytes: []const u8,
    i: usize = 0,

    pub fn init(string: []const u8) Utf8Iterator {
        return .{ .bytes = string };
    }

    pub fn nextCodepointSlice(it: *Self) ?[]const u8 {
        const slice = it.peek() orelse return null;
        it.i += slice.len;
        return slice;
    }

    pub fn peek(it: *const Self) ?[]const u8 {
        if (it.i >= it.bytes.len) {
            return null;
        }

        var i = it.i;
        const cp_len = unicode.utf8ByteSequenceLength(it.bytes[i]) catch 1;
        i += cp_len;
        return it.bytes[i - cp_len .. i];
    }

    pub fn peekCodePoint(it: *const Self) !?u21 {
        const slice = it.peek() orelse return null;
        return try unicode.utf8Decode(slice);
    }
};

pub const ReverseUtf8View = struct {
    const Self = @This();
    string: []const u8,
    index: u64,
    done: bool = false,

    pub fn init(string: []const u8) ReverseUtf8View {
        return .{
            .string = string,
            .index = string.len -| 1,
        };
    }

    pub fn nextSlice(self: *Self) ?[]const u8 {
        const result = self.peekWithIndex() orelse return null;
        if (self.index == 0) self.done = true;
        self.index = result.index;
        return result.slice;
    }

    pub fn peek(self: *Self) ?[]const u8 {
        return (self.peekWithIndex() orelse return null).slice;
    }

    fn peekWithIndex(self: *const Self) ?struct { slice: []const u8, index: usize } {
        if (self.done or self.string.len == 0) return null;

        var i = self.index;
        while (byteType(self.string[i]) != .start_byte and i > 0) {
            i -= 1;
        }

        const len = unicode.utf8ByteSequenceLength(self.string[i]) catch 1;
        const slice = self.string[i .. i + len];
        i -|= 1;
        return .{ .slice = slice, .index = i };
    }

    pub fn nextCodePoint(self: *Self) !?u21 {
        const slice = self.prevSlice() orelse return null;
        return unicode.utf8Decode(slice);
    }

    pub fn peekCodePoint(self: *Self) !?u21 {
        const slice = self.peek() orelse return null;
        return try unicode.utf8Decode(slice);
    }
};

test "ReverseUtf8View" {
    const sequences = [_][]const u8{ "ا", "ه", "ل", "ا", "h", "e", "l", "l", "o", "و", "س", "ه", "ل", "ا", "." };
    const string = try std.mem.concat(std.testing.allocator, u8, &sequences);
    defer std.testing.allocator.free(string);
    var iter = ReverseUtf8View.init(string);

    var cp_index = sequences.len -| 1;
    while (iter.nextSlice()) |slice| {
        try std.testing.expectEqualStrings(slice, sequences[cp_index]);
        cp_index -|= 1;
    }
}
