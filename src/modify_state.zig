const std = @import("std");

const core = @import("core.zig");
const utils = core.utils;

const internal = core.internal;

const file_io = core.file_io;

const log = std.log.scoped(.modify);

pub const Events = struct {
    pub const max_events_per_call = 32;
    pub const Array = [max_events_per_call]Event;
    events: Array = undefined,
    len: usize = 0,

    pub fn create(events: []const Event) Events {
        std.debug.assert(events.len < max_events_per_call);
        var array: Array = undefined;
        std.mem.copyForwards(Event, &array, events);
        return .{ .events = array, .len = events.len };
    }

    pub fn concat(events: []const Events) Events {
        var event_count: usize = 0;
        for (events) |e| event_count += e.len;

        std.debug.assert(event_count < max_events_per_call);

        var result = Events{};
        for (events) |e| {
            std.mem.copyForwards(
                Event,
                result.events[result.len..],
                e.events[0..e.len],
            );

            result.len += e.len;
        }

        return result;
    }
};

pub const Event = union(enum) {
    pub const BufferEvent = struct { bhandle: core.BufferHandle };
    pub const CursorMove = struct { buffer_window: core.BufferWindow.ID, old_point: core.Buffer.Point, new_point: core.Buffer.Point };
    pub const Change = struct { bhandle: core.BufferHandle, change: core.Buffer.Change };
    pub const BufferWindowFocused = struct { bhandle: core.BufferHandle, buffer_window: core.BufferWindow.ID };

    buffer_created: BufferEvent,
    before_killing_buffer: BufferEvent,
    after_change: Change,
    after_cursor_move: CursorMove,
    buffer_window_focused: BufferWindowFocused,
    thabit_shutdown,
};

pub const EventListeners = struct {
    pub const Callback = fn (ptr: *anyopaque, event: Event) anyerror!void;
    pub const Interface = struct {
        ptr: *anyopaque,
        callback: *const Callback,
    };

    interfaces: std.ArrayList(Interface),

    pub fn init(allocator: std.mem.Allocator) EventListeners {
        return .{ .interfaces = std.ArrayList(Interface).init(allocator) };
    }

    pub fn deinit(self: *EventListeners) void {
        self.interfaces.deinit();
    }

    pub fn dispatch(self: *EventListeners, event: Event) void {
        for (self.interfaces.items) |iface| {
            iface.callback(iface.ptr, event) catch continue;
        }
    }

    pub fn attach(self: *EventListeners, ptr: *anyopaque, callback: *const Callback) !void {
        if (self.findByPtr(ptr) != null) return;

        try self.interfaces.append(.{ .ptr = ptr, .callback = callback });
    }

    pub fn detach(self: *EventListeners, ptr: *anyopaque, callback: *const Callback) void {
        for (self.interfaces.items, 0..) |iface, i| {
            if (iface.callback == callback and iface.ptr == ptr) {
                _ = self.interfaces.orderedRemove(i);
                return;
            }
        }
    }

    pub fn detachByCallback(self: *EventListeners, callback: *const Callback) void {
        const index = self.findByCallback(callback) orelse return;
        _ = self.interfaces.orderedRemove(index);
    }

    pub fn detachByPtr(self: *EventListeners, ptr: *anyopaque) void {
        const index = self.findByPtr(ptr) orelse return;
        _ = self.interfaces.orderedRemove(index);
    }

    pub fn findByPtr(self: *EventListeners, ptr: *anyopaque) ?usize {
        for (self.interfaces.items, 0..) |iface, i| {
            if (iface.ptr == ptr) {
                return i;
            }
        }
        return null;
    }

    pub fn findByCallback(self: *EventListeners, callback: *const Callback) ?usize {
        for (self.interfaces.items, 0..) |iface, i| {
            if (iface.callback == callback) {
                return i;
            }
        }
        return null;
    }
};

pub const ModifyObserver = struct {
    pub const Callback = fn (modifications: []const Modify, when: When) void;
    pub const When = enum { before, after };
    callback: *const Callback,
};

pub const DeferredModify = struct {
    arena: std.heap.ArenaAllocator,
    observers: std.ArrayList(ModifyObserver),
    event_listeners: EventListeners,
    calls: std.ArrayList(Modify),

    pub fn init(gpa: std.mem.Allocator) !DeferredModify {
        return .{
            .calls = try std.ArrayList(Modify).initCapacity(gpa, 1024),
            .observers = try std.ArrayList(ModifyObserver).initCapacity(gpa, 1024),
            .arena = std.heap.ArenaAllocator.init(gpa),
            .event_listeners = EventListeners.init(gpa),
        };
    }

    pub fn deinit(self: *DeferredModify) void {
        self.observers.deinit();
        self.event_listeners.deinit();
        self.calls.deinit();
        self.arena.deinit();
    }

    pub fn clearRetainingCapacity(self: *DeferredModify) void {
        self.calls.clearRetainingCapacity();
        _ = self.arena.reset(.retain_capacity);
    }

    pub fn add(self: *DeferredModify, comptime kind: std.meta.Tag(Modify), args: anytype) !void {
        const T = std.meta.FieldType(Modify, kind);
        const call = if (@hasDecl(T, "clone"))
            try T.clone(&self.arena, args)
        else
            T.copy(args);

        const call_union = @unionInit(Modify, @tagName(kind), call);

        try self.calls.append(call_union);
    }

    pub fn addObserver(self: *DeferredModify, callback: *const ModifyObserver.Callback) !void {
        try self.observers.append(.{ .callback = callback });
    }

    pub fn removeObserver(self: *DeferredModify, callback: *const ModifyObserver.Callback) void {
        for (self.observers.items, 0..) |ob, i| {
            if (ob.callback == callback) {
                _ = self.observers.swapRemove(i);
                break;
            }
        }
    }
};

pub const Modify = union(enum) {
    replace_bytes: struct {
        handle: core.BufferHandle,
        start_point: core.Buffer.Point,
        end_point: core.Buffer.Point,
        string: []const u8,

        pub fn clone(arena: *std.heap.ArenaAllocator, args: anytype) !@This() {
            return .{
                .handle = args.handle,
                .start_point = args.start_point,
                .end_point = args.end_point,
                .string = try arena.allocator().dupe(u8, args.string),
            };
        }

        pub fn call(self: @This(), state: *core.State) !Events {
            return try replaceBytes(state, self.handle, self.start_point, self.end_point, self.string);
        }
    },

    set_cursor: struct {
        window: core.BufferWindow.ID,
        point: core.Buffer.Point,

        pub fn copy(args: anytype) @This() {
            return .{ .window = args.window, .point = args.point };
        }

        pub fn call(self: @This(), state: *core.State) Events {
            return setCursor(state, self.window, self.point);
        }
    },

    create_buffer: struct {
        path: []const u8,
        /// asserts handle is unique and doesn't exists
        handle: core.BufferHandle,

        pub fn clone(arena: *std.heap.ArenaAllocator, args: anytype) !@This() {
            return .{
                .handle = args.handle,
                .path = try arena.allocator().dupe(u8, args.path),
            };
        }

        pub fn call(self: @This(), state: *core.State) !Events {
            return try createBuffer(state, self.path, self.handle);
        }
    },

    create_window: struct {
        /// asserts handle is unique and exists
        handle: core.BufferHandle,
        /// asserts window is unique and doesn't exist
        window: core.BufferWindow.ID,

        pub fn copy(args: anytype) @This() {
            return .{ .window = args.window, .handle = args.handle };
        }

        pub fn call(self: @This(), state: *core.State) !void {
            try createBufferWindow(state, self.handle, self.window);
        }
    },

    create_tab_with_window: struct {
        window: core.BufferWindow.ID,

        pub fn copy(args: anytype) @This() {
            return .{ .window = args.window };
        }

        pub fn call(self: @This(), state: *core.State) !void {
            try createTabWithWindow(state, self.window);
        }
    },

    set_buffer_type: struct {
        handle: core.BufferHandle,
        buffer_type: []const u8,

        pub fn clone(arena: *std.heap.ArenaAllocator, args: anytype) !@This() {
            return .{
                .handle = args.handle,
                .buffer_type = try arena.allocator().dupe(u8, args.buffer_type),
            };
        }

        pub fn call(self: @This(), state: *core.State) !void {
            try setBufferType(state, self.handle, self.buffer_type);
        }
    },

    set_buffer_path: struct {
        handle: core.BufferHandle,
        new_path: []const u8,

        pub fn clone(arena: *std.heap.ArenaAllocator, args: anytype) !@This() {
            return .{
                .handle = args.handle,
                .new_path = try arena.allocator().dupe(u8, args.new_path),
            };
        }

        pub fn call(self: @This(), state: *core.State) !void {
            try setBufferPathAbsolute(state, self.handle, self.new_path);
        }
    },

    set_buffer_handle: struct {
        window: core.BufferWindow.ID,
        new_handle: core.BufferHandle,

        pub fn copy(args: anytype) @This() {
            return .{
                .new_handle = args.new_handle,
                .window = args.window,
            };
        }

        pub fn call(self: @This(), state: *core.State) void {
            setBufferHandle(state, self.window, self.new_handle);
        }
    },

    open_window: struct {
        window: core.BufferWindow.ID,
        tab_index: usize,
        kind: union(enum) {
            split: struct {
                other_window: core.BufferWindow.ID,
                opts: core.WindowSplitOptions,
            },
            float: core.draw.Rect, // position
        },

        pub fn copy(args: anytype) @This() {
            return .{
                .window = args.window,
                .tab_index = args.tab_index,
                .kind = args.kind,
            };
        }

        pub fn call(self: @This(), state: *core.State) !void {
            try switch (self.kind) {
                .float => |rect| floatWindow(state, self.tab_index, self.window, rect),
                .split => |split| splitWindow(state, self.tab_index, split.other_window, self.window, split.opts),
            };
        }
    },

    push_jump_point: struct {
        window: core.BufferWindow.ID,
        path: []const u8,
        point: core.Buffer.Point,

        pub fn clone(arena: *std.heap.ArenaAllocator, args: anytype) !@This() {
            return .{
                .window = args.window,
                .point = args.point,
                .path = try arena.allocator().dupe(u8, args.path),
            };
        }

        pub fn call(self: @This(), state: *core.State) !void {
            try pushJumpPoint(state, self.window, self.path, self.point);
        }
    },

    jump_list_jump: struct {
        window: core.BufferWindow.ID,
        kind: enum { next, prev },

        pub fn copy(args: anytype) @This() {
            return .{ .window = args.window, .kind = args.kind };
        }

        pub fn call(self: @This(), state: *core.State) !Events {
            const kind: u8 = @intFromEnum(self.kind);
            return try jumpListJump(state, self.window, @enumFromInt(kind));
        }
    },

    set_focused_window: struct {
        tab_index: usize,
        window: core.BufferWindow.ID,
        focus_tab: bool,

        pub fn copy(args: anytype) @This() {
            return .{
                .tab_index = args.tab_index,
                .window = args.window,
                .focus_tab = args.focus_tab,
            };
        }

        pub fn call(self: @This(), state: *core.State) !Events {
            return try setFocusedWindow(state, self.tab_index, self.window, self.focus_tab);
        }
    },

    save_buffer: struct {
        handle: core.BufferHandle,
        options: core.SaveOptions,

        pub fn copy(args: anytype) @This() {
            return .{ .handle = args.handle, .options = args.options };
        }

        pub fn call(self: @This(), state: *core.State) !void {
            try saveBuffer(state, self.handle, self.options);
        }
    },

    kill_buffer: struct {
        handle: core.BufferHandle,
        options: core.KillOptions,

        pub fn copy(args: anytype) @This() {
            return .{ .handle = args.handle, .options = args.options };
        }

        pub fn call(self: @This(), state: *core.State) !Events {
            return try killBuffer(state, self.handle, self.options);
        }
    },

    kill_window: struct {
        window: core.BufferWindow.ID,

        pub fn copy(args: anytype) @This() {
            return .{ .window = args.window };
        }

        pub fn call(self: @This(), state: *core.State) Events {
            return killWindow(state, self.window);
        }
    },

    set_buffer_selection: struct {
        handle: core.BufferHandle,
        kind: core.SetSelectionType,

        pub fn copy(args: anytype) @This() {
            return .{ .handle = args.handle, .kind = args.kind };
        }

        pub fn call(self: @This(), state: *core.State) !void {
            try setBufferSelection(state, self.handle, self.kind);
        }
    },

    modify_history: struct {
        handle: core.BufferHandle,
        operation: core.HistoryOperation,

        pub fn copy(args: anytype) @This() {
            return .{ .handle = args.handle, .operation = args.operation };
        }

        pub fn call(self: @This(), state: *core.State) !Events {
            return try modifyHistory(state, self.handle, self.operation);
        }
    },

    scroll_window: struct {
        window: core.BufferWindow.ID,
        scroll_by: u32,
        dir: core.ScrollDir,

        pub fn copy(args: anytype) @This() {
            return .{
                .window = args.window,
                .scroll_by = args.scroll_by,
                .dir = args.dir,
            };
        }

        pub fn call(self: @This(), state: *core.State) Events {
            return scrollWindow(state, self.window, self.dir, self.scroll_by);
        }
    },

    pub fn call(modify: Modify, state: *core.State) !Events {
        // std.debug.print("Defer call {s}: {any}\n", .{ @tagName(modify), modify });
        switch (modify) {
            .replace_bytes => |args| return try args.call(state),
            .jump_list_jump => |args| return try args.call(state),
            .create_buffer => |args| return try args.call(state),
            .kill_buffer => |args| return try args.call(state),
            .set_focused_window => |args| return try args.call(state),
            .modify_history => |args| return try args.call(state),

            .kill_window => |args| return args.call(state),
            .set_cursor => |args| return args.call(state),
            .scroll_window => |args| return args.call(state),

            .create_window => |args| try args.call(state),
            .set_buffer_type => |args| try args.call(state),
            .set_buffer_path => |args| try args.call(state),
            .create_tab_with_window => |args| try args.call(state),
            .open_window => |args| try args.call(state),
            .push_jump_point => |args| try args.call(state),
            .save_buffer => |args| try args.call(state),
            .set_buffer_selection => |args| try args.call(state),

            .set_buffer_handle => |args| args.call(state),
        }

        return Events{};
    }

    pub fn modifiesBufferContent(modify: Modify) bool {
        return switch (modify) {
            .replace_bytes => true,
            .modify_history => |args| switch (args.operation) {
                .undo, .redo => true,
                .push_snapshot => false,
            },
            else => false,
        };
    }
};

pub fn canReplaceBytes(state: *core.State, handle: core.BufferHandle, string: []const u8) !void {
    var buff = try getBuffer(state, handle);
    try buff.canReplace(string);
}

pub fn canReplaceBytesWithRange(state: *core.State, handle: core.BufferHandle, index: usize, delete_len: usize, string: []const u8) !void {
    var buff = try getBuffer(state, handle);
    try buff.canReplaceWithRange(index, delete_len, string);
}

pub fn replaceBytes(state: *core.State, handle: core.BufferHandle, start_point: core.Buffer.Point, end_point: core.Buffer.Point, string: []const u8) !Events {
    const max = std.math.maxInt(u32);
    std.debug.assert(start_point.row != max and start_point.col != max);
    std.debug.assert(end_point.row != max and end_point.col != max);

    var buff = try getBuffer(state, handle);

    const index = buff.getIndex(start_point);
    const bytes_to_delete = if (!std.meta.eql(start_point, end_point))
        buff.getIndex(end_point) - index
    else
        0;

    try canReplaceBytesWithRange(state, handle, index, bytes_to_delete, string);

    const lines_before: i32 = @intCast(buff.lineCount());

    try buff.replace(index, bytes_to_delete, string);

    const lines_after: i32 = @intCast(buff.lineCount());

    const line_count_diff: i32 = lines_after - lines_before;
    const change = core.Buffer.Change{
        .start_index = index,
        .start_point = start_point,
        .delete_len = bytes_to_delete,
        .inserted_len = string.len,
        .delete_end_point = end_point,
    };

    {
        state.marks.updateMarks(.{ .file_path = buff.path.items }, change, line_count_diff);
        state.marks.updateMarks(.{ .bhandle = handle }, change, line_count_diff);
    }
    try state.search_keeper.updateResultsFor(handle);

    return Events.create(&.{.{ .after_change = .{ .bhandle = handle, .change = change } }});
}

pub fn setCursor(state: *core.State, window: core.BufferWindow.ID, point: core.Buffer.Point) Events {
    const cursor_mark = state.buffer_windows.getBy(window).?.cursor_mark;
    const old = core.buffer_window.cursor(window).?;
    const bhandle = core.buffer_window.bufferHandle(window).?;

    const max_cols = core.buffer.codePointCountAtLine(bhandle, point.row) catch return Events{};
    const buff_data = core.buffer.getBufferData(bhandle) catch return Events{};
    const new_point = core.Buffer.Point{
        .row = @min(buff_data.line_count -| 1, point.row),
        .col = @min(max_cols - 1, point.col),
    };

    setMark(state, cursor_mark, new_point);

    return Events.create(&.{.{
        .after_cursor_move = .{
            .buffer_window = window,
            .old_point = old,
            .new_point = new_point,
        },
    }});
}

pub fn setMark(state: *core.State, mark_key: core.Marks.MarkKey, point: core.Buffer.Point) void {
    state.marks.setMark(mark_key, point);
}

pub fn putMark(state: *core.State, association: core.Marks.Association, point: core.Buffer.Point) !core.Marks.MarkKey {
    return state.marks.putMark(association, point);
}

pub fn removeMark(state: *core.State, mark_key: core.Marks.MarkKey) void {
    state.marks.removeMark(mark_key);
}

pub fn createBuffer(state: *core.State, path: []const u8, handle: core.BufferHandle) !Events {
    std.debug.assert(std.fs.path.isAbsolute(path) or path.len == 0);
    std.debug.assert(state.buffers.getPtrBy(handle) == null); // the buffer must be created first

    try state.buffers.growCapacity(1);
    if (path.len > 0) {
        try state.path_bt_storage.ensureUnusedCapacity(2);
    }

    var buffer = if (path.len > 0)
        try file_io.bufferFromFile(state.allocator, handle, path)
    else
        try core.Buffer.init(state.allocator, handle, "", "");
    errdefer buffer.deinit();

    if (state.data_dir_path) |dir_path| if (buffer.path.items.len > 0) {
        try file_io.opened_buffers.addPathToFileInDir(state.allocator, dir_path, buffer.path.items);
    };

    if (buffer.path.items.len > 0) {
        state.path_bt_storage.put(buffer.path.items) catch unreachable;
        state.path_bt_storage.put(buffer.buffer_type.items) catch unreachable;
    }

    state.buffers.add(buffer) catch unreachable;

    log.info("Created buffer [{?}] {s}", .{ buffer.handle, buffer.path.items });

    return Events.create(&.{
        .{ .buffer_created = .{ .bhandle = buffer.handle } },
    });
}

pub fn createBufferWindow(state: *core.State, bhandle: core.BufferHandle, window: core.BufferWindow.ID) !void {
    std.debug.assert(state.buffers.getPtrBy(bhandle) != null); // The buffer must be created first
    std.debug.assert(state.buffer_windows.getPtrBy(window) == null); // The window must not exist

    try state.buffer_windows.growCapacity(1);
    const mark = putMark(state, .{ .bhandle = bhandle }, .{}) catch |err| switch (err) {
        error.FilePathNotAbsolute => unreachable,
        else => |e| return e,
    };
    const bw = core.BufferWindow.init(
        state.allocator,
        window,
        bhandle,
        0,
        mark,
    );

    state.buffer_windows.add(bw) catch unreachable;
}

pub fn setBufferType(state: *core.State, bhandle: core.BufferHandle, new_bt: []const u8) !void {
    const bt = try state.path_bt_storage.getAndPut(new_bt);

    var buff = try getBuffer(state, bhandle);
    try buff.buffer_type.ensureTotalCapacityPrecise(buff.allocator, bt.len);
    buff.buffer_type.clearRetainingCapacity();
    buff.buffer_type.appendSliceAssumeCapacity(bt);
}

pub fn canSetBufferPathAbsolute(state: *core.State, new_path: []const u8) !void {
    std.debug.assert(std.fs.path.isAbsolute(new_path));

    try state.path_bt_storage.ensureUnusedCapacity(1);

    const is_dir = file_io.isDir(new_path) catch |err| switch (err) {
        error.FileNotFound => false,
        else => return err,
    };

    if (is_dir) return error.BufferCannotBeDir;

    if ((try core.buffer.bufferWithPathExistsAbsolute(new_path)).b())
        return error.DuplicateFilePath;
}

pub fn setBufferPathAbsolute(state: *core.State, handle: core.BufferHandle, new_path: []const u8) !void {
    try canSetBufferPathAbsolute(state, new_path);

    var buff = try getBuffer(state, handle);

    const old_path = try state.allocator.dupe(u8, buff.path.items);
    defer state.allocator.free(old_path);

    try buff.path.ensureTotalCapacity(state.allocator, new_path.len);
    buff.path.clearRetainingCapacity();
    buff.path.appendSliceAssumeCapacity(new_path);

    _ = state.path_bt_storage.put(buff.path.items) catch unreachable;

    log.info("Changed Path for buffer [{?}] {s} => {s}", .{ buff.handle, old_path, buff.path.items });
}

pub fn setBufferHandle(state: *core.State, window: core.BufferWindow.ID, new_bhandle: core.BufferHandle) void {
    const bw = state.buffer_windows.getPtrBy(window) orelse return;

    if (bw.bhandle.handle == new_bhandle.handle) return;

    state.marks.removeMark(bw.cursor_mark);
    bw.cursor_mark = putMark(state, .{ .bhandle = new_bhandle }, .{}) catch |err|
        switch (err) {
        error.OutOfMemory, error.FilePathNotAbsolute => unreachable,
    };

    bw.bhandle = new_bhandle;
}

pub fn createTabWithWindow(state: *core.State, window: core.BufferWindow.ID) !void {
    std.debug.assert(core.tabs.tabOfWindow(window) == null);

    const index = state.tabs.items.len;
    const tab = try internal.Tab.init(state.allocator, window);
    try state.tabs.append(tab);
    state.tabs_view.adjustView(state.tabs.items.len);
    if (state.tabs_view.selected_index == null)
        state.tabs_view.selected_index = index;
}

pub fn splitWindow(state: *core.State, tab_index: usize, other_window: core.BufferWindow.ID, new_window: core.BufferWindow.ID, opts: core.WindowSplitOptions) !void {
    if (internal.tabOfWindow(new_window)) |old_tab| { // effectively moves the window
        old_tab.tab.close(new_window);
    }

    const tab = getTabOrLast(state, tab_index);
    _ = try tab.split(other_window, new_window, opts.size, opts.dir);
}

pub fn floatWindow(state: *core.State, tab_index: usize, window: core.BufferWindow.ID, rect: core.draw.Rect) !void {
    if (internal.tabOfWindow(window)) |old_tab| { // effectively moves the window
        old_tab.tab.close(window);
    }

    const tab = getTabOrLast(state, tab_index);
    try tab.addFloating(window, rect);
}

pub fn pushJumpPoint(state: *core.State, window: core.BufferWindow.ID, path: []const u8, point: core.Buffer.Point) !void {
    var bw = state.buffer_windows.getPtrBy(window) orelse return;

    if (!std.fs.path.isAbsolute(path)) return;
    var jl = &bw.jump_list;

    if (jl.list.getLastOrNull()) |last| {
        const last_point = core.marks.get(last.mark).?;
        if (std.mem.eql(u8, last.file_path, path) and
            std.meta.eql(point, last_point))
        {
            return;
        }
    }

    const mk = putMark(state, .{ .file_path = path }, point) catch |err| switch (err) {
        error.FilePathNotAbsolute => unreachable,
        error.OutOfMemory => |e| return e,
    };
    try jl.push(path, mk);
    jl.moveLast();
}

pub fn jumpListJump(state: *core.State, window: core.BufferWindow.ID, kind: enum { next, prev }) !Events {
    var bw = state.buffer_windows.getPtrBy(window) orelse return Events{};

    switch (kind) {
        .next => bw.jump_list.moveNext(),
        .prev => bw.jump_list.movePrev(),
    }

    const pos = bw.jump_list.getPosition() orelse return Events{};
    if (core.buffer.getHandleByPathAbsolute(pos.file_path)) |h| {
        _ = h;

        // FIXME: Execute this function but instantly
        // if (core.focused.handle() != h) _ = try core.openByHandle(h, .{});

        return setCursor(state, window, core.marks.get(pos.mark) orelse return Events{});
    } else {
        // FIXME: Execute this function but instantly
        // _ = try core.openByPath(pos.file_path, .{});
        unreachable;
    }

    return Events{};
}

pub fn setFocusedWindow(state: *core.State, tab_index: usize, window: core.BufferWindow.ID, focus_tab: bool) !Events {
    if (getTabOrLastIndex(state, tab_index) != tab_index) return Events{};
    const tab = &state.tabs.items[tab_index];

    if (focus_tab) {
        state.tabs_view.selected_index = tab_index;
    }

    if (tab.hasWindow(window) == null) return Events{};
    if (tab.focused_window.id == window.id) return Events{};
    tab.focused_window = window;

    const handle = core.buffer_window.bufferHandle(window).?;
    const data = try core.buffer.getBufferData(handle);
    log.debug("Tab {}: Currently focused buffer [{?}] {s} Window id [{}]", .{ tab_index, data.handle, data.path, window });

    return Events.create(&.{
        .{ .buffer_window_focused = .{ .buffer_window = window, .bhandle = data.handle } },
    });
}

pub fn canSaveBuffer(state: *core.State, handle: core.BufferHandle, options: core.SaveOptions) !void {
    _ = options;

    const buff = try getBuffer(state, handle);
    if (buff.path.items.len == 0)
        return error.SavingPathlessBuffer;
}

pub fn saveBuffer(state: *core.State, handle: core.BufferHandle, options: core.SaveOptions) !void {
    try canSaveBuffer(state, handle, options);

    var buff = try getBuffer(state, handle);

    try file_io.writeToFile(buff, buff.path.items, options.force_save);
    buff.dirty = false;

    log.info("Saved buffer [{?}] {s}", .{ buff.handle, buff.path.items });
}

pub fn canKillBuffer(state: *core.State, handle: core.BufferHandle, options: core.KillOptions) !void {
    const buff = state.buffers.getPtrBy(handle) orelse return;
    if (!options.force_kill and buff.dirty)
        return error.KillingDirtyBuffer;
}

pub fn killBuffer(state: *core.State, handle: core.BufferHandle, options: core.KillOptions) !Events {
    try canKillBuffer(state, handle, options);

    var buff = state.buffers.getPtrBy(handle) orelse return Events{};

    const windows_to_kill = try internal.getWindowsWithBuffer(state.allocator, handle);
    defer state.allocator.free(windows_to_kill);

    if (core.dataDirPath()) |data_path| if (buff.path.items.len > 0) {
        try file_io.opened_buffers.removePathToFileInDir(state.allocator, data_path, buff.path.items);
    };

    log.info("Killed buffer [{?}] {s}", .{ buff.handle, buff.path.items });
    buff.deinit();
    state.buffers.deleteBy(handle);

    var events = Events.create(&.{
        .{ .before_killing_buffer = .{ .bhandle = handle } },
    });

    for (windows_to_kill) |window| {
        const e = killWindow(state, window);
        events = Events.concat(&.{ events, e });
    }

    return events;
}

pub fn killWindow(state: *core.State, window_to_remove: core.BufferWindow.ID) Events {
    var bw = state.buffer_windows.getPtrBy(window_to_remove) orelse return Events{};
    removeCustomDrawing(state, &.{window_to_remove});
    removeMark(state, bw.cursor_mark);
    bw.deinit();
    state.buffer_windows.deleteBy(window_to_remove);

    log.info("Killed buffer window [{}]", .{window_to_remove});

    // tab handling

    if (core.internal.tabOfWindow(window_to_remove)) |result| {
        result.tab.close(window_to_remove);
    }

    const selected_tab = state.tabs_view.selected_index.?;
    const tab = &state.tabs.items[selected_tab];
    if (tab.emptyPanes()) {
        tab.deinit();
        _ = state.tabs.orderedRemove(selected_tab);
        state.tabs_view.selectNext(state.tabs.items.len, .stop);
    }

    // TODO: Instead of closing a tab then creating a new one, just reuse the closed one
    if (state.tabs.items.len == 0) {
        var create_buffer_event = Events{};
        const handle = core.internal.getPathlessTypelessBuffer() orelse blk: {
            const h = core.BufferHandle{ .handle = state.nextUniqueId() };
            create_buffer_event = createBuffer(state, "", h) catch |err| switch (err) {
                // If there are no buffers and we're creating a pathless buffer then
                // there's no need to allocate memory, but
                // the Buffer object will allocate one byte so it may fail.
                // FIXME: Make the Buffer object not allocate any memory
                error.OutOfMemory => unreachable,
                // file system errors are unreachable when creating a pathless buffer
                else => unreachable,
            };
            break :blk h;
        };

        const window = core.BufferWindow.ID{ .id = state.nextUniqueId() };
        createBufferWindow(state, handle, window) catch |err| switch (err) {
            // If there are no window then there's no need to allocate memory.
            error.OutOfMemory => unreachable,
        };

        createTabWithWindow(state, window) catch |err| switch (err) {
            // FIXME: This IS reachable, so try to reuse the closed tabs memory
            error.OutOfMemory => unreachable,
        };

        return create_buffer_event;
    }

    return Events{};
}

pub fn setBufferSelection(state: *core.State, handle: core.BufferHandle, kind: core.SetSelectionType) !void {
    var buff = try getBuffer(state, handle);
    switch (kind) {
        .reset => buff.selection.reset(),
        .set_kind => |k| buff.selection.kind = k,
        .set_anchor => |point| buff.selection.anchor = point,
    }
}

pub fn canModifyHistory(state: *core.State, handle: core.BufferHandle, operation: core.HistoryOperation) !void {
    _ = operation;

    _ = try getBuffer(state, handle);
    try state.histories.ensureUnusedCapacity(1);
}

pub fn modifyHistory(state: *core.State, handle: core.BufferHandle, operation: core.HistoryOperation) !Events {
    try canModifyHistory(state, handle, operation);

    const buff = getBuffer(state, handle) catch unreachable;
    var stack = blk: {
        const gop = state.histories.getOrPut(handle) catch undefined;
        if (!gop.found_existing) {
            gop.value_ptr.* = .{ .allocator = state.allocator };
        }
        break :blk gop.value_ptr;
    };

    const size = buff.size();
    const lc = buff.lineCount();

    switch (operation) {
        .push_snapshot => {
            stack.pushHistory(buff, &state.marks) catch |err| switch (err) {
                error.OutOfMemory => |e| return e,
                error.DuplicateHistroy => return Events{}, // no point in returning this error
            };
        },
        .undo => {
            stack.undo(buff, &state.marks) catch |err| switch (err) {
                error.OutOfMemory => return err,
            };
            try buff.insureLastByteIsNewline();
        },
        .redo => {
            stack.redo(buff, &state.marks) catch |err| switch (err) {
                error.OutOfMemory => return err,
            };
            try buff.insureLastByteIsNewline();
        },
    }

    return if (operation == .push_snapshot)
        Events{}
    else
        Events.create(&.{.{ .after_change = .{ .bhandle = handle, .change = .{
            .delete_len = size,
            .inserted_len = buff.size(),
            .start_point = .{},
            .start_index = 0,
            .delete_end_point = .{ .row = lc },
        } } }});
}

pub fn scrollWindow(state: *core.State, window: core.BufferWindow.ID, dir: core.ScrollDir, scroll_by: u32) Events {
    var bw = state.buffer_windows.getPtrBy(window) orelse return Events{};
    const data = core.buffer.getBufferData(bw.bhandle) catch return Events{};
    const cursor = core.buffer_window.cursor(window).?;

    const new_point = switch (dir) {
        .up => bw.scrollUp(cursor, scroll_by, data.line_count),
        .down => bw.scrollDown(cursor, scroll_by, data.line_count),
    };

    if (!std.meta.eql(new_point, cursor)) {
        return setCursor(state, window, new_point);
    }

    return Events{};
}

pub fn addCustomDrawing(state: *core.State, windows: []const core.BufferWindow.ID, ptr: *anyopaque, drawFn: *const internal.CustomDrawing.DrawFn) !void {
    try state.custom_drawing.ensureUnusedCapacity(1);
    const wins = try state.allocator.dupe(core.BufferWindow.ID, windows);
    state.custom_drawing.appendAssumeCapacity(.{
        .windows = wins,
        .ptr = ptr,
        .draw = drawFn,
    });
}

pub fn removeCustomDrawing(state: *core.State, windows: []const core.BufferWindow.ID) void {
    for (state.custom_drawing.items, 0..) |item, i| {
        for (windows) |win| {
            if (utils.anyEql(core.BufferWindow.ID, item.windows, win)) {
                const removed = state.custom_drawing.swapRemove(i);
                state.allocator.free(removed.windows);
                return;
            }
        }
    }
}

// Helpers
fn getBuffer(state: *core.State, handle: core.BufferHandle) !*core.Buffer {
    return state.buffers.getPtrBy(handle) orelse error.BufferDoesNotExist;
}

fn getTabOrLast(state: *core.State, index: usize) *internal.Tab {
    return &state.tabs.items[getTabOrLastIndex(state, index)];
}

fn getTabOrLastIndex(state: *core.State, index: usize) usize {
    return if (index >= state.tabs.items.len)
        state.tabs.items.len - 1
    else
        index;
}

fn getOrPutBufferHistory(state: *core.State, handle: core.BufferHandle) !*core.HistoryStack {
    const gop = try state.histories.getOrPut(handle);

    if (!gop.found_existing) {
        gop.value_ptr.* = .{ .allocator = state.allocator };
    }

    return gop.value_ptr;
}
