const std = @import("std");
const builtin = @import("builtin");
const print = std.debug.print;
const time = std.time;
const ArrayList = std.ArrayList;

const core = @import("core");

const command_line = core.command_line;
const State = core.State;

const options = @import("thabit_options");

const sdl = @import("ui/sdl2.zig");
const opengl = @import("ui/opengl.zig");

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    try sdl.init();
    defer sdl.deinit();

    try opengl.init(gpa.allocator());
    defer opengl.deinit();

    try core.internal.initThabit(allocator);
    defer core.internal.deinitThabit();

    var arena_instance = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena_instance.deinit();
    const arena = arena_instance.allocator();

    while (core.internal.state().run_editor) {
        defer {
            _ = arena_instance.reset(.retain_capacity);
        }

        const next_callback_time = core.internal.nextTimedCallbackTime();
        if (core.internal.state().extra_frames > 0) {
            sdl.poll();
        } else if (core.internal.state().notifications.count() > 0 or next_callback_time != null) {
            const ms = @min(
                next_callback_time orelse std.math.maxInt(usize),
                std.time.ms_per_s * 1,
            );
            sdl.waitEventTimeout(ms);
            core.extraFrames(.two);
        } else {
            sdl.waitEvent();
            core.extraFrames(.two);
        }

        try core.internal.eventLoopTick(&sdl.key_queue, &sdl.char_queue);

        try opengl.render(arena);
    }
}
