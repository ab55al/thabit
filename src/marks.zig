const std = @import("std");

const Buffer = @import("buffer.zig");
const BufferHandle = @import("core.zig").BufferHandle;
const Point = Buffer.Point;

const StringStorageUnmanaged = @import("slice_storage.zig").StringStorageUnmanaged;

pub const Marks = @This();

const Key = u32;
pub const MarkKey = struct {
    key: Key,
    association: Association,
};

pub const KeyPoint = struct {
    key: Key,
    point: Point,
};

pub const BufferMarks = std.ArrayListUnmanaged(KeyPoint);

pub const Association = union(enum) {
    file_path: []const u8,
    bhandle: BufferHandle,
};

file_path_marks: std.StringArrayHashMapUnmanaged(BufferMarks) = .{},
bhandle_marks: std.AutoArrayHashMapUnmanaged(BufferHandle, BufferMarks) = .{},

string_storage: StringStorageUnmanaged = .{},
allocator: std.mem.Allocator,
_next_mark_key: u32 = 0,

pub fn init(allocator: std.mem.Allocator) Marks {
    return .{ .allocator = allocator };
}

pub fn deinit(self: *Marks) void {
    for (self.file_path_marks.values()) |*marks| {
        marks.*.deinit(self.allocator);
    }
    self.file_path_marks.deinit(self.allocator);
    self.string_storage.deinit(self.allocator);

    for (self.bhandle_marks.values()) |*marks| {
        marks.*.deinit(self.allocator);
    }

    self.bhandle_marks.deinit(self.allocator);
}

pub fn putMark(self: *Marks, association: Association, point: Point) !MarkKey {
    const as = switch (association) {
        .file_path => |fp| Association{ .file_path = try self.string_storage.getAndPut(self.allocator, fp) },
        .bhandle => |bh| Association{ .bhandle = bh },
    };

    var marks = switch (as) {
        .file_path => |fp| try self.getAndPutBufferMarksFP(fp),
        .bhandle => |bh| try self.getAndPutBufferMarksBH(bh),
    };

    const key = self.nextMarkKey();
    try marks.append(self.allocator, .{ .key = key, .point = point });

    return .{ .key = key, .association = as };
}

pub fn getMark(self: *Marks, mark_key: MarkKey) ?Point {
    const bmarks = self.getBufferMarks(mark_key.association) orelse return null;
    const index = bufferMarksGet(bmarks, mark_key.key) orelse return null;
    return bmarks.items[index].point;
}

pub fn setMark(self: *Marks, mark_key: MarkKey, point: Point) void {
    var marks = self.getBufferMarks(mark_key.association) orelse return;
    const index = bufferMarksGet(marks, mark_key.key) orelse return;
    marks.items[index].point = point;
}

pub fn removeMark(self: *Marks, mark_key: MarkKey) void {
    var marks = self.getBufferMarks(mark_key.association) orelse return;
    const index = bufferMarksGet(marks, mark_key.key) orelse return;
    _ = marks.swapRemove(index);
}

pub fn updateMarks(self: *Marks, association: Association, change: Buffer.Change, line_count_diff: i32) void {
    const marks = self.getBufferMarks(association) orelse return;
    const abs_line_diff: u32 = @abs(line_count_diff);

    if (line_count_diff > 0) {
        for (marks.items) |*mark| {
            if (change.start_point.row >= mark.point.row) continue; // no need to update
            mark.point.row +|= abs_line_diff;
        }
    } else if (line_count_diff < 0) {
        for (marks.items) |*mark| {
            if (change.start_point.row >= mark.point.row) continue; // no need to update
            mark.point.row -|= abs_line_diff;
        }
    }
}

pub fn updateFromSlice(self: *Marks, association: Association, new_marks: []const KeyPoint) !void {
    var marks = self.getBufferMarks(association) orelse return;

    try marks.ensureTotalCapacity(self.allocator, new_marks.len);

    for (new_marks) |kp| {
        const index = bufferMarksGet(marks, kp.key) orelse continue;
        marks.items[index].point = kp.point;
    }
}

pub fn ensureTotalCapacity(self: *Marks, association: Association, size: usize) !void {
    var marks = self.getBufferMarks(association) orelse return;

    try marks.ensureTotalCapacity(self.allocator, size);
}

pub fn toSlice(self: *Marks, allocator: std.mem.Allocator, association: Association) ![]KeyPoint {
    const marks = self.getBufferMarks(association) orelse return &.{};

    const res = try allocator.alloc(KeyPoint, marks.items.len);
    std.mem.copyForwards(KeyPoint, res, marks.items);
    return res;
}

pub fn getBufferMarks(self: *Marks, association: Association) ?*BufferMarks {
    return switch (association) {
        .file_path => |fp| self.file_path_marks.getPtr(fp),
        .bhandle => |bh| self.bhandle_marks.getPtr(bh),
    };
}

fn bufferMarksGet(buffer_marks: *BufferMarks, key: Key) ?usize {
    for (buffer_marks.items, 0..) |kp, i| {
        if (kp.key == key)
            return i;
    }

    return null;
}

fn getAndPutBufferMarksFP(self: *Marks, file_path: []const u8) !*BufferMarks {
    if (!std.fs.path.isAbsolute(file_path)) return error.FilePathNotAbsolute;

    const ft = try self.string_storage.getAndPut(self.allocator, file_path);
    const gop = try self.file_path_marks.getOrPut(self.allocator, ft);
    errdefer _ = self.file_path_marks.swapRemove(ft);

    if (!gop.found_existing) {
        gop.value_ptr.* = .{};
    }

    return gop.value_ptr;
}

fn getAndPutBufferMarksBH(self: *Marks, bhandle: BufferHandle) !*BufferMarks {
    const gop = try self.bhandle_marks.getOrPut(self.allocator, bhandle);
    errdefer _ = self.bhandle_marks.swapRemove(bhandle);

    if (!gop.found_existing) {
        gop.value_ptr.* = .{};
    }

    return gop.value_ptr;
}

fn nextMarkKey(self: *Marks) Key {
    const key = self._next_mark_key;
    self._next_mark_key += 1;
    return key;
}
