const std = @import("std");
const core = @import("core.zig");

pub const Config = struct {
    window_line_number: enum { relative, absolute, none } = .relative,
    cursor: struct {
        shape: enum { thin, thick, underline } = .thick,
        color: core.draw.Color = core.draw.Color.white,
    } = .{},
    /// Used only for the search command
    defualt_search_source: core.SearchResults.Source = .rip_grep,
};

pub const max_word_delimiters = 64;
pub const WordDelimiters = std.BoundedArray(u21, max_word_delimiters);
pub fn wordDelimitersInit(delims: []const u21) WordDelimiters {
    const len = @min(delims.len, max_word_delimiters);
    const slice = delims[0..len];
    return WordDelimiters.fromSlice(slice) catch unreachable;
}
pub const BufferTypeConfig = struct {
    word_delimiters: WordDelimiters = WordDelimiters.init(0) catch unreachable,
    tab_width: u8 = 4,
    expand_tab: bool = false,
};

pub const default_config_map = std.ComptimeStringMap(BufferTypeConfig, .{
    .{ "zig", zig },
    .{ "zon", zig },

    .{ "c", c },
    .{ "h", c },
});

pub const symbols = [_]u21{
    '(', ')', '[', ']', '{', '}',  '<', '>',
    '=', '-', ';', '.', ',', '\\', '/',
};

pub const zig = BufferTypeConfig{
    .word_delimiters = wordDelimitersInit(&symbols),
    .tab_width = 4,
    .expand_tab = true,
};

pub const c = BufferTypeConfig{
    .word_delimiters = wordDelimitersInit(&symbols),
    .tab_width = 4,
    .expand_tab = false,
};
