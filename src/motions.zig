const std = @import("std");
const builtin = @import("builtin");
const print = std.debug.print;
const unicode = std.unicode;
const ascii = std.ascii;

const indexOf = std.mem.indexOf;

const core = @import("core.zig");

const BufferWindow = @import("buffer_window.zig").BufferWindow;
const Buffer = @import("buffer.zig");
const BufferIterator = Buffer.BufferIterator;
const Point = Buffer.Point;
const PointRange = Buffer.PointRange;

const BufferHandle = core.BufferHandle;

const utils = @import("utils.zig");
const utf8 = @import("utf8.zig");

/// `stop_eol` is short for `Stop at End of Line`
pub const MotionKind = union(enum) {
    const WordOptions = struct { stop_eol: bool = false };
    full_word: WordOptions,
    word: WordOptions,
    full_word_end: WordOptions,
    word_end: WordOptions,

    next_col,
    next_row,
    end_of_row,
    bottom_of_buffer,
    first_non_whitespace,

    full_back_word,
    back_word_end,
    full_back_word_end,
    back_word,
    prev_col,
    prev_row,
    start_of_row,
    top_of_buffer,

    find_cp: u21,
    till_cp: u21,

    back_find_cp: u21,
    back_till_cp: u21,

    pub fn getPoint(motion: MotionKind, allocator: std.mem.Allocator, handle: BufferHandle, point: Point, delimiters: []const u21) ?Point {
        return switch (motion) {
            .full_word => |opts| word(allocator, handle, point, .{ .stop_eol = opts.stop_eol }),
            .word => |opts| word(allocator, handle, point, .{ .delimiters = delimiters, .stop_eol = opts.stop_eol }),
            .full_word_end => |opts| wordEnd(allocator, handle, point, .{ .stop_eol = opts.stop_eol }),
            .word_end => |opts| wordEnd(allocator, handle, point, .{ .delimiters = delimiters, .stop_eol = opts.stop_eol }),

            .next_col => point.addCol(1),

            .first_non_whitespace => firstNonWhiteSpace(allocator, handle, point),
            .next_row => nextRow(point),
            .end_of_row => endOfRow(handle, point),
            .bottom_of_buffer => bottomOfBuffer(handle, point),

            .find_cp => |cp| find(allocator, handle, point, cp, 1),
            .till_cp => |cp| till(allocator, handle, point, cp, 1),

            // //
            // //
            // //

            .back_word_end => backWordEnd(allocator, handle, point, .{ .delimiters = delimiters }),
            .full_back_word_end => backWordEnd(allocator, handle, point, .{}),
            .back_word => backWord(allocator, handle, point, .{ .delimiters = delimiters }),
            .full_back_word => backWord(allocator, handle, point, .{}),
            .prev_col => point.subCol(1),

            .prev_row => prevRow(point),

            .start_of_row => startOfRow(point),
            .top_of_buffer => topOfBuffer(),

            .back_find_cp => |cp| backFind(allocator, handle, point, cp, 1),
            .back_till_cp => |cp| backTill(allocator, handle, point, cp, 1),
        };
    }

    pub fn getPointRepeat(motion: MotionKind, allocator: std.mem.Allocator, handle: BufferHandle, point: Point, delimiters: []const u21, repeat: usize) ?Point {
        var p = point;
        for (0..repeat) |_| {
            p = motion.getPoint(allocator, handle, p, delimiters) orelse break;
        }

        return p;
    }

    pub fn isForward(kind: MotionKind) bool {
        return switch (kind) {
            .first_non_whitespace,
            .full_word_end,
            .full_word,
            .word,
            .word_end,
            .next_col,
            .next_row,
            .end_of_row,
            .bottom_of_buffer,
            .find_cp,
            .till_cp,
            => true,

            .full_back_word,
            .back_word_end,
            .full_back_word_end,
            .back_word,
            .prev_col,
            .prev_row,
            .start_of_row,
            .top_of_buffer,
            .back_find_cp,
            .back_till_cp,
            => false,
        };
    }
};

pub const WordArgs = struct { delimiters: []const u21 = &.{}, stop_eol: bool = false };
pub fn word(allocator: std.mem.Allocator, handle: BufferHandle, point: Point, args: WordArgs) ?Point {
    var iter = core.buffer.LineIterator.init(allocator, handle, point, .{ .row = point.row +| 2 }) catch return null;
    defer iter.deinit();
    const first_line = (iter.next() catch null) orelse return null;
    if (wordString(first_line.line, args.delimiters)) |col|
        return .{ .row = first_line.number, .col = col +| point.col };

    if (args.stop_eol) return .{
        .row = first_line.number,
        .col = core.buffer.lastColAtRow(handle, first_line.number) catch return null,
    };

    if (iter.next() catch null) |second_line| {
        if (firstNonWhiteSpaceString(second_line.line)) |col|
            return .{ .row = second_line.number, .col = col };

        if (second_line.empty())
            return .{ .row = second_line.number };
    } else {
        const last_col = core.buffer.lastColAtRow(handle, point.row +| 1) catch return null;
        return .{ .row = point.row +| 1, .col = last_col };
    }

    return null;
}

pub fn wordEnd(allocator: std.mem.Allocator, handle: BufferHandle, point: Point, args: WordArgs) ?Point {
    var iter = core.buffer.LineIterator.init(allocator, handle, point, .{ .row = std.math.maxInt(u32) }) catch return null;
    defer iter.deinit();

    while (iter.next() catch null) |result| {
        const offset = if (result.number == point.row) point.col else 0;

        if (wordEndString(result.line, args.delimiters)) |col| {
            return .{ .row = result.number, .col = col +| offset };
        }

        if (args.stop_eol) return .{
            .row = result.number,
            .col = core.buffer.lastColAtRow(handle, result.number) catch return null,
        };
    }

    return null;
}

pub fn backWord(allocator: std.mem.Allocator, handle: BufferHandle, point: Point, args: WordArgs) ?Point {
    var iter = core.buffer.ReverseLineIterator.init(allocator, handle, .{}, point.addCol(1)) catch return null;
    defer iter.deinit();

    const d = core.buffer.getBufferData(handle) catch return null;
    if (iter.start >= d.size -| 1) return null;

    while (iter.next() catch null) |result| {
        if (backWordString(result.line, args.delimiters)) |col|
            return .{ .row = result.number, .col = col };

        if (args.stop_eol)
            return .{ .row = result.number };
    }

    return null;
}

pub fn backWordEnd(allocator: std.mem.Allocator, handle: BufferHandle, point: Point, args: WordArgs) ?Point {
    var iter = core.buffer.ReverseLineIterator.init(allocator, handle, .{}, point.addCol(1)) catch return null;
    defer iter.deinit();

    const d = core.buffer.getBufferData(handle) catch return null;
    if (iter.start >= d.size -| 1) return null;

    while (iter.next() catch null) |result| {
        if (backWordEndString(result.line, args.delimiters)) |col|
            return .{ .row = result.number, .col = col };

        if (args.stop_eol)
            return .{ .row = result.number };
    }

    return null;
}

pub fn find(allocator: std.mem.Allocator, handle: BufferHandle, point: Point, cp_to_find: u21, max_lines: u32) ?Point {
    const first_cp: ?u21 = core.buffer.getCodepointAt(handle, point) catch null;
    const offset: u32 = if (first_cp == cp_to_find) 1 else 0;

    var iter = core.buffer.LineIterator.init(allocator, handle, point.addCol(offset), .{ .row = point.row +| max_lines }) catch return null;
    defer iter.deinit();

    var col = point.col +| offset;
    while (iter.next() catch null) |result| {
        defer col = 0;
        var uiter = utf8.Utf8Iterator{ .bytes = result.line };
        while (uiter.nextCodepointSlice()) |slice| {
            defer col += 1;
            const cp = unicode.utf8Decode(slice) catch continue;

            if (cp == cp_to_find)
                return .{ .row = result.number, .col = col };
        }
    }
    return null;
}

pub fn backFind(allocator: std.mem.Allocator, handle: BufferHandle, point: Point, cp_to_find: u21, max_lines: u32) ?Point {
    const first_cp: ?u21 = core.buffer.getCodepointAt(handle, point) catch null;
    const offset: u32 = if (first_cp == cp_to_find) 0 else 1;

    var iter = core.buffer.ReverseLineIterator.init(allocator, handle, .{ .row = point.row -| max_lines }, point.addCol(offset)) catch return null;
    defer iter.deinit();

    while (iter.next() catch null) |result| {
        var i: usize = result.line.len -| 1;
        var uiter = utf8.ReverseUtf8View.init(result.line);
        while (uiter.nextSlice()) |slice| {
            defer i -|= slice.len;
            const cp = unicode.utf8Decode(slice) catch continue;

            if (cp == cp_to_find) return .{
                .row = result.number,
                .col = utils.getColOfIndex(result.line, i),
            };
        }
    }
    return null;
}

pub fn till(allocator: std.mem.Allocator, handle: BufferHandle, point: Point, cp_to_find: u21, max_lines: u32) ?Point {
    const res = find(allocator, handle, point, cp_to_find, max_lines) orelse return null;
    return res.subCol(1);
}

pub fn backTill(allocator: std.mem.Allocator, handle: BufferHandle, point: Point, cp_to_find: u21, max_lines: u32) ?Point {
    const res = backFind(allocator, handle, point, cp_to_find, max_lines) orelse return null;
    return res.addCol(1);
}

// a word is:
// white-space - non-white-space
// none-delimiter - delimiter
// delimiter - none-delimiter
pub fn wordString(string: []const u8, delimiters: []const u21) ?u32 {
    var iter = utf8.Utf8Iterator{ .bytes = string };
    var col: u32 = 0;
    while (iter.nextCodepointSlice()) |slice| {
        defer col += 1;
        const cp = unicode.utf8Decode(slice) catch continue;
        const next_cp = (iter.peekCodePoint() catch continue) orelse return null;

        if (isWhiteSpace(cp) and !isWhiteSpace(next_cp))
            return col + 1;

        if (!inList(cp, delimiters) and inList(next_cp, delimiters))
            return col + 1;

        if (inList(cp, delimiters) and !inList(next_cp, delimiters))
            return col + 1;
    }

    return null;
}

// a word end is:
// non-white-space - white-space
// none-delimiter - delimiter
// delimiter - none-delimiter
// non-white-space - end-of-string
pub fn wordEndString(string: []const u8, delimiters: []const u21) ?u32 {
    var iter = utf8.Utf8Iterator{ .bytes = string };
    var col: u32 = 1;
    _ = iter.nextCodepointSlice(); // skip the first col
    while (iter.nextCodepointSlice()) |slice| {
        defer col += 1;
        const cp = unicode.utf8Decode(slice) catch continue;
        const next_cp = (iter.peekCodePoint() catch null) orelse return null;

        if (!isWhiteSpace(cp) and isWhiteSpace(next_cp))
            return col + 1;

        if (!isWhiteSpace(cp) and isEndOfRow(next_cp))
            return col + 1;

        if (!inList(cp, delimiters) and inList(next_cp, delimiters))
            return col + 1;

        if (inList(cp, delimiters) and !inList(next_cp, delimiters))
            return col + 1;
    }

    return null;
}

// a back word is:
// white-space - non-white-space
// none-delimiter - delimiter
// delimiter - none-delimiter
// start of line - non-white-space
pub fn backWordString(string: []const u8, delimiters: []const u21) ?u32 {
    var col: u32 = @truncate(utf8.countCodepoints(string) -| 1);
    var iter = utf8.ReverseUtf8View.init(string);
    _ = iter.nextSlice(); // skip the first col

    while (iter.nextSlice()) |slice| {
        col -|= 1;

        const cp = unicode.utf8Decode(slice) catch continue;
        const prev_cp = iter.peekCodePoint() catch continue;

        if (prev_cp == null and !isWhiteSpace(cp))
            return col -| 1;

        if (isWhiteSpace(prev_cp) and !isWhiteSpace(cp))
            return col;

        if ((!inList(prev_cp, delimiters) and inList(cp, delimiters)) or
            (inList(prev_cp, delimiters) and !inList(cp, delimiters)))
            return col;
    }

    return null;
}

// a back word end is:
// non-white-space - white-space
// none-delimiter - delimiter
// delimiter - none-delimiter
pub fn backWordEndString(string: []const u8, delimiters: []const u21) ?u32 {
    var col: u32 = @truncate(utf8.countCodepoints(string) -| 1);
    var iter = utf8.ReverseUtf8View.init(string);

    while (iter.nextSlice()) |slice| {
        defer col -|= 1;

        const cp = unicode.utf8Decode(slice) catch continue;
        const prev_cp = iter.peekCodePoint() catch continue;

        if (!isWhiteSpace(prev_cp) and isWhiteSpace(cp))
            return col -| 1;

        if ((!inList(prev_cp, delimiters) and inList(cp, delimiters)) or
            (inList(prev_cp, delimiters) and !inList(cp, delimiters)))
            return col -| 1;
    }

    return null;
}

pub fn firstNonWhiteSpaceString(string: []const u8) ?u32 {
    var uiter = utf8.Utf8Iterator{ .bytes = string };
    var col: u32 = 0;
    while (uiter.nextCodepointSlice()) |slice| {
        defer col += 1;
        const cp = unicode.utf8Decode(slice) catch continue;
        if (isWhiteSpace(cp)) continue else return col;
    }

    return null;
}

pub fn nextRow(point: Point) Point {
    return point.addRow(1);
}

pub fn prevRow(point: Point) Point {
    return point.subRow(1);
}

pub fn endOfRow(handle: BufferHandle, point: Point) ?Point {
    const cols = core.buffer.lastColAtRow(handle, point.row) catch return null;
    return point.setCol(cols);
}

pub fn startOfRow(point: Point) Point {
    return point.setCol(0);
}

pub fn topOfBuffer() Point {
    return .{ .row = 0, .col = 0 };
}

pub fn bottomOfBuffer(handle: BufferHandle, point: Point) Point {
    const data = core.buffer.getBufferData(handle) catch return point;
    return .{ .row = data.line_count };
}

pub fn firstNonWhiteSpace(allocator: std.mem.Allocator, handle: BufferHandle, point: Point) Point {
    const defualt = Point{ .row = point.row };
    var iter = core.buffer.LineIterator.init(allocator, handle, point.setCol(0), point) catch return defualt;
    defer iter.deinit();

    const result = (iter.next() catch return defualt) orelse return defualt;
    const col = firstNonWhiteSpaceString(result.line) orelse return defualt;

    return point.setCol(col);
}

pub fn inList(cp: ?u21, delimiters: []const u21) bool {
    const code_point = cp orelse return false;
    return std.mem.count(u21, delimiters, &.{code_point}) != 0;
}

pub fn isWhiteSpace(cp: ?u21) bool {
    return if (cp) |c| switch (c) {
        ' ', '\t', '\n', '\r' => true,
        else => false,
    } else false;
}

pub fn isEndOfRow(cp: ?u21) bool {
    return if (cp) |c| switch (c) {
        '\n', '\r' => true,
        else => false,
    } else false;
}
