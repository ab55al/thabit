const std = @import("std");
const print = std.debug.print;
const fs = std.fs;

const Buffer = @import("buffer.zig");

pub const data_dir_name = if (@import("builtin").mode == .Debug) "thabit_debug" else "thabit";
pub const opened_buffers_file_name = "opened_buffers";

pub const Error = error{
    DifferentModTimes,
};

pub fn bufferFromFile(allocator: std.mem.Allocator, handle: Buffer.Handle, file_path: []const u8) !Buffer {
    std.debug.assert(fs.path.isAbsolute(file_path));

    const file = fs.openFileAbsolute(file_path, .{}) catch |err| switch (err) {
        error.FileNotFound => {
            // file will be created later on save
            return try Buffer.init(allocator, handle, "\n", file_path);
        },
        else => return err,
    };
    defer file.close();
    const metadata = try file.metadata();
    const perms = metadata.permissions();
    try file.seekTo(0);
    const buf = try file.readToEndAlloc(allocator, metadata.size());
    defer allocator.free(buf);

    var buffer = try Buffer.init(allocator, handle, buf, file_path);
    buffer.file_last_mod_time = metadata.modified();
    buffer.read_only = perms.readOnly();

    return buffer;
}

pub fn writeToFile(buffer: *Buffer, file_path: []const u8, force_write: bool) !void {
    var file_dir = try fs.openDirAbsolute(fs.path.dirname(file_path).?, .{});
    defer file_dir.close();
    var file = file_dir.openFile(file_path, .{});

    if (file == error.FileNotFound)
        try writeToNewFile(buffer, file_path)
    else
        try writeAndReplaceFile(buffer, file_path, force_write);

    (file catch return).close();
}

fn writeToNewFile(buffer: *Buffer, file_path: []const u8) !void {
    const allocator = buffer.allocator;
    var file_dir = try fs.openDirAbsolute(fs.path.dirname(file_path).?, .{});
    defer file_dir.close();

    const new_file = try fs.createFileAbsolute(file_path, .{});
    defer new_file.close();
    const content_of_buffer = try buffer.getAllLines(allocator);
    defer allocator.free(content_of_buffer);

    try new_file.writeAll(content_of_buffer);
    const stat = try file_dir.statFile(file_path);
    buffer.file_last_mod_time = stat.mtime;
}

fn writeAndReplaceFile(buffer: *Buffer, file_path: []const u8, force_write: bool) !void {
    const allocator = buffer.allocator;
    const new_file_suffix = ".editor-new";
    const original_file_suffix = ".editor-original";

    const new_file_path = try std.mem.concat(allocator, u8, &.{
        file_path,
        new_file_suffix,
    });
    defer allocator.free(new_file_path);

    const original_tmp_file_path = try std.mem.concat(allocator, u8, &.{
        file_path,
        original_file_suffix,
    });
    defer allocator.free(original_tmp_file_path);

    var file_dir = try fs.openDirAbsolute(fs.path.dirname(file_path).?, .{});
    defer file_dir.close();

    if (!force_write) {
        const stat = try file_dir.statFile(file_path);
        if (stat.mtime != buffer.file_last_mod_time)
            return Error.DifferentModTimes;
    }

    const new_file = try fs.createFileAbsolute(new_file_path, .{});
    defer new_file.close();

    const content_of_buffer = try buffer.getAllLines(allocator);
    defer allocator.free(content_of_buffer);

    try new_file.writeAll(content_of_buffer);
    try std.os.rename(file_path, original_tmp_file_path);
    try std.os.rename(new_file_path, file_path);
    try file_dir.deleteFile(original_tmp_file_path);
    const stat = try file_dir.statFile(file_path);
    buffer.file_last_mod_time = stat.mtime;
}

pub fn getAbsolutePath(allocator: std.mem.Allocator, file_path: []const u8, buf: []u8) ![]u8 {
    if (fs.path.isAbsolute(file_path)) {
        const path = try fs.path.resolve(allocator, &.{file_path});
        defer allocator.free(path);
        std.mem.copyForwards(u8, buf, path);
        return buf[0..path.len];
    } else {
        return fs.cwd().realpath(file_path, buf) catch |err| {
            switch (err) {
                error.FileNotFound => {
                    var out: [std.fs.MAX_PATH_BYTES]u8 = undefined;
                    const cwd = try std.os.getcwd(&out);

                    const path = try fs.path.resolve(allocator, &.{ cwd, file_path });
                    defer allocator.free(path);

                    std.mem.copyForwards(u8, buf, path);
                    return buf[0..path.len];
                },
                else => return err,
            }
        };
    }
}

pub fn isDir(path: []const u8) !bool {
    var file = try fs.openFileAbsolute(path, .{});
    defer file.close();
    const stat = try file.stat();
    return stat.kind == .directory;
}

pub fn findLineInFile(allocator: std.mem.Allocator, file: std.fs.File, max_size: usize, string: []const u8) !bool {
    try file.seekTo(0);

    var list = std.ArrayList(u8).init(allocator);
    defer list.deinit();
    var done = false;
    while (!done) {
        file.reader().readUntilDelimiterArrayList(&list, '\n', max_size) catch |err| switch (err) {
            error.EndOfStream => done = true,
            else => |e| return e,
        };

        if (std.mem.eql(u8, string, list.items)) return true;
    }

    return false;
}

pub const opened_buffers = struct {
    pub fn openFileInDir(allocator: std.mem.Allocator, dir_path: []const u8) !std.fs.File {
        const file_path = try std.fs.path.join(allocator, &.{ dir_path, opened_buffers_file_name });
        defer allocator.free(file_path);

        const file = std.fs.openFileAbsolute(file_path, .{ .mode = .read_write, .lock = .exclusive }) catch |err| switch (err) {
            error.FileNotFound => try std.fs.createFileAbsolute(file_path, .{ .read = true, .lock = .exclusive }),
            else => |e| return e,
        };
        return file;
    }

    pub fn addPathToFileInDir(allocator: std.mem.Allocator, dir_path: []const u8, path_to_add: []const u8) !void {
        const file = try openFileInDir(allocator, dir_path);
        defer file.close();
        try addPathToFile(allocator, file, path_to_add);
    }

    pub fn removePathToFileInDir(allocator: std.mem.Allocator, dir_path: []const u8, path_to_remove: []const u8) !void {
        const file = try openFileInDir(allocator, dir_path);
        defer file.close();
        try removePathToFile(allocator, file, path_to_remove);
    }

    pub fn pathExistsInDir(allocator: std.mem.Allocator, dir_path: []const u8, path_to_find: []const u8) !bool {
        const file = try opened_buffers.openFileInDir(allocator, dir_path);
        defer file.close();

        return pathExists(allocator, file, path_to_find);
    }

    pub fn addPathToFile(allocator: std.mem.Allocator, file: std.fs.File, path_to_add: []const u8) !void {
        std.debug.assert(std.fs.path.isAbsolute(path_to_add));

        // for when the file is forcefully opened
        if (!try pathExists(allocator, file, path_to_add)) {
            try file.seekTo(try file.getEndPos());
            try file.writeAll(path_to_add);
            try file.writeAll("\n");
        }
    }

    pub fn removePathToFile(allocator: std.mem.Allocator, file: std.fs.File, path_to_remove: []const u8) !void {
        std.debug.assert(std.fs.path.isAbsolute(path_to_remove));

        try file.seekTo(0);
        var list = std.ArrayList(u8).fromOwnedSlice(
            allocator,
            try file.readToEndAlloc(allocator, std.math.maxInt(u16)),
        );
        defer list.deinit();

        try file.seekTo(0);
        try file.setEndPos(0);
        var iter = std.mem.splitSequence(u8, list.items, path_to_remove);
        while (iter.next()) |str| {
            if (std.fs.path.isAbsolute(str)) {
                try file.writeAll(str);
                if (str[str.len - 1] != '\n') try file.writeAll("\n");
            }
        }
    }

    pub fn pathExists(allocator: std.mem.Allocator, file: std.fs.File, path_to_find: []const u8) !bool {
        return try findLineInFile(allocator, file, std.fs.MAX_PATH_BYTES, path_to_find);
    }
};

const PathAndKind = struct {
    path: []const u8,
    kind: fs.File.Kind,
};

pub fn filesInDir(allocator: std.mem.Allocator, path: []const u8) ![]PathAndKind {
    var iter_dir = try fs.openDirAbsolute(path, .{ .iterate = true });
    var walker = try iter_dir.walk(allocator);
    defer walker.deinit();

    var list = std.ArrayList(PathAndKind).init(allocator);
    defer list.deinit();

    errdefer for (list.items) |p| allocator.free(p.path);

    while (true) {
        const res = (walker.next() catch continue) orelse break;

        try list.append(.{
            .path = try allocator.dupe(u8, res.path),
            .kind = res.kind,
        });

        // don't recursively iterate
        if (res.kind == .directory) {
            _ = walker.stack.pop();
            continue;
        }
    }

    return list.toOwnedSlice();
}
