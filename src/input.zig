const std = @import("std");
const print = std.debug.print;
const unicode = std.unicode;
const AutoHashMap = std.AutoHashMap;
const StringHashMapUnmanaged = std.StringHashMapUnmanaged;
const AutoHashMapUnmanaged = std.AutoHashMapUnmanaged;
const meta = std.meta;
const utils = @import("utils.zig");

pub const RegularCall = *const fn (repeat_count: usize) anyerror!void;
pub const RegularVTable = struct {
    regular: RegularCall,
    repeat: ?RegularCall = null,
};

pub const Key = struct {
    pub const max_string_len = 6 + 14; // 6 for modifiers. 14 for function_key maximum length

    kind: Kind,
    mod: Modifiers = .none,

    pub fn toString(key: Key, out: []u8) []u8 {
        std.debug.assert(out.len >= max_string_len); // 6 for modifiers. 14 for function_key maximum length

        if (key.kind.isFunctionKey()) {
            var size: u16 = 0;
            const mod_str = key.mod.toString();
            std.mem.copyForwards(u8, out, mod_str);
            size += @intCast(mod_str.len);

            const function_key_str = key.kind.functionKeyToString();
            std.mem.copyForwards(u8, out[mod_str.len..], function_key_str);
            size += @intCast(function_key_str.len);
            return out[0..size];
        } else {
            return key.asciiKeyToString(out);
        }
    }

    pub fn create(mod: Modifiers, key: Key.Kind) Key {
        return .{ .kind = key, .mod = mod };
    }

    pub fn asciiKeyToString(key: Key, out: []u8) []u8 {
        std.debug.assert(!key.kind.isFunctionKey());

        const byte: u8 = @intFromEnum(key.kind);

        if (!key.mod.contains(.shift)) {
            const mod_str = key.mod.toString();
            std.mem.copyForwards(u8, out, mod_str);
            out[mod_str.len] = byte;
            return out[0 .. mod_str.len + 1];
        } else {
            const new_mod = key.mod.remove(.shift);
            const new_byte = switch (byte) {
                '0' => ')',
                '1' => '!',
                '2' => '@',
                '3' => '#',
                '4' => '$',
                '5' => '%',
                '6' => '^',
                '7' => '&',
                '8' => '*',
                '9' => '(',
                '\'' => '"',
                '`' => '~',

                else => std.ascii.toUpper(byte),
            };

            const mod_str = new_mod.toString();
            std.mem.copyForwards(u8, out, mod_str);
            out[mod_str.len] = new_byte;
            return out[0 .. mod_str.len + 1];
        }
    }

    pub fn eql(a: Key, b: Key) bool {
        return a.mod == b.mod and a.kind == b.kind;
    }

    pub fn eqlSlice(a: []const Key, b: []const Key) bool {
        if (a.len != b.len) return false;
        for (a, b) |av, bv|
            if (!av.eql(bv))
                return false;

        return true;
    }

    /// This enum has some ASCII characters the missing ones can be achieved by doing `shift+key`
    pub const Kind = enum(u8) {
        a = 97,
        b = 98,
        c = 99,
        d = 100,
        e = 101,
        f = 102,
        g = 103,
        h = 104,
        i = 105,
        j = 106,
        k = 107,
        l = 108,
        m = 109,
        n = 110,
        o = 111,
        p = 112,
        q = 113,
        r = 114,
        s = 115,
        t = 116,
        u = 117,
        v = 118,
        w = 119,
        x = 120,
        y = 121,
        z = 122,

        zero = 48,
        one = 49,
        two = 50,
        three = 51,
        four = 52,
        five = 53,
        six = 54,
        seven = 55,
        eight = 56,
        nine = 57,

        single_quote = 39,
        comma = 44,
        dash = 45,
        dot = 46,
        slash = 47,
        semicolon = 59,
        equal = 61,
        left_bracket = 91,
        backslash = 92,
        right_bracket = 93,
        backtick = 96,

        unknown = 129,
        space,
        escape,
        enter,
        tab,
        backspace,
        insert,
        delete,
        right,
        left,
        down,
        up,
        page_up,
        page_down,
        home,
        end,
        caps_lock,
        scroll_lock,
        num_lock,
        print_screen,
        pause,
        f1,
        f2,
        f3,
        f4,
        f5,
        f6,
        f7,
        f8,
        f9,
        f10,
        f11,
        f12,
        kp_enter,

        left_shift,
        right_shift,
        left_control,
        right_control,
        left_alt,
        right_alt,

        pub fn functionKeyToString(key: Kind) []const u8 {
            return switch (key) {
                .space => "<SPACE>",
                .escape => "<ESCAPE>",
                .enter => "<ENTER>",
                .tab => "<TAB>",
                .backspace => "<BACKSPACE>",
                .insert => "<INSERT>",
                .delete => "<DELETE>",
                .right => "<RIGHT>",
                .left => "<LEFT>",
                .down => "<DOWN>",
                .up => "<UP>",
                .page_up => "<PAGE_UP>",
                .page_down => "<PAGE_DOWN>",
                .home => "<HOME>",
                .end => "<END>",
                .caps_lock => "<CAPS_LOCK>",
                .scroll_lock => "<SCROLL_LOCK>",
                .num_lock => "<NUM_LOCK>",
                .print_screen => "<PRINT_SCREEN>",
                .pause => "<PAUSE>",
                .f1 => "<F1>",
                .f2 => "<F2>",
                .f3 => "<F3>",
                .f4 => "<F4>",
                .f5 => "<F5>",
                .f6 => "<F6>",
                .f7 => "<F7>",
                .f8 => "<F8>",
                .f9 => "<F9>",
                .f10 => "<F10>",
                .f11 => "<F11>",
                .f12 => "<F12>",
                .kp_enter => "<KP_ENTER>",
                .left_shift => "<L_SHIFT>",
                .right_shift => "<R_SHIFT>",
                .left_control => "<L_CONTROL>",
                .right_control => "<R_CONTROL>",
                .left_alt => "<L_ALT>",
                .right_alt => "<R_ALT>",
                .unknown => "<UNKNOWN>",

                else => unreachable,
            };
        }

        pub fn isFunctionKey(kind: Kind) bool {
            // zig fmt: off
            return switch (kind) {
                .a, .b, .c, .d, .e, .f, .g, .h, .i, .j, .k, .l, .m, .n, .o, .p, .q, .r, .s, .t, .u, .v, .w, .x, .y, .z, .zero, .one, .two, .three, .four, .five, .six, .seven, .eight, .nine, .single_quote, .comma, .dash, .dot, .slash, .semicolon, .equal, .left_bracket, .backslash, .right_bracket, .backtick
                => false,
                .space, .escape, .enter, .tab, .backspace, .insert, .delete, .right, .left, .down, .up, .page_up, .page_down, .home, .end, .caps_lock, .scroll_lock, .num_lock, .print_screen, .pause, .f1, .f2, .f3, .f4, .f5, .f6, .f7, .f8, .f9, .f10, .f11, .f12,.kp_enter, .left_shift, .right_shift, .left_control, .right_control, .left_alt, .right_alt, .unknown
                => true,
            };
            // zig fmt: on
        }
    };
};

pub const Modifiers = enum(u3) {
    none = 0,
    shift = 1, // 001
    control = 2, // 010
    control_shift = 3,
    alt = 4, // 100
    alt_shift = 5,
    control_alt = 6,
    control_alt_shift = 7,

    pub fn toString(mod: Modifiers) []const u8 {
        return switch (mod) {
            .none => "",
            .shift => "S_",
            .control => "C_",
            .control_shift => "C_S_",
            .alt => "A_",
            .alt_shift => "A_S_",
            .control_alt => "C_A_",
            .control_alt_shift => "C_S_A_",
        };
    }

    pub fn contains(mod: Modifiers, contained: enum { shift, control, alt }) bool {
        const mod_int = @intFromEnum(mod);
        return switch (contained) {
            .shift => mod_int & 0b001 == 1,
            .control => mod_int & 0b010 == 1,
            .alt => mod_int & 0b100 == 1,
        };
    }

    pub fn remove(mod: Modifiers, to_remove: enum { shift, control, alt }) Modifiers {
        const mod_int = @intFromEnum(mod);
        return switch (to_remove) {
            .shift => @enumFromInt(mod_int & 0b110),
            .control => @enumFromInt(mod_int & 0b101),
            .alt => @enumFromInt(mod_int & 0b011),
        };
    }

    pub fn add(mod: Modifiers, to_add: enum { shift, control, alt }) Modifiers {
        const mod_int = @intFromEnum(mod);
        return switch (to_add) {
            .shift => @enumFromInt(mod_int | 0b001),
            .control => @enumFromInt(mod_int | 0b010),
            .alt => @enumFromInt(mod_int | 0b100),
        };
    }
};
