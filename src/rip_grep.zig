const std = @import("std");

pub const Match = struct {
    pub const Submatch = struct { text: []const u8, start: usize, end: usize };

    path: []const u8,
    lines: []const u8,
    submatches: []const Submatch,
    line_number: usize,
    absolute_offset: usize,

    pub fn deinit(match: Match, allocator: std.mem.Allocator) void {
        allocator.free(match.path);
        allocator.free(match.lines);
        for (match.submatches) |submatch|
            allocator.free(submatch.text);

        allocator.free(match.submatches);
    }
};

/// this should be used as a prefix.
/// To concate the strings at compile time do
/// `argv ++ .{extra_one, extra_two}`
pub const argv_vim_grep: []const []const u8 = &.{
    "rg",
    "--block-buffered",
    "--vimgrep",
};

/// this should be used as a prefix.
/// To concate the strings at compile time do
/// `argv ++ .{extra_one, extra_two}`
pub const argv_vim_grep_no_dupe: []const []const u8 = &.{
    "rg",
    "--no-heading",
    "--block-buffered",
    "--line-number",
    "--column",
};

/// this should be used as a prefix.
/// To concate the strings at compile time do
/// `argv ++ .{extra_one, extra_two}`
pub const argv_json: []const []const u8 = &.{
    "rg",
    "--no-heading",
    "--block-buffered",
    "--json",
};

pub const VimGrep = struct {
    path: []const u8,
    text: []const u8,
    row: u32,
    col: u32,

    pub fn parse(string: []const u8) !VimGrep {
        var iter = std.mem.splitScalar(u8, string, ':');
        const path = iter.next() orelse return error.InvalidString;
        const row = try std.fmt.parseInt(u32, iter.next() orelse return error.InvalidString, 10);
        const col = try std.fmt.parseInt(u32, iter.next() orelse return error.InvalidString, 10);
        const text = iter.rest();

        return .{
            .path = path,
            .row = row,
            .col = col,
            .text = text,
        };
    }
};

pub fn spawnRipGrep(allocator: std.mem.Allocator, argv: []const []const u8, stdout: *std.ArrayList(u8), stderr: *std.ArrayList(u8), stdin_content: []const u8) !void {
    var child = std.ChildProcess.init(argv, allocator);

    if (stdin_content.len > 0) child.stdin_behavior = .Pipe;
    child.stdout_behavior = .Pipe;
    child.stderr_behavior = .Pipe;

    try child.spawn();

    if (child.stdin) |stdin| {
        try stdin.writeAll(stdin_content);
        stdin.close();
        try child.collectOutput(stdout, stderr, std.math.maxInt(usize));
    } else {
        try child.collectOutput(stdout, stderr, std.math.maxInt(usize));
        _ = try child.kill();
    }
}

pub fn dupeSubmatches(allocator: std.mem.Allocator, json_submatches: []const RipGrepJson.MatchData.JsonSubmatch) ![]Match.Submatch {
    if (json_submatches.len == 0) return &.{};

    var len: usize = 0;
    const slice = try allocator.alloc(Match.Submatch, json_submatches.len);
    errdefer {
        for (slice[0..len]) |match| allocator.free(match.text);
        allocator.free(slice);
    }

    for (json_submatches, 0..) |jsm, i| {
        len = i;

        slice[i] = .{
            .text = try allocator.dupe(u8, jsm.match.text),
            .start = @intCast(jsm.start),
            .end = @intCast(jsm.end),
        };
    }

    return slice;
}

pub fn freeMatches(allocator: std.mem.Allocator, matches: []const Match) void {
    for (matches) |match| match.deinit(allocator);
    allocator.free(matches);
}

pub fn getMatchesFromJsonLines(allocator: std.mem.Allocator, json_lines: []const u8) ![]Match {
    var list = std.ArrayList(Match).init(allocator);
    errdefer {
        for (list.items) |match| match.deinit(allocator);
        list.deinit();
    }

    var arena = std.heap.ArenaAllocator.init(allocator);
    defer arena.deinit();
    var iter = std.mem.split(u8, json_lines, "\n");
    while (iter.next()) |line| {
        defer _ = arena.reset(.retain_capacity);

        if (line.len > 0) {
            const value = try std.json.parseFromSliceLeaky(RipGrepJson, arena.allocator(), line, .{ .ignore_unknown_fields = true });

            switch (value) {
                .match => |json_match| {
                    const path = try allocator.dupe(u8, json_match.path.text);
                    errdefer allocator.free(path);
                    const lines = try allocator.dupe(u8, json_match.lines.text);
                    errdefer allocator.free(lines);
                    const submatches = try dupeSubmatches(allocator, json_match.submatches);

                    const match = Match{
                        .submatches = submatches,
                        .path = path,
                        .lines = lines,
                        .line_number = @intCast(json_match.line_number),
                        .absolute_offset = @intCast(json_match.absolute_offset),
                    };

                    try list.append(match);
                },

                else => {},
            }
        }
    }

    return try list.toOwnedSlice();
}

pub const RipGrepJson = union(enum) {
    match: MatchData,
    begin: BeginData,
    end: EndData,
    summary: SummaryData,

    pub const Text = struct { text: []const u8 };

    pub const Elapsed = struct {
        secs: i64,
        nanos: i64,
        human: []const u8,
    };

    pub const Stats = struct {
        elapsed: Elapsed,
        searches: i64,
        searches_with_match: i64,
        bytes_searched: i64,
        bytes_printed: i64,
        matched_lines: i64,
        matches: i64,
    };

    pub const SummaryData = struct {
        elapsed_total: Elapsed,
        stats: Stats,
    };

    pub const BeginData = struct {
        path: Text,
    };

    pub const EndData = struct {
        path: Text,
        binary_offset: ?i64,
        stats: Stats,
    };

    pub const MatchData = struct {
        const JsonSubmatch = struct { match: Text, start: i64, end: i64 };

        path: Text,
        lines: Text,
        line_number: i64,
        absolute_offset: i64,
        submatches: []const JsonSubmatch,
    };

    /// https://docs.rs/grep-printer/latest/grep_printer/struct.JSON.html
    pub fn jsonParse(allocator: std.mem.Allocator, source: anytype, options: std.json.ParseOptions) std.json.ParseError(@TypeOf(source.*))!@This() {
        const old_source = source.*;
        const data_type = try dataType(source);
        // reset the source while not leaking memory
        source.deinit();
        source.* = old_source;

        try advanceUntillData(source);

        if (try source.peekNextTokenType() != .object_begin) return error.UnexpectedToken;

        const map = .{
            .{ "begin", BeginData },
            .{ "end", EndData },
            .{ "summary", SummaryData },
            .{ "match", MatchData },
        };

        inline for (map) |tuple| {
            const name = tuple.@"0";
            const Type = tuple.@"1";

            if (std.mem.eql(u8, data_type, name)) {
                const sub_data = try std.json.innerParse(Type, allocator, source, options);
                const result = @unionInit(RipGrepJson, name, sub_data);

                // the `type` field might come after `data` field so just skip to end
                while (try source.next() != .end_of_document) {}
                return result;
            }
        }

        return error.UnexpectedToken;
    }

    pub fn dataType(source: anytype) ![]const u8 {
        var token = try source.next();
        while (token != .end_of_document) : (token = try source.next()) {
            if (token == .string and std.mem.eql(u8, "type", token.string)) {
                return switch (try source.next()) {
                    .string => |result| result,
                    else => return error.UnexpectedToken,
                };
            }
        } else return error.UnexpectedEndOfInput;
    }

    pub fn advanceUntillData(source: anytype) !void {
        var token = try source.next();
        while (token != .end_of_document) : (token = try source.next()) {
            if (token == .string and std.mem.eql(u8, "data", token.string)) {
                return;
            }
        } else return error.UnexpectedEndOfInput;
    }
};
