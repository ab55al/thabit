const std = @import("std");
const core = @import("core.zig");

const rip_grep = @import("rip_grep.zig");

pub const SearchResults = struct {
    pub const Source = enum {
        rip_grep,
        exact,
    };

    source: Source,
    bhandle: core.BufferHandle,
    search_string: []const u8,
    ranges: []core.Buffer.PointRange,

    pub fn freeData(sr: SearchResults, allocator: std.mem.Allocator) void {
        allocator.free(sr.search_string);
        allocator.free(sr.ranges);
    }
};

/// Searches the buffer for *string* and returns a slice of indices of all occurrences
/// within the given range
pub fn exact(allocator: std.mem.Allocator, bhandle: core.BufferHandle, string: []const u8, start_row: u32, end_row: u32) !?SearchResults {
    if (string.len == 0) return null;

    const search_string_cols: u32 = @truncate(try std.unicode.utf8CountCodepoints(string));

    var ranges = std.ArrayListUnmanaged(core.Buffer.PointRange){};
    defer ranges.deinit(allocator);

    var iter = try core.buffer.LineIterator.init(allocator, bhandle, .{ .row = start_row }, .{ .row = end_row });
    defer iter.deinit();

    while (try iter.next()) |res| {
        const full_line = res.line;
        var slicer: u64 = 0;
        while (slicer < full_line.len) { // get all matches of string in the same line
            const line = full_line[slicer..];
            if (line.len < string.len) break;

            const index = std.mem.indexOf(u8, line, string);
            if (index) |i| {
                const abs_index = i + slicer;
                const col = core.utils.getColOfIndex(full_line, abs_index);
                try ranges.append(allocator, .{
                    .start = .{ .row = res.number, .col = col },
                    .end = .{ .row = res.number, .col = col + search_string_cols },
                });
                slicer += string.len + i;
            } else break;
        }
    }

    return if (ranges.items.len == 0) null else .{
        .bhandle = bhandle,
        .search_string = try allocator.dupe(u8, string),
        .ranges = try ranges.toOwnedSlice(allocator),
        .source = .exact,
    };
}

pub fn ripGrep(allocator: std.mem.Allocator, bhandle: core.BufferHandle, pattern: []const u8, start_row: u32, end_row: u32) !?SearchResults {
    const content = try core.buffer.getLines(allocator, bhandle, start_row, end_row);
    defer allocator.free(content);

    var stdout = std.ArrayList(u8).init(allocator);
    var stderr = std.ArrayList(u8).init(allocator);
    defer {
        stdout.deinit();
        stderr.deinit();
    }
    const argv = rip_grep.argv_json ++ .{pattern};
    try rip_grep.spawnRipGrep(allocator, argv, &stdout, &stderr, content);

    var ranges = std.ArrayList(core.Buffer.PointRange).init(allocator);
    defer ranges.deinit();

    var arena = std.heap.ArenaAllocator.init(allocator);
    defer arena.deinit();

    var iter = std.mem.split(u8, stdout.items, "\n");
    while (iter.next()) |line| {
        if (line.len == 0) continue;
        defer _ = arena.reset(.retain_capacity);

        const value = try std.json.parseFromSliceLeaky(rip_grep.RipGrepJson, arena.allocator(), line, .{ .ignore_unknown_fields = true });

        switch (value) {
            .match => |json_match| {
                const line_number: usize = @intCast(json_match.line_number);
                for (json_match.submatches) |sm| {
                    const row = line_number + start_row - 1;
                    try ranges.append(.{
                        .start = .{ .row = @intCast(row), .col = @intCast(sm.start) },
                        .end = .{ .row = @intCast(row), .col = @intCast(sm.end) },
                    });
                }
            },

            else => {},
        }
    }

    return .{
        .bhandle = bhandle,
        .search_string = try allocator.dupe(u8, pattern),
        .ranges = try ranges.toOwnedSlice(),
        .source = .rip_grep,
    };
}

pub const SearchKeeper = struct {
    const RangeList = std.ArrayListUnmanaged(core.Buffer.PointRange);

    pub const SearchResultsDynamic = struct {
        source: SearchResults.Source,
        bhandle: core.BufferHandle,
        search_string: []const u8,
        ranges: RangeList = .{},

        pub fn deinit(sr: *SearchResultsDynamic, allocator: std.mem.Allocator) void {
            allocator.free(sr.search_string);
            sr.ranges.deinit(allocator);
        }
    };

    allocator: std.mem.Allocator,
    search_results: std.ArrayListUnmanaged(SearchResultsDynamic) = .{},

    pub fn init(allocator: std.mem.Allocator) SearchKeeper {
        return .{ .allocator = allocator };
    }

    pub fn deinit(self: *SearchKeeper) void {
        self.removeResults();
        self.search_results.deinit(self.allocator);
    }

    pub fn addResults(self: *SearchKeeper, results: SearchResults) !void {
        try self.search_results.append(self.allocator, .{
            .bhandle = results.bhandle,
            .search_string = results.search_string,
            .ranges = RangeList.fromOwnedSlice(results.ranges),
            .source = results.source,
        });
    }

    pub fn updateRanges(self: *SearchKeeper, handle: core.BufferHandle, change: core.Buffer.Change) void {
        const ranges = blk: {
            for (self.search_results.items) |results|
                if (results.bhandle == handle)
                    break :blk &results.ranges;

            return;
        };

        const abs_line_diff: u32 = std.math.absCast(change.line_count_diff);

        if (change.line_count_diff > 0) {
            for (ranges.items) |*point| {
                if (change.start_point.row >= point.point.row) continue; // no need to update
                point.point.row +|= abs_line_diff;
            }
        } else if (change.line_count_diff < 0) {
            for (ranges.items) |*point| {
                if (change.start_point.row >= point.point.row) continue; // no need to update
                point.point.row -|= abs_line_diff;
            }
        }
    }

    pub fn updateResultsFor(self: *SearchKeeper, handle: core.BufferHandle) !void {
        const buffer_data = try core.buffer.getBufferData(handle);

        var results = blk: {
            for (self.search_results.items) |*results|
                if (results.bhandle.handle == handle.handle)
                    break :blk results;

            return;
        };

        const new_results = switch (results.source) {
            .exact => try exact(self.allocator, handle, results.search_string, 0, buffer_data.line_count),
            .rip_grep => try ripGrep(self.allocator, handle, results.search_string, 0, buffer_data.line_count),
        };

        results.ranges.deinit(self.allocator);

        if (new_results) |res| {
            self.allocator.free(res.search_string);
            results.ranges = RangeList.fromOwnedSlice(res.ranges);
        } else results.ranges = .{};
    }

    pub fn addResultsAssumeCapacity(self: *SearchKeeper, results: SearchResults) void {
        self.addResults(results) catch unreachable;
    }

    pub fn removeResults(self: *SearchKeeper) void {
        for (self.search_results.items) |*results|
            results.deinit(self.allocator);

        self.search_results.resize(self.allocator, 0) catch unreachable;
    }

    pub fn rangesForBuffer(self: *SearchKeeper, handle: core.BufferHandle) ?[]const core.Buffer.PointRange {
        for (self.search_results.items) |results| {
            if (results.bhandle.handle == handle.handle) {
                if (results.ranges.items.len == 0) return null;
                return results.ranges.items;
            }
        }

        return null;
    }
};
