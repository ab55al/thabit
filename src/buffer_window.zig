const std = @import("std");
const print = std.debug.print;
const ArrayList = std.ArrayList;
const math = std.math;

const Buffer = @import("core.zig").Buffer;
const BufferHandle = @import("core.zig").BufferHandle;
const PositionList = @import("core.zig").PositionList;

const MarkKey = @import("core.zig").Marks.MarkKey;
const utils = @import("utils.zig");

pub const FloatingBufferWindow = struct {
    bhandle: BufferHandle,
    bw: BufferWindow.ID,
    open: bool = false,
};

pub const BufferWindow = struct {
    pub const ID = struct { id: u32 };

    id: ID,
    bhandle: BufferHandle,
    first_visiable_row: u32 = 0,
    /// BufferWindow always associates with the buffer handle
    cursor_mark: MarkKey,
    jump_list: PositionList,

    visible_lines: u32 = 0, // Set every frame

    pub fn init(allocator: std.mem.Allocator, id: ID, bhandle: BufferHandle, first_visiable_row: u32, cursor_mark: MarkKey) BufferWindow {
        return .{
            .id = id,
            .bhandle = bhandle,
            .first_visiable_row = first_visiable_row,
            .cursor_mark = cursor_mark,
            .jump_list = PositionList.init(allocator),
        };
    }

    pub fn deinit(buffer_window: *BufferWindow) void {
        buffer_window.jump_list.deinit();
    }

    pub fn lastVisibleRow(buffer_window: *BufferWindow, line_count: u32) u32 {
        var res = buffer_window.first_visiable_row + buffer_window.visible_lines;
        // const line_count = (core.buffer.getData(buffer_window.bhandle) catch unreachable).line_count;
        res = utils.bound(res, 0, line_count -| 1);
        return res;
    }

    pub fn absoluteBufferIndexFromRelative(buffer_win: *BufferWindow, relative: u64) u64 {
        if (buffer_win.first_visiable_row == 0) return relative;

        const offset: u64 = buffer_win.buffer.indexOfFirstByteAtRow(buffer_win.first_visiable_row);
        utils.assert(relative +| offset <= buffer_win.buffer.lines.size, "You may have passed an absolute index into this function");
        return relative +| offset;
    }

    pub fn relativeBufferIndexFromAbsolute(buffer_win: *BufferWindow, absolute: u64) u64 {
        if (buffer_win.first_visiable_row == 0) return absolute;

        const offset: u64 = buffer_win.buffer.indexOfFirstByteAtRow(buffer_win.first_visiable_row);
        return absolute -| offset;
    }

    pub fn relativeBufferRowFromAbsolute(buffer_win: *BufferWindow, absolute: u64) u64 {
        if (buffer_win.first_visiable_row == 0) return absolute;
        return absolute -| buffer_win.first_visiable_row + 1;
    }

    /// Scrolls the window and returns the cursor's new position
    pub fn scrollDown(buffer_win: *BufferWindow, cursor: Buffer.Point, offset: u32, line_count: u32) Buffer.Point {
        buffer_win.first_visiable_row += offset;
        buffer_win.first_visiable_row = @min(
            line_count,
            buffer_win.first_visiable_row,
        );

        return buffer_win.cursorFollowWindow(cursor, line_count);
    }

    /// Scrolls the window and returns the cursor's new position
    pub fn scrollUp(buffer_win: *BufferWindow, cursor: Buffer.Point, offset: u32, line_count: u32) Buffer.Point {
        buffer_win.first_visiable_row -|= offset;
        return buffer_win.cursorFollowWindow(cursor, line_count);
    }

    pub fn centerScreen(bw: *BufferWindow, cursor: Buffer.Point) isize {
        const cursor_row: isize = @intCast(cursor.row - bw.first_visiable_row);
        const mid: isize = @intCast(bw.visible_lines / 2);
        const diff: isize = cursor_row - mid;
        return diff;
    }

    pub fn cursorFollowWindow(buffer_win: *BufferWindow, cursor: Buffer.Point, line_count: u32) Buffer.Point {
        var view = utils.ListView{
            .offset = buffer_win.first_visiable_row,
            .len = buffer_win.visible_lines,
            .selected_index = cursor.row,
        };

        view.indexFollowView(line_count);

        const row = view.selected_index orelse return cursor;
        return cursor.setRow(@truncate(row));
    }

    pub fn windowFollowCursor(buffer_win: *BufferWindow, cursor: Buffer.Point, line_count: u32) void {
        var view = utils.ListView{
            .offset = buffer_win.first_visiable_row,
            .len = buffer_win.visible_lines,
            .selected_index = cursor.row,
        };

        view.viewFollowIndex(line_count);
        buffer_win.first_visiable_row = @truncate(view.offset);
    }
};

pub const WindowTree = struct {
    pub const Orient = enum {
        vertical,
        horizantal,

        pub fn flip(orient: Orient) Orient {
            return switch (orient) {
                .vertical => .horizantal,
                .horizantal => .vertical,
            };
        }
    };

    pub const Dir = enum {
        left,
        right,
        up,
        down,

        pub fn flip(dir: Dir) Dir {
            return switch (dir) {
                .left => .right,
                .right => .left,
                .up => .down,
                .down => .up,
            };
        }

        pub fn orient(dir: Dir) Orient {
            return switch (dir) {
                .left, .right => .vertical,
                .up, .down => .horizantal,
            };
        }

        pub fn fromOrient(o: Orient, child_dir: enum { left, right }) Dir {
            return switch (o) {
                .vertical => if (child_dir == .left) .left else .right,
                .horizantal => if (child_dir == .left) .up else .down,
            };
        }
    };

    pub const Data = union(enum) {
        split: struct {
            orient: Orient,
            left_size: f32,

            pub fn rightSize(sp: @This()) f32 {
                return 1 - sp.left_size;
            }

            pub fn setRight(sp: *@This(), size: f32) void {
                sp.left_size = 1 - size;
            }
        },
        window: BufferWindow.ID,
    };
    pub const Tree = BinaryTree(Data);
    pub const Node = Tree.Node;
    tree: Tree,

    pub fn init(allocator: std.mem.Allocator) WindowTree {
        return .{ .tree = Tree.init(allocator) };
    }

    pub fn deinit(self: *WindowTree) void {
        self.tree.deinit();
    }

    pub fn setRoot(self: *WindowTree, node: *Node) void {
        std.debug.assert(self.tree.root == null);
        self.tree.root = node;
    }

    /// Splits the window
    /// return true when the split succeeds
    pub fn split(self: *WindowTree, window: BufferWindow.ID, new_window: BufferWindow.ID, size: f32, dir: Dir) !bool {
        std.debug.assert(self.tree.root != null);

        const node = self.getWindowNode(window) orelse return false;

        const left, const left_size, const right = switch (dir) {
            .left, .up => .{ new_window, size, window },
            .right, .down => .{ window, 1 - size, new_window },
        };

        node.* = Node{
            .parent = node.parent,
            .data = .{ .split = .{ .orient = dir.orient(), .left_size = left_size } },
        };

        if (node.parent) |parent| {
            std.debug.assert(node == parent.left or node == parent.right);
        }

        const left_node = try self.createWindow(left);
        errdefer self.tree.allocator.destroy(left_node);
        const right_node = try self.createWindow(right);

        node.setLeft(left_node);
        node.setRight(right_node);

        return true;
    }
    pub fn remove(self: *WindowTree, window: BufferWindow.ID) void {
        const removed_node = self.getWindowNode(window) orelse return;

        if (removed_node.parent) |parent| {
            const left_child = removed_node == parent.left;

            const new_node = blk: {
                const node_to_overtake = if (left_child) parent.right.? else parent.left.?;
                if (node_to_overtake.left) |left| left.parent = parent;
                if (node_to_overtake.right) |right| right.parent = parent;

                break :blk Node{
                    .parent = parent.parent,
                    .right = node_to_overtake.right,
                    .left = node_to_overtake.left,
                    .data = node_to_overtake.data,
                };
            };

            self.tree.allocator.destroy(parent.splitLeft().?);
            self.tree.allocator.destroy(parent.splitRight().?);
            parent.* = new_node;
        } else { // node is root
            self.tree.allocator.destroy(removed_node);
            self.tree.root = null;
        }
    }

    pub fn resizeDir(self: *WindowTree, window: BufferWindow.ID, size: f32, resize_dir: Dir) void {
        const node = self.getWindowNode(window) orelse return;
        const parent = node.parent.?;
        const left_child = parent.left == node;
        const right_child = !left_child;
        const sp = &parent.data.split;

        if (sp.orient == resize_dir.orient()) {
            const parent_to_size, const new_size = blk: {
                if (right_child and (resize_dir == .up or resize_dir == .left)) {
                    break :blk .{ parent, size };
                } else if (left_child and (resize_dir == .down or resize_dir == .right)) {
                    break :blk .{ parent, size };
                }

                const res = (if (left_child)
                    self.firstAncestorWhereWindowInRight(node, window, resize_dir.orient())
                else
                    self.firstAncestorWhereWindowInLeft(node, window, resize_dir.orient())) orelse return;

                break :blk .{ res.ancestor, res.newSize(parent, size) };
            };

            const s = @max(0.1, @min(0.90, new_size));
            switch (sp.orient) {
                .vertical => resizeVerticalFamily(parent_to_size, s, resize_dir),
                .horizantal => resizeHorizantalFamily(parent_to_size, s, resize_dir),
            }
        } else if (sp.orient != resize_dir.orient()) {
            const res = switch (resize_dir) {
                // only when horizantal
                .up => self.firstAncestorWhereWindowInRight(node, window, .horizantal),
                .down => self.firstAncestorWhereWindowInLeft(node, window, .horizantal),
                // only when vertical
                .left => self.firstAncestorWhereWindowInRight(node, window, .vertical),
                .right => self.firstAncestorWhereWindowInLeft(node, window, .vertical),
            } orelse return;

            const new_size = @max(0.1, @min(0.90, res.newSize(parent, size)));

            switch (res.ancestor.data.split.orient) {
                .horizantal => resizeHorizantalFamily(res.ancestor, new_size, resize_dir),
                .vertical => resizeVerticalFamily(res.ancestor, new_size, resize_dir),
            }
        }
    }

    pub fn resizeHorizantalFamily(parent: *Node, size: f32, dir: Dir) void {
        std.debug.assert(dir == .up or dir == .down);
        std.debug.assert(parent.data == .split);

        // a left child can only resize down
        // a right child can only resize up

        switch (dir) {
            .up => parent.data.split.setRight(size),
            .down => parent.data.split.left_size = size,
            else => unreachable,
        }
    }

    pub fn resizeVerticalFamily(parent: *Node, size: f32, dir: Dir) void {
        std.debug.assert(dir == .left or dir == .right);
        std.debug.assert(parent.data == .split);

        // a left child can only resize right
        // a right child can only resize left

        switch (dir) {
            .left => parent.data.split.setRight(size),
            .right => parent.data.split.left_size = size,
            else => unreachable,
        }
    }

    const AncestorResult = struct {
        const CameFrom = enum {
            left,
            right,
        };
        ancestor: *Node,
        came_from: CameFrom,

        fn splitSize(res: AncestorResult) f32 {
            return switch (res.came_from) {
                .left => res.ancestor.data.split.left_size,
                .right => res.ancestor.data.split.rightSize(),
            };
        }

        fn growAmount(res: AncestorResult, node: *Node, size: f32) f32 {
            return switch (res.came_from) {
                .left => @abs(size - node.data.split.left_size),
                .right => @abs(size - node.data.split.rightSize()),
            };
        }

        fn newSize(res: AncestorResult, node: *Node, size: f32) f32 {
            const grow_amount = res.growAmount(node, size);
            return switch (res.came_from) {
                .left => (@min(0.95, res.splitSize() + grow_amount)),
                .right => (@min(0.95, res.splitSize() + grow_amount)),
            };
        }
    };

    fn firstAncestorWhereWindowInRight(self: *WindowTree, node: *Node, window: BufferWindow.ID, orient: Orient) ?AncestorResult {
        var opt_node = node.parent;
        while (opt_node) |n| {
            if (n.data.split.orient == orient and self.inRightTree(n, window)) {
                return .{ .ancestor = n, .came_from = .right };
            }
            opt_node = n.parent;
        }

        return null;
    }

    fn firstAncestorWhereWindowInLeft(self: *WindowTree, node: *Node, window: BufferWindow.ID, orient: Orient) ?AncestorResult {
        var opt_node = node.parent;
        while (opt_node) |n| {
            if (n.data.split.orient == orient and self.inLeftTree(n, window)) {
                return .{ .ancestor = n, .came_from = .left };
            }
            opt_node = n.parent;
        }

        return null;
    }

    fn inRightTree(self: *WindowTree, node: *Node, id: BufferWindow.ID) bool {
        return self.tree.find(node.right, .{ .window = id }, getWindowEql) != null;
    }

    fn inLeftTree(self: *WindowTree, node: *Node, id: BufferWindow.ID) bool {
        return self.tree.find(node.left, .{ .window = id }, getWindowEql) != null;
    }

    pub fn createWindow(self: *WindowTree, id: BufferWindow.ID) !*Node {
        return try self.tree.create(.{ .window = id });
    }

    pub fn getWindowNode(self: *const WindowTree, id: BufferWindow.ID) ?*Node {
        return self.tree.find(self.tree.root, .{ .window = id }, getWindowEql);
    }

    fn getWindowEql(a: Data, b: Data) bool {
        if (std.meta.activeTag(a) != std.meta.activeTag(b)) return false;

        return switch (a) {
            .window => a.window.id == b.window.id,
            .split => false,
        };
    }

    pub fn toArray(self: *const WindowTree, opt_node: ?*Tree.Node, list: *std.ArrayList(*Tree.Node)) !void {
        try self.tree.toArray(opt_node, list);
    }
};

fn BinaryTree(comptime T: type) type {
    return struct {
        const Self = @This();
        pub const Node = struct {
            parent: ?*Node = null,
            left: ?*Node = null,
            right: ?*Node = null,
            data: T,

            pub fn insertLeft(parent: *Node, arg_node: *Node) void {
                const old_left = parent.splitLeft();
                parent.setLeft(arg_node);
                if (arg_node.leftMost()) |lm| lm.setLeft(old_left orelse return);
            }

            pub fn insertRight(parent: *Node, arg_node: *Node) void {
                const old_right = parent.splitRight();
                parent.setRight(arg_node);
                if (arg_node.rightMost()) |lm| lm.setRight(old_right orelse return);
            }

            pub fn leftMost(parent: *Node) ?*Node {
                var node = parent;
                while (node.left) |n| node = n;
                return node;
            }

            pub fn rightMost(parent: *Node) ?*Node {
                var node = parent;
                while (node.right) |n| node = n;
                return node;
            }

            fn splitLeft(parent: *Node) ?*Node {
                var left = parent.left orelse return null;
                left.parent = null;
                parent.left = null;
                return left;
            }

            fn splitRight(parent: *Node) ?*Node {
                var right = parent.right orelse return null;
                right.parent = null;
                parent.right = null;
                return right;
            }

            pub fn setRight(parent: *Node, node: *Node) void {
                std.debug.assert(parent.right == null);
                parent.right = node;
                node.parent = parent;
            }

            pub fn setLeft(parent: *Node, node: *Node) void {
                std.debug.assert(parent.left == null);
                parent.left = node;
                node.parent = parent;
            }
        };

        allocator: std.mem.Allocator,
        root: ?*Node = null,

        pub fn init(allocator: std.mem.Allocator) Self {
            return .{ .allocator = allocator };
        }

        pub fn deinit(self: *Self) void {
            const root = deinitTree(self.root, self.allocator);
            if (root) |r| self.allocator.destroy(r);
        }

        pub fn count(self: *Self) usize {
            var ctx = CountContext{};
            self.inorderRecurse(self.root, &ctx);
            return ctx.count;
        }

        pub fn deinitTree(opt_node: ?*Node, allocator: std.mem.Allocator) ?*Node {
            const node = opt_node orelse return null;

            const left = deinitTree(node.left, allocator);
            if (left) |l| allocator.destroy(l);
            const right = deinitTree(node.right, allocator);
            if (right) |r| allocator.destroy(r);

            return node;
        }

        pub fn create(self: *Self, data: T) !*Node {
            const node = try self.allocator.create(Node);
            node.* = Node{ .data = data };
            return node;
        }

        pub fn setRoot(self: *Self, node: *Node) void {
            std.debug.assert(self.root == null);
            self.root = node;
        }

        pub fn toArray(self: *const Self, opt_node: ?*Node, list: *std.ArrayList(*Node)) !void {
            const node = opt_node orelse return;

            if (node.left) |left| try self.toArray(left, list);
            try list.append(node);
            if (node.right) |right| try self.toArray(right, list);
        }

        pub fn find(self: *const Self, opt_node: ?*Node, data: T, eql: *const fn (a: T, b: T) bool) ?*Node {
            const node = opt_node orelse return null;

            if (node.left) |left| {
                if (self.find(left, data, eql)) |found| return found;
            }
            if (eql(data, node.data)) return node;

            if (node.right) |right| {
                if (self.find(right, data, eql)) |found| return found;
            }

            return null;
        }

        pub fn inorderRecurse(self: *const Self, opt_node: ?*Node, context: anytype) void {
            const node = opt_node orelse return;

            self.inorderRecurse(node.left, context);
            context.call(node);
            self.inorderRecurse(node.right, context);
        }

        const CountContext = struct {
            count: usize = 0,

            pub fn call(ctx: *@This(), _: *Node) void {
                ctx.count += 1;
            }
        };
    };
}

test "Binary tree" {
    const allocator = std.testing.allocator;
    const Tree = BinaryTree(usize);
    var tree = Tree.init(allocator);
    defer tree.deinit();

    const node_0 = try tree.create(0);
    const node_1 = try tree.create(1);
    const node_2 = try tree.create(2);
    const node_3 = try tree.create(3);

    tree.setRoot(node_0);
    node_0.setLeft(node_1);
    node_0.setRight(node_2);
    node_2.setRight(node_3);

    const c = struct {
        fn eqlUsize(a: usize, b: usize) bool {
            return a == b;
        }
    };

    const one = tree.find(tree.root, 1, c.eqlUsize).?;
    try std.testing.expect(one.data == 1);

    var list = std.ArrayList(*Tree.Node).init(allocator);
    defer list.deinit();
    try tree.toArray(tree.root, &list);

    try std.testing.expect(list.items[0].data == 1);
    try std.testing.expect(list.items[1].data == 0);
    try std.testing.expect(list.items[2].data == 2);
    try std.testing.expect(list.items[3].data == 3);

    var ctx = Tree.CountContext{};
    tree.inorderRecurse(tree.root, &ctx);
    try std.testing.expect(ctx.count == 4);
}
