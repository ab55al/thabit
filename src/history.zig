const std = @import("std");

const Buffer = @import("buffer.zig");
const BufferHandle = Buffer.Handle;
const PieceTable = Buffer.PieceTable;
const NaryTree = @import("nary.zig").NaryTree;
const Marks = @import("marks.zig");

pub const HistoryData = struct {
    bhandle: BufferHandle,
    bhandle_marks: []const Marks.KeyPoint,
    file_path_marks: []const Marks.KeyPoint,
    pieces: []const PieceTable.PieceNode.Data,

    pub fn deinit(self: *HistoryData, allocator: std.mem.Allocator) void {
        allocator.free(self.bhandle_marks);
        allocator.free(self.file_path_marks);
        allocator.free(self.pieces);
    }

    pub fn eql(a: HistoryData, b: HistoryData) bool {
        if (a.bhandle.handle != b.bhandle.handle) return false;

        if (a.bhandle_marks.len != b.bhandle_marks.len) return false;
        if (a.file_path_marks.len != b.file_path_marks.len) return false;
        if (a.pieces.len != b.pieces.len) return false;

        for (a.bhandle_marks, b.bhandle_marks) |am, bm|
            if (!std.meta.eql(am, bm)) return false;

        for (a.file_path_marks, b.file_path_marks) |am, bm|
            if (!std.meta.eql(am, bm)) return false;

        for (a.pieces, b.pieces) |ap, bp|
            if (!std.meta.eql(ap, bp)) return false;

        return true;
    }
};

pub const HistoryStack = struct {
    const HistoryTree = NaryTree(HistoryData);
    const HistoryList = std.ArrayListUnmanaged(HistoryData);

    const Self = @This();

    allocator: std.mem.Allocator,
    list: HistoryList = .{},
    index: usize = 0,

    pub fn deinit(self: *Self) void {
        for (self.list.items) |*data|
            data.deinit(self.allocator);

        self.list.deinit(self.allocator);
    }

    pub fn pushHistory(self: *Self, buffer: *Buffer, marks: *Marks) !void {
        const pieces = try buffer.lines.tree.treeToPieceDataArray(self.allocator);
        errdefer self.allocator.free(pieces);

        const bhandle_marks = try marks.toSlice(self.allocator, .{ .bhandle = buffer.handle });
        errdefer self.allocator.free(bhandle_marks);
        const file_path_marks = try marks.toSlice(self.allocator, .{ .file_path = buffer.path.items });
        errdefer self.allocator.free(file_path_marks);

        const data = HistoryData{
            .bhandle = buffer.handle,
            .bhandle_marks = bhandle_marks,
            .file_path_marks = file_path_marks,
            .pieces = pieces,
        };

        if (self.list.items.len == 0) {
            var original_piece = try self.allocator.alloc(PieceTable.PieceNode.Data, 1);
            original_piece[0] = buffer.lines.original_content_piece.pieceData();
            const original_data = HistoryData{
                .bhandle = buffer.handle,
                .bhandle_marks = &.{},
                .file_path_marks = &.{},
                .pieces = original_piece,
            };
            try self.list.append(self.allocator, original_data);
        }

        if (self.list.getLast().eql(data))
            return error.DuplicateHistroy;

        try self.list.append(self.allocator, data);

        self.index = self.list.items.len - 1;
    }

    pub fn undo(self: *Self, buffer: *Buffer, marks: *Marks) !void {
        if (self.list.items.len == 0) return;
        if (self.index == 0) return;
        self.index -= 1;

        const data: HistoryData = self.list.items[self.index];

        try applyData(data, buffer, marks);
    }

    pub fn redo(self: *Self, buffer: *Buffer, marks: *Marks) !void {
        if (self.list.items.len == 0) return;
        if (self.index == self.list.items.len - 1) return;

        self.index += 1;

        try applyData(self.list.items[self.index], buffer, marks);
    }

    fn applyData(data: HistoryData, buffer: *Buffer, marks: *Marks) !void {
        const tree_slice = data.pieces;

        try marks.ensureTotalCapacity(.{ .file_path = buffer.path.items }, data.file_path_marks.len);
        try marks.ensureTotalCapacity(.{ .bhandle = buffer.handle }, data.bhandle_marks.len);

        const new_tree = try PieceTable.SplayTree.treeFromSlice(buffer.allocator, tree_slice);

        buffer.lines.tree.deinitAndSetAsNewTree(buffer.allocator, new_tree);

        marks.updateFromSlice(.{ .file_path = buffer.path.items }, data.file_path_marks) catch unreachable;
        marks.updateFromSlice(.{ .bhandle = buffer.handle }, data.bhandle_marks) catch unreachable;

        buffer.version += 1;
    }
};
