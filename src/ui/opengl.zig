const std = @import("std");

const core = @import("core");

const c = @import("c.zig");
const sdl = @import("sdl2.zig");
const draw = core.draw;
const drawing = draw.drawing;
const thabit_options = core.thabit_options;
const math = @import("math.zig");
const utils = core.utils;

fn thereIsHighImportanceDialog() bool {
    return core.internal.state().input_mode == .dialog and
        core.internal.state().dialogs.queue.items[0].importance == .high;
}

const fragment_shader_source =
    \\#version 330 core
    \\precision mediump float;
    \\
    \\
    \\uniform sampler2D active_texture_unit;
    \\
    \\out vec4 FragColor;
    \\
    \\in vec4 color;
    \\in vec2 texture_uv;
    \\
    \\void main()
    \\{
    \\    vec4 sample = vec4(1,1,1, texture(active_texture_unit, texture_uv).r);
    \\    FragColor = color * sample;
    \\}
;

const shapes_fragment =
    \\#version 330 core
    \\precision mediump float;
    \\
    \\out vec4 FragColor;
    \\
    \\in vec4 color;
    \\
    \\void main()
    \\{
    \\    FragColor = color;
    \\}
;

const gl = struct {
    pub var initialized = false;
    pub var context: std.meta.Child(c.SDL_GLContext) = undefined;

    pub var fonts: draw.Fonts = undefined;

    pub var device: Device = undefined;
    pub var text_atlas: Atlas = undefined;
    pub var drawing_buffers = false;
};

pub fn init(allocator: std.mem.Allocator) !void {
    _ = c.SDL_GL_SetAttribute(c.SDL_GL_DOUBLEBUFFER, 1);
    _ = c.SDL_GL_SetAttribute(c.SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    _ = c.SDL_GL_SetAttribute(c.SDL_GL_CONTEXT_MINOR_VERSION, 3);
    _ = c.SDL_GL_SetAttribute(c.SDL_GL_CONTEXT_PROFILE_MASK, c.SDL_GL_CONTEXT_PROFILE_CORE);

    gl.context = c.SDL_GL_CreateContext(sdl.sdl_window) orelse {
        return error.GLContextCouldNotBeCreated;
    };

    const wrap = struct {
        fn SDL_GL_GetProcAddress(proc: [*c]const u8) callconv(.C) ((*const fn () callconv(.C) void)) {
            return @ptrCast(c.SDL_GL_GetProcAddress(proc));
        }
    };
    _ = c.gladLoadGL(wrap.SDL_GL_GetProcAddress);

    const vertex_shader = try Shader.create(Vertex.vertex_shader_source, .vertex);
    const fragment_shader = try Shader.create(fragment_shader_source, .fragment);
    const shapes_fragment_shader = try Shader.create(shapes_fragment, .fragment);
    const text_program = try ShaderProgram.createAndDeleteShaders(
        allocator,
        &.{ vertex_shader, fragment_shader },
        &.{ "projection", "active_texture_unit" },
    );
    text_program.setUniformi1("active_texture_unit", 0);

    const shapes_program = try ShaderProgram.createAndDeleteShaders(
        allocator,
        &.{ vertex_shader, shapes_fragment_shader },
        &.{"projection"},
    );

    gl.device = Device.init(text_program, shapes_program);
    gl.text_atlas = Atlas.init(allocator);

    gl.fonts = try draw.Fonts.init(allocator);
    try gl.fonts.addFromFile(thabit_options.assets_path ++ "/ProggyClean.ttf", 32);
    try gl.fonts.addFromFile(thabit_options.assets_path ++ "/Amiri-Regular.ttf", 16);

    gl.initialized = true;
}

pub fn deinit() void {
    if (!gl.initialized) @panic("OpenGL not initialized");

    gl.fonts.deinit();

    gl.device.deinit();
    gl.text_atlas.deinit();
    c.gladLoaderUnloadGL();
}

pub fn render(arena: std.mem.Allocator) !void {
    if (!gl.initialized) @panic("OpenGL not initialized");

    c.glDisable(c.GL_CULL_FACE);
    c.glDisable(c.GL_DEPTH_TEST);
    c.glDisable(c.GL_SCISSOR_TEST);

    c.glEnable(c.GL_BLEND);
    c.glBlendFunc(c.GL_SRC_ALPHA, c.GL_ONE_MINUS_SRC_ALPHA);
    c.glBlendEquation(c.GL_FUNC_ADD);

    var w: c_int = 0;
    var h: c_int = 0;
    c.SDL_GetWindowSize(sdl.sdl_window, &w, &h);
    c.glViewport(0, 0, w, h);
    sdl.window_size = .{ .w = @floatFromInt(w), .h = @floatFromInt(h) };

    const projection = math.orthoMatrix(0, @floatFromInt(w), @floatFromInt(h), 0, 0, 1);
    gl.device.text_program.setUniformM4("projection", projection);
    gl.device.shapes_program.setUniformM4("projection", projection);

    c.glClearColor(0.5, 0.5, 0.5, 1);
    c.glClear(c.GL_COLOR_BUFFER_BIT);

    {
        const line_height: f32 = @floatFromInt(gl.fonts.faces.items[0].*.size.*.metrics.height >> 6);
        const common_context = draw.UIContext{
            .allocator = core.getAllocator(),
            .arena = arena,
            .line_height = line_height,
            .mouse_x = sdl.mouse.x,
            .mouse_y = sdl.mouse.y,
            .mouse_clicked = sdl.mouse.clicked,
            .cursor_rect = drawing.focused_cursor_rect,
            .tab_rect = .{},

            .bounding_rect = .{ .w = sdl.window_size.w, .h = sdl.window_size.h },
            .fonts = &gl.fonts,
        };

        var tab = try drawing.drawTabBar(common_context);
        defer tab.draw_list.deinit();

        var status_line = try drawing.drawStatusLine(common_context, sdl.char_queue);
        defer status_line.draw_list.deinit();

        const cli_context = utils.override(common_context, .{
            .bounding_rect = .{ .w = sdl.window_size.w, .h = line_height },
        });
        var cli = try drawing.drawCLI(cli_context);
        defer cli.draw_list.deinit();

        var y: f32 = 0;

        y += tab.size.h;
        status_line.draw_list.translateAllVetices(0, y);
        status_line.cursor_rect = status_line.cursor_rect.offsetY(y);

        y += status_line.size.h;
        cli.draw_list.translateAllVetices(0, y);
        cli.cursor_rect = cli.cursor_rect.offsetY(y);

        y += cli.size.h;

        const buffer_tab = core.internal.selectedTab().*;
        const buffers_height_offset = status_line.size.h + tab.size.h + cli.size.h;
        const buffer_tab_context = utils.override(common_context, .{
            .bounding_rect = .{ .x = 0, .y = y, .w = sdl.window_size.w, .h = @max(0, sdl.window_size.h - buffers_height_offset) },
        });
        const buffers = try drawing.drawBuffersInTab(
            buffer_tab_context,
            buffer_tab,
            draw.Color.rgba(0x32_32_32_FF),
        );
        defer {
            for (buffers.lists) |*list| list.deinit();
        }

        const focused_tab = core.tabs.focusedTab().tab;
        if (core.command_line.isOpen()) {
            drawing.focused_cursor_rect = cli.cursor_rect;
        } else if (focused_tab.hasWindow(focused_tab.focused_window) == .pane) {
            drawing.focused_cursor_rect = buffers.cursor_rect;
        }

        var auto_complete_menu = try drawing.drawAutoCompleteMenu(common_context);
        defer auto_complete_menu.draw_list.deinit();

        var notifications = try drawing.drawNotifications(common_context);
        defer notifications.draw_list.deinit();

        try renderDrawList(arena, tab.draw_list);
        try renderDrawList(arena, status_line.draw_list);
        try renderDrawList(arena, cli.draw_list);
        gl.drawing_buffers = true;
        for (buffers.lists) |list| {
            try renderDrawList(arena, list);
        }
        gl.drawing_buffers = false;
        try renderDrawList(arena, auto_complete_menu.draw_list);
        try renderDrawList(arena, notifications.draw_list);
    }
    //
    //
    c.SDL_GL_SwapWindow(sdl.sdl_window);
}

pub const Batcher = struct {
    allocator: std.mem.Allocator,
    batches: std.ArrayListUnmanaged(Batch) = .{},

    pub fn init(allocator: std.mem.Allocator) Batcher {
        return .{ .allocator = allocator };
    }

    pub fn deinit(batcher: *Batcher) void {
        for (batcher.batches.items) |*batch| batch.deinit();
        batcher.batches.deinit(batcher.allocator);
    }

    pub fn add(batcher: *Batcher, program: ShaderProgram, vertices: []const f32, indices: []const u32) !void {
        if (batcher.batches.items.len == 0 or
            batcher.batches.getLast().program.id != program.id)
        {
            var batch = Batch.init(batcher.allocator, program);
            try batch.add(vertices, indices);
            try batcher.batches.append(batcher.allocator, batch);
            //
        } else if (batcher.batches.getLast().program.id == program.id) {
            var last = &batcher.batches.items[batcher.batches.items.len - 1];
            try last.add(vertices, indices);
        }
    }

    pub fn flushAndRender(batcher: *Batcher, device: Device, texture_id: u32) void {
        for (batcher.batches.items) |*batch| {
            batch.render(device, texture_id);
            batch.deinit();
        }

        batcher.batches.clearRetainingCapacity();
    }

    const Batch = struct {
        vertices: std.ArrayList(f32),
        indices: std.ArrayList(u32),
        max_index: u32 = 0,
        program: ShaderProgram,

        pub fn init(allocator: std.mem.Allocator, program: ShaderProgram) Batch {
            return .{
                .vertices = std.ArrayList(f32).init(allocator),
                .indices = std.ArrayList(u32).init(allocator),
                .program = program,
            };
        }

        pub fn deinit(batch: *Batch) void {
            batch.vertices.deinit();
            batch.indices.deinit();
        }

        pub fn add(batch: *Batch, vertices: []const f32, indices: []const u32) !void {
            const max_indices = 6;
            std.debug.assert(indices.len % 3 == 0);
            std.debug.assert(indices.len <= max_indices);

            var is: [max_indices]u32 = undefined;
            const len = @min(is.len, indices.len);
            for (0..len) |i| {
                is[i] = indices[i] + batch.max_index;
            }

            {
                const old_max = batch.max_index;
                const max_index = std.mem.max(u32, is[0..len]);
                batch.max_index = @max(max_index + 1, old_max);
            }

            try batch.vertices.appendSlice(vertices);
            try batch.indices.appendSlice(is[0..len]);

            { // Making sure indices are provided correctly
                const max_i = std.mem.max(u32, indices);
                const max_qi = comptime std.mem.max(u32, &Vertex.quad_indices);
                const max_ti = comptime std.mem.max(u32, &Vertex.tri_indices);
                std.debug.assert(max_i == max_qi or max_i == max_ti);
            }
        }

        pub fn render(batch: *Batch, device: Device, texture_id: u32) void {
            if (batch.vertices.items.len == 0) return;

            device.copyDataToOpenGL(batch.vertices.items, batch.indices.items);
            batch.program.use();
            c.glBindVertexArray(device.vao);

            c.glActiveTexture(c.GL_TEXTURE0);
            c.glBindTexture(c.GL_TEXTURE_2D, texture_id);

            c.glDrawElements(c.GL_TRIANGLES, @intCast(batch.indices.items.len), c.GL_UNSIGNED_INT, @ptrFromInt(0));

            batch.vertices.clearRetainingCapacity();
            batch.indices.clearRetainingCapacity();
            batch.max_index = 0;
            c.glUseProgram(0);
        }
    };
};

fn renderDrawList(arena: std.mem.Allocator, dl: draw.DrawList) !void {
    _ = arena;
    var batcher = Batcher.init(core.getAllocator());
    defer batcher.deinit();
    const dim_colors = gl.drawing_buffers and thereIsHighImportanceDialog();

    for (dl.shapes.items) |shape| {
        const points = dl.getVertices(shape);

        const color = if (dim_colors)
            drawing.lowDim(shape.color)
        else
            shape.color;

        switch (shape.kind) {
            .remove_clip_rect => {
                batcher.flushAndRender(gl.device, gl.text_atlas.texture_id);
                c.glDisable(c.GL_SCISSOR_TEST);
            },
            .clip_rect => {
                batcher.flushAndRender(gl.device, gl.text_atlas.texture_id);

                const rect = dl.rectFromVertices(shape);
                const y = sdl.window_size.h - (rect.y + rect.h);
                c.glEnable(c.GL_SCISSOR_TEST);
                c.glScissor(
                    @intFromFloat(rect.x),
                    @intFromFloat(y),
                    @intFromFloat(rect.w + 1),
                    @intFromFloat(rect.h + 1),
                );
            },
            .rect => {
                const rect = dl.rectFromVertices(shape);
                if (shape.fill) {
                    const vs = Vertex.createQuad(rect.x, rect.y, rect.w, rect.h, color);
                    try batcher.add(gl.device.shapes_program, &vs, &Vertex.quad_indices);
                } else {
                    for (linesOfRect(rect)) |line| {
                        const vs = Vertex.createLine(line[0], line[1], color);
                        try batcher.add(gl.device.shapes_program, &vs, &Vertex.quad_indices);
                    }
                }
            },

            .line => {
                const vs = Vertex.createLine(points[0], points[1], color);
                try batcher.add(gl.device.shapes_program, &vs, &Vertex.quad_indices);
            },

            .text => |slice| {
                const text = dl.getText(slice);
                var iter = std.unicode.Utf8View.initUnchecked(text).iterator();
                const point = points[0];
                var x = point[0];
                var y = point[1];

                while (iter.nextCodepoint()) |cp| {
                    const glyph = gl.fonts.getOrLoadMetrics(cp) catch |err| switch (err) {
                        error.UndefinedCharacterCode => try gl.fonts.getOrLoadMetrics('?'),
                        else => return err,
                    };

                    const face = gl.fonts.faces.items[glyph.face_index];
                    const face_height: f32 = @floatFromInt(face.*.size.*.metrics.ascender >> 6);
                    const baseline: f32 = (face_height) + y;

                    const offset_to_baseline: f32 = blk: {
                        const gbottom = @as(f32, @floatFromInt(glyph.height)) + y;
                        break :blk baseline - gbottom;
                    };
                    const size_below_baseline: f32 = @floatFromInt(glyph.height - glyph.hori_bearing_y);

                    const x_off: f32 = @floatFromInt((glyph.hori_advance - glyph.width) >> 1);
                    const uv = gl.text_atlas.glyphs.get(cp) orelse blk: {
                        const bitmap = (gl.fonts.getOrLoadBitmap(cp) catch null) orelse (try gl.fonts.getOrLoadBitmap('?')).?;
                        try gl.text_atlas.insert(cp, bitmap);
                        break :blk gl.text_atlas.glyphs.get(cp).?;
                    };

                    const vs = Vertex.createQuadUV(
                        x + x_off,
                        y + offset_to_baseline + size_below_baseline,
                        @floatFromInt(glyph.width),
                        @floatFromInt(glyph.height),
                        color,
                        uv.top_right,
                        uv.bottom_left,
                    );

                    x += @floatFromInt(glyph.hori_advance);
                    if (cp == '\n') {
                        x = point[0];
                        y += @floatFromInt(glyph.vert_advance);
                    }
                    try batcher.add(gl.device.text_program, &vs, &Vertex.quad_indices);
                }
            },
        }
    }

    batcher.flushAndRender(gl.device, gl.text_atlas.texture_id);
}

/// ShaderProgram stores a map of locations of uniforms. The string identifying the uniforms is assumed to be statically
/// stored.
const ShaderProgram = struct {
    const UniformsMap = std.StringHashMap(i32);
    id: u32,
    uniforms: UniformsMap,

    pub fn create(allocator: std.mem.Allocator, shaders: []const Shader, uniforms: []const [:0]const u8) !ShaderProgram {
        var map = UniformsMap.init(allocator);

        try map.ensureUnusedCapacity(@truncate(uniforms.len));

        const id = c.glCreateProgram();
        errdefer c.glDeleteProgram(id);

        for (shaders) |shader| {
            c.glAttachShader(id, shader.id);
        }

        c.glLinkProgram(id);

        var success: c_int = 0;
        c.glGetProgramiv(id, c.GL_LINK_STATUS, &success);

        if (success == 0) {
            var buf: [512:0]u8 = undefined;
            c.glGetProgramInfoLog(id, buf.len, null, &buf);

            const len = std.mem.len(@as([*:0]u8, @ptrCast(&buf)));
            std.log.err("Program linking failed {s}", .{buf[0..len]});
            return error.FailedToLinkProgram;
        }

        for (uniforms) |uniform| {
            const loc = c.glGetUniformLocation(id, uniform);
            map.putAssumeCapacity(uniform, loc);
        }

        return .{ .id = id, .uniforms = map };
    }

    pub fn createAndDeleteShaders(allocator: std.mem.Allocator, shaders: []const Shader, uniforms: []const [:0]const u8) !ShaderProgram {
        const program = create(allocator, shaders, uniforms);
        for (shaders) |shader| shader.delete();
        return program;
    }

    pub fn setUniformM4(program: ShaderProgram, name: []const u8, value: [4 * 4]f32) void {
        program.use();
        const loc = program.getUniform(name);
        c.glUniformMatrix4fv(loc, 1, c.GL_FALSE, &value);
    }

    pub fn setUniformi1(program: ShaderProgram, name: []const u8, value: i32) void {
        program.use();
        const loc = program.getUniform(name);
        c.glUniform1i(loc, value);
    }

    pub fn getUniform(program: ShaderProgram, name: []const u8) i32 {
        const loc = program.uniforms.get(name) orelse @panic("Uniform does not exist");

        var count: i32 = 0;
        c.glGetProgramiv(program.id, c.GL_ACTIVE_ATTRIBUTES, &count);

        var buf: [512]u8 = undefined;
        var len: c_int = 0;
        var size: c_int = 0;
        var vtype: c_int = 0;

        var found = false;
        for (0..@as(usize, @intCast(count))) |i| {
            c.glGetActiveUniform(program.id, @intCast(i), buf.len, &len, &size, @ptrCast(&vtype), &buf);

            const uniform_name = buf[0..@intCast(len)];
            if (std.mem.eql(u8, uniform_name, name)) {
                found = true;
                break;
            }
        }

        if (!found) {
            @panic("Couldn't find uniform in program. Might've been silently removed");
        }

        return loc;
    }

    pub fn use(p: ShaderProgram) void {
        c.glUseProgram(p.id);
    }
};

const Shader = struct {
    id: u32,

    pub fn create(source: [:0]const u8, kind: enum { vertex, fragment }) !Shader {
        const shader_kind = switch (kind) {
            .vertex => c.GL_VERTEX_SHADER,
            .fragment => c.GL_FRAGMENT_SHADER,
        };
        const id: u32 = c.glCreateShader(@intCast(shader_kind));
        errdefer c.glDeleteShader(id);

        const ptr: [*c]const [*c]const u8 = @ptrCast(&.{source.ptr});
        c.glShaderSource(id, 1, ptr, null);
        c.glCompileShader(id);

        var success: c_int = 0;
        c.glGetShaderiv(id, c.GL_COMPILE_STATUS, &success);

        if (success == 0) {
            var buf: [512:0]u8 = undefined;
            c.glGetShaderInfoLog(id, buf.len, null, &buf);

            const len = std.mem.len(@as([*:0]u8, @ptrCast(&buf)));
            std.log.err("Creating shader failed {s}", .{buf[0..len]});
            return error.FailedToCreateShader;
        }

        return .{ .id = id };
    }

    pub fn delete(shader: Shader) void {
        c.glDeleteShader(shader.id);
    }
};

pub const Device = struct {
    vao: u32,
    vbo: u32,
    ebo: u32,

    text_program: ShaderProgram,
    shapes_program: ShaderProgram,

    pub fn init(text_program: ShaderProgram, shapes_program: ShaderProgram) Device {
        var device = Device{
            .vao = undefined,
            .vbo = undefined,
            .ebo = undefined,
            .text_program = text_program,
            .shapes_program = shapes_program,
        };

        c.glGenVertexArrays(1, &device.vao);
        c.glGenBuffers(1, &device.vbo);
        c.glGenBuffers(1, &device.ebo);

        c.glBindVertexArray(device.vao);

        c.glBindBuffer(c.GL_ARRAY_BUFFER, device.vbo);
        c.glBindBuffer(c.GL_ELEMENT_ARRAY_BUFFER, device.ebo);

        Vertex.setVertexAttributes();

        c.glBindVertexArray(0);
        c.glBindBuffer(c.GL_ARRAY_BUFFER, 0);
        c.glBindBuffer(c.GL_ELEMENT_ARRAY_BUFFER, 0);

        return device;
    }

    pub fn deinit(device: *Device) void {
        device.shapes_program.uniforms.deinit();
        device.text_program.uniforms.deinit();
    }

    pub fn copyDataToOpenGL(device: Device, vertices: []const f32, indices: []const u32) void {
        c.glBindVertexArray(device.vao);
        c.glBindBuffer(c.GL_ARRAY_BUFFER, device.vbo);
        c.glBindBuffer(c.GL_ELEMENT_ARRAY_BUFFER, device.ebo);

        const v_size: c_long = @intCast(@sizeOf(f32) * vertices.len);
        const e_size: c_long = @intCast(@sizeOf(u32) * indices.len);

        c.glBufferData(c.GL_ARRAY_BUFFER, v_size, vertices.ptr, c.GL_DYNAMIC_DRAW);
        c.glBufferData(c.GL_ELEMENT_ARRAY_BUFFER, e_size, indices.ptr, c.GL_DYNAMIC_DRAW);

        _ = c.glUnmapBuffer(c.GL_ARRAY_BUFFER);
        _ = c.glUnmapBuffer(c.GL_ELEMENT_ARRAY_BUFFER);
    }
};

pub const Vertex = extern struct {
    pub const vertex_array_size = 8;
    pub const VertexArray = [vertex_array_size]f32;
    comptime {
        if (@sizeOf(Vertex) != @sizeOf(VertexArray))
            @compileError("Vertex and VertexArray do not match");
    }

    pub const quad_indices = [6]u32{ 0, 1, 2, 2, 3, 0 };
    pub const tri_indices = [3]u32{ 0, 1, 2 };
    pub const vertex_shader_source =
        \\#version 330 core
        \\precision highp float;
        \\
        \\layout (location = 0) in vec2 attr_pos;
        \\layout (location = 1) in vec2 attr_text_uv;
        \\layout (location = 2) in vec4 attr_color;
        \\
        \\uniform mat4 projection;
        \\
        \\out vec4 color;
        \\out vec2 texture_uv;
        \\
        \\void main()
        \\{
        \\    gl_Position = projection * vec4(attr_pos, 0, 1);
        \\    color = attr_color;
        \\    texture_uv = attr_text_uv;
        \\}
    ;

    position: [2]f32,
    text_uv: [2]f32,
    color: [4]f32,

    pub fn setVertexAttributes() void {
        const T = Vertex;

        inline for (std.meta.fields(T), 0..) |field, pos| {
            const offset: *allowzero anyopaque = @ptrFromInt(@offsetOf(T, field.name));
            const len = @typeInfo(field.type).Array.len;

            c.glEnableVertexAttribArray(pos);
            c.glVertexAttribPointer(pos, len, c.GL_FLOAT, c.GL_FALSE, @sizeOf(T), offset);
        }
    }

    pub fn createLine(p0: [2]f32, p1: [2]f32, color: draw.Color) [4 * vertex_array_size]f32 {
        if (p0[0] == p1[0]) {
            return createQuad(p0[0], p0[1], 1, @abs(p0[1] - p1[1]), color);
        } else if (p0[1] == p1[1]) {
            return createQuad(p0[0], p0[1], @abs(p0[0] - p1[0]), 1, color);
        } else { // diagonal line
            const colo = color.asFloatArray();

            const min = if (p0[0] < p1[0] and p0[1] < p1[1]) p0 else p1;
            const max = if (std.mem.eql(f32, &min, &p0)) p1 else p0;

            const p2 = [2]f32{ max[0] + 1, max[1] + 1 };
            const p3 = [2]f32{ min[0] + 1, min[1] + 1 };

            const v0 = Vertex{ .position = p0, .text_uv = .{ 1, 1 }, .color = colo };
            const v1 = Vertex{ .position = p1, .text_uv = .{ 1, 1 }, .color = colo };
            const v2 = Vertex{ .position = p2, .text_uv = .{ 1, 1 }, .color = colo };
            const v3 = Vertex{ .position = p3, .text_uv = .{ 1, 1 }, .color = colo };

            return verticesToArray(4, .{ v0, v1, v2, v3 });
        }
    }

    pub fn createQuad(x: f32, y: f32, w: f32, h: f32, color: draw.Color) [4 * vertex_array_size]f32 {
        return createQuadUV(x, y, w, h, color, .{ 1, 1 }, .{ 1, 1 });
    }

    pub fn createQuadUV(x: f32, y: f32, w: f32, h: f32, color: draw.Color, top_right: [2]f32, bottom_left: [2]f32) [4 * vertex_array_size]f32 {
        const top = top_right[1];
        const right = top_right[0];
        const bottom = bottom_left[1];
        const left = bottom_left[0];

        const colo = color.asFloatArray();
        const v0 = Vertex{
            .position = .{ x + w, y + h },
            .text_uv = .{ right, bottom },
            .color = colo,
        };
        const v1 = Vertex{
            .position = .{ x + w, y },
            .text_uv = .{ right, top },
            .color = colo,
        };
        const v2 = Vertex{
            .position = .{ x, y },
            .text_uv = .{ left, top },
            .color = colo,
        };
        const v3 = Vertex{
            .position = .{ x, y + h },
            .text_uv = .{ left, bottom },
            .color = colo,
        };

        return verticesToArray(4, .{ v0, v1, v2, v3 });
    }

    pub fn createTriangle(p0: [2]f32, p1: [2]f32, p2: [2]f32, color: draw.Color) [3 * vertex_array_size]f32 {
        const colo = color.asFloatArray();
        const v0 = Vertex{
            .position = p0,
            .color = colo,
            .text_uv = .{ 1, 1 },
        };
        const v1 = Vertex{
            .position = p1,
            .color = colo,
            .text_uv = .{ 1, 1 },
        };
        const v2 = Vertex{
            .position = p2,
            .color = colo,
            .text_uv = .{ 1, 1 },
        };

        return verticesToArray(3, .{ v0, v1, v2 });
    }

    pub fn verticesToArray(comptime count: usize, vertices: [count]Vertex) [count * vertex_array_size]f32 {
        var vs: [count * vertex_array_size]f32 = undefined;
        var i: usize = 0;
        var j: usize = 0;
        while (i < vs.len) {
            const v = vertices[j];
            std.mem.copyForwards(f32, vs[i .. i + vertex_array_size], &vertexToArray(v));

            i += vertex_array_size;
            j += 1;
        }

        return vs;
    }

    pub fn vertexToArray(v: Vertex) [vertex_array_size]f32 {
        return v.position ++ v.text_uv ++ v.color;
    }

    pub fn quadIndicesOffset(offset: u32) [6]u32 {
        return .{ 0 + offset, 1 + offset, 2 + offset, 2 + offset, 3 + offset, 0 + offset };
    }

    pub fn triangleIndicesOffset(offset: u32) [3]u32 {
        return .{ 0 + offset, 1 + offset, 2 + offset };
    }
};

pub const Atlas = struct {
    const UV = struct {
        bottom_left: [2]f32,
        top_right: [2]f32,
    };
    const CP = u32;

    texture_id: u32,
    glyphs: std.AutoHashMap(CP, UV),

    width: u32 = 1024,
    height: u32 = 1024,
    row_baseline: u32 = 0,
    tallest_glyph_in_row: u32 = 0,
    row_extent: u32 = 0,

    pub fn init(allocator: std.mem.Allocator) Atlas {
        var texture_id: u32 = undefined;

        c.glPixelStorei(c.GL_UNPACK_ALIGNMENT, 1);
        c.glGenTextures(1, &texture_id);
        c.glBindTexture(c.GL_TEXTURE_2D, texture_id);

        const atlas = Atlas{
            .texture_id = texture_id,
            .glyphs = std.AutoHashMap(CP, UV).init(allocator),
        };

        c.glTexImage2D(
            c.GL_TEXTURE_2D,
            0,
            c.GL_RED,
            @intCast(atlas.width),
            @intCast(atlas.height),
            0,
            c.GL_RED,
            c.GL_UNSIGNED_BYTE,
            null,
        );

        c.glTexParameteri(c.GL_TEXTURE_2D, c.GL_TEXTURE_WRAP_S, c.GL_CLAMP_TO_EDGE);
        c.glTexParameteri(c.GL_TEXTURE_2D, c.GL_TEXTURE_WRAP_T, c.GL_CLAMP_TO_EDGE);
        c.glTexParameteri(c.GL_TEXTURE_2D, c.GL_TEXTURE_MIN_FILTER, c.GL_LINEAR);
        c.glTexParameteri(c.GL_TEXTURE_2D, c.GL_TEXTURE_MAG_FILTER, c.GL_LINEAR);

        c.glBindTexture(c.GL_TEXTURE_2D, 0);

        return atlas;
    }

    pub fn deinit(atlas: *Atlas) void {
        atlas.glyphs.deinit();
    }

    pub fn insert(atlas: *Atlas, cp: u32, bitmap: draw.Fonts.Bitmap) !void {
        try atlas.glyphs.ensureUnusedCapacity(1);

        c.glBindTexture(c.GL_TEXTURE_2D, atlas.texture_id);

        if (atlas.row_extent + bitmap.width > atlas.width) atlas.advanceToNextRow();
        if (atlas.row_baseline + bitmap.rows > atlas.height) {
            @panic("There's no space here!");
        }

        c.glPixelStorei(c.GL_UNPACK_ALIGNMENT, 1);
        c.glTexSubImage2D(
            c.GL_TEXTURE_2D,
            0,
            @intCast(atlas.row_extent), // x offset
            @intCast(atlas.row_baseline), // y offset
            @intCast(bitmap.width),
            @intCast(bitmap.rows),
            c.GL_RED,
            c.GL_UNSIGNED_BYTE,
            bitmap.buffer,
        );

        const uv_coords = uvCoordsInAtlas(bitmap, atlas.row_extent, atlas.row_baseline, atlas.width, atlas.height);
        atlas.glyphs.putAssumeCapacity(cp, uv_coords);

        atlas.row_extent += bitmap.width;
        atlas.tallest_glyph_in_row = @max(atlas.tallest_glyph_in_row, bitmap.rows);

        c.glBindTexture(c.GL_TEXTURE_2D, 0);
        c.glActiveTexture(0);
    }

    fn advanceToNextRow(atlas: *Atlas) void {
        atlas.row_baseline += atlas.tallest_glyph_in_row;
        atlas.tallest_glyph_in_row = 0;
        atlas.row_extent = 0;
    }

    pub fn uvCoordsInAtlas(bitmap: draw.Fonts.Bitmap, x_offset_in_atlas: u32, y_offset_in_atlas: u32, atlas_width: u32, atlas_height: u32) UV {
        const bl = [2]f32{
            uOffset(@floatFromInt(x_offset_in_atlas), @floatFromInt(atlas_width)),
            vOffset(@floatFromInt(y_offset_in_atlas + bitmap.rows), @floatFromInt(atlas_height)),
        };
        const tr = [2]f32{
            uOffset(@floatFromInt(x_offset_in_atlas + bitmap.width), @floatFromInt(atlas_width)),
            vOffset(@floatFromInt(y_offset_in_atlas), @floatFromInt(atlas_height)),
        };
        return .{ .bottom_left = bl, .top_right = tr };
    }

    pub fn uOffset(x_offset_in_atlas: f32, atlas_width: f32) f32 {
        return (x_offset_in_atlas) / (atlas_width);
    }
    pub fn vOffset(y_offset_in_atlas: f32, atlas_height: f32) f32 {
        return (y_offset_in_atlas) / (atlas_height);
    }
};

fn linesOfRect(rect: draw.Rect) [4][2][2]f32 {
    const tl = rect.topLeft();
    const tr = [2]f32{ rect.right(), rect.y };
    const br = rect.bottomRight();
    const bl = [2]f32{ rect.x, rect.bottom() };

    const top_to_bottom = [2][2][2]f32{
        .{ tl, bl },
        .{ tr, br },
    };
    const left_to_right = [2][2][2]f32{
        .{ tl, tr },
        .{ bl, br },
    };

    return top_to_bottom ++ left_to_right;
}
