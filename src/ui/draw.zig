const std = @import("std");

pub const drawing = @import("drawing_functions.zig");

pub const Rect = struct {
    pub const Size = struct {
        w: f32 = 0,
        h: f32 = 0,

        pub fn add(a: Size, b: Size) Size {
            return .{ .w = a.w + b.w, .h = a.h + b.h };
        }
    };

    const FieldAsEnum = blk: {
        const EnumField = std.builtin.Type.EnumField;
        var efields: []const EnumField = &[_]EnumField{};
        for (std.meta.fields(Rect), 0..) |field, i| {
            efields = efields ++ &[_]EnumField{.{ .name = field.name, .value = i }};
        }

        break :blk @Type(.{ .Enum = .{
            .tag_type = u8,
            .fields = efields,
            .decls = &.{},
            .is_exhaustive = true,
        } });
    };

    x: f32 = 0,
    y: f32 = 0,
    w: f32 = 0,
    h: f32 = 0,

    pub fn setAll(value: f32) Rect {
        return .{ .x = value, .y = value, .w = value, .h = value };
    }

    pub fn set(rect: Rect, comptime field: FieldAsEnum, value: f32) Rect {
        var r = rect;
        @field(r, @tagName(field)) = value;
        return r;
    }

    pub fn offsetX(rect: Rect, offset: f32) Rect {
        var r = rect;
        r.x += offset;
        return r;
    }

    pub fn offsetY(rect: Rect, offset: f32) Rect {
        var r = rect;
        r.y += offset;
        return r;
    }

    pub fn pad(rect: Rect, padding: f32) Rect {
        return .{
            .x = rect.x + padding,
            .y = rect.y + padding,
            .w = rect.w - padding,
            .h = rect.h - padding,
        };
    }

    pub fn right(rect: Rect) f32 {
        return rect.x + rect.w;
    }
    pub fn bottom(rect: Rect) f32 {
        return rect.y + rect.h;
    }

    pub fn topLeft(rect: Rect) [2]f32 {
        return .{ rect.x, rect.y };
    }

    pub fn bottomRight(rect: Rect) [2]f32 {
        return .{ rect.right(), rect.bottom() };
    }

    pub fn size(rect: Rect) Size {
        return .{ .w = rect.w, .h = rect.h };
    }
};

pub const Color = struct {
    r: u8,
    g: u8,
    b: u8,
    a: u8,

    pub const white = rgba(0xFFFFFFFF);

    pub fn rgba(hex: u32) Color {
        return .{
            .r = @truncate((hex & 0xFF_000000) >> 24),
            .g = @truncate((hex & 0x00_FF_0000) >> 16),
            .b = @truncate((hex & 0x0000_FF_00) >> 8),
            .a = @truncate(hex & 0x000000_FF),
        };
    }

    pub fn argb(hex: u32) Color {
        return .{
            .a = @truncate((hex & 0xFF_000000) >> 24),
            .r = @truncate((hex & 0x00_FF_0000) >> 16),
            .g = @truncate((hex & 0x0000_FF_00) >> 8),
            .b = @truncate(hex & 0x000000_FF),
        };
    }

    pub fn asFloatArray(color: Color) [4]f32 {
        return .{
            (@as(f32, @floatFromInt(color.r)) / 255),
            (@as(f32, @floatFromInt(color.g)) / 255),
            (@as(f32, @floatFromInt(color.b)) / 255),
            (@as(f32, @floatFromInt(color.a)) / 255),
        };
    }
};

pub const DrawList = struct {
    pub const Shape = struct {
        pub const Kind = union(enum) {
            text: Slice,
            rect,
            clip_rect,
            remove_clip_rect,
            line,

            pub fn vertexCount(kind: Kind) usize {
                return switch (kind) {
                    .remove_clip_rect => 0,
                    .clip_rect, .rect => 2,
                    .line => 2,
                    .text => 1,
                };
            }
        };

        pub fn end(shape: Shape) usize {
            return shape.offset + shape.kind.vertexCount();
        }

        offset: usize,
        color: Color,
        kind: Kind,
        fill: bool = false,
    };

    pub const Slice = struct { offset: usize, len: usize };
    pub const Vertex = [2]f32;

    shapes: std.ArrayList(Shape),
    vertices: std.ArrayList(Vertex),
    string_bytes: std.ArrayList(u8),

    pub fn init(allocator: std.mem.Allocator) DrawList {
        return .{
            .shapes = std.ArrayList(Shape).init(allocator),
            .vertices = std.ArrayList(Vertex).init(allocator),
            .string_bytes = std.ArrayList(u8).init(allocator),
        };
    }

    pub fn deinit(list: *DrawList) void {
        list.vertices.deinit();
        list.shapes.deinit();
        list.string_bytes.deinit();
    }

    pub fn addRect(list: *DrawList, rect: Rect, color: Color) !void {
        const kind: Shape.Kind = .rect;
        try list.ensureUnusedCapacity(1, kind.vertexCount());

        list.shapes.appendAssumeCapacity(.{
            .offset = list.vertices.items.len,
            .color = color,
            .kind = kind,
            .fill = false,
        });
        list.vertices.appendSliceAssumeCapacity(&.{ rect.topLeft(), rect.bottomRight() });
    }

    pub fn addRectFill(list: *DrawList, rect: Rect, color: Color) !void {
        const kind: Shape.Kind = .rect;
        try list.ensureUnusedCapacity(1, kind.vertexCount());

        list.shapes.appendAssumeCapacity(.{
            .offset = list.vertices.items.len,
            .color = color,
            .kind = kind,
            .fill = true,
        });
        list.vertices.appendSliceAssumeCapacity(&.{ rect.topLeft(), rect.bottomRight() });
    }

    pub fn addClipRect(list: *DrawList, rect: Rect) !void {
        const kind: Shape.Kind = .clip_rect;
        try list.ensureUnusedCapacity(1, kind.vertexCount());

        list.shapes.appendAssumeCapacity(.{
            .offset = list.vertices.items.len,
            .color = Color.white,
            .kind = kind,
        });
        list.vertices.appendSliceAssumeCapacity(&.{ rect.topLeft(), rect.bottomRight() });
    }

    pub fn removeClipRect(list: *DrawList) !void {
        const kind: Shape.Kind = .remove_clip_rect;
        try list.ensureUnusedCapacity(1, kind.vertexCount());

        list.shapes.appendAssumeCapacity(.{
            .offset = 0,
            .color = Color.white,
            .kind = kind,
        });
    }

    pub fn addLine(list: *DrawList, p1: Vertex, p2: Vertex, color: Color) !void {
        const kind: Shape.Kind = .line;
        try list.ensureUnusedCapacity(1, kind.vertexCount());

        list.shapes.appendAssumeCapacity(.{
            .offset = list.vertices.items.len,
            .color = color,
            .kind = kind,
        });
        list.vertices.appendSliceAssumeCapacity(&.{ p1, p2 });
    }

    pub fn addText(list: *DrawList, p1: Vertex, text: []const u8, color: Color) !void {
        try list.string_bytes.ensureUnusedCapacity(text.len);

        const slice = Slice{ .offset = list.string_bytes.items.len, .len = text.len };
        const kind: Shape.Kind = .{ .text = slice };

        try list.ensureUnusedCapacity(1, kind.vertexCount());

        list.string_bytes.appendSliceAssumeCapacity(text);
        list.shapes.appendAssumeCapacity(.{
            .offset = list.vertices.items.len,
            .color = color,
            .kind = kind,
        });
        list.vertices.appendAssumeCapacity(p1);
    }

    pub fn getText(list: *const DrawList, slice: Slice) []const u8 {
        return list.string_bytes.items[slice.offset .. slice.offset + slice.len];
    }

    pub fn getVertices(list: *const DrawList, shape: Shape) []const [2]f32 {
        if (shape.kind.vertexCount() == 0) return &.{};

        return list.vertices.items[shape.offset..shape.end()];
    }

    pub fn ensureUnusedCapacity(list: *DrawList, shapes: usize, vertices: usize) !void {
        try list.shapes.ensureUnusedCapacity(shapes);
        try list.vertices.ensureUnusedCapacity(vertices);
    }

    pub fn rectFromVertices(list: *const DrawList, shape: Shape) Rect {
        std.debug.assert(shape.kind == .rect or shape.kind == .clip_rect);

        const vertices = list.vertices.items[shape.offset .. shape.offset + shape.kind.vertexCount()];
        const left_top = vertices[0];
        const right_bottom = vertices[1];
        return .{
            .x = left_top[0],
            .y = left_top[1],
            .w = right_bottom[0] - left_top[0],
            .h = right_bottom[1] - left_top[1],
        };
    }

    pub fn translateAllVetices(list: *DrawList, x_offset: f32, y_offset: f32) void {
        for (list.vertices.items, 0..) |_, i| {
            list.vertices.items[i][0] += x_offset;
            list.vertices.items[i][1] += y_offset;
        }
    }

    pub fn joinList(list: *DrawList, new_list: DrawList) !void {
        try list.ensureUnusedCapacity(new_list.shapes.items.len, new_list.vertices.items.len);
        try list.string_bytes.ensureUnusedCapacity(new_list.string_bytes.items.len);

        for (new_list.shapes.items) |shape| {
            const vertices = new_list.getVertices(shape);
            switch (shape.kind) {
                .text => |text| list.addText(vertices[0], new_list.getText(text), shape.color) catch unreachable,
                .rect => {
                    const rect = new_list.rectFromVertices(shape);
                    if (shape.fill) {
                        list.addRectFill(rect, shape.color) catch unreachable;
                    } else {
                        list.addRect(rect, shape.color) catch unreachable;
                    }
                },
                .clip_rect => list.addClipRect(new_list.rectFromVertices(shape)) catch unreachable,
                .remove_clip_rect => list.removeClipRect() catch unreachable,
                .line => list.addLine(vertices[0], vertices[1], shape.color) catch unreachable,
            }
        }
    }
};

const WindowTree = @import("../buffer_window.zig").WindowTree;
const BufferWindowID = @import("../buffer_window.zig").BufferWindow.ID;
const WindowRect = struct {
    rect: Rect,
    window_id: BufferWindowID,
};
pub fn calculateWindowRects(allocator: std.mem.Allocator, tree: *const WindowTree, bounding_rect: Rect) ![]const WindowRect {
    var list = std.ArrayList(WindowRect).init(allocator);
    defer list.deinit();

    const root = tree.tree.root orelse return &.{};
    if (root.data == .split) {
        try calculateWindowRectForSubtree(tree, root, bounding_rect, &list);
    } else {
        try list.append(.{ .window_id = root.data.window, .rect = bounding_rect });
    }

    return try list.toOwnedSlice();
}

pub fn calculateWindowRectForSubtree(tree: *const WindowTree, node: *WindowTree.Node, bounding_rect: Rect, list: *std.ArrayList(WindowRect)) !void {
    if (node.data == .window) {
        unreachable;
    }

    // a split node must always have two children
    const split = node.data.split;
    const left_child = node.left.?;
    const right_child = node.right.?;

    const rects = cutRectOrient(split.orient, split.left_size, split.rightSize(), bounding_rect);

    switch (left_child.data) {
        .window => |id| try list.append(.{ .window_id = id, .rect = rects.left }),
        .split => try calculateWindowRectForSubtree(tree, left_child, rects.left, list),
    }

    switch (right_child.data) {
        .window => |id| try list.append(.{ .window_id = id, .rect = rects.right }),
        .split => try calculateWindowRectForSubtree(tree, right_child, rects.right, list),
    }
}

const CutRectsOrientResult = struct { left: Rect, right: Rect };
pub fn cutRectOrient(orient: WindowTree.Orient, left_size: f32, right_size: f32, bounding_rect: Rect) CutRectsOrientResult {
    const lc_dir = WindowTree.Dir.fromOrient(orient, .left);
    const rc_dir = WindowTree.Dir.fromOrient(orient, .right);
    const left = cutRect(lc_dir, left_size, bounding_rect);
    const right = cutRect(rc_dir, right_size, bounding_rect);

    return .{ .left = left, .right = right };
}

pub fn cutRect(dir: WindowTree.Dir, percent: f32, bounding_rect: Rect) Rect {
    const r = bounding_rect;

    const width = switch (dir) {
        .left, .right => r.w * percent,
        .up, .down => r.w,
    };
    const height = switch (dir) {
        .left, .right => r.h,
        .up, .down => r.h * percent,
    };

    return switch (dir) {
        .left => .{ .x = r.x, .y = r.y, .w = width, .h = height },
        .right => .{ .x = r.x + @abs(width - r.w), .y = r.y, .w = width, .h = height },
        .up => .{ .x = r.x, .y = r.y, .w = width, .h = height },
        .down => .{ .x = r.x, .y = r.y + @abs(height - r.h), .w = width, .h = height },
    };
}

pub const freetype = if (@import("../core.zig").thabit_options.use_freetype)
    @cImport({
        // @cInclude("ft2build.h");
        // @cDefine("#include", "FT_FREETYPE_H");
        @cInclude("freetype/freetype.h");
    })
else
    void;

pub const Fonts = struct {
    pub const Library = std.meta.Child(freetype.FT_Library);
    pub const Face = freetype.FT_Face;

    pub const Bitmap = struct {
        rows: u32,
        width: u32,
        pitch: i32,
        buffer: [*c]u8,
    };

    pub const Glyph = struct {
        face_index: usize,
        index: usize,

        width: isize,
        height: isize,

        hori_bearing_x: isize,
        hori_bearing_y: isize,
        hori_advance: isize,

        vert_bearing_x: isize,
        vert_bearing_y: isize,
        vert_advance: isize,
    };

    allocator: std.mem.Allocator,
    glyphs: std.AutoHashMap(u32, Glyph),
    faces: std.ArrayList(Face),
    lib: Library,

    pub fn init(allocator: std.mem.Allocator) !Fonts {
        var lib: freetype.FT_Library = undefined;
        if (freetype.FT_Init_FreeType(&lib) != 0) {
            return error.FreeTypeFailedToInit;
        }

        return .{
            .allocator = allocator,
            .glyphs = std.AutoHashMap(u32, Glyph).init(allocator),
            .faces = std.ArrayList(Face).init(allocator),
            .lib = lib.?,
        };
    }

    // TODO: Use point sizes
    pub fn addFromFile(font: *Fonts, path: [:0]const u8, size: u32) !void {
        try font.faces.ensureUnusedCapacity(1);
        var face: Face = undefined;

        var err = freetype.FT_New_Face(font.lib, path, 0, &face) != freetype.FT_Err_Ok;
        if (err) return error.CouldNotOpenFont;
        errdefer _ = freetype.FT_Done_Face(face);

        err = freetype.FT_Set_Pixel_Sizes(face, 0, size) != freetype.FT_Err_Ok;
        if (err) return error.CouldNotSetSize;

        font.faces.appendAssumeCapacity(face);
    }

    pub fn deinit(font: *Fonts) void {
        _ = freetype.FT_Done_FreeType(font.lib);
        font.glyphs.deinit();
        font.faces.deinit();
    }

    pub fn textSize(font: *Fonts, text: []const u8) !Rect.Size {
        var iter = std.unicode.Utf8View.initUnchecked(text).iterator();
        var size = Rect.Size{};
        var line_size: f32 = 0;
        while (iter.nextCodepoint()) |cp| {
            const glyph = font.getOrLoadMetrics(cp) catch |err| switch (err) {
                error.UndefinedCharacterCode => try font.getOrLoadMetrics('?'),
                else => |e| return e,
            };

            size.w += @floatFromInt(glyph.hori_advance);
            line_size = @max(@as(f32, @floatFromInt(glyph.vert_advance)), line_size);
            if (cp == '\n') {
                size.h += line_size;
                line_size = 0;
            }
        }

        size.h = @max(size.h, line_size);

        return size;
    }

    pub fn getOrLoadMetrics(font: *Fonts, cp: u32) !Glyph {
        const gop = try font.glyphs.getOrPut(cp);
        if (gop.found_existing) return gop.value_ptr.*;
        errdefer _ = font.glyphs.remove(cp);

        var err: freetype.FT_Error = freetype.FT_Err_Ok;
        var index: freetype.FT_UInt = 0;
        for (font.faces.items, 0..) |face, face_index| {
            index = freetype.FT_Get_Char_Index(face, cp);
            if (index == 0) continue;

            const flags = freetype.FT_LOAD_NO_BITMAP;
            err = freetype.FT_Load_Glyph(face, index, flags);
            if (err != freetype.FT_Err_Ok) continue;

            const metrics = face.*.glyph.*.metrics;
            gop.value_ptr.* = glyphFromMetrics(face_index, index, metrics);
            return gop.value_ptr.*;
        } else {
            if (index == 0) return error.UndefinedCharacterCode;
            if (err != freetype.FT_Err_Ok) return error.CouldNotLoadGlyph;
        }

        return error.UnknownFreeTypeError;
    }

    pub fn getOrLoadBitmap(font: *Fonts, cp: u32) !?Bitmap {
        var load_err = false;
        var render_err = false;
        var index: freetype.FT_UInt = 0;
        for (font.faces.items) |face| {
            index = freetype.FT_Get_Char_Index(face, cp);
            if (index == 0) continue;

            const flags = freetype.FT_LOAD_RENDER;
            load_err = freetype.FT_Load_Glyph(face, index, flags) != freetype.FT_Err_Ok;
            if (load_err) continue;

            render_err = freetype.FT_Render_Glyph(face.*.glyph, freetype.FT_RENDER_MODE_NORMAL) != freetype.FT_Err_Ok;
            if (render_err) continue;

            const bitmap = face.*.glyph.*.bitmap;
            return Bitmap{
                .rows = bitmap.rows,
                .width = bitmap.width,
                .pitch = bitmap.pitch,
                .buffer = bitmap.buffer orelse return null,
            };
        } else {
            if (index == 0) return error.UndefinedCharacterCode;
            if (load_err) return error.CouldNotLoadGlyph;
            if (render_err) return error.CouldNotRenderGlyph;
        }

        return error.UnknownFreeTypeError;
    }

    pub fn yOffset(font: Fonts, g: Glyph) isize {
        const bbox_ymax = font.face.*.bbox.yMax >> 6;
        return bbox_ymax - (g.hori_bearing_y >> 6);
    }

    pub fn xOffset(_: Fonts, g: Glyph) isize {
        const advance = g.hori_advance >> 6;
        return (advance - g.width) >> 1;
    }

    // fn cloneBitmapFromCurrentFace(font: *Font) !Bitmap {
    //     const bitmap = font.face.*.glyph.*.bitmap;
    //     return Bitmap{
    //         .rows = bitmap.rows,
    //         .width = bitmap.width,
    //         .pitch = bitmap.pitch,
    //         .buffer = try font.allocator.dupe(u8, bitmap.buffer0[bitmap.rows * bitmap.pitch]),
    //     };
    // }

    fn glyphFromMetrics(face_index: usize, index: usize, metrics: freetype.FT_Glyph_Metrics) Glyph {
        return Glyph{
            .face_index = face_index,
            .index = index,
            .width = metrics.width >> 6,
            .height = metrics.height >> 6,

            .hori_bearing_x = metrics.horiBearingX >> 6,
            .hori_bearing_y = metrics.horiBearingY >> 6,
            .hori_advance = metrics.horiAdvance >> 6,

            .vert_bearing_x = metrics.vertBearingX >> 6,
            .vert_bearing_y = metrics.vertBearingY >> 6,
            .vert_advance = metrics.vertAdvance >> 6,
        };
    }
};

pub const UIContext = struct {
    allocator: std.mem.Allocator,
    arena: std.mem.Allocator,

    mouse_x: f32,
    mouse_y: f32,
    mouse_clicked: bool,
    cursor_rect: Rect,

    tab_rect: Rect,
    bounding_rect: Rect,
    line_height: f32,

    fonts: *Fonts,
};
