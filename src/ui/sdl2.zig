const std = @import("std");

const core = @import("core");
const utils = core.utils;
const draw = core.draw;
const DrawList = draw.DrawList;

const Rect = core.draw.Rect;
const Size = core.draw.Rect.Size;
const Color = core.draw.Color;
const rgba = core.draw.Color.rgba;

const c = @import("c.zig");

fn maxVisibleRows(height: f32, line_height: f32) f32 {
    return (height / line_height) + 1;
}

fn getBufferWindowPtr(id: core.BufferWindow.ID) ?*core.BufferWindow {
    return core.internal.state().buffer_windows.getPtrBy(id);
}

fn thereIsHighImportanceDialog() bool {
    return core.internal.state().input_mode == .dialog and
        core.internal.state().dialogs.queue.items[0].importance == .high;
}

var conf = &core.config.config;

pub var initialized = false;

pub var key_queue = core.KeyQueue.init(0) catch unreachable;
pub var char_queue = core.CharQueue.init(0) catch unreachable;

pub var sdl_window: *c.SDL_Window = undefined;
pub var window_size = Size{ .w = 500, .h = 500 };

pub const mouse = struct {
    pub var x: f32 = -1;
    pub var y: f32 = -1;
    pub var clicked = false;
};

var events = std.BoundedArray(c.SDL_Event, 1024).init(0) catch unreachable;

pub fn init() !void {
    if (c.SDL_Init(c.SDL_INIT_EVENTS | c.SDL_INIT_VIDEO) < 0) {
        return error.SDLFailedToInitialize;
    }

    sdl_window = c.SDL_CreateWindow(
        "Thabit",
        c.SDL_WINDOWPOS_UNDEFINED,
        c.SDL_WINDOWPOS_UNDEFINED,
        @intFromFloat(window_size.w),
        @intFromFloat(window_size.h),
        c.SDL_WINDOW_SHOWN | c.SDL_WINDOW_RESIZABLE | c.SDL_WINDOW_OPENGL,
    ) orelse return error.SDLWindowFailedToBeCreated;

    initialized = true;
}

pub fn deinit() void {
    if (!initialized) @panic("SDL not initialized");

    c.SDL_DestroyWindow(sdl_window);
    c.SDL_Quit();
}

pub fn poll() void {
    var event: c.SDL_Event = undefined;
    while (c.SDL_PollEvent(&event) == 1) {
        events.append(event) catch break;
    }

    handleEvents();
}

pub fn waitEvent() void {
    var event: c.SDL_Event = undefined;
    _ = c.SDL_WaitEvent(&event);
    events.append(event) catch std.log.warn("Event queue full", .{});
    poll();
}

pub fn waitEventTimeout(time: usize) void {
    var event: c.SDL_Event = undefined;
    _ = c.SDL_WaitEventTimeout(&event, @truncate(@as(isize, @intCast(time))));
    events.append(event) catch std.log.warn("Event queue full", .{});

    poll();
}

fn handleEvents() void {
    if (!initialized) @panic("SDL not initialized");

    mouse.clicked = false;
    defer events.resize(0) catch unreachable;
    for (events.slice()) |event| {
        switch (event.type) {
            c.SDL_QUIT => {
                core.exit();
                return;
            },
            c.SDL_KEYDOWN => {
                const key = sdlKeyToThabitKey(event.key.keysym);
                if (key.kind != .unknown)
                    key_queue.append(key) catch continue;

                core.extraFrames(.one);
            },
            c.SDL_KEYUP => {},
            c.SDL_TEXTINPUT => {
                const len = blk: {
                    for (event.text.text, 0..) |b, i| {
                        if (b == 0) break :blk i;
                    }
                    continue;
                };
                var iter = (std.unicode.Utf8View.init(event.text.text[0..len]) catch continue).iterator();
                while (iter.nextCodepoint()) |cp| {
                    char_queue.append(cp) catch continue;
                }

                core.extraFrames(.one);
            },
            c.SDL_WINDOWEVENT => _ = c.SDL_SetWindowSize(sdl_window, event.window.data1, event.window.data2),

            c.SDL_MOUSEMOTION, c.SDL_MOUSEBUTTONDOWN, c.SDL_MOUSEBUTTONUP => {
                core.extraFrames(.two);

                switch (event.type) {
                    c.SDL_MOUSEMOTION => {
                        mouse.x = @floatFromInt(event.motion.x);
                        mouse.y = @floatFromInt(event.motion.y);
                    },
                    c.SDL_MOUSEBUTTONDOWN, c.SDL_MOUSEBUTTONUP => {
                        mouse.x = @floatFromInt(event.button.x);
                        mouse.y = @floatFromInt(event.button.y);
                        mouse.clicked = event.button.clicks == 1;
                    },
                    else => unreachable,
                }
            },

            else => {
                // std.log.info("SDL: Unknown event {}", .{event.type});
                continue;
            },
        }
    }
}

pub fn sdlKeyToThabitKey(sdl_key: c.SDL_Keysym) core.input.Key {
    var mod = core.input.Modifiers.none;
    if (eqlBitAnd(sdl_key.mod, c.KMOD_LSHIFT) or eqlBitAnd(sdl_key.mod, c.KMOD_RSHIFT)) {
        mod = mod.add(.shift);
    }

    if (eqlBitAnd(sdl_key.mod, c.KMOD_LCTRL) or eqlBitAnd(sdl_key.mod, c.KMOD_RCTRL)) {
        mod = mod.add(.control);
    }

    if (eqlBitAnd(sdl_key.mod, c.KMOD_LALT) or eqlBitAnd(sdl_key.mod, c.KMOD_RALT)) {
        mod = mod.add(.alt);
    }

    const kind: core.input.Key.Kind = switch (sdl_key.sym) {
        '`',
        '\'',
        ',',
        '.',
        '-',
        '/',
        ';',
        '=',
        '[',
        ']',
        '\\',
        '0'...'9',
        'a'...'z',
        => |int| @enumFromInt(int),
        //
        c.SDLK_BACKSPACE => .backspace,
        c.SDLK_SPACE => .space,
        c.SDLK_ESCAPE => .escape,
        c.SDLK_RETURN => .enter,
        c.SDLK_TAB => .tab,
        c.SDLK_INSERT => .insert,
        c.SDLK_DELETE => .delete,
        c.SDLK_LEFT => .left,
        c.SDLK_RIGHT => .right,
        c.SDLK_UP => .up,
        c.SDLK_DOWN => .down,
        c.SDLK_PAGEUP => .page_up,
        c.SDLK_PAGEDOWN => .page_down,
        c.SDLK_HOME => .home,
        c.SDLK_END => .end,
        c.SDLK_F1 => .f1,
        c.SDLK_F2 => .f2,
        c.SDLK_F3 => .f3,
        c.SDLK_F4 => .f4,
        c.SDLK_F5 => .f5,
        c.SDLK_F6 => .f6,
        c.SDLK_F7 => .f7,
        c.SDLK_F8 => .f8,
        c.SDLK_F9 => .f9,
        c.SDLK_F10 => .f10,
        c.SDLK_F11 => .f11,
        c.SDLK_F12 => .f12,

        else => |code| blk: {
            _ = code;
            // var name = c.SDL_GetKeyName(sdl_key.sym);
            // std.debug.print("unkown keycode: {} {s}\n", .{ code, name });
            break :blk .unknown;
        },
    };

    return .{ .kind = kind, .mod = mod };
}

fn eqlBitAnd(a: anytype, b: anytype) bool {
    return a & b == b;
}
