const std = @import("std");

const core = @import("../core.zig");
const dmodify = core.dmodify;

const draw = core.draw;
const DrawList = draw.DrawList;
const Rect = draw.Rect;
const Size = draw.Rect.Size;
const Color = draw.Color;
const rgba = draw.Color.rgba;
const utils = core.utils;

var conf = &core.config.config;
pub var global_tab_char_width: f32 = 4;
pub var focused_cursor_rect = Rect{};

pub const DrawResult = struct {
    draw_list: DrawList,
    size: Size = .{ .w = 0, .h = 0 },
    real_size: Size = .{ .w = 0, .h = 0 },
    cursor_rect: Rect = Rect.setAll(0),
};

const UIContext = draw.UIContext;

pub const DrawBufferOptions = struct {
    bg_color: Color = rgba(0x32_32_32_FF),
    line_numbers: @TypeOf(core.config.config.window_line_number),
    max_lines: ?u32 = null,
};

pub const DrawBuffersInTabResult = struct {
    cursor_rect: Rect,
    lists: []DrawList,
};

// TODO: Copy the tabs data so that operation done on it by custom drawers won't affect this function
pub fn drawBuffersInTab(ctx: UIContext, tab: core.internal.Tab, bg_color: Color) !DrawBuffersInTabResult {
    const max_size = ctx.bounding_rect.size();

    const panes = try draw.calculateWindowRects(ctx.arena, &tab.panes, ctx.bounding_rect);

    // for custom drawing
    var done_drawing = std.AutoHashMap(core.BufferWindow.ID, void).init(ctx.arena);

    // draw normal panes
    var list = std.ArrayList(DrawList).init(ctx.arena);
    var cursor_rect = focused_cursor_rect;
    for (panes) |pane| {
        if (core.internal.windowHasCustomDrawing(pane.window_id)) continue;

        const buffer_context = utils.override(ctx, .{
            .bounding_rect = pane.rect,
        });
        const result = try defaultBufferDrawing(buffer_context, pane.window_id, .{
            .bg_color = bg_color,
            .line_numbers = conf.window_line_number,
        });
        try list.append(result.draw_list);

        if (core.focused.bufferWindow().id == pane.window_id.id) {
            cursor_rect = result.cursor_rect;
        }
    }

    // draw custom panes
    for (panes) |pane| {
        if (!core.internal.windowHasCustomDrawing(pane.window_id)) continue;
        if (done_drawing.getKey(pane.window_id)) |_| continue;

        const custom_ctx = utils.override(ctx, .{
            .bounding_rect = pane.rect,
            .tab_rect = ctx.bounding_rect,
        });

        const custom_draw = core.internal.getCustomDrawingFor(pane.window_id).?;
        const result = custom_draw.draw(custom_draw.ptr, custom_ctx) catch continue;
        try list.append(result.draw_list);

        if (core.focused.bufferWindow().id == pane.window_id.id) {
            cursor_rect = result.cursor_rect;
        }

        for (custom_draw.windows) |win|
            try done_drawing.put(win, {});
    }

    // draw normal floatings
    for (tab.floats.items) |float| {
        if (core.internal.windowHasCustomDrawing(float.window)) continue;

        const opts = DrawBufferOptions{
            .bg_color = bg_color,
            .line_numbers = conf.window_line_number,
        };

        const window_rect = percentRectToPixelRect(float.rect, max_size);
        const buffer_context = utils.override(ctx, .{
            .bounding_rect = window_rect,
        });
        const result = try defaultBufferDrawing(buffer_context, float.window, opts);
        try list.append(result.draw_list);

        if (core.focused.bufferWindow().id == float.window.id) {
            cursor_rect = result.cursor_rect;
        }
    }

    // draw custom floatings
    for (tab.floats.items) |float| {
        if (!core.internal.windowHasCustomDrawing(float.window)) continue;
        if (done_drawing.getKey(float.window)) |_| continue;

        const window_rect = percentRectToPixelRect(float.rect, max_size);
        const custom_ctx = utils.override(ctx, .{
            .bounding_rect = window_rect,
            .cursor_rect = cursor_rect,
            .tab_rect = ctx.bounding_rect,
        });

        const custom_draw = core.internal.getCustomDrawingFor(float.window).?;
        const result = custom_draw.draw(custom_draw.ptr, custom_ctx) catch continue;
        try list.append(result.draw_list);

        for (custom_draw.windows) |win|
            try done_drawing.put(win, {});
    }

    return .{ .cursor_rect = cursor_rect, .lists = try list.toOwnedSlice() };
}

pub fn defaultBufferDrawing(ctx: UIContext, window: core.BufferWindow.ID, opts: DrawBufferOptions) !DrawResult {
    var buffer_widget = try bufferWidget(ctx, window, opts);
    defer {
        buffer_widget.selection.deinit();
        buffer_widget.text.deinit();
        buffer_widget.above_text.deinit();
        buffer_widget.line_numbers.deinit();
    }

    var dl = DrawList.init(ctx.allocator);
    try dl.addClipRect(ctx.bounding_rect);
    try dl.addRectFill(ctx.bounding_rect, opts.bg_color);
    try dl.addRect(ctx.bounding_rect, rgba(0xFFFFFF_88));

    try dl.joinList(buffer_widget.selection);
    try dl.joinList(buffer_widget.line_numbers);
    try dl.joinList(buffer_widget.text);
    try dl.joinList(buffer_widget.above_text);

    if (buffer_widget.should_focus) {
        const tab_index = core.tabs.tabOfWindow(window).?.index;
        try dmodify.tabs.setFocusedWindowIn(tab_index, window);
    }

    return .{
        .draw_list = dl,
        .size = .{},
        .real_size = .{},
        .cursor_rect = buffer_widget.cursor_rect,
    };
}

pub fn bufferDrawSelection(font: *draw.Fonts, dl: *DrawList, start: [2]f32, bhandle: core.BufferHandle, cursor: core.Buffer.Point, row: u32, line: []const u8) !void {
    var selection = try core.buffer.getSelection(bhandle);
    const selection_range = selection.get(cursor);

    var srange = selection_range;
    if (selection.kind == .line) srange.end.row -|= 1;

    if (selection.selected() and
        row >= srange.start.row and row <= srange.end.row)
    {
        const line_start_index = try core.buffer.lineStartIndex(bhandle, row);
        const line_end_index = try core.buffer.lineEndIndex(bhandle, row);
        const relative_line_end = line_end_index - line_start_index + 1;

        const start_index = switch (selection.kind) {
            .line => 0,
            .block => utils.getIndexOfCol(line, srange.start.col),
            .regular => if (row > srange.start.row) 0 else utils.getIndexOfCol(line, srange.start.col),
        };
        const end_index = switch (selection.kind) {
            .line => line.len,
            .block => utils.getIndexOfCol(line, srange.end.col),
            .regular => if (row < srange.end.row) relative_line_end else utils.getIndexOfCol(line, srange.end.col),
        };

        const size = try font.textSize(line[start_index..end_index]);
        const x_offset = if (start_index == 0) 0 else (try font.textSize(line[0..start_index])).w;

        const xx = start[0] + x_offset;
        try dl.addRectFill(.{ .x = xx, .y = start[1], .w = size.w, .h = size.h }, rgba(0xFFFFFF_80));
    }
}

pub fn drawTabBar(ctx: UIContext) !DrawResult {
    const window_size = ctx.bounding_rect.size();

    const tab_size = Size{ .w = window_size.w, .h = ctx.line_height };
    const rect = Rect{ .x = 0, .y = 0, .w = tab_size.w, .h = tab_size.h };

    var dl = DrawList.init(ctx.allocator);
    try dl.addClipRect(rect);

    const bg = rgba(0x32_32_80_FF);
    try dl.addRectFill(rect, bg);

    var x: f32 = 0;
    const y: f32 = 0;

    for (core.internal.state().tabs.items, 0..) |tab, i| {
        const root = tab.panes.tree.root.?;
        const node = root.leftMost() orelse root;
        const id = node.data.window;
        const bw = core.internal.state().buffer_windows.getPtrBy(id) orelse continue;
        const data = core.buffer.getBufferData(bw.bhandle) catch continue;
        const basename = std.fs.path.basename(data.path);
        const name = if (basename.len == 0) "[Pathless]" else basename;

        const size = try ctx.fonts.textSize(name);
        var color = Color{ .r = bg.r, .g = bg.g | 0x80, .b = bg.b, .a = bg.a / 2 };

        if (inRect(ctx.mouse_x, ctx.mouse_y, Rect{ .x = x, .y = y, .w = size.w, .h = size.h })) {
            color.a = 0xFF;

            if (bw.id.id == core.focused.bufferWindow().id) {
                color.r = 0xFF;
            }

            if (ctx.mouse_clicked and i != core.internal.selectedTabIndex()) {
                try dmodify.tabs.setFocusedWindowAndTab(tab.focused_window);
            }
        }

        const s = try ctx.fonts.textSize(name);
        try dl.addRectFill(.{ .x = x, .y = y, .w = s.w, .h = s.h }, color);
        try dl.addText(.{ x, y }, name, Color.white);
        x += 5 + size.w;
    }

    return .{ .draw_list = dl, .size = tab_size };
}

pub fn drawStatusLine(ctx: UIContext, char_queue: core.CharQueue) !DrawResult {
    const status_line_bg = rgba(0x80_32_32_FF);
    var text_bg_color = status_line_bg;
    text_bg_color.r = 0xFF;
    text_bg_color.a = 0x50;
    const window_size = ctx.bounding_rect.size();

    switch (core.internal.state().input_mode) {
        .dialog => {
            const dialog = core.internal.state().dialogs.queue.items[0];
            return try drawDialog(ctx, dialog.kind, dialog.message, status_line_bg, char_queue);
        },
        else => {
            const rect = Rect{
                .x = 0,
                .y = 0,
                .w = window_size.w,
                .h = ctx.line_height,
            };

            var dl = DrawList.init(ctx.allocator);
            try dl.addClipRect(rect);
            var x = rect.x;
            const y = rect.y;
            const spacing = (try ctx.fonts.textSize("m")).w;
            const status_line = core.internal.state().status_line;

            try dl.addRectFill(.{ .x = x, .y = y, .w = window_size.w, .h = ctx.line_height }, status_line_bg);

            for (status_line.left.items) |widget| {
                if (widget.value.items.len == 0) continue;
                const size = try ctx.fonts.textSize(widget.value.items);
                try dl.addRectFill(.{ .x = x, .y = y, .w = size.w, .h = size.h }, text_bg_color);
                try dl.addText(.{ x, y }, widget.value.items, Color.white);
                x += size.w + spacing;
            }

            const mid_width = blk: {
                var res: f32 = 0;
                for (status_line.mid.items) |widget|
                    res += (try ctx.fonts.textSize(widget.value.items)).w;

                break :blk res;
            };

            x = @floor(center(rect, mid_width));

            for (status_line.mid.items) |widget| {
                if (widget.value.items.len == 0) continue;

                const size = try ctx.fonts.textSize(widget.value.items);
                try dl.addRectFill(.{ .x = x, .y = y, .w = size.w, .h = size.h }, text_bg_color);
                try dl.addText(.{ x, y }, widget.value.items, Color.white);

                x += size.w + spacing;
            }

            const right_width = blk: {
                var res: f32 = 0;
                for (status_line.right.items) |widget|
                    res += (try ctx.fonts.textSize(widget.value.items)).w;

                break :blk res;
            };

            x = @floor(rightAlign(rect, right_width));

            for (status_line.right.items) |widget| {
                if (widget.value.items.len == 0) continue;
                const size = try ctx.fonts.textSize(widget.value.items);
                try dl.addRectFill(.{ .x = x, .y = y, .w = size.w, .h = size.h }, text_bg_color);
                try dl.addText(.{ x, y }, widget.value.items, Color.white);
                x += size.w + spacing;
            }

            return .{ .draw_list = dl, .size = .{ .w = window_size.w, .h = ctx.line_height } };
        },
    }
}

pub fn drawDialog(ctx: UIContext, dialog_kind: core.Dialog.Kind, dialog_message: []const u8, bg_color: Color, char_queue: core.CharQueue) !DrawResult {
    const window_size = ctx.bounding_rect.size();
    var text_bg_color = bg_color;
    text_bg_color.r = 0xFF;
    text_bg_color.a = 0x50;

    var message_result = try drawDialogMessage(ctx, dialog_message, bg_color, text_bg_color);
    defer message_result.draw_list.deinit();

    const rect = Rect{
        .x = 0,
        .y = message_result.size.h,
        .w = window_size.w,
        .h = ctx.line_height,
    };

    var max_height = message_result.size.h;

    const x = rect.x;
    const y = rect.y;

    var dl = DrawList.init(ctx.allocator);
    defer dl.deinit();
    try dl.joinList(message_result.draw_list);
    switch (dialog_kind) {
        .key => |key| {
            var result = try drawDialogKey(ctx, key, bg_color);
            defer result.draw_list.deinit();

            result.draw_list.translateAllVetices(0, max_height);
            try dl.joinList(result.draw_list);

            max_height += result.size.h;
        },
        .text => {
            var buf: [core.internal.char_queue_size * 4:0]u8 = undefined;
            var i: usize = 0;

            for (char_queue.slice()) |cp| {
                i += std.unicode.utf8Encode(cp, buf[i..]) catch unreachable;
            }
            buf[i] = 0;

            const size = try ctx.fonts.textSize(buf[0..i]);
            try dl.addRectFill(.{ .x = x, .y = y, .w = size.w, .h = size.h }, text_bg_color);
            try dl.addText(.{ x, y }, buf[0..i], Color.white);
        },
    }

    var clip = DrawList.init(ctx.allocator);
    const r = Rect{ .x = 0, .y = 0, .w = window_size.w, .h = max_height };
    try clip.addClipRect(r);
    try clip.addRectFill(r, bg_color);
    try clip.joinList(dl);

    return .{ .draw_list = clip, .size = .{ .w = window_size.w, .h = max_height } };
}

pub fn drawDialogMessage(ctx: UIContext, message: []const u8, bg_color: Color, text_bg_color: Color) !DrawResult {
    _ = text_bg_color;
    var dl = DrawList.init(ctx.allocator);
    const window_size = ctx.bounding_rect.size();

    const rect = Rect{
        .x = 0,
        .y = 0,
        .w = window_size.w,
        .h = ctx.line_height,
    };

    const x = rect.x;
    var y = rect.y;

    var iter = std.mem.splitScalar(u8, message, '\n');
    while (iter.next()) |string| {
        const size = try ctx.fonts.textSize(string);

        try dl.addRectFill(.{ .x = x, .y = y, .w = window_size.w, .h = size.h }, bg_color);
        try dl.addText(.{ x, y }, string, Color.white);

        y += size.h;
    }

    return .{ .draw_list = dl, .size = .{ .w = window_size.w, .h = y } };
}

pub fn drawDialogKey(ctx: UIContext, key: core.Dialog.Key, bg_color: Color) !DrawResult {
    const window_size = ctx.bounding_rect.size();
    const rect = Rect{
        .x = 0,
        .y = 0,
        .w = window_size.w,
        .h = ctx.line_height,
    };

    var dl = DrawList.init(ctx.allocator);
    var text_bg_color = bg_color;
    text_bg_color.r = 0xFF;
    text_bg_color.a = 0x50;

    try dl.addRectFill(rect, bg_color);

    const m_size = try ctx.fonts.textSize("m");
    var x = rect.x;

    for (key.keys_info) |kd| {
        var buf: [core.input.Key.max_string_len]u8 = undefined;
        const k = try std.fmt.allocPrintZ(ctx.arena, "{s}: {s}", .{ kd.key.toString(&buf), kd.description });

        const size = try ctx.fonts.textSize(k);
        try dl.addRectFill(.{ .x = x, .y = rect.y, .w = size.w, .h = size.h }, text_bg_color);
        try dl.addText(.{ x, rect.y }, k, Color.white);

        x += size.w;
        x += m_size.w;
    }

    return .{ .draw_list = dl, .size = .{ .w = window_size.w, .h = ctx.line_height } };
}

const Element = struct {
    axis: f32 = 0,
    margin: f32 = 0,
    size: f32 = 0,
};

fn layoutOnAxis(elements: []Element, arg_axis: f32) f32 {
    var axis = arg_axis;

    for (elements, 0..) |_, i| {
        const margin = elements[i].margin;
        axis += margin;
        elements[i].axis = axis;
        axis += elements[i].size + margin;
    }

    return axis - arg_axis;
}

pub fn drawCLI(ctx: UIContext) !DrawResult {
    if (!core.command_line.isOpen()) return .{ .draw_list = DrawList.init(ctx.allocator) };

    const bg_color = rgba(0x32_80_32_FF);

    var buffer_widget = try bufferWidget(
        ctx,
        core.command_line.bufferWindow(),
        .{ .bg_color = bg_color, .line_numbers = .none },
    );
    defer {
        buffer_widget.selection.deinit();
        buffer_widget.text.deinit();
        buffer_widget.above_text.deinit();
        buffer_widget.line_numbers.deinit();
    }

    var clip = DrawList.init(ctx.allocator);
    try clip.addClipRect(ctx.bounding_rect);
    try clip.addRectFill(ctx.bounding_rect, bg_color);
    try clip.addRect(ctx.bounding_rect, bg_color);

    try clip.joinList(buffer_widget.selection);
    try clip.joinList(buffer_widget.text);
    try clip.joinList(buffer_widget.above_text);

    return .{
        .draw_list = clip,
        .size = buffer_widget.bounding_rect.size(),
        .real_size = buffer_widget.bounding_rect.size(),
        .cursor_rect = buffer_widget.cursor_rect,
    };
}

pub fn drawAutoCompleteMenu(ctx: UIContext) !DrawResult {
    const engine = &core.builtin.auto_complete.engine_state;
    const entries_range = engine.viewRange();
    const entries_count = engine.nonFilteredEntriesCount();

    var dl = DrawList.init(ctx.allocator);
    try dl.removeClipRect();

    if (entries_range.start == entries_range.end) return .{ .draw_list = dl };
    const cursor_rect = focused_cursor_rect;
    const selected_index = blk: {
        const index = engine.view.selected_index orelse break :blk null;
        break :blk index - engine.view.offset;
    };

    var max_label_size = Size{ .w = 0, .h = 0 };
    var max_kind_size = Size{ .w = 0, .h = 0 };
    var max_source_size = Size{ .w = 0, .h = 0 };

    for (0..entries_count) |entry_index| {
        const entry = engine.getNonFilteredEntry(entry_index);

        const label_size = try ctx.fonts.textSize(entry.label);
        const kind_size = try ctx.fonts.textSize(entry.kind);
        const source_size = try ctx.fonts.textSize(entry.source);

        max_label_size.w = @max(max_label_size.w, label_size.w);
        max_label_size.h = @max(max_label_size.h, label_size.h);

        max_kind_size.w = @max(max_kind_size.w, kind_size.w);
        max_kind_size.h = @max(max_kind_size.h, kind_size.h);

        max_source_size.w = @max(max_source_size.w, source_size.w);
        max_source_size.h = @max(max_source_size.h, source_size.h);
    }

    const m_size = try ctx.fonts.textSize("m");

    const hmargin = @floor(m_size.w / 4);
    var elements = [_]Element{
        .{ .margin = hmargin, .size = max_label_size.w }, // label
        .{ .margin = @floor(m_size.w / 2), .size = 1 }, // sep
        .{ .margin = hmargin, .size = max_kind_size.w }, // kind
        .{ .margin = @floor(m_size.w / 2), .size = 1 }, // sep
        .{ .margin = hmargin, .size = max_source_size.w }, // source
    };
    const total_width = layoutOnAxis(&elements, cursor_rect.x);

    const seperators = [_]f32{ elements[1].axis, elements[3].axis };
    const label_x = elements[0].axis;
    const kind_x = elements[2].axis;
    const source_x = elements[4].axis;

    const vpadding = m_size.h / 2;
    const line_height = ctx.line_height;
    var rect = Rect{
        .x = cursor_rect.x,
        .y = cursor_rect.bottom(),
        .w = total_width,
        .h = vpadding + line_height * @as(f32, @floatFromInt(entries_range.end - entries_range.start)),
    };

    const pos = getPosBelowOrAboveCursor(cursor_rect, rect.h, ctx.bounding_rect.h);
    rect.y = pos[1];

    const bg_color = rgba(0x22_22_22_FF);
    try dl.addRectFill(rect, bg_color);

    var y = rect.y + @floor(vpadding / 2);
    for (entries_range.start..entries_range.end, 0..) |entry_index, i| {
        const entry = engine.getNonFilteredEntry(entry_index);

        if (selected_index != null and selected_index.? == i) {
            try dl.addRectFill(
                .{ .x = rect.x, .y = y, .w = rect.w, .h = line_height },
                rgba(0x00_FF_00_55),
            );
        }

        const color = rgba(0xFFFFFF_FF);

        try dl.addText(.{ label_x, y }, entry.label, color);
        try dl.addText(.{ kind_x, y }, entry.kind, color);
        try dl.addText(.{ source_x, y }, entry.source, color);

        y += line_height;
    }

    for (seperators) |sep_x| {
        try dl.addLine(
            .{ sep_x, rect.y + 1 },
            .{ sep_x, @floor(rect.bottom() - 2) },
            rgba(0xFFFFFF_33),
        );
    }

    try dl.addRect(rect, rgba(0xFFFFFF_FF));

    // render the selected text
    blk: {
        const entry = engine.getSelectedEntry() orelse break :blk;
        var max_etext_size = Size{ .w = 0, .h = 0 };
        {
            var iter = std.mem.splitScalar(u8, entry.text, '\n');
            while (iter.next()) |line| {
                const size = try ctx.fonts.textSize(line);
                max_etext_size.w = @max(max_etext_size.w, size.w);
                max_etext_size.h += size.h;
            }
        }

        const text_rect = Rect{
            .x = rect.right(),
            .y = rect.y,
            .w = max_etext_size.w,
            .h = max_etext_size.h,
        };

        try dl.addRectFill(text_rect, bg_color);
        try dl.addRect(text_rect, rgba(0xFFFFFF_FF));

        var text_y = text_rect.y;
        var iter = std.mem.splitScalar(u8, entry.text, '\n');
        while (iter.next()) |line| {
            try dl.addText(.{ text_rect.x, text_y }, line, Color.white);
            text_y += line_height;
        }
    }

    return .{ .draw_list = dl };
}

pub fn drawNotifications(ctx: UIContext) !DrawResult {
    var gs = core.internal.state();

    var dl = DrawList.init(ctx.allocator);
    try dl.removeClipRect();
    const width: f32 = blk: {
        var iter = gs.notifications.data.iterator();
        var result: f32 = 0;
        while (iter.next()) |kv| {
            const n = kv.key_ptr.*;
            const title = try if (n.duplicates > 0)
                std.fmt.allocPrint(ctx.arena, "({}) [{d:.0}] {s}", .{ n.duplicates, n.remaining_time, n.title })
            else
                std.fmt.allocPrint(ctx.arena, "[{d:.0}] {s}", .{ n.remaining_time, n.title });

            const title_size = try ctx.fonts.textSize(title);
            const message_size = try ctx.fonts.textSize(n.message);

            const size = @max(title_size.w, message_size.w);
            result = @max(result, size);
        }
        break :blk result;
    };

    const x = ctx.bounding_rect.x + (ctx.bounding_rect.w - width);
    const padding: f32 = 5;
    var y = ctx.bounding_rect.y + (padding * 2);

    var iter = gs.notifications.data.iterator();
    while (iter.next()) |kv| {
        var n = kv.key_ptr;
        if (n.remaining_time <= 0) continue;

        const text = try if (n.duplicates > 0)
            std.fmt.allocPrint(ctx.arena, "({}) [{d:.0}] {s}\n{s}", .{ n.duplicates, n.remaining_time, n.title, n.message })
        else
            std.fmt.allocPrint(ctx.arena, "[{d:.0}] {s}\n{s}", .{ n.remaining_time, n.title, n.message });

        const size = try ctx.fonts.textSize(text);

        const notiy_rect = Rect{
            .x = x - (padding * 2),
            .y = y - padding,
            .w = width + padding,
            .h = (size.h * 2) + padding,
        };

        const mouse_over_notiy = inRect(ctx.mouse_x, ctx.mouse_y, notiy_rect);

        var alpha: u32 = 0x000000_FF;
        var bg: u32 = 0x000000_FF;

        if (mouse_over_notiy) {
            bg = 0xFFFFFF_11;
            alpha = 0x000000_66;
        }

        if (mouse_over_notiy and ctx.mouse_clicked) {
            n.remaining_time = 0;
        }

        try dl.addRectFill(notiy_rect, rgba(bg));
        try dl.addRectFill(notiy_rect, rgba(0xFFFFFF_55));

        const text_color = rgba(0xFFFFFF_00 | alpha);
        try dl.addText(.{ x - padding, y }, text, text_color);
        y += notiy_rect.h + (padding);
    }

    return .{ .draw_list = dl };
}

////////////////////////////////////////////////////////////////////////////////

pub fn lowDim(color: Color) Color {
    return .{
        .r = color.r -| (color.r / 3),
        .g = color.g -| (color.g / 3),
        .b = color.b -| (color.b / 3),
        .a = color.a,
    };
}

pub fn getCursorRect(font: *draw.Fonts, x: f32, y: f32, text: []const u8) !Rect {
    const size = try font.textSize(text);
    return switch (conf.cursor.shape) {
        .thin => .{ .x = x, .y = y, .w = 1, .h = size.h },
        .thick => .{ .x = x, .y = y, .w = size.w, .h = size.h },
        .underline => .{ .x = x, .y = y + size.h - 1, .w = size.w, .h = 2 },
    };
}

pub fn inRect(x: f32, y: f32, rect: Rect) bool {
    return utils.inRange(x, rect.x, rect.right()) and
        utils.inRange(y, rect.y, rect.bottom());
}

fn rightAlign(rect: Rect, item_width: f32) f32 {
    return rect.x + (rect.w - item_width);
}

fn center(rect: Rect, item_width: f32) f32 {
    return rect.x + ((rect.w / 2) - (item_width / 2));
}

fn maxVisibleRows(height: f32, line_height: f32) f32 {
    return (height / line_height) + 1;
}

fn getBufferWindowPtr(id: core.BufferWindow.ID) ?*core.BufferWindow {
    return core.internal.state().buffer_windows.getPtrBy(id);
}

fn thereIsHighImportanceDialog() bool {
    return core.internal.state().input_mode == .dialog and
        core.internal.state().dialogs.queue.items[0].importance == .high;
}

const DisplayerData = struct {
    left: f32,
    right: f32,
    bottom: f32,
    top: f32,
    slice: []const u8,

    pub fn pmin(self: DisplayerData) [2]f32 {
        return .{ self.left, self.top };
    }

    pub fn pmax(self: DisplayerData) [2]f32 {
        return .{ self.right, self.bottom };
    }
};

/// Figures out where a range's position is
fn displayerData(font: *draw.Fonts, line: []const u8, start_pos: [2]f32, range: core.ColorRange, kind: core.RowInfo.RangeKind) !DisplayerData {
    const byte_range = switch (kind) {
        .col => .{ .start = utils.getIndexOfCol(line, @truncate(range.start)), .end = utils.getIndexOfCol(line, @truncate(range.end)) },
        .byte => .{ .start = range.start, .end = range.end },
    };

    const slice = line[byte_range.start..byte_range.end];
    const slice_size = try font.textSize(slice);

    const x_offset = (try font.textSize(line[0..byte_range.start])).w;

    const left = start_pos[0] + x_offset;
    const right = left + slice_size.w;

    const top = start_pos[1];
    const bottom = start_pos[1] + slice_size.h;

    return .{
        .left = left,
        .right = right,
        .bottom = bottom,
        .top = top,
        .slice = slice,
    };
}

pub fn bufferGetCursorRect(font: *draw.Fonts, bw: *core.BufferWindow, start: [2]f32, line: []const u8, current_row: u32, cursor: core.Buffer.Point) !?Rect {
    if (current_row != cursor.row) return null;

    const index =
        (try core.buffer.getIndex(bw.bhandle, cursor)) -
        (try core.buffer.lineStartIndex(bw.bhandle, cursor.row));

    const cp_len = std.unicode.utf8ByteSequenceLength(line[index]) catch 1;
    const pre_cursor_size = try font.textSize(line[0..index]);
    const rect = try getCursorRect(
        font,
        start[0] + pre_cursor_size.w,
        start[1],
        line[index .. index + cp_len],
    );

    return .{
        .x = rect.x,
        .y = rect.y,
        .w = rect.w,
        .h = rect.h,
    };
}

pub fn getPosBelowOrAboveCursor(cursor_rect: Rect, max_height: f32, os_window_height: f32) [2]f32 {
    const x = cursor_rect.x;
    var y: f32 = 0;

    if (cursor_rect.y <= os_window_height / 2) {
        y = cursor_rect.bottom();
    } else {
        y = cursor_rect.y - max_height;
    }

    return .{ x, y };
}

pub fn percentRectToPixelRect(percent_rect: Rect, size: Size) Rect {
    return .{
        .x = @floor(size.w * percent_rect.x),
        .y = @floor(size.h * percent_rect.y),
        .w = size.w * percent_rect.w,
        .h = size.h * percent_rect.h,
    };
}

pub fn pixelToPercentRect(pixel_rect: Rect, size: Size) Rect {
    return .{
        .x = pixel_rect.x / size.w,
        .y = pixel_rect.y / size.h,
        .w = pixel_rect.w / size.w,
        .h = pixel_rect.h / size.h,
    };
}

const BufferWidget = struct {
    selection: DrawList,
    line_numbers: DrawList,
    text: DrawList,
    above_text: DrawList,

    bounding_rect: Rect,

    cursor_rect: Rect = .{},
    clicked_on_point: ?core.Buffer.Point = null,
    should_focus: bool = false,

    pub fn drawBackground(bounding_rect: Rect, bg_color: Color) DrawList {
        var background = DrawList.init(core.getAllocator());
        try background.addRectFill(bounding_rect, bg_color);
        try background.addRect(bounding_rect, rgba(0xFFFFFF_88));
        return background;
    }
};

pub fn bufferWidget(ctx: UIContext, window: core.BufferWindow.ID, opts: DrawBufferOptions) !BufferWidget {
    const bw = getBufferWindowPtr(window) orelse return error.WindowDoesNotExist;
    const data = try core.buffer.getBufferData(bw.bhandle);
    const config = core.config.get(data.buffer_type);
    const max_size = ctx.bounding_rect.size();
    global_tab_char_width = @floatFromInt(config.tab_width);

    var selection = DrawList.init(ctx.allocator);
    var line_numbers = DrawList.init(ctx.allocator);
    var text = DrawList.init(ctx.allocator);
    var above_text = DrawList.init(ctx.allocator);

    const max_visiable: u32 = @intFromFloat(@floor(maxVisibleRows(max_size.h, ctx.line_height)));
    const last_row: u32 = utils.bound(bw.first_visiable_row + max_visiable, 0, data.line_count -| 1);
    var line_number_rect: Rect = switch (opts.line_numbers) {
        .none => .{ .x = ctx.bounding_rect.x, .y = ctx.bounding_rect.y },
        else => try lineNumbersRect(ctx.fonts, ctx.bounding_rect, last_row),
    };

    const x: f32 = line_number_rect.right();
    var y: f32 = ctx.bounding_rect.y;

    const cursor = core.buffer_window.cursor(bw.id).?;

    bw.windowFollowCursor(cursor, data.line_count);
    var cursor_rect = Rect{};

    bw.visible_lines = max_visiable;
    var width: f32 = 0;
    var height: f32 = 0;

    const line_numbers_size = Size{};
    _ = line_numbers_size;
    const text_size = Size{};
    _ = text_size;
    for (bw.first_visiable_row..data.line_count) |usize_row| {
        const row: u32 = @intCast(usize_row);
        const drawn_lines = row - bw.first_visiable_row;

        if (y >= ctx.bounding_rect.bottom() and row > bw.first_visiable_row) {
            bw.visible_lines = drawn_lines;
            break;
        }

        if (opts.max_lines) |max| if (drawn_lines >= max) {
            line_number_rect.h = y;
            bw.visible_lines = drawn_lines;
            break;
        };

        const line = try core.buffer.getLine(ctx.arena, data.handle, row);

        const line_text_result = try bufferWidgetDrawLine(&above_text, ctx.fonts, &text, ctx.arena, .{ x, y }, row, bw, line);
        const line_number_result = try drawLineNumber(ctx.fonts, line_number_rect, y, &line_numbers, cursor, row, opts);

        defer {
            y += line_text_result.h;
            height += line_text_result.h;
            width = @max(width, line_text_result.w + line_number_result.w);
        }

        try bufferDrawSelection(ctx.fonts, &selection, .{ x, y }, bw.bhandle, cursor, row, line);

        if (try bufferGetCursorRect(ctx.fonts, bw, .{ x, y }, line, row, cursor)) |rect| {
            cursor_rect = rect;
            if (bw.id.id == core.focused.bufferWindow().id) {
                try above_text.addRectFill(rect, rgba(0xFFFFFF_80));
            }
        }
    }

    const bounding_rect = Rect{
        .x = ctx.bounding_rect.x,
        .y = ctx.bounding_rect.y,
        .w = width,
        .h = height,
    };

    return BufferWidget{
        .selection = selection,
        .line_numbers = line_numbers,
        .text = text,
        .above_text = above_text,

        .bounding_rect = bounding_rect,

        .cursor_rect = cursor_rect,
        .clicked_on_point = null,
        .should_focus = ctx.mouse_clicked and inRect(ctx.mouse_x, ctx.mouse_y, bounding_rect),
    };
}

fn bufferWidgetDrawLine(
    above_text: *DrawList,
    fonts: *draw.Fonts,
    text_dl: *DrawList,
    arena: std.mem.Allocator,
    start: [2]f32,
    row: u32,
    bw: *core.BufferWindow,
    line: []const u8,
) !Size {
    const x = start[0];
    const y = start[1];

    const text_size = try fonts.textSize(line);
    try text_dl.addText(.{ x, y }, line, Color.white);
    var real_width = text_size.w;

    for (core.internal.state().buffer_displayers.items) |dis| {
        const ranges = dis.get(arena, bw.bhandle, row) catch continue;

        for (ranges.text) |text_range| {
            const dd = try displayerData(fonts, line, .{ x, y }, text_range, ranges.range_kind);
            const text_color = rgba(text_range.color);

            try text_dl.addText(.{ dd.left, dd.top }, dd.slice, text_color);
        }

        for (ranges.underline) |underline_range| {
            const dd = try displayerData(fonts, line, .{ x, y }, underline_range, ranges.range_kind);

            const underline_color = rgba(underline_range.color);

            try above_text.addLine(.{ dd.left, dd.bottom }, .{ dd.right, dd.bottom }, underline_color);
        }

        if (ranges.virtual_text.text.len > 0) {
            const len = std.mem.indexOf(u8, ranges.virtual_text.text, "\n") orelse
                ranges.virtual_text.text.len;

            const virtual = ranges.virtual_text.text[0..len];
            try above_text.addText(.{ x + text_size.w + 1, y }, virtual, rgba(ranges.virtual_text.color));

            const virtual_size = try fonts.textSize(virtual);
            real_width += virtual_size.w + 1;
        }
    }

    return .{ .w = real_width, .h = text_size.h };
}

fn drawLineNumber(fonts: *draw.Fonts, line_numbers_rect: Rect, line_y: f32, dl: *DrawList, cursor: core.Buffer.Point, row: u32, opts: DrawBufferOptions) !Size {
    var buf: [64]u8 = undefined;
    const num = switch (opts.line_numbers) {
        .none => "",
        .relative => blk: {
            const diff = utils.diff(cursor.row, row);
            const res = if (diff == 0) row else diff;
            break :blk std.fmt.bufPrint(&buf, "{}", .{res}) catch unreachable;
        },
        .absolute => std.fmt.bufPrint(&buf, "{}", .{row}) catch unreachable,
    };

    const line_numbers = try fonts.textSize(num);
    if (num.len == 0) {
        std.debug.assert(line_numbers.w == 0 and line_numbers.h == 0);
    }
    try dl.addText(
        .{ @floor(center(line_numbers_rect, line_numbers.w)), line_y },
        num,
        Color.white,
    );

    return line_numbers;
}

fn lineNumbersRect(fonts: *draw.Fonts, bounding_rect: Rect, last_row: u32) !Rect {
    var buf: [64]u8 = undefined;

    const line_col_text = std.fmt.bufPrint(&buf, "{}", .{last_row}) catch unreachable;
    return Rect{
        .x = bounding_rect.x,
        .y = bounding_rect.y,
        .w = (try fonts.textSize(line_col_text)).w + 5,
        .h = bounding_rect.h,
    };
}
