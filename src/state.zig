const std = @import("std");
const print = std.debug.print;
const ArrayList = std.ArrayList;
const builtin = @import("builtin");

const dialogs = @import("dialogs.zig");
const Registers = @import("registers.zig");
const buffer_window = @import("buffer_window.zig");
const BufferWindow = buffer_window.BufferWindow;
const WindowTree = buffer_window.WindowTree;
const core = @import("core.zig");
const internal = @import("internal.zig");
const Buffer = @import("buffer.zig");
const notify = @import("notify.zig");
const utils = @import("utils.zig");
const command_line = @import("command_line.zig");
const Marks = @import("marks.zig");
const PositionList = @import("position_list.zig");
const StatusLine = @import("status_line.zig");
const BufferHandle = core.BufferHandle;
const history = @import("history.zig");
const search = @import("search.zig");

const VisibleBuffersList = std.ArrayList(BufferWindow.ID);
const BufferWindowNode = VisibleBuffersList.Node;

const UniqueOptionalSegmentedList = utils.UniqueOptionalSegmentedList;

const BufferList = UniqueOptionalSegmentedList(Buffer, .handle, 128);
const BufferWindowList = UniqueOptionalSegmentedList(BufferWindow, .id, 128);
const BufferDisplayers = std.ArrayList(core.BufferDisplayer);
const modify = @import("modify_state.zig");
const Modify = modify.Modify;

const Key = @import("input.zig").Key;

pub const State = @This();

pub const EditorInputMode = enum {
    dialog,
    input_layer,
    ignore,
};

registers: Registers,
buffers: BufferList,
buffer_windows: BufferWindowList,

tabs: std.ArrayList(internal.Tab),
tabs_view: utils.ListView = .{},

cli: command_line.CommandLine,
histories: std.AutoHashMap(BufferHandle, history.HistoryStack),

search_keeper: search.SearchKeeper,

dialogs: dialogs.Dialogs,
input_mode: EditorInputMode = .input_layer,
custom_drawing: std.ArrayList(internal.CustomDrawing),

timed_callbacks: std.ArrayList(internal.TimedCallback),
buffer_displayers: BufferDisplayers,
notifications: notify.Notifications,
marks: Marks,

path_bt_storage: core.slice_storage.StringStorage,
data_dir_path: ?[]const u8 = null,
buffer_type_configs: std.StringHashMap(core.config.BufferTypeConfig),
status_line: StatusLine,

allocator: std.mem.Allocator,
extra_frames: u32 = 2,
next_unique_id: u32 = 0,
deferred_calls: modify.DeferredModify,

run_editor: bool = true,

pub fn init(allocator: std.mem.Allocator) !State {
    var gs = State{
        .buffers = BufferList.init(allocator),
        .buffer_windows = BufferWindowList.init(allocator),
        .buffer_displayers = BufferDisplayers.init(allocator),
        .tabs = try std.ArrayList(internal.Tab).initCapacity(allocator, 128),
        .notifications = notify.Notifications.init(),
        .allocator = allocator,
        .registers = Registers.init(allocator),
        .marks = Marks.init(allocator),
        .status_line = StatusLine.init(allocator),
        .histories = std.AutoHashMap(BufferHandle, history.HistoryStack).init(allocator),
        .dialogs = dialogs.Dialogs.init(allocator),
        .search_keeper = search.SearchKeeper.init(allocator),
        .timed_callbacks = std.ArrayList(internal.TimedCallback).init(allocator),
        .buffer_type_configs = std.StringHashMap(core.config.BufferTypeConfig).init(allocator),
        .path_bt_storage = core.slice_storage.StringStorage.init(allocator),
        .custom_drawing = std.ArrayList(internal.CustomDrawing).init(allocator),
        .deferred_calls = try modify.DeferredModify.init(allocator),

        // the following needs to put their data in state so initialize them later
        .cli = undefined,
    };
    errdefer {
        gs.deinit();
    }

    try cliInit(&gs);
    try internal.default_status_line.initDefualtStatusLine(&gs);

    return gs;
}

pub fn nextUniqueId(s: *State) u32 {
    const h = s.next_unique_id;
    s.next_unique_id += 1;
    return h;
}

fn cliInit(gs: *State) !void {
    const cli_bhandle = try internal.deferCreateBuffer(gs, "");
    const id = try internal.deferCreateWindow(gs, cli_bhandle);
    var cli = core.CommandLine.init(gs.allocator, id, cli_bhandle);
    try command_line.default_commands.setDefaultCommands(&cli);

    gs.cli = cli;
}

pub fn deinit(g: *State) void {
    for (g.tabs.items) |*tab| {
        tab.panes.deinit();
        tab.floats.deinit();
    }

    {
        var iter = g.buffers.iterator();
        while (iter.next()) |buffer| buffer.deinit();
    }

    {
        var iter = g.buffer_windows.iterator();
        while (iter.next()) |win| win.deinit();
    }

    {
        var values = g.histories.valueIterator();
        while (values.next()) |list| list.deinit();
    }

    inline for (std.meta.fields(State)) |field| {
        if (@typeInfo(field.type) == .Struct and @hasDecl(field.type, "deinit"))
            @field(g, field.name).deinit();
    }
}
