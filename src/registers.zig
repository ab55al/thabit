const std = @import("std");

const utils = @import("utils.zig");

const Registers = @This();

pub const Named = enum {
    // zig fmt: off
    default, a, b, c, d, e, f, g, h, i, j, k, l,
    m, n, o, p, q, r, s, t, u, v, w, x, y, z,
    // zig fmt: on

};

const StringArray = std.ArrayListUnmanaged(u8);
const CustomRegisterHashMap = std.ArrayHashMapUnmanaged(RegisterArray, StringArray, HashContext, false);
pub const register_array_capacity = 64;
pub const RegisterArray = std.BoundedArray(u8, register_array_capacity);

data: CustomRegisterHashMap = .{},
allocator: std.mem.Allocator,

pub fn init(allocator: std.mem.Allocator) Registers {
    return .{ .allocator = allocator };
}

pub fn deinit(self: *Registers) void {
    var iter = self.data.iterator();
    while (iter.next()) |kv| {
        kv.value_ptr.deinit(self.allocator);
    }

    self.data.deinit(self.allocator);
}

pub fn copyTo(self: *Registers, register: []const u8, content: []const u8) !void {
    const key = RegisterArray.fromSlice(register) catch return error.RegisterNameTooBig;
    const gop = try self.data.getOrPut(self.allocator, key);
    errdefer _ = self.data.orderedRemove(gop.key_ptr.*);

    if (!gop.found_existing) {
        gop.value_ptr.* = StringArray{};
    }

    gop.value_ptr.*.clearRetainingCapacity();
    try gop.value_ptr.*.appendSlice(self.allocator, content);
}

pub fn copyToAndOwn(self: *Registers, register: []const u8, content: []u8) !void {
    const key = RegisterArray.fromSlice(register) catch return error.RegisterNameTooBig;
    const gop = try self.data.getOrPut(self.allocator, key);

    if (!gop.found_existing) {
        gop.value_ptr.* = StringArray{};
    }

    gop.value_ptr.*.deinit(self.allocator);
    gop.value_ptr.* = StringArray.fromOwnedSlice(content);
}

pub fn getFrom(self: *Registers, register: []const u8) ?[]const u8 {
    const key = RegisterArray.fromSlice(register) catch return null;
    const value = self.data.get(key) orelse return null;
    return value.items;
}

pub const HashContext = struct {
    pub fn hash(_: @This(), s: RegisterArray) u32 {
        return @truncate(std.hash_map.hashString(s.slice()));
    }
    pub fn eql(_: @This(), a: RegisterArray, b: RegisterArray, _: usize) bool {
        return std.hash_map.eqlString(a.slice(), b.slice());
    }
};
