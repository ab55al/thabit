const std = @import("std");
const print = std.debug.print;
const ArrayList = std.ArrayList;
const ArrayListUnmanaged = std.ArrayListUnmanaged;
const assert = std.debug.assert;

const utils = @import("utils.zig");

const PieceTable = @This();

const max_new_pieces_in_delete = 2;
const max_new_pieces_in_insert = 2;
const max_new_pieces_in_change = max_new_pieces_in_insert + max_new_pieces_in_delete;

const PiecesPool = std.BoundedArray(*PieceNode, max_new_pieces_in_change * 10);

add_buffer: ArrayListUnmanaged(u8) = .{},
newline_indices: ArrayListUnmanaged(u64) = .{},

/// instead of an original content slice use this
original_content_piece: PieceNode = .{},
tree: SplayTree = .{},

/// The purpose of this pool is to provide a function similar to
/// ArrayList.ensureUnusedCapacity()
pieces_pool: PiecesPool = PiecesPool.init(0) catch unreachable,

pub fn init(allocator: std.mem.Allocator, buf: []const u8) !PieceTable {
    if (buf.len == 0) return .{};

    var add_buffer = ArrayListUnmanaged(u8){};
    var newline_indices = ArrayListUnmanaged(u64){};

    errdefer {
        add_buffer.deinit(allocator);
        newline_indices.deinit(allocator);
    }

    try add_buffer.appendSlice(allocator, buf);
    try pushNewlineIndices(allocator, &newline_indices, buf, 0);

    var root = try allocator.create(PieceNode);
    root.* = .{
        .offset = 0,
        .len = add_buffer.items.len,
        .newline_offset = 0,
        .newline_count = newline_indices.items.len,
    };

    const piece_table = PieceTable{
        .add_buffer = add_buffer,
        .newline_indices = newline_indices,
        .original_content_piece = root.*,
        .tree = root.toTree(),
    };

    return piece_table;
}

pub fn deinit(pt: *PieceTable, allocator: std.mem.Allocator) void {
    pt.add_buffer.deinit(allocator);
    pt.newline_indices.deinit(allocator);

    for (pt.pieces_pool.slice()) |piece| allocator.destroy(piece);

    const root = PieceNode.deinitTree(pt.tree.root, allocator);
    if (root) |r| allocator.destroy(r);
}

pub fn change(pt: *PieceTable, allocator: std.mem.Allocator, index: u64, deletion_len: u64, string: []const u8) !void {
    const newline_count = std.mem.count(u8, string, "\n");

    pt.fillPiecesPool(allocator) catch |err| if (pt.pieces_pool.len < max_new_pieces_in_change) return err;
    try pt.add_buffer.ensureUnusedCapacity(allocator, string.len);
    try pt.newline_indices.ensureUnusedCapacity(allocator, newline_count);

    if (deletion_len > 0) pt.delete(allocator, index, deletion_len);
    if (string.len > 0) pt.insert(allocator, index, string, newline_count);
}

fn insert(pt: *PieceTable, allocator: std.mem.Allocator, index: u64, string: []const u8, newline_count: u64) void {
    assert(string.len > 0);

    var new_tree = SplayTree{};
    if (pt.tree.root != null) {
        var node_info = pt.tree.findNode(index);
        var node = node_info.piece;
        // force insert after piece
        if (node.previousNode()) |pn| if (node_info.relative_index == 0) {
            node = pn;
            node_info = .{ .piece = pn, .relative_index = pn.len };
        };

        pt.tree.splay(node);
        const is_last_node = pt.tree.isLastNode(node);
        const is_first_node = pt.tree.isFirstNode(node);

        if (node.offset + node.len >= pt.add_buffer.items.len and node_info.relative_index >= node.len) {
            // modify the piece
            node.len += string.len;
            node.newline_count += newline_count;
            pt.tree.size += string.len;
            pt.tree.newline_count += newline_count;
        } else {
            pt.tree.removeNode(node);

            if (node_info.relative_index > 0 and node_info.relative_index < node.len) { // split
                const split_piece = node.split(pt, node_info.relative_index);

                if (split_piece.left.len > 0) {
                    var piece = pt.pieces_pool.pop();
                    piece.* = split_piece.left.toPiece();
                    new_tree.appendTree(piece.toTree());
                }

                // middle piece
                node.* = pt.newAddPiece(string.len, newline_count);
                new_tree.appendTree(node.toTree());

                if (split_piece.right.len > 0) {
                    var piece = pt.pieces_pool.pop();
                    piece.* = split_piece.right.toPiece();
                    new_tree.appendTree(piece.toTree());
                }
            } else {
                var new_piece = pt.pieces_pool.pop();
                new_piece.* = pt.newAddPiece(string.len, newline_count);

                new_tree.setAsNewTree(node.toTree());

                if (node_info.relative_index == 0)
                    new_tree.prependTree(new_piece.toTree())
                else
                    new_tree.appendTree(new_piece.toTree());
            }

            if (pt.tree.root == null) {
                pt.tree.setAsNewTree(new_tree);
            } else if (is_last_node) { // append to tree
                pt.tree.appendTree(new_tree);
            } else if (is_first_node) {
                pt.tree.prependTree(new_tree);
            } else if (new_tree.root != null) {
                const insert_index = (index - node_info.relative_index);
                const ni = pt.tree.findNode(insert_index);
                const n = ni.piece;
                pt.tree.insertBeforeTree(n, new_tree);
            }
        }
    } else {
        var new_piece = pt.pieces_pool.pop();
        new_piece.* = pt.newAddPiece(string.len, newline_count);
        pt.tree.setAsNewTree(new_piece.toTree());
    }

    pushNewlineIndices(allocator, &pt.newline_indices, string, pt.add_buffer.items.len) catch unreachable;
    pt.add_buffer.appendSliceAssumeCapacity(string);
}

fn delete(pt: *PieceTable, allocator: std.mem.Allocator, start_index: u64, deletion_len: u64) void {
    assert(deletion_len > 0);

    const end_index = start_index + deletion_len - 1;
    const start_node_info = pt.tree.findNode(start_index);
    const end_node_info = pt.tree.findNode(end_index);

    const is_last_node = pt.tree.isLastNode(start_node_info.piece) or pt.tree.isLastNode(end_node_info.piece);
    const is_first_node = pt.tree.isFirstNode(start_node_info.piece);

    // extarct the middle tree holding all nodes between start_index and end_index (inclusive)
    const deleted_tree = pt.tree.removeNodes(start_index, end_index);

    const left = start_node_info.piece.split(pt, start_node_info.relative_index).left;
    const right = end_node_info.piece.split(pt, end_node_info.relative_index + 1).right;

    var new_tree = SplayTree{};
    if (left.len > 0) {
        var piece = pt.pieces_pool.pop();
        piece.* = left.toPiece();
        new_tree.appendTree(piece.toTree());
    }
    if (right.len > 0) {
        var piece = pt.pieces_pool.pop();
        piece.* = right.toPiece();
        new_tree.appendTree(piece.toTree());
    }

    if (is_last_node) {
        pt.tree.appendTree(new_tree);
    } else if (is_first_node) {
        pt.tree.prependTree(new_tree);
    } else if (pt.tree.root == null) {
        pt.tree.setAsNewTree(new_tree);
    } else {
        const insert_index = (start_index - start_node_info.relative_index);
        const piece = pt.tree.findNode(insert_index).piece;
        pt.tree.insertBeforeTree(piece, new_tree);
    }

    const root = PieceNode.deinitTree(deleted_tree.root, allocator);
    if (root) |r| allocator.destroy(r);
}

pub fn byteAt(pt: *PieceTable, index: u64) u8 {
    var node_info = pt.tree.findNode(index);
    return node_info.piece.content(pt)[node_info.relative_index];
}

fn newAddPiece(pt: *PieceTable, string_len: u64, newline_in_string: u64) PieceNode {
    return .{
        .newline_offset = pt.newline_indices.items.len,
        .newline_count = newline_in_string,

        .offset = pt.add_buffer.items.len,
        .len = string_len,
    };
}

fn fillPiecesPool(pt: *PieceTable, allocator: std.mem.Allocator) !void {
    if (pt.pieces_pool.len > max_new_pieces_in_change) return;

    while (pt.pieces_pool.len != pt.pieces_pool.buffer.len)
        pt.pieces_pool.appendAssumeCapacity(try allocator.create(PieceNode));
}

fn pushNewlineIndices(allocator: std.mem.Allocator, list: *ArrayListUnmanaged(u64), string: []const u8, offset: u64) !void {
    @setRuntimeSafety(false);
    for (string, 0..) |c, i|
        if (c == '\n')
            try list.append(allocator, i + offset);
    @setRuntimeSafety(true);
}

pub const SplayTree = struct {
    root: ?*PieceNode = null,
    size: u64 = 0,
    newline_count: u64 = 0,

    pub fn destroyIfEmpty(tree: *SplayTree, allocator: std.mem.Allocator, nodes: []const *PieceNode) void {
        for (nodes) |node| {
            if (node.len > 0) continue;

            tree.removeNode(node);
            allocator.destroy(node);
        }
    }

    pub fn insertAfter(tree: *SplayTree, node: *PieceNode, new_node: *PieceNode) void {
        utils.assert(new_node.len > 0, "");
        const right_subtree = tree.splitRightTree(node);

        const left_subtree = SplayTree{
            .root = node,
            .size = tree.size,
            .newline_count = tree.newline_count,
        };

        tree.root = new_node;
        tree.size = new_node.len;
        tree.newline_count = new_node.newline_count;

        tree.setAsLeftSubTree(new_node, left_subtree);
        if (right_subtree.root != null)
            tree.setAsRightSubTree(new_node, right_subtree);
    }

    pub fn insertBefore(tree: *SplayTree, node: *PieceNode, new_node: *PieceNode) void {
        utils.assert(new_node.len > 0, "");

        tree.splay(node);
        if (node.previousNode()) |p| {
            tree.insertAfter(p, new_node);
        } else { // left is null
            tree.setAsLeftSubTree(node, new_node.toTree());
        }
    }

    pub fn insertAfterTree(tree: *SplayTree, node: *PieceNode, new_tree: SplayTree) void {
        const right_st = tree.splitRightTree(node);
        tree.setAsRightSubTree(node, new_tree);
        tree.appendTree(right_st);
    }

    pub fn insertBeforeTree(tree: *SplayTree, node: *PieceNode, new_tree: SplayTree) void {
        const left_st = tree.splitLeftTree(node);
        tree.setAsLeftSubTree(node, new_tree);
        tree.prependTree(left_st);
    }

    /// Splay the largest node then set _subtree_ to be the right subtree
    pub fn appendTree(tree: *SplayTree, subtree: SplayTree) void {
        if (subtree.root == null) return;

        if (tree.root == null) {
            tree.setAsNewTree(subtree);
        } else if (tree.root != null) {
            const biggest_node = tree.root.?.rightMostNode();
            tree.splay(biggest_node);

            utils.assert(tree.root.?.right == null, "");

            tree.setAsRightSubTree(tree.root.?, subtree);
        }
    }

    pub fn prependTree(tree: *SplayTree, new_tree: SplayTree) void {
        if (new_tree.root == null) return;

        if (tree.root == null) {
            tree.setAsNewTree(new_tree);
        } else {
            const first = tree.findNode(0).piece;
            tree.splay(first);
            tree.setAsLeftSubTree(first, new_tree);
        }
    }

    pub fn removeNode(tree: *SplayTree, node: *PieceNode) void {
        const left_st = tree.splitLeftTree(node);
        const right_st = tree.splitRightTree(node);

        tree.* = SplayTree{};
        tree.appendTree(left_st);
        tree.appendTree(right_st);

        // reset tree related data
        node.* = node.pieceData().toPiece();
    }

    pub fn findNode(tree: *SplayTree, index: u64) struct {
        piece: *PieceNode,
        relative_index: u64,
    } {
        utils.assert(tree.root != null, "");

        var i = index;

        var node = tree.root.?;
        while (true) {
            if (i < node.left_subtree_len) {
                node = node.left.?;
            } else if (i >= node.left_subtree_len and i < node.left_subtree_len + node.len) {
                // found it
                i -= node.left_subtree_len;
                break;
            } else if (node.right) |right| {
                i -= node.left_subtree_len + node.len;
                node = right;
            } else {
                break;
            }
        }

        tree.splay(node);
        return .{
            .piece = node,
            .relative_index = i,
        };
    }

    pub fn findNodeWithLine(tree: *SplayTree, pt: *const PieceTable, newline_index_of_node: u64) struct {
        piece: *PieceNode,
        newline_index: u64, // absolute
    } {
        utils.assert(newline_index_of_node <= tree.newline_count, "newline_index_of_node cannot be greater than the total newline in the table");
        utils.assert(tree.root != null, "pieces_root must not be null");

        var node = tree.root.?;
        var i = newline_index_of_node;

        var abs_nl_index: u64 = 0;

        while (true) {
            if (i < node.left_subtree_newline_count) {
                node = node.left.?;
            } else if (i >= node.left_subtree_newline_count and i < node.left_subtree_newline_count + node.newline_count) {
                // found it
                i -= node.left_subtree_newline_count;
                abs_nl_index += node.left_subtree_len;
                break;
            } else if (node.right) |right| {
                i -= node.left_subtree_newline_count + node.newline_count;
                abs_nl_index += node.left_subtree_len + node.len;
                node = right;
            } else break;
        }

        if (node.newline_count > 0) abs_nl_index += node.relativeNewlineIndex(pt, i);

        tree.splay(node);
        return .{
            .piece = node,
            .newline_index = abs_nl_index,
        };
    }

    fn isLastNode(tree: *SplayTree, node: *PieceNode) bool {
        if (tree.root) |root|
            return node == root.rightMostNode()
        else
            return false;
    }

    fn isFirstNode(tree: *SplayTree, node: *PieceNode) bool {
        if (tree.root) |root|
            return node == root.leftMostNode()
        else
            return false;
    }

    fn splay(tree: *SplayTree, node: *PieceNode) void {
        while (node.parent) |parent| {
            const kind: enum { zig, zig_zig_left, zig_zig_right, zig_zag_left_right, zig_zag_right_left } =
                if (parent.parent == null) // parent is root
                .zig
            else if (PieceNode.bothLeftChildOfGrandparent(node, parent))
                .zig_zig_right
            else if (PieceNode.bothRightChildOfGrandparent(node, parent))
                .zig_zig_left
            else if (parent.right == node and parent.parent.?.left == parent)
                .zig_zag_left_right
            else
                .zig_zag_right_left;

            switch (kind) {
                .zig => {
                    if (parent.left == node)
                        node.rightRotate()
                    else
                        node.leftRotate();
                },
                .zig_zig_right => {
                    parent.rightRotate();
                    node.rightRotate();
                },
                .zig_zig_left => {
                    parent.leftRotate();
                    node.leftRotate();
                },
                .zig_zag_right_left => {
                    node.rightRotate();
                    node.leftRotate();
                },

                .zig_zag_left_right => {
                    node.leftRotate();
                    node.rightRotate();
                },
            }
        }
        tree.root = node;
    }

    fn splitRightTree(tree: *SplayTree, node: *PieceNode) SplayTree {
        tree.splay(node);

        if (node.right == null) return SplayTree{};

        const right_subtree_root = node.right;
        const subtree_size = tree.rightSubTreeSizeOfRoot();

        node.right = null;
        if (right_subtree_root) |rsr| rsr.parent = null;

        tree.size -|= subtree_size.size;
        tree.newline_count -|= subtree_size.newline_count;

        return .{
            .root = right_subtree_root,
            .size = subtree_size.size,
            .newline_count = subtree_size.newline_count,
        };
    }

    fn splitLeftTree(tree: *SplayTree, node: *PieceNode) SplayTree {
        tree.splay(node);

        if (node.left == null) return SplayTree{};

        const left_subtree_root = node.left;
        node.left = null;

        var subtree_size = PieceNode.Size{};
        if (left_subtree_root) |lsr| {
            lsr.parent = null;
            subtree_size = .{
                .size = node.left_subtree_len,
                .newline_count = node.left_subtree_newline_count,
            };
        }

        node.left_subtree_newline_count = 0;
        node.left_subtree_len = 0;

        tree.size -|= subtree_size.size;
        tree.newline_count -|= subtree_size.newline_count;

        return .{
            .root = left_subtree_root,
            .size = subtree_size.size,
            .newline_count = subtree_size.newline_count,
        };
    }

    fn removeNodes(tree: *SplayTree, start_index: u64, end_index: u64) SplayTree {
        const left_node = tree.findNode(start_index).piece;
        const right_node = tree.findNode(end_index).piece;

        if (left_node == right_node) {
            tree.removeNode(left_node);
            return left_node.toTree();
        } else {
            var left_st = tree.splitLeftTree(left_node);
            const right_st = tree.splitRightTree(right_node);

            const middle_tree = tree.*;

            left_st.appendTree(right_st);
            tree.setAsNewTree(left_st);

            return middle_tree;
        }
    }

    pub fn rightSubTreeSizeOfRoot(tree: *SplayTree) PieceNode.Size {
        const root = tree.root orelse return .{};
        return if (root.right == null) .{} else .{
            .size = tree.size -| (root.len + root.left_subtree_len),
            .newline_count = tree.newline_count -| (root.newline_count + root.left_subtree_newline_count),
        };
    }

    pub fn deinitAndSetAsNewTree(tree: *SplayTree, allocator: std.mem.Allocator, new_tree: SplayTree) void {
        const old_root = PieceNode.deinitTree(tree.root, allocator);
        if (old_root) |r| allocator.destroy(r);

        tree.setAsNewTree(new_tree);
    }

    fn setAsNewTree(tree: *SplayTree, subtree: SplayTree) void {
        if (subtree.root) |root| utils.assert(root.parent == null, "");
        tree.root = subtree.root;
        tree.size = subtree.size;
        tree.newline_count = subtree.newline_count;
    }

    fn setAsLeftSubTree(tree: *SplayTree, node: *PieceNode, subtree: SplayTree) void {
        utils.assert(node.left == null, "");
        tree.splay(node);
        if (subtree.root == null) return;

        node.left = subtree.root;
        node.left_subtree_len = subtree.size;
        node.left_subtree_newline_count = subtree.newline_count;

        subtree.root.?.parent = node;

        tree.size += subtree.size;
        tree.newline_count += subtree.newline_count;
    }

    fn setAsRightSubTree(tree: *SplayTree, node: *PieceNode, subtree: SplayTree) void {
        utils.assert(node.right == null, "");
        tree.splay(node);
        if (subtree.root == null) return;

        node.right = subtree.root;
        subtree.root.?.parent = node;

        tree.size += subtree.size;
        tree.newline_count += subtree.newline_count;
    }

    pub fn treeDepth(tree: *SplayTree) u32 {
        const helper = struct {
            fn f(node: *PieceNode, level: u32) u32 {
                const left = if (node.left) |left| f(left, level + 1) else level;
                const right = if (node.right) |right| f(right, level + 1) else level;
                return @max(left, right);
            }
        }.f;
        return if (tree.root) |root| helper(root, 0) + 1 else 0;
    }

    pub fn printTreeOrdered(tree: *SplayTree, pt: ?*PieceTable, allocator: std.mem.Allocator) !void {
        var al = ArrayList(*PieceNode).init(allocator);
        defer al.deinit();
        tree.treeToArray(tree.root, &al) catch unreachable;
        for (al.items) |n| {
            n.print(false);
            if (pt) |p| std.debug.print("{any}", .{n.content(p)});

            std.debug.print("\n", .{});
        }
    }

    pub fn printTreeTraverseTrace(tree: *SplayTree, pt: *const PieceTable) void {
        const helper = struct {
            fn recurse(s_tree: *SplayTree, piece_table: *const PieceTable, node: ?*PieceNode) void {
                if (node == null) return;

                if (node.?.left) |left| {
                    print("going left\n", .{});
                    s_tree.recurse(piece_table, left);
                }

                const n = node.?;
                n.print(true);
                if (node.?.right) |right| {
                    print("going right\n", .{});
                    s_tree.recurse(piece_table, right);
                }

                print("going up\n", .{});
            }
        };

        std.debug.print("( TREE START\n", .{});
        helper.recurse(tree, pt, tree.roor);
        std.debug.print("TREE END )\n", .{});
    }

    pub fn treeToArray(tree: *SplayTree, node: ?*PieceNode, array_list: *ArrayList(*PieceNode)) std.mem.Allocator.Error!void {
        if (node == null) return;

        if (node.?.left) |left|
            try tree.treeToArray(left, array_list);

        try array_list.append(node.?);

        if (node.?.right) |right|
            try tree.treeToArray(right, array_list);
    }

    pub fn piecesCount(tree: *SplayTree) u64 {
        const helper = struct {
            fn recurse(t: *SplayTree, piece: ?*PieceNode, count: *u64) void {
                if (piece == null) return;

                if (piece.?.left) |left|
                    recurse(t, left, count);

                count.* += 1;

                if (piece.?.right) |right|
                    recurse(t, right, count);
            }
        };

        if (tree.root == null) return 0;
        var res: u64 = 0;
        helper.recurse(tree, tree.root, &res);
        return res;
    }

    pub fn treeToPieceDataArray(tree: *SplayTree, allocator: std.mem.Allocator) ![]const PieceNode.Data {
        const helper = struct {
            fn recurse(t: *SplayTree, node: ?*PieceNode, array_list: *ArrayList(PieceNode.Data)) std.mem.Allocator.Error!void {
                if (node == null) return;

                if (node.?.left) |left|
                    try recurse(t, left, array_list);

                try array_list.append(node.?.pieceData());

                if (node.?.right) |right|
                    try recurse(t, right, array_list);
            }
        };

        var array_list = try ArrayList(PieceNode.Data).initCapacity(allocator, tree.piecesCount());
        helper.recurse(tree, tree.root, &array_list) catch unreachable;
        return array_list.items;
    }

    pub fn treeFromSlice(allocator: std.mem.Allocator, infos: []const PieceNode.Data) !SplayTree {
        utils.assert(infos.len > 0, "");

        const root = try allocator.create(PieceNode);
        root.* = infos[0].toPiece();

        errdefer {
            const res_root = PieceNode.deinitTree(root, allocator);
            if (res_root) |r| allocator.destroy(r);
        }

        var size = root.len;
        var newline_count = root.newline_count;

        var previous_node = root;
        for (infos[1..]) |piece_info| {
            var node = try allocator.create(PieceNode);
            node.* = piece_info.toPiece();

            previous_node.right = node;
            node.parent = previous_node;

            size += piece_info.len;
            newline_count += piece_info.newline_count;

            previous_node = node;
        }

        return .{
            .root = root,
            .size = size,
            .newline_count = newline_count,
        };
    }
};

pub const PieceNode = struct {
    /// A wrapper around the same members in the PieceNode
    pub const Data = struct {
        newline_offset: u64,
        newline_count: u64,
        offset: u64,
        len: u64,

        pub fn toPiece(data: Data) PieceNode {
            return .{
                .offset = data.offset,
                .len = data.len,
                .newline_offset = data.newline_offset,
                .newline_count = data.newline_count,
            };
        }
    };

    pub const Size = struct {
        size: u64 = 0,
        newline_count: u64 = 0,
    };

    parent: ?*PieceNode = null,
    left: ?*PieceNode = null,
    right: ?*PieceNode = null,

    left_subtree_len: u64 = 0,
    left_subtree_newline_count: u64 = 0,

    offset: u64 = 0,
    len: u64 = 0,
    newline_offset: u64 = 0,
    newline_count: u64 = 0,

    pub fn deinitTree(piece: ?*PieceNode, allocator: std.mem.Allocator) ?*PieceNode {
        if (piece == null) return null;

        const left = deinitTree(piece.?.left, allocator);
        if (left) |l| allocator.destroy(l);
        const right = deinitTree(piece.?.right, allocator);
        if (right) |r| allocator.destroy(r);

        return piece;
    }

    pub fn content(piece: *const PieceNode, pt: *const PieceTable) []const u8 {
        return pt.add_buffer.items[piece.offset .. piece.offset + piece.len];
    }

    pub fn print(node: *PieceNode, new_line: bool) void {
        std.debug.print("s{}-{} l{} nl{} | {} {} {s}", .{
            node.offset,
            node.offset + node.len,
            node.len,
            node.newline_count,
            //
            node.left_subtree_len,
            node.left_subtree_newline_count,
            node.source.toString(),
        });

        if (new_line) std.debug.print("\n", .{});
    }

    pub fn pieceData(piece: PieceNode) Data {
        return .{
            .newline_offset = piece.newline_offset,
            .newline_count = piece.newline_count,
            .offset = piece.offset,
            .len = piece.len,
        };
    }

    pub fn toTree(node: *PieceNode) SplayTree {
        const sts = node.subtreeSize();

        return .{
            .root = node,
            .size = sts.size,
            .newline_count = sts.newline_count,
        };
    }

    pub fn rightSubTreeSize(node: *PieceNode) Size {
        if (node.right == null) return Size{};

        const subtree_size: Size =
            if (node.parent != null and node.parent.?.left == node)
            .{ .size = node.parent.?.left_subtree_len, .newline_count = node.parent.?.left_subtree_newline_count }
        else
            node.subtreeSize();

        return .{
            .size = subtree_size.size - (node.len + node.left_subtree_len),
            .newline_count = subtree_size.newline_count - (node.newline_count + node.left_subtree_newline_count),
        };
    }

    pub fn subtreeSize(const_node: *PieceNode) Size {
        var subtree_size = Size{};
        var node: ?*PieceNode = const_node;
        while (node) |n| {
            subtree_size.size += n.len + n.left_subtree_len;
            subtree_size.newline_count += n.newline_count + n.left_subtree_newline_count;
            node = n.right;
        }

        return subtree_size;
    }

    fn bothLeftChildOfGrandparent(node: *PieceNode, parent: *PieceNode) bool {
        return parent.left == node and parent.parent.?.left == parent;
    }

    fn bothRightChildOfGrandparent(node: *PieceNode, parent: *PieceNode) bool {
        return parent.right == node and parent.parent.?.right == parent;
    }

    fn leftRotate(node: *PieceNode) void {
        utils.assert(node.parent != null, "");

        var pivot = node;
        var root = node.parent.?;

        const grand_parent = root.parent;

        const pivot_new_left_subtree_size = PieceNode.Size{
            .size = root.left_subtree_len + root.len + pivot.left_subtree_len,
            .newline_count = root.left_subtree_newline_count + root.newline_count + pivot.left_subtree_newline_count,
        };

        root.right = pivot.left;
        pivot.left = root;

        root.resetChildrenToParent();
        pivot.resetChildrenToParent();

        pivot.left_subtree_len = pivot_new_left_subtree_size.size;
        pivot.left_subtree_newline_count = pivot_new_left_subtree_size.newline_count;

        pivot.parent = grand_parent;
        if (grand_parent) |gp| {
            if (gp.left == root)
                gp.left = pivot
            else
                gp.right = pivot;
        }
    }

    fn rightRotate(node: *PieceNode) void {
        utils.assert(node.parent != null, "");
        var pivot = node;
        var root = node.parent.?;

        const root_new_right_subtree_size = pivot.rightSubTreeSize();

        const grand_parent = root.parent;

        root.left = pivot.right;
        pivot.right = root;
        root.parent = pivot;

        root.resetChildrenToParent();
        pivot.resetChildrenToParent();

        root.left_subtree_len = root_new_right_subtree_size.size;
        root.left_subtree_newline_count = root_new_right_subtree_size.newline_count;

        pivot.parent = grand_parent;
        if (grand_parent) |gp| {
            if (gp.left == root)
                gp.left = pivot
            else
                gp.right = pivot;
        }
    }

    fn resetChildrenToParent(node: *PieceNode) void {
        if (node.left) |left| left.parent = node;
        if (node.right) |right| right.parent = node;
    }

    fn previousNode(const_node: *PieceNode) ?*PieceNode {
        if (const_node.left) |left| {
            var node = left;
            while (node.right) |right| node = right;
            return node;
        }

        return null;
    }

    fn nextNode(const_node: *PieceNode) ?*PieceNode {
        if (const_node.right) |right| {
            var node = right;
            while (node.left) |left| node = left;
            return node;
        }

        return null;
    }

    fn rootOfNode(const_node: *PieceNode) ?*PieceNode {
        var node = const_node.parent orelse return null;
        while (true) node = node.parent orelse return node;
    }

    /// Takes: the nth new line
    /// Returns: the relative index within the slice of the piece
    fn relativeNewlineIndex(piece: *PieceNode, pt: *const PieceTable, offset: u64) u64 {
        assert(piece.newline_count > 0);
        const newline_indices = pt.newline_indices.items;

        var index = piece.newline_offset + offset;
        if (index >= newline_indices.len)
            index = newline_indices.len -| 1;

        return newline_indices[index] -| piece.offset;
    }

    fn newlineCountBeforeRelativeIndex(piece: *PieceNode, pt: *PieceTable, index: u64) u64 {
        var num: u64 = 0;
        while (num < piece.newline_count) : (num += 1) {
            const relative_index = piece.relativeNewlineIndex(pt, num);
            if (relative_index >= index) break;
        }
        return num;
    }

    fn rightMostNode(const_node: *PieceNode) *PieceNode {
        var node = const_node;
        while (node.right) |right| {
            node = right;
        }
        return node;
    }

    fn leftMostNode(const_node: *PieceNode) *PieceNode {
        var node = const_node;
        while (node.left) |left| node = left;
        return node;
    }

    /// creates two pieces left and right where
    /// left.len = offset..index
    /// right.len = index..end
    pub fn split(piece: *PieceNode, pt: *PieceTable, index: u64) struct {
        left: Data,
        right: Data,
    } {
        // assert(index <= piece.len);
        const newline_count_for_left = piece.newlineCountBeforeRelativeIndex(pt, index);

        const left = Data{
            .newline_offset = piece.newline_offset,
            .newline_count = newline_count_for_left,

            .offset = piece.offset,
            .len = index,
        };

        const right = Data{
            .newline_offset = piece.newline_offset + newline_count_for_left,
            .newline_count = piece.newline_count - newline_count_for_left,

            .offset = piece.offset + index,
            .len = piece.len -| index,
        };

        return .{
            .left = left,
            .right = right,
        };
    }
};
