const std = @import("std");
const print = std.debug.print;
const fs = std.fs;
const ArrayListUnmanaged = std.ArrayListUnmanaged;
const eql = std.mem.eql;
const assert = std.debug.assert;

const log = std.log.scoped(.core);

const state = internal.state;
const modify = @import("modify_state.zig");

////////////////////////////////////////////////////////////////////////////////

pub const file_io = @import("file_io.zig");
pub const internal = @import("internal.zig");

pub const State = @import("state.zig");

pub const motions = @import("motions.zig");
pub const text_objects = @import("text_objects.zig");
pub const utils = @import("utils.zig");
pub const utf8 = @import("utf8.zig");
pub const slice_storage = @import("slice_storage.zig");

pub const CommandLine = internal.CommandLine;
pub const input = internal.input;
pub const Registers = @import("registers.zig");
pub const hooks = internal.hooks;
pub const Marks = internal.Marks;

pub const BufferHandle = internal.BufferHandle;
pub const Buffer = internal.Buffer;
pub const BufferWindow = internal.BufferWindow;

pub const KeyQueue = internal.KeyQueue;
pub const CharQueue = internal.CharQueue;

pub const HistoryStack = @import("history.zig").HistoryStack;
pub const PositionList = @import("position_list.zig");
pub const StatusLine = @import("status_line.zig");
pub const CustomDrawing = internal.CustomDrawing;

pub const Tab = internal.Tab;
const buffer_displayer = @import("buffer_displayer.zig");
pub usingnamespace buffer_displayer;
pub usingnamespace @import("notify.zig");
pub const draw = @import("ui/draw.zig");

pub const dialogs = @import("dialogs.zig");
pub const rip_grep = @import("rip_grep.zig");
pub const exactSearch = @import("search.zig").exact;
pub const ripGrepSearch = @import("search.zig").ripGrep;
pub const SearchResults = @import("search.zig").SearchResults;
pub const Dialog = dialogs.Dialogs.Dialog;
pub const ModifyObserver = @import("modify_state.zig").ModifyObserver;
pub const Modify = @import("modify_state.zig").Modify;
pub const EventListeners = @import("modify_state.zig").EventListeners;
pub const Event = @import("modify_state.zig").Event;

pub const thabit_options = internal.thabit_options;
pub const input_layer = internal.input_layer;
pub const user = internal.user;

////////////////////////////////////////////////////////////////////////////////
//
//
//
//

pub const builtin = struct {
    pub const auto_complete = @import("builtin/auto_complete/api.zig");
};

/// Contains functions who's calls are deferred
pub const dmodify = @import("core_defer_modify.zig");

//
//
//
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//
// Data definitions
//
//

pub const scratch_buffer_type = "Thabit:Scratch";

pub const Error = error{
    SavingPathlessBuffer,
    KillingDirtyBuffer,
    BufferAlreadyExists,
    BufferCannotBeDir,
    BufferDoesNotExist,
    BufferOpenByAnotherProcess,
};

pub const OpenOptions = struct {
    first_visiable_row: u32 = 0,
    where: union(enum) {
        here,
        new_tab,
        dir: internal.WindowTree.Dir,
    } = .here,
    /// respected only when `where == .dir`
    size: f32 = 0.5,
};

pub const SaveOptions = struct { force_save: bool = false };

pub const KillOptions = struct { force_kill: bool = false };

pub const BufferData = struct {
    handle: BufferHandle,
    size: usize,
    line_count: u32,
    path: []const u8,
    buffer_type: []const u8,
    dirty: bool,
    read_only: bool,
    version: u32,
};

pub const SetSelectionType = union(enum) {
    reset,
    set_kind: Buffer.Selection.Kind,
    set_anchor: Buffer.Point,
};

pub const BufferPathExistsResult = enum {
    in_current_proc,
    in_another_proc,
    false,

    pub fn b(self: BufferPathExistsResult) bool {
        return switch (self) {
            .false => false,
            else => true,
        };
    }
};

pub const HistoryOperation = enum {
    push_snapshot,
    undo,
    redo,
};

pub const WindowSplitOptions = struct {
    dir: internal.WindowTree.Dir,
    size: f32 = 0.5,
};

pub const TabData = struct {
    tab: internal.Tab,
    index: usize,
};

pub const JumpData = struct {
    id: BufferWindow.ID,
    path: []const u8,
    point: Buffer.Point,
};

pub const ScrollDir = enum {
    up,
    down,
};

pub const VisiableLinesResult = struct {
    first_row: u32,
    last_row: u32,
    count: u32,
};

pub const SearchOptions = struct {
    buffers_to_search: enum { all, visiable } = .visiable,
    search_source: SearchResults.Source = .rip_grep,
};

//
//
// Data definitions
//
////////////////////////////////////////////////////////////////////////////////

pub const buffer = struct {
    pub fn getBufferData(handle: BufferHandle) !BufferData {
        var buff = try internal.getBufferPtr(handle);
        // path and buffer_type will always be stored in
        // the `path_bt_storage` during buffer creation and renames
        const path = state().path_bt_storage.get(buff.path.items).?;
        const bt = state().path_bt_storage.get(buff.buffer_type.items).?;

        return .{
            .handle = buff.handle,
            .size = buff.size(),
            .line_count = buff.lineCount(),
            .path = path,
            .buffer_type = bt,
            .dirty = buff.dirty,
            .read_only = buff.read_only,
            .version = buff.version,
        };
    }

    pub fn bufferExists(handle: BufferHandle) bool {
        return state().buffers.getPtrBy(handle) != null;
    }

    pub fn getHandleByPath(arg_path: []const u8) !?BufferHandle {
        const result = try internal.getAbsolutePath(arg_path);
        const absolute_path = result.buf[0..result.len];
        return getHandleByPathAbsolute(absolute_path);
    }

    pub fn getHandleByPathAbsolute(path: []const u8) ?BufferHandle {
        std.debug.assert(std.fs.path.isAbsolute(path));

        var iter = state().buffers.iterator();
        while (iter.next()) |buff| {
            if (std.mem.eql(u8, path, buff.path.items))
                return buff.handle;
        }

        return null;
    }

    pub fn bufferWithPathExists(arg_path: []const u8) !BufferPathExistsResult {
        const result = try internal.getAbsolutePath(arg_path);
        const absolute_path = result.buf[0..result.len];
        return try bufferWithPathExistsAbsolute(absolute_path);
    }

    pub fn bufferWithPathExistsAbsolute(path: []const u8) !BufferPathExistsResult {
        std.debug.assert(std.fs.path.isAbsolute(path));

        var iter = state().buffers.iterator();
        while (iter.next()) |buff| {
            if (std.mem.eql(u8, path, buff.path.items))
                return .in_current_proc;
        }

        if (dataDirPath()) |dir| {
            if (try file_io.opened_buffers.pathExistsInDir(getAllocator(), dir, path))
                return .in_another_proc;
        }

        return .false;
    }

    pub fn getSelection(handle: BufferHandle) !Buffer.Selection {
        const buff = try internal.getBufferPtr(handle);
        return buff.selection;
    }

    pub fn getPoint(handle: BufferHandle, index: usize) !Buffer.Point {
        var buff = try internal.getBufferPtr(handle);
        return buff.getPoint(index);
    }

    pub fn getIndex(handle: BufferHandle, point: Buffer.Point) !usize {
        var buff = try internal.getBufferPtr(handle);

        return buff.getIndex(point);
    }

    pub fn moveIndex(handle: BufferHandle, index: usize, offset: isize) !usize {
        var buff = try internal.getBufferPtr(handle);
        return buff.moveIndex(index, offset);
    }

    pub fn lineStartIndex(handle: BufferHandle, row: u32) !usize {
        var buff = try internal.getBufferPtr(handle);
        return buff.indexOfFirstByteAtRow(row);
    }

    pub fn lineEndIndex(handle: BufferHandle, row: u32) !usize {
        var buff = try internal.getBufferPtr(handle);
        return buff.indexOfLastByteAtRow(row);
    }

    pub fn rowOfIndex(handle: BufferHandle, index: usize) !Buffer.RowOfIndex {
        var buff = try internal.getBufferPtr(handle);
        return buff.rowOfIndex(index);
    }

    pub fn getLines(allocator: std.mem.Allocator, handle: BufferHandle, start: u32, end: u32) ![]u8 {
        var buff = try internal.getBufferPtr(handle);
        return try buff.getLines(allocator, start, end);
    }

    pub fn getLine(allocator: std.mem.Allocator, handle: BufferHandle, row: u32) ![]u8 {
        return getLines(allocator, handle, row, row);
    }

    pub fn getCodepointAt(handle: BufferHandle, point: Buffer.Point) !u21 {
        var buff = try internal.getBufferPtr(handle);
        return buff.codePointAt(buff.getIndex(point)) catch |err| switch (err) {
            error.Utf8ExpectedContinuation,
            error.Utf8OverlongEncoding,
            error.Utf8EncodesSurrogateHalf,
            error.Utf8CodepointTooLarge,
            => error.InvalidCodepoint,
        };
    }

    pub fn getBufferRange(allocator: std.mem.Allocator, handle: BufferHandle, prange: Buffer.PointRange) ![]u8 {
        var buff = try internal.getBufferPtr(handle);
        const start = buff.getIndex(prange.start);
        const end = buff.getIndex(prange.end);

        return try buff.getStringRange(allocator, .{ .start = start, .end = end });
    }

    pub fn codePointCountAtLine(handle: BufferHandle, row: u32) !u32 {
        var buff = try internal.getBufferPtr(handle);
        return buff.countCodePointsAtRow(row);
    }

    pub fn lastColAtRow(handle: BufferHandle, row: u32) !u32 {
        return (try codePointCountAtLine(handle, row)) - 1;
    }

    pub const LineIterator = struct {
        const Self = @This();

        list: std.ArrayList(u8),
        handle: BufferHandle,
        start: u64,
        end: u64,
        current_row: u32,

        pub fn init(allocator: std.mem.Allocator, handle: BufferHandle, start: Buffer.Point, end: Buffer.Point) !LineIterator {
            var buff = try internal.getBufferPtr(handle);
            const range = buff.pointRangeToRange(.{ .start = start, .end = end });

            return .{
                .list = std.ArrayList(u8).init(allocator),
                .handle = handle,
                .start = range.start,
                .end = range.end,
                .current_row = start.min(end).row,
            };
        }

        pub fn deinit(iter: *Self) void {
            iter.list.deinit();
        }

        pub const NextResult = struct {
            line: []const u8,
            number: u32,

            pub fn empty(self: @This()) bool {
                return self.line.len == 1;
            }
        };
        pub fn next(self: *Self) !?NextResult {
            var buff = try internal.getBufferPtr(self.handle);

            const current_row = self.current_row;
            self.list.clearRetainingCapacity();
            try self.list.ensureTotalCapacity(buff.lineSize(current_row));

            while (current_row == self.current_row) {
                const string = self.nextPieceContent(buff) orelse return null;
                self.list.appendSliceAssumeCapacity(string);
            }

            return .{ .line = self.list.items, .number = current_row };
        }

        fn nextPieceContent(self: *Self, buff: *Buffer) ?[]const u8 {
            if (self.start >= self.end) {
                return null;
            }
            const current_row_end = @min(buff.indexOfLastByteAtRow(self.current_row) + 1, self.end);

            const piece_info = buff.lines.tree.findNode(self.start);
            const slice = piece_info.piece.content(&buff.lines)[piece_info.relative_index..];

            const end = @min(slice.len, current_row_end -| self.start);

            const relevant_content = slice[0..end];
            self.start += relevant_content.len;
            if (self.start >= current_row_end) self.current_row += 1;

            return relevant_content;
        }
    };

    pub const ReverseLineIterator = struct {
        const Self = @This();

        list: std.ArrayList(u8),
        handle: BufferHandle,
        start: usize,
        end: usize,
        current_row: u32,

        start_row: u32,
        end_row: u32,

        pub fn init(allocator: std.mem.Allocator, handle: BufferHandle, start: Buffer.Point, end: Buffer.Point) !ReverseLineIterator {
            var buff = try internal.getBufferPtr(handle);
            const range = buff.pointRangeToRange(.{ .start = start, .end = end });

            return .{
                .list = std.ArrayList(u8).init(allocator),
                .handle = handle,
                .start = range.start,
                .end = range.end,
                .current_row = start.max(end).row,

                .start_row = start.min(end).row,
                .end_row = start.max(end).row,
            };
        }

        pub fn deinit(iter: *Self) void {
            iter.list.deinit();
        }

        pub const NextResult = struct { line: []const u8, number: u32 };
        pub fn next(self: *Self) !?NextResult {
            if (self.end <= self.start) return null;

            var buff = try internal.getBufferPtr(self.handle);

            const current_row = self.current_row;
            self.list.clearRetainingCapacity();
            try self.list.ensureTotalCapacity(buff.lineSize(current_row));

            var iter = if (self.start_row == self.end_row)
                Buffer.BufferIterator.init(buff, self.start, self.end)
            else if (self.current_row == self.start_row)
                Buffer.BufferIterator.init(buff, self.start, buff.indexOfLastByteAtRow(self.start_row) + 1)
            else if (self.current_row == self.end_row)
                Buffer.BufferIterator.init(buff, buff.indexOfFirstByteAtRow(self.end_row), self.end)
            else
                Buffer.BufferIterator.init(buff, buff.indexOfFirstByteAtRow(self.current_row), buff.indexOfLastByteAtRow(self.current_row) + 1);

            while (iter.next()) |string|
                self.list.appendSliceAssumeCapacity(string);

            self.current_row -|= 1;
            self.end -|= self.list.items.len;

            return .{ .line = self.list.items, .number = current_row };
        }
    };
};

pub const buffer_window = struct {
    pub fn exists(window: BufferWindow.ID) bool {
        return state().buffer_windows.getBy(window) != null;
    }

    pub fn bufferHandle(id: BufferWindow.ID) ?BufferHandle {
        const bw = internal.getBufferWindow(id) orelse return null;
        return bw.bhandle;
    }

    pub fn cursor(id: BufferWindow.ID) ?Buffer.Point {
        const bw = internal.getBufferWindow(id) orelse return null;
        return marks.get(bw.cursor_mark).?;
    }

    pub fn visiableLines(id: BufferWindow.ID) ?VisiableLinesResult {
        const bw = internal.getBufferWindow(id) orelse return null;
        const data = buffer.getBufferData(bw.bhandle) catch return null;
        return .{
            .first_row = bw.first_visiable_row,
            .last_row = bw.lastVisibleRow(data.line_count),
            .count = bw.visible_lines,
        };
    }
};

pub const tabs = struct {
    pub fn focusedTab() TabData {
        const index = state().tabs_view.selected_index.?;
        const tab = state().tabs.items[index];
        return .{ .tab = tab, .index = index };
    }

    pub fn tabOfWindow(window: BufferWindow.ID) ?TabData {
        const result = internal.tabOfWindow(window) orelse return null;
        return .{ .tab = result.tab.*, .index = result.index };
    }
};

pub const jump_list = struct {
    pub fn currentJumpData() JumpData {
        return .{
            .id = focused.bufferWindow(),
            .path = focused.getData().path,
            .point = focused.cursor(),
        };
    }
};

pub const marks = struct {
    pub fn put(association: Marks.Association, point: Buffer.Point) !Marks.MarkKey {
        return modify.putMark(state(), association, point);
    }

    pub fn set(mark_key: Marks.MarkKey, point: Buffer.Point) void {
        modify.setMark(state(), mark_key, point);
    }

    pub fn remove(mark_key: Marks.MarkKey) void {
        modify.removeMark(state(), mark_key);
    }

    pub fn get(mark_key: Marks.MarkKey) ?Buffer.Point {
        return state().marks.getMark(mark_key);
    }
};

pub const search = struct {
    pub fn searchLines(string: []const u8, start_row: u32, end_row: u32, opts: SearchOptions) !void {
        state().search_keeper.removeResults();
        errdefer state().search_keeper.removeResults();

        const allocator = state().search_keeper.allocator;

        const len = switch (opts.buffers_to_search) {
            .all => state().buffers.count(),
            .visiable => internal.selectedTab().panes.tree.count(),
        };
        try state().search_keeper.search_results.ensureUnusedCapacity(state().search_keeper.allocator, len);

        switch (opts.buffers_to_search) {
            .all => {
                var iter = state().buffers.iterator();
                while (iter.next()) |b| {
                    const end = @min(end_row, b.lineCount());
                    const args = .{ allocator, b.handle, string, start_row, end };

                    const results = switch (opts.search_source) {
                        .exact => try @call(.auto, exactSearch, args),
                        .rip_grep => try @call(.auto, ripGrepSearch, args),
                    } orelse continue;

                    state().search_keeper.addResultsAssumeCapacity(results);
                }
            },
            .visiable => {
                const tab = internal.selectedTab();

                var iter = state().buffer_windows.iterator();
                while (iter.next()) |win| {
                    if (tab.hasWindow(win.id) == null) continue;

                    const data = try buffer.getBufferData(win.bhandle);
                    const end = @min(end_row, data.line_count);
                    const args = .{ allocator, win.bhandle, string, start_row, end };
                    const results = switch (opts.search_source) {
                        .exact => try @call(.auto, exactSearch, args),
                        .rip_grep => try @call(.auto, ripGrepSearch, args),
                    } orelse continue;

                    state().search_keeper.addResultsAssumeCapacity(results);
                }
            },
        }
    }

    pub fn nextSearchResultPoint(handle: BufferHandle, point: Buffer.Point) ?Buffer.Point {
        const ranges = state().search_keeper.rangesForBuffer(handle) orelse return null;

        for (ranges) |range| {
            if (range.start.row > point.row)
                return range.start;
            if (range.start.row == point.row and range.start.col > point.col)
                return range.start;
        }

        return null;
    }

    pub fn prevSearchResultPoint(handle: BufferHandle, point: Buffer.Point) ?Buffer.Point {
        const ranges = state().search_keeper.rangesForBuffer(handle) orelse return null;

        var i: usize = ranges.len;
        while (i > 0) {
            i -= 1;
            const range = ranges[i];

            if (range.start.row < point.row)
                return range.start;
            if (range.start.row == point.row and range.start.col < point.col)
                return range.start;
        }

        return null;
    }
};

pub const registers = struct {
    pub fn copyTo(register: []const u8, content: []const u8) !void {
        try state().registers.copyTo(register, content);
    }

    pub fn copyToByRange(register: []const u8, bhandle: BufferHandle, range: Buffer.PointRange) !void {
        const string = try buffer.getBufferRange(state().registers.allocator, bhandle, range);
        errdefer state().registers.allocator.free(string);
        try state().registers.copyToAndOwn(register, string);
    }

    pub fn getFrom(register: []const u8) []const u8 {
        return state().registers.getFrom(register) orelse "";
    }
};

pub const event_listener = struct {
    pub fn attach(ptr: *anyopaque, callback: *const EventListeners.Callback) !void {
        const self = &state().deferred_calls.event_listeners;
        if (self.findByPtr(ptr) != null) return;

        try self.interfaces.append(.{ .ptr = ptr, .callback = callback });
    }

    pub fn detach(ptr: *anyopaque, callback: *const EventListeners.Callback) void {
        const self = &state().deferred_calls.event_listeners;
        for (self.interfaces.items, 0..) |iface, i| {
            if (iface.callback == callback and iface.ptr == ptr) {
                _ = self.interfaces.orderedRemove(i);
                return;
            }
        }
    }

    pub fn detachByCallback(callback: *const EventListeners.Callback) void {
        const self = &state().deferred_calls.event_listeners;
        const index = self.findByCallback(callback) orelse return;
        _ = self.interfaces.orderedRemove(index);
    }

    pub fn detachByPtr(ptr: *anyopaque) void {
        const self = &state().deferred_calls.event_listeners;
        const index = self.findByPtr(ptr) orelse return;
        _ = self.interfaces.orderedRemove(index);
    }

    pub fn findByPtr(ptr: *anyopaque) ?usize {
        const self = &state().deferred_calls.event_listeners;
        for (self.interfaces.items, 0..) |iface, i| {
            if (iface.ptr == ptr) {
                return i;
            }
        }
        return null;
    }

    pub fn findByCallback(callback: *const EventListeners.Callback) ?usize {
        const self = &state().deferred_calls.event_listeners;
        for (self.interfaces.items, 0..) |iface, i| {
            if (iface.callback == callback) {
                return i;
            }
        }
        return null;
    }
};

pub const custom_draw = struct {
    pub fn add(windows: []const BufferWindow.ID, ptr: *anyopaque, drawFn: *const internal.CustomDrawing.DrawFn) !void {
        try modify.addCustomDrawing(state(), windows, ptr, drawFn);
    }

    pub fn remove(windows: []const BufferWindow.ID) void {
        modify.removeCustomDrawing(state(), windows);
    }
};

pub const status_line = struct {
    pub fn add(name: []const u8, alignment: StatusLine.Alignment) !void {
        try state().status_line.add(name, alignment);
    }

    pub fn set(name: []const u8, value: []const u8) void {
        state().status_line.setValue(name, value);
    }
};

// This should be instant ... for now ?
pub const dialog = struct {
    pub fn keyDialog(comptime Closure: type, closure_init_args: anytype, message: []const u8, key_desc: []const Dialog.KeyDesc, importance: Dialog.Importance) !void {
        const ptr = try state().dialogs.createDialog(Closure, closure_init_args);
        errdefer {
            if (@hasDecl(Closure, "deinit")) Closure.deinit(ptr);
        }

        const key_dialog = try Dialog.initKey(getAllocator(), ptr, Closure, message, key_desc, importance);
        state().dialogs.pushDialog(key_dialog) catch unreachable;
        state().input_mode = .dialog;
    }

    pub fn textDialog(comptime Closure: type, closure_init_args: anytype, message: []const u8, importance: Dialog.Importance) !void {
        const ptr = try state().dialogs.createDialog(Closure, closure_init_args);
        errdefer {
            if (@hasDecl(Closure, "deinit")) Closure.deinit(ptr);
        }

        const text_dialog = try Dialog.initText(getAllocator(), ptr, Closure, message, importance);
        state().dialogs.pushDialog(text_dialog) catch unreachable;
        state().input_mode = .dialog;
    }
};

// TODO: Instead of having special handling for the command line buffer and window,
// use the same existing logic for creating and displaying the buffer and window
pub const command_line = struct {
    fn cliBuffer() *Buffer {
        const h = handle();
        return internal.getBufferPtr(h) catch unreachable;
    }

    pub fn handle() BufferHandle {
        return state().cli.bhandle;
    }

    pub fn bufferWindow() BufferWindow.ID {
        return state().cli.buffer_window;
    }

    pub fn isOpen() bool {
        return state().cli.open;
    }

    pub fn open() bool {
        if (state().dialogs.peek() != null) return false;

        state().cli.open = true;
        return true;
    }

    pub fn openAndSet(content: []const u8) bool {
        utils.assert(std.mem.count(u8, content, "\n") == 0, "Newlines aren't allowed");
        if (command_line.open()) {
            dmodify.buffer.setContent(command_line.handle(), content) catch return false;
            const cols = buffer.codePointCountAtLine(command_line.handle(), 0) catch unreachable;
            dmodify.buffer_window.setCursor(command_line.bufferWindow(), .{ .col = cols }) catch return false;

            return true;
        }

        return false;
    }

    pub fn close() void {
        if (!state().cli.open) return;

        state().cli.open = false;
        cliBuffer().clear() catch |err| {
            print("cloudn't clear command_line buffer err={}", .{err});
        };

        dmodify.buffer_window.setCursor(command_line.bufferWindow(), .{}) catch return;
    }

    pub fn run() void {
        var cli_buffer = cliBuffer();
        var command_str: [4096]u8 = undefined;
        const len = cli_buffer.size();

        const command_line_content = cli_buffer.getAllLines(state().allocator) catch return;
        defer state().allocator.free(command_line_content);
        std.mem.copyForwards(u8, &command_str, command_line_content);

        command_line.close();
        state().cli.run(state().allocator, command_str[0 .. len - 1]) catch |err| {
            notify("Command Line Error:", .{}, "{s}", .{@errorName(err)}, 3);
        };
    }

    pub fn addCommand(comptime command: []const u8, comptime fn_ptr: anytype, comptime description: []const u8) !void {
        try state().cli.addCommand(command, fn_ptr, description);
    }
};

////////////////////////////////////////////////////////////////////////////////
//
// functions without a name space
//
//
// For now these functions will be placed here.
//

// TODO: Make this call deferred
pub fn exit() void {
    var iter = state().buffers.iterator();
    while (iter.next()) |buff| {
        if (buff.dirty and buff.path.items.len > 0) {
            dialogs.startForceExit() catch return;
            return;
        }
    }

    state().run_editor = false;
}

pub fn getAllocator() std.mem.Allocator {
    return state().allocator;
}

pub fn dataDirPath() ?[]const u8 {
    return state().data_dir_path;
}

pub fn extraFrames(count: enum { one, two }) void {
    internal.state().extra_frames = switch (count) {
        .one => @max(internal.state().extra_frames, 1),
        .two => 2,
    };
}

pub fn notify(comptime title_fmt: []const u8, title_values: anytype, comptime message_fmt: []const u8, message_values: anytype, time: f32) void {
    internal.state().notifications.add(title_fmt, title_values, message_fmt, message_values, time) catch |err| std.debug.print("{!}\n", .{err});
}

pub fn notifyMessage(comptime message_fmt: []const u8, message_values: anytype, time: f32) void {
    internal.state().notifications.add("", .{}, message_fmt, message_values, time) catch |err| std.debug.print("{!}\n", .{err});
}

pub fn putBufferDisplayer(interface: buffer_displayer.BufferDisplayer) !void {
    try internal.state().buffer_displayers.append(interface);
}

pub fn addTimedCallback(ptr: *anyopaque, callback: internal.TimedCallback.Callback, requested_time: u64) !void {
    try state().timed_callbacks.append(.{
        .ptr = ptr,
        .callback = callback,
        .requested_time = requested_time,
        .timer = try std.time.Timer.start(),
    });
}

pub fn addModifyObserver(callback: *const ModifyObserver.Callback) !void {
    try state().deferred_calls.addObserver(callback);
}

pub fn removeModifyObserver(callback: *const ModifyObserver.Callback) void {
    state().deferred_calls.removeObserver(callback);
}

//
//
// functions without a name space
//
////////////////////////////////////////////////////////////////////////////////

pub const config = struct {
    const configuration = @import("configuration.zig");
    pub usingnamespace configuration;

    const Config = configuration.Config;
    const BufferTypeConfig = configuration.BufferTypeConfig;

    pub var config: Config = .{};

    pub fn get(buffer_type: []const u8) BufferTypeConfig {
        const ptr = getAndPutConfig(buffer_type) catch return getStatic(buffer_type);
        return ptr.*;
    }

    pub fn set(buffer_type: []const u8, values: anytype) void {
        const ptr =
            state().buffer_type_configs.getPtr(buffer_type) orelse
            getAndPutConfig(buffer_type) catch
            return;

        ptr.* = override(ptr.*, values);
    }

    pub fn override(source: BufferTypeConfig, values: anytype) BufferTypeConfig {
        var new = source;
        inline for (std.meta.fields(@TypeOf(values))) |field| {
            switch (std.enums.nameCast(std.meta.FieldEnum(BufferTypeConfig), field.name)) {
                .word_delimiters => {
                    new.word_delimiters = configuration.wordDelimitersInit(
                        values.word_delimiters.slice(),
                    );
                },
                else => @field(new, field.name) = @field(values, field.name),
            }
        }
        return new;
    }

    fn getAndPutConfig(buffer_type: []const u8) !*BufferTypeConfig {
        const bt = try state().path_bt_storage.getAndPut(buffer_type);

        const gop = try state().buffer_type_configs.getOrPut(bt);

        if (!gop.found_existing) {
            gop.value_ptr.* = getStatic(buffer_type);
        }

        return gop.value_ptr;
    }

    fn getStatic(buffer_type: []const u8) BufferTypeConfig {
        if (@hasDecl(user, "config_map")) {
            if (user.config_map.get(buffer_type)) |conf| {
                return conf;
            }
        }

        return configuration.default_config_map.get(buffer_type) orelse .{};
    }
};

pub const Pair = struct {
    buffer: BufferHandle,
    window: BufferWindow.ID,

    pub fn init() !Pair {
        const fb = try dmodify.buffer.createPathless();
        errdefer dmodify.buffer.kill(fb, .{ .force_kill = true }) catch unreachable;
        const fw = try dmodify.buffer_window.create(fb);
        return .{ .buffer = fb, .window = fw };
    }

    pub fn deinit(pair: Pair) void {
        // the window is automatically killed with the buffer
        dmodify.buffer.kill(pair.buffer, .{ .force_kill = true }) catch return;
    }

    pub fn ensureExisting(pair: *Pair) !void {
        if (!buffer.bufferExists(pair.buffer)) {
            pair.buffer = try dmodify.buffer.createPathless();
        }
        if (!buffer_window.exists(pair.window)) {
            pair.window = try dmodify.buffer_window.create(pair.buffer);
        }
    }
};

pub const focused = struct {
    pub fn handle() BufferHandle {
        return get().handle;
    }

    pub fn getData() BufferData {
        return buffer.getBufferData(handle()) catch unreachable;
    }

    pub fn bufferWindow() BufferWindow.ID {
        if (command_line.isOpen()) {
            return command_line.bufferWindow();
        } else {
            return internal.selectedTab().focused_window;
        }
    }

    pub fn cursor() Buffer.Point {
        return buffer_window.cursor(focused.bufferWindow()).?;
    }

    pub fn visiableLines() VisiableLinesResult {
        return buffer_window.visiableLines(focused.bufferWindow()).?;
    }

    pub fn getPoint(index: usize) Buffer.Point {
        var buff = get();
        return buff.getPoint(index);
    }

    pub fn getIndex(point: Buffer.Point) usize {
        return buffer.getIndex(handle(), point).?;
    }

    pub fn moveIndex(index: usize, offset: isize) usize {
        return buffer.moveIndex(handle(), index, offset).?;
    }

    pub fn lineStartIndex(row: u32) usize {
        return buffer.lineStartIndex(handle(), row) catch unreachable;
    }

    pub fn lineEndIndex(row: u32) usize {
        return buffer.lineEndIndex(handle(), row) catch unreachable;
    }

    pub fn rowOfIndex(index: usize) Buffer.RowOfIndex {
        return buffer.rowOfIndex(handle(), index) catch unreachable;
    }

    pub fn nextSearchResultPoint(point: Buffer.Point) ?Buffer.Point {
        return search.nextSearchResultPoint(handle(), point);
    }

    pub fn prevSearchResultPoint(point: Buffer.Point) ?Buffer.Point {
        return search.prevSearchResultPoint(handle(), point);
    }

    pub fn getSelection() Buffer.Selection {
        return get().selection;
    }

    pub fn getSize() Buffer.Size {
        return buffer.getSize(handle() orelse return null);
    }

    pub fn getLines(allocator: std.mem.Allocator, start: u32, end: u32) ![]u8 {
        return buffer.getLines(allocator, handle(), start, end) catch |err| switch (err) {
            error.BufferDoesNotExist => unreachable,
            else => |e| return e,
        };
    }

    pub fn getLine(allocator: std.mem.Allocator, row: u32) ![]u8 {
        return try buffer.getLine(allocator, handle(), row);
    }

    pub fn getCodepointAt(point: Buffer.Point) !u21 {
        var buff = get();
        return try buff.codePointAt(buff.getIndex(point));
    }

    pub fn getBufferRange(allocator: std.mem.Allocator, prange: Buffer.PointRange) ![]u8 {
        return try buffer.getBufferRange(allocator, handle(), prange);
    }

    pub fn codePointCountAtLine(row: u32) u32 {
        return buffer.codePointCountAtLine(handle(), row) catch unreachable;
    }

    pub fn lastColAtRow(row: u32) u32 {
        return focused.codePointCountAtLine(row) - 1;
    }

    fn get() *Buffer {
        const id = focused.bufferWindow();
        const bw = internal.getBufferWindow(id).?;
        const h = bw.bhandle;
        return internal.getBufferPtr(h) catch unreachable;
    }
};
