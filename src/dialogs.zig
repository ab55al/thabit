const std = @import("std");

const core = @import("core.zig");
const dmodify = core.dmodify;

const utils = core.utils;
const input = core.input;

const PointRange = core.Buffer.PointRange;
const Point = core.Buffer.Point;

const k = core.input.Key.create;
const keyDialog = core.dialog.keyDialog;
const texDialog = core.dialog.textDialog;

pub const Dialogs = struct {
    /// Used for the *anyopaque pointers since destroying them individually
    /// is not possible
    ptrs_arena: std.heap.ArenaAllocator,
    queue: std.ArrayList(Dialog),

    pub fn init(allocator: std.mem.Allocator) Dialogs {
        return .{
            .queue = std.ArrayList(core.Dialog).init(allocator),
            .ptrs_arena = std.heap.ArenaAllocator.init(allocator),
        };
    }

    pub fn createDialog(dialogs: *Dialogs, comptime Closure: type, closure_init_args: anytype) !*anyopaque {
        try dialogs.queue.ensureUnusedCapacity(1);
        const data = comptime verifyClosure(Closure);
        const ptr = try dialogs.ptrs_arena.allocator().create(Closure);

        if (!data.callback_only) {
            const ReturnType = @typeInfo(@TypeOf(Closure.init)).Fn.return_type.?;

            switch (@typeInfo(ReturnType)) {
                .ErrorUnion => ptr.* = try @call(.auto, Closure.init, closure_init_args),
                else => ptr.* = @call(.auto, Closure.init, closure_init_args),
            }
        }

        return ptr;
    }

    const ClosureData = struct {
        callback_only: bool,
    };
    fn verifyClosure(comptime Closure: type) ClosureData {
        const has_init = @hasDecl(Closure, "init");
        comptime {
            const has_deinit = @hasDecl(Closure, "deinit");
            const closure_name = "Closure `" ++ @typeName(Closure) ++ "`";
            const common = "Dialog: " ++ closure_name ++ " must contain an init, deinit and callback functions ";
            if (has_init and !has_deinit) {
                @compileError(common ++ "but no `init` function was found");
            }

            if (!has_init and has_deinit) {
                @compileError(common ++ "but no `deinit` function was found");
            }

            if (!@hasDecl(Closure, "callback")) {
                @compileError(common ++ "but no `callback` function was found");
            }
        }

        return .{
            .callback_only = !has_init,
        };
    }

    pub fn pushDialog(dialogs: *Dialogs, dialog: Dialog) !void {
        try dialogs.queue.append(dialog);
    }

    pub fn peek(dialogs: *const Dialogs) ?Dialog {
        const queue = dialogs.queue.items;
        if (queue.len == 0) return null;

        return queue[0];
    }

    pub fn pop(dialogs: *Dialogs) ?Dialog {
        const queue = dialogs.queue.items;
        if (queue.len == 0) return null;

        return dialogs.queue.orderedRemove(0);
    }

    pub fn deinitAllDialogs(dialogs: *Dialogs) void {
        for (dialogs.queue.items) |*d| {
            d.deinit();
        }
        dialogs.queue.clearAndFree();
        _ = dialogs.ptrs_arena.reset(.free_all);
    }

    pub fn deinit(dialogs: *Dialogs) void {
        dialogs.ptrs_arena.deinit();
        dialogs.queue.deinit();
    }

    pub const Dialog = struct {
        pub const Importance = enum { low, high };
        pub const Key = struct {
            pub const KeyCallback = fn (ptr: *anyopaque, pressed_key: input.Key) Result;
            keys_info: []const KeyDesc,
            callback: *const KeyCallback,
        };

        pub const Text = struct {
            pub const TextCallback = fn (ptr: *anyopaque, text: []const u8) Result;
            callback: *const TextCallback,
        };

        pub const Result = enum { done, not_done };
        pub const KeyDesc = struct { key: input.Key, description: []const u8 };
        pub const KindTag = std.meta.Tag(Kind);
        pub const Kind = union(enum) {
            key: Key,
            text: Text,
        };

        allocator: std.mem.Allocator,

        ptr: *anyopaque,
        deinitClosure: ?*const fn (ptr: *anyopaque) void = null,

        string_storage: core.slice_storage.StringStorageUnmanaged = .{},
        kind: Kind,
        message: []const u8,
        importance: Importance = .low,

        pub fn initText(allocator: std.mem.Allocator, ptr: *anyopaque, closure: anytype, message: []const u8, importance: Importance) !Dialog {
            var storage = core.slice_storage.StringStorageUnmanaged{};
            errdefer storage.deinit(allocator);
            return .{
                .allocator = allocator,
                .ptr = ptr,
                .message = try storage.getAndPut(allocator, message),
                .kind = .{ .text = .{ .callback = closure.callback } },
                .deinitClosure = if (@hasDecl(closure, "deinit")) closure.deinit else null,
                .string_storage = storage,
                .importance = importance,
            };
        }

        pub fn initKey(allocator: std.mem.Allocator, ptr: *anyopaque, closure: anytype, message: []const u8, keys_desc: []const KeyDesc, importance: Importance) !Dialog {
            var storage = core.slice_storage.StringStorageUnmanaged{};
            errdefer storage.deinit(allocator);
            const keys_info = try allocator.alloc(KeyDesc, keys_desc.len);
            errdefer allocator.free(keys_info);

            for (keys_desc, 0..) |key, i| {
                keys_info[i] = .{
                    .key = key.key,
                    .description = try storage.getAndPut(allocator, key.description),
                };
            }

            return .{
                .allocator = allocator,
                .ptr = ptr,
                .message = try storage.getAndPut(allocator, message),
                .kind = .{ .key = .{ .callback = closure.callback, .keys_info = keys_info } },
                .string_storage = storage,
                .deinitClosure = if (@hasDecl(closure, "deinit")) closure.deinit else null,
                .importance = importance,
            };
        }

        pub fn deinit(self: *Dialog) void {
            if (self.deinitClosure) |deinitClosure| deinitClosure(self.ptr);

            switch (self.kind) {
                .key => self.allocator.free(self.kind.key.keys_info),
                else => {},
            }

            self.string_storage.deinit(self.allocator);
        }
    };
};

pub const Replace = struct {
    const Self = @This();

    pub const key_desc: []const Dialogs.Dialog.KeyDesc = &.{
        .{ .key = k(.none, .y), .description = "Accept" },
        .{ .key = k(.none, .n), .description = "Reject" },
        .{ .key = k(.none, .a), .description = "Accept All" },
        .{ .key = k(.none, .c), .description = "Cancel" },
    };

    replacement: []const u8,

    pub fn init(replacement: []const u8) !Self {
        const allocator = core.getAllocator();
        const self = Self{
            .replacement = try allocator.dupe(u8, replacement),
        };
        errdefer allocator.free(self.replacement);

        if (core.internal.state().search_keeper.rangesForBuffer(core.focused.handle())) |ranges| {
            try dmodify.buffer_window.setCursor(core.focused.bufferWindow(), ranges[0].start);
        } else {
            return error.CannotInit;
        }

        return self;
    }

    pub fn deinit(ptr: *anyopaque) void {
        const self: *Self = @ptrCast(@alignCast(ptr));
        core.getAllocator().free(self.replacement);
    }

    pub fn callback(ptr: *anyopaque, key: core.input.Key) core.Dialog.Result {
        const self: *Self = @ptrCast(@alignCast(ptr));
        const rep = self.replacement;

        return switch (key.kind) {
            .y => blk: {
                const prange = getRange() orelse return .done;
                dmodify.buffer.replaceRange(core.focused.handle(), prange, rep) catch break :blk .done;
                setNextRange();
                break :blk .not_done;
            },
            .a => blk: {
                const og = core.internal.state().search_keeper.rangesForBuffer(core.focused.handle()) orelse
                    break :blk .done;
                const ranges = core.getAllocator().dupe(PointRange, og) catch unreachable;

                const cursor = core.focused.cursor();
                var i = ranges.len;
                while (i > 0) {
                    i -= 1;
                    const pr = ranges[i];
                    if (pr.start.row < cursor.row) break;
                    if (pr.start.row == cursor.row and pr.start.col < cursor.col) break;

                    dmodify.buffer.replaceRange(core.focused.handle(), pr, rep) catch break :blk .done;
                }

                core.getAllocator().free(ranges);
                break :blk .done;
            },

            .n => blk: {
                const ranges = core.internal.state().search_keeper.rangesForBuffer(core.focused.handle()) orelse
                    break :blk .done;
                if (std.meta.eql(ranges[ranges.len - 1].start, core.focused.cursor())) {
                    break :blk .done;
                }

                setNextRange();
                break :blk .not_done;
            },
            .c => .done,
            else => .not_done,
        };
    }

    fn compareFn(comptime context: type, key: Point, mid: PointRange) std.math.Order {
        _ = context;
        if (key.row < mid.start.row) return .lt;
        if (key.row > mid.start.row) return .gt;

        if (key.col < mid.start.col) return .lt;
        if (key.col > mid.start.col) return .gt;

        return .eq;
    }

    pub fn getRange() ?PointRange {
        const ranges = core.internal.state().search_keeper.rangesForBuffer(core.focused.handle()) orelse return null;
        const cursor = core.focused.cursor();
        const i = std.sort.binarySearch(PointRange, cursor, ranges, void, compareFn) orelse return null;
        return ranges[i];
    }

    pub fn setNextRange() void {
        const cursor = core.focused.cursor();
        if (core.search.nextSearchResultPoint(core.focused.handle(), cursor)) |next|
            dmodify.buffer_window.setCursor(core.focused.bufferWindow(), next) catch return;
    }
};

pub const ForceOpenBuffer = struct {
    path: []const u8,
    opts: core.OpenOptions,

    pub fn callback(self: *ForceOpenBuffer, confirm: bool) void {
        if (confirm) {
            const handle = core.internal.deferCreateBuffer(core.internal.state(), self.path) catch return;
            const opts = if (std.mem.eql(u8, core.focused.getData().buffer_type, core.scratch_buffer_type))
                utils.override(self.opts, .{ .where = .here })
            else
                self.opts;

            _ = dmodify.buffer.openByHandle(handle, opts) catch return;
        }
    }
};

pub fn startForceOpenBuffer(path: []const u8, opts: core.OpenOptions) !void {
    std.debug.assert(std.fs.path.isAbsolute(path));

    const key_desc: []const Dialogs.Dialog.KeyDesc = &.{
        .{ .key = k(.none, .y), .description = "Force open" },
        .{ .key = k(.none, .n), .description = "Cancel" },
    };
    const fmt = "Buffer open by another process.\n{s}";
    var buf: [std.fs.MAX_PATH_BYTES + fmt.len]u8 = undefined;
    const message = try std.fmt.bufPrint(&buf, fmt, .{path});
    const v = ForceOpenBuffer{
        .path = path,
        .opts = opts,
    };
    try keyDialog(
        YesNo(ForceOpenBuffer),
        .{ ForceOpenBuffer.callback, v },
        message,
        key_desc,
        .high,
    );
}

pub fn startForceExit() !void {
    if (!force_exit.currently_open) {
        try keyDialog(force_exit, .{}, "You have unsaved buffers. Force exit ?", force_exit.key_desc, .high);
        force_exit.currently_open = true;
    }
}

pub const force_exit = struct {
    pub const key_desc: []const Dialogs.Dialog.KeyDesc = &.{
        .{ .key = k(.none, .y), .description = "Yes" },
        .{ .key = k(.none, .n), .description = "No" },
    };

    pub var currently_open = false;
    pub fn callback(_: *anyopaque, key: core.input.Key) core.Dialog.Result {
        if (key.mod != .none) return .not_done;

        switch (key.kind) {
            .y => {
                keyDialog(
                    YesNo(void),
                    .{ confirmCallback, {} },
                    "Are you sure?",
                    YesNo(void).key_desc,
                    .high,
                ) catch return .done;
            },
            .n => {},
            else => return .not_done,
        }

        currently_open = false;
        return .done;
    }

    pub fn confirmCallback(_: *void, confirm: bool) void {
        if (confirm) {
            core.internal.state().run_editor = false;
            currently_open = false;
        }
    }
};

pub fn YesNo(comptime T: type) type {
    return struct {
        const Self = @This();

        pub const key_desc: []const Dialogs.Dialog.KeyDesc = &.{
            .{ .key = k(.none, .y), .description = "Yes" },
            .{ .key = k(.none, .n), .description = "No" },
        };

        pub const Callback = *const fn (value: *T, bool) void;
        data: T,
        providedCallback: Callback,

        pub fn init(cb: Callback, data: T) Self {
            return .{
                .data = data,
                .providedCallback = cb,
            };
        }
        pub fn deinit(_: *anyopaque) void {}

        pub fn callback(ptr: *anyopaque, key: core.input.Key) core.Dialog.Result {
            if (key.mod != .none) return .not_done;
            const self: *Self = @ptrCast(@alignCast(ptr));

            switch (key.kind) {
                .y => self.providedCallback(&self.data, true),
                .n => self.providedCallback(&self.data, false),
                else => return .not_done,
            }

            return .done;
        }
    };
}

pub const test_text_from_buffer = struct {
    pub fn callback(_: *anyopaque, text: []const u8) core.dialog.Result {
        if (std.ascii.indexOfIgnoreCase(text, "stop") != null) {
            return .done;
        } else {
            return .not_done;
        }
    }
};
