const std = @import("std");
const print = @import("std").debug.print;
const ArrayList = std.ArrayList;
const count = std.mem.count;

const core = @import("core.zig");
const dmodify = core.dmodify;

const Buffer = @import("buffer.zig");
const internal = @import("internal.zig");
const buffer_window = @import("buffer_window.zig");
const file_io = @import("file_io.zig");
const Marks = @import("marks.zig");
const utils = @import("utils.zig");
const dialogs = @import("dialogs.zig");

pub const FuncType = *const fn ([]Token) CommandRunError!void;
pub const CommandType = struct {
    function: FuncType,
    description: []const u8,
};

const ParseError = error{
    InvalidDoubleQuoteNumber,
    InvalidNumberOfDoubleQuote,
    ContainsInvalidValues,
};

const CommandRunError = error{
    FunctionCommandMismatchedTypes,
    ExtraArgs,
    MissingArgs,
    CommandDoesNotExist,
};

pub const CommandLine = struct {
    bhandle: internal.BufferHandle,
    buffer_window: internal.BufferWindow.ID,
    functions: std.StringHashMap(CommandType),
    open: bool = false,

    pub fn init(allocator: std.mem.Allocator, win_id: buffer_window.BufferWindow.ID, cli_bhandle: internal.BufferHandle) CommandLine {
        const cli = CommandLine{
            .bhandle = cli_bhandle,
            .buffer_window = win_id,
            .functions = std.StringHashMap(CommandType).init(allocator),
        };

        return cli;
    }

    pub fn deinit(cli: *CommandLine) void {
        cli.functions.deinit();
    }

    pub fn addCommand(cli: *CommandLine, comptime command: []const u8, comptime fn_ptr: anytype, comptime description: []const u8) !void {
        const fn_info = @typeInfo(@TypeOf(fn_ptr)).Fn;
        if (fn_info.return_type.? != void)
            @compileError("The command's function return type needs to be void");
        if (fn_info.is_var_args)
            @compileError("The command's function cannot be variadic");

        if (command.len == 0) return error.EmptyCommand;
        if (std.mem.count(u8, command, " ") > 0) return error.CommandContainsSpaces;

        try cli.functions.put(command, .{
            .function = beholdMyFunctionInator(fn_ptr),
            .description = description,
        });
    }

    pub fn run(cli: *CommandLine, allocator: std.mem.Allocator, command_string: []const u8) !void {
        _ = allocator;

        var command_end_index: usize = 0;
        var command: []const u8 = "";
        {
            var lexer = Lexer.init(command_string);
            const cmd = (try lexer.nextToken()) orelse return;
            command_end_index = lexer.pos;
            command = switch (cmd) {
                .string => |str| str,
                else => return error.CommandNotAString,
            };
        }

        var tokens = std.BoundedArray(Token, 256).init(0) catch unreachable;
        {
            var lexer = Lexer.init(command_string[command_end_index..]);
            while (try lexer.nextToken()) |token|
                tokens.append(token) catch return error.TooManyArgs;
        }

        try cli.call(command, tokens.slice());
    }

    fn call(cli: *CommandLine, command: []const u8, args: []Token) !void {
        const com = cli.functions.get(command);

        if (com) |c| {
            try c.function(args);
        } else return CommandRunError.CommandDoesNotExist;
    }
};

pub fn beholdMyFunctionInator(comptime function: anytype) FuncType {
    const fn_info = @typeInfo(@TypeOf(function)).Fn;

    return struct {
        pub fn inator(tokens: []Token) CommandRunError!void {
            if (fn_info.params.len == 0) {
                function();
            } else if (tokens.len > 0 and tokens.len == fn_info.params.len) {
                const Tuple = std.meta.ArgsTuple(@TypeOf(function));
                var args_tuple: Tuple = undefined;
                inline for (args_tuple, 0..) |_, index| {
                    const TupleElement = @TypeOf(args_tuple[index]);

                    switch (@typeInfo(TupleElement)) {
                        .Int => {
                            args_tuple[index] = switch (tokens[index]) {
                                .int, .float => blk: {
                                    const min = std.math.minInt(TupleElement);
                                    const max = std.math.maxInt(TupleElement);

                                    break :blk switch (tokens[index]) {
                                        .int => |int| @intCast(utils.bound(int, min, max)),
                                        .float => |float| @intFromFloat(utils.bound(float, min, max)),
                                        else => unreachable,
                                    };
                                },

                                else => return CommandRunError.FunctionCommandMismatchedTypes,
                            };
                        },

                        .Float => {
                            args_tuple[index] = switch (tokens[index]) {
                                .int, .float => blk: {
                                    const min = std.math.floatMin(TupleElement);
                                    const max = std.math.floatMax(TupleElement);

                                    const signed_min: isize = @intFromFloat(@floor(min));
                                    const signed_max: isize = @intFromFloat(@ceil(max));

                                    break :blk switch (tokens[index]) {
                                        .int => |int| @floatFromInt(utils.bound(int, signed_min, signed_max)),
                                        .float => |float| @floatCast(utils.bound(float, min, max)),
                                        else => unreachable,
                                    };
                                },

                                else => return CommandRunError.FunctionCommandMismatchedTypes,
                            };
                        },

                        .Pointer => {
                            if (TupleElement != []const u8)
                                @compileError("`[]const u8` is the only pointer and slice type allowed but you have provided " ++
                                    "`" ++ @typeName(TupleElement) ++ "`");

                            args_tuple[index] = switch (tokens[index]) {
                                .string => |string| string,
                                else => return CommandRunError.FunctionCommandMismatchedTypes,
                            };
                        },

                        .Bool => {
                            args_tuple[index] = switch (tokens[index]) {
                                .bool => |b| b,
                                else => return CommandRunError.FunctionCommandMismatchedTypes,
                            };
                        },

                        else => @compileError("Only types allowed are `[]const u8`, `bool`, signed and unsigned ints and floats"),
                    }
                }

                @call(.auto, function, args_tuple);
            } else if (tokens.len < fn_info.params.len) {
                return CommandRunError.MissingArgs;
            } else if (tokens.len > fn_info.params.len) {
                return CommandRunError.ExtraArgs;
            }
        }
    }.inator;
}

const Token = union(enum) {
    int: isize,
    float: f64,
    string: []const u8,
    bool: bool,

    pub fn eql(a: Token, b: Token) bool {
        if (std.meta.activeTag(a) != std.meta.activeTag(b)) return false;

        return switch (a) {
            .string => std.mem.eql(u8, a.string, b.string),
            else => std.meta.eql(a, b),
        };
    }
};

const Lexer = struct {
    input: []const u8,
    pos: u64,
    char: ?u8,

    pub fn init(input: []const u8) Lexer {
        return .{
            .input = input,
            .pos = 0,
            .char = if (input.len > 0) input[0] else null,
        };
    }

    pub fn tokens(lexer: *Lexer, allocator: std.mem.Allocator) ![]Token {
        var toks = std.ArrayList(Token).init(allocator);
        while (try lexer.nextToken()) |token| try toks.append(token);
        return toks.toOwnedSlice();
    }

    pub fn nextToken(lexer: *Lexer) !?Token {
        lexer.skipWhiteSpace();
        const char = lexer.char orelse return null;
        return switch (char) {
            '"' => blk: {
                lexer.advance();
                const start = lexer.pos;

                lexer.advanceUntilEqual('"') catch return error.InvalidDoubleQuoteNumber;
                const end = lexer.pos;
                lexer.advance();

                break :blk Token{ .string = lexer.input[start..end] };
            },
            '-', '0'...'9' => blk: {
                const start = lexer.pos;

                while (lexer.char) |c| if (std.ascii.isDigit(c) or c == '.' or c == '-') lexer.advance() else break;

                const end = lexer.pos;
                const number_string = lexer.input[start..end];

                errdefer std.log.warn("failed to parse number: {s}\n", .{number_string});

                const dot_count = std.mem.count(u8, number_string, ".");
                break :blk switch (dot_count) {
                    0 => Token{ .int = try std.fmt.parseInt(isize, number_string, 10) },
                    1 => Token{ .float = try std.fmt.parseFloat(f64, number_string) },
                    else => return error.InvalidNumberOfDots,
                };
            },
            else => blk: {
                { // bool
                    var bool_lexer = lexer.*;
                    var boolean = true;
                    if (bool_lexer.peekMany("true".len)) |string| {
                        if (std.mem.eql(u8, string, "true")) {
                            bool_lexer.advanceMany("true".len);
                            boolean = true;
                        }
                    }

                    if (bool_lexer.peekMany("false".len)) |string| {
                        if (std.mem.eql(u8, string, "false")) {
                            bool_lexer.advanceMany("false".len);
                            boolean = false;
                        }
                    }

                    if (bool_lexer.char == null or std.ascii.isWhitespace(bool_lexer.char.?)) {
                        lexer.* = bool_lexer;
                        break :blk Token{ .bool = boolean };
                    }
                } // bool end

                { // quote less string
                    const start = lexer.pos;
                    while (lexer.char) |c| if (std.ascii.isWhitespace(c)) break else lexer.advance();
                    const end = lexer.pos;
                    break :blk Token{ .string = lexer.input[start..end] };
                }
            },
        };
    }

    pub fn advance(lexer: *Lexer) void {
        lexer.pos += 1;
        lexer.char = if (lexer.pos >= lexer.input.len) null else lexer.input[lexer.pos];
    }

    pub fn advanceMany(lexer: *Lexer, num: u64) void {
        lexer.pos += num;
        lexer.char = if (lexer.pos >= lexer.input.len) null else lexer.input[lexer.pos];
    }

    pub fn peekMany(lexer: *const Lexer, num: u64) ?[]const u8 {
        return if (lexer.pos + num <= lexer.input.len)
            lexer.input[lexer.pos .. lexer.pos + num]
        else
            null;
    }

    pub fn advanceUntilEqual(lexer: *Lexer, char: u8) !void {
        const index = std.mem.indexOf(u8, lexer.input[lexer.pos..], &.{char}) orelse return error.NoCharFound;
        lexer.pos += index;
        lexer.char = lexer.input[lexer.pos];
    }

    pub fn skipWhiteSpace(lexer: *Lexer) void {
        while (lexer.char) |char| if (std.ascii.isWhitespace(char)) lexer.advance() else break;
    }
};

test "valid tokens" {
    const input =
        \\     "hello there"       1234 -4321  "  "  123.  99.99 -88.88 true false quote_less_string
    ;

    var lexer = Lexer.init(input);
    var tokens = try lexer.tokens(std.testing.allocator);
    defer std.testing.allocator.free(tokens);

    try std.testing.expect(tokens[0].eql(.{ .string = "hello there" }));
    try std.testing.expect(tokens[1].eql(.{ .int = 1234 }));
    try std.testing.expect(tokens[2].eql(.{ .int = -4321 }));
    try std.testing.expect(tokens[3].eql(.{ .string = "  " }));
    try std.testing.expect(tokens[4].eql(.{ .float = 123 }));
    try std.testing.expect(tokens[5].eql(.{ .float = 99.99 }));
    try std.testing.expect(tokens[6].eql(.{ .float = -88.88 }));
    try std.testing.expect(tokens[7].eql(.{ .bool = true }));
    try std.testing.expect(tokens[8].eql(.{ .bool = false }));
    try std.testing.expect(tokens[9].eql(.{ .string = "quote_less_string" }));
    try std.testing.expect(tokens.len == 10);
}

test "invalid tokens" {
    const input =
        \\     "hello
    ;

    var lexer = Lexer.init(input);
    const tokens = lexer.tokens(std.testing.allocator);
    try std.testing.expectError(ParseError.InvalidDoubleQuoteNumber, tokens);
}

pub const default_commands = struct {
    pub fn setDefaultCommands(cli: *CommandLine) !void {
        try cli.addCommand("o", openHere, "Open a buffer on the current window");
        try cli.addCommand("or", openDir(.right), "Open a buffer to the right of the current window");
        try cli.addCommand("ol", openDir(.left), "Open a buffer to the left of the current window");
        try cli.addCommand("ou", openDir(.up), "Open a buffer above the current window");
        try cli.addCommand("od", openDir(.down), "Open a buffer below the current window");
        try cli.addCommand("ot", newTab, "Open a buffer in a new tab");

        try cli.addCommand("rename", rename, "Rename the focused buffer");

        try cli.addCommand("save", saveFocused, "Save the buffer");
        try cli.addCommand("saveAs", saveAsFocused, "Save the buffer as");
        try cli.addCommand("forceSave", forceSaveFocused, "Force the buffer to save");
        try cli.addCommand("close", closeFocused, "Closes the focused buffer window");
        try cli.addCommand("sq", saveAndQuitFocused, "Save and kill the focused buffer window");
        try cli.addCommand("forceSaveAndQuit", forceSaveAndQuitFocused, "Force save and kill the focused buffer window");
        try cli.addCommand("kill", kill, "kill the focused buffer");

        try cli.addCommand("search", search, "Search");
        try cli.addCommand("sr", searchAndReplace, "Search and replace");
    }

    fn open(file_path: []const u8, opts: core.OpenOptions) void {
        if (file_path.len == 0) return;

        const data = core.jump_list.currentJumpData();

        _ = dmodify.buffer.openByPath(file_path, opts) catch |err| {
            print("open command: err={}\n", .{err});
        };

        dmodify.jump_list.pushOldAndCurrent(data) catch return;
    }

    fn openHere(file_path: []const u8) void {
        open(file_path, .{ .where = .here });
    }

    fn openDir(comptime dir: buffer_window.WindowTree.Dir) fn ([]const u8) void {
        return struct {
            fn func(file_path: []const u8) void {
                open(file_path, .{ .where = .{ .dir = dir } });
            }
        }.func;
    }

    fn search(string: []const u8) void {
        const opts = core.SearchOptions{
            .search_source = core.config.config.defualt_search_source,
            .buffers_to_search = .visiable,
        };
        core.search.searchLines(string, 0, std.math.maxInt(u32), opts) catch |err| {
            print("search command: err={}\n", .{err});
        };
    }

    fn searchAndReplace(search_term: []const u8, replace: []const u8) void {
        core.search.searchLines(search_term, 0, std.math.maxInt(u32), .{}) catch |err| {
            print("search command: err={}\n", .{err});
        };

        var buf: [2048]u8 = undefined;
        const message = std.fmt.bufPrint(&buf, "Replace {s} with {s} ?", .{ search_term, replace }) catch unreachable;

        const closure = dialogs.Replace;
        core.dialog.keyDialog(closure, .{replace}, message, closure.key_desc, .low) catch |err| {
            print("dialog error: err={}\n", .{err});
        };
    }

    fn newTab(file_path: []const u8) void {
        if (file_path.len == 0) return;
        _ = dmodify.buffer.openByPath(file_path, .{ .where = .new_tab }) catch |err| {
            print("newTab command: err={}\n", .{err});
        };
    }

    fn rename(new_name: []const u8) void {
        if (new_name.len == 0) return;

        const handle = core.focused.handle();
        dmodify.buffer.renameFromDir(handle, new_name) catch |err| {
            print("rename command: err={}\n", .{err});
        };
    }

    fn kill() void {
        const handle = core.focused.handle();

        dmodify.buffer.kill(handle, .{}) catch |err| {
            print("kill command: err={}\n", .{err});
        };
    }

    fn saveFocused() void {
        const handle = core.focused.handle();
        dmodify.buffer.save(handle, .{}) catch |err| {
            if (err == file_io.Error.DifferentModTimes) {
                print("The file's contents might've changed since last load\n", .{});
                print("To force saving use forceSave", .{});
            } else {
                print("err={}\n", .{err});
            }
        };
    }

    fn saveAsFocused(path: []const u8) void {
        if (path.len == 0) return;
        const handle = core.focused.handle();

        dmodify.buffer.renameFromDir(handle, path) catch return;
        dmodify.buffer.save(handle, .{}) catch |err| {
            if (err == file_io.Error.DifferentModTimes) {
                print("The file's contents might've changed since last load\n", .{});
                print("To force saving use forceSave", .{});
            } else {
                print("err={}\n", .{err});
            }
        };
    }

    fn forceSaveFocused() void {
        const handle = core.focused.handle();
        dmodify.buffer.save(handle, .{ .force_save = true }) catch |err|
            print("err={}\n", .{err});
    }

    fn closeFocused() void {
        dmodify.buffer_window.close(core.focused.bufferWindow()) catch return;
    }

    fn saveAndQuitFocused() void {
        const handle = core.focused.handle();
        dmodify.buffer.save(handle, .{}) catch |err| {
            if (err == file_io.Error.DifferentModTimes) {
                print("The file's contents might've changed since last load\n", .{});
                print("To force saving use forceSaveAndQuit\n", .{});
            } else {
                print("err={}\n", .{err});
            }
        };
        dmodify.buffer_window.close(core.focused.bufferWindow()) catch return;
    }

    fn forceSaveAndQuitFocused() void {
        const handle = core.focused.handle();
        dmodify.buffer.save(handle, .{ .force_save = true }) catch |err| {
            print("err={}\n", .{err});
        };

        dmodify.buffer_window.close(core.focused.bufferWindow()) catch return;
    }
};
