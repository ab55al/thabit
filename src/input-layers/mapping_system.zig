const std = @import("std");

const core = @import("core");
const Key = core.input.Key;
const KeyStorageUnmanaged = core.slice_storage.SliceStorageUnmanaged(Key, KeySliceHashContext);
const StringStorageUnmanaged = core.slice_storage.StringStorageUnmanaged;

pub const max_key_count = 100;
pub const max_current_keys_string = max_key_count * 3 * Key.max_string_len;

pub fn MappingSystem(comptime ConditionData: type) type {
    comptime {
        if (!@hasField(ConditionData, "buffer_type")) {
            @compileError("MappingSystem: ConditionData must have a `buffer_type` field");
        }

        if (!@hasDecl(ConditionData, "eql")) {
            @compileError("MappingSystem: ConditionData must implement an `eql` function");
        }
    }
    return struct {
        pub const RunResult = union(enum) {
            need_more_keys,
            mapping_does_not_exist,
            done: struct {
                vtable: core.input.RegularVTable,
                repeat: usize,
            },
        };

        const Self = @This();

        pub const KeyData = struct {
            vtable: core.input.RegularVTable,
            condition_data: ConditionData,
            description: []const u8,
        };
        pub const Entry = struct {
            keys: []const Key,
            data: std.ArrayListUnmanaged(KeyData) = .{},
        };

        allocator: std.mem.Allocator,
        entries: std.ArrayListUnmanaged(Entry) = .{},

        current_key_repeats: std.BoundedArray(Key, max_key_count) = std.BoundedArray(Key, max_key_count).init(0) catch unreachable,
        current_key_queue: std.BoundedArray(Key, max_key_count) = std.BoundedArray(Key, max_key_count).init(0) catch unreachable,

        key_storage: KeyStorageUnmanaged = .{},
        string_storage: StringStorageUnmanaged = .{},

        pub fn init(allocator: std.mem.Allocator) Self {
            return .{ .allocator = allocator };
        }

        pub fn deinit(self: *Self) void {
            for (self.entries.items) |*entry| {
                entry.data.deinit(self.allocator);
            }

            self.entries.deinit(self.allocator);
            self.key_storage.deinit(self.allocator);
            self.string_storage.deinit(self.allocator);
        }

        pub fn accumulateKey(self: *Self, key: Key) void {
            const is_repeat_key = switch (key.kind) {
                .one,
                .two,
                .three,
                .four,
                .five,
                .six,
                .seven,
                .eight,
                .nine,
                => true and key.mod == .none,
                .zero => key.mod == .none and self.current_key_repeats.len > 0,
                else => false,
            };

            if (is_repeat_key and self.current_key_queue.len == 0) {
                self.current_key_repeats.append(key) catch self.clearCurrentKeys();
            } else {
                self.current_key_queue.append(key) catch self.clearCurrentKeys();
            }
        }

        pub fn run(self: *Self, required_condition: ConditionData) !RunResult {
            const repeat = blk: {
                var buf: [max_current_keys_string]u8 = undefined;
                var len: usize = 0;
                for (self.current_key_repeats.slice()) |key| {
                    if (key.mod != .none) break :blk 1;
                    len += (key.toString(buf[len..])).len;
                }
                break :blk std.fmt.parseInt(usize, buf[0..len], 10) catch 1;
            };

            const keys = self.current_key_queue.slice();
            switch (self.getMapping(keys, required_condition)) {
                .prefix_keys => return .need_more_keys,
                .does_not_exist => return .mapping_does_not_exist,
                .exact_keys => |key_data| {
                    try key_data.vtable.regular(repeat);

                    const run_result = .{
                        .vtable = key_data.vtable,
                        .repeat = repeat,
                    };
                    return .{ .done = run_result };
                },
            }

            return .mapping_does_not_exist;
        }

        pub fn put(self: *Self, arg_keys: []const Key, data: KeyData) !void {
            const desc = try self.string_storage.getAndPut(self.allocator, data.description);
            const keys = try self.key_storage.getAndPut(self.allocator, arg_keys);

            const entry = self.getEntry(keys) orelse blk: {
                try self.entries.append(self.allocator, .{ .keys = keys });
                break :blk &self.entries.items[self.entries.items.len - 1];
            };

            try entry.data.append(self.allocator, .{
                .vtable = data.vtable,
                .condition_data = data.condition_data,
                .description = desc,
            });
        }

        pub fn getEntry(self: *const Self, keys: []const Key) ?*Entry {
            for (self.entries.items) |*entry| {
                if (Key.eqlSlice(keys, entry.keys)) return entry;
            }

            return null;
        }

        pub fn getCurrentKeysString(self: *Self, out: []u8) []u8 {
            std.debug.assert(out.len >= max_current_keys_string);

            var len: usize = 0;
            for (self.current_key_repeats.slice()) |key| len += key.toString(out[len..]).len;
            for (self.current_key_queue.slice()) |key| len += key.toString(out[len..]).len;

            return out[0..len];
        }

        pub fn clearCurrentKeys(self: *Self) void {
            self.current_key_repeats.resize(0) catch unreachable;
            self.current_key_queue.resize(0) catch unreachable;
        }

        pub fn getMapping(self: *const Self, keys: []const Key, required: ConditionData) union(enum) { exact_keys: KeyData, prefix_keys, does_not_exist } {
            for (self.entries.items) |entry| {
                if (keys.len > entry.keys.len) continue;

                const part_keys = entry.keys[0..keys.len];
                for (entry.data.items) |key_data| {
                    const condition = required.eql(key_data.condition_data);
                    if (Key.eqlSlice(keys, entry.keys) and condition) return .{ .exact_keys = key_data };
                    if (Key.eqlSlice(keys, part_keys) and condition) return .prefix_keys;
                }
            }

            return .does_not_exist;
        }
    };
}

pub const KeySliceHashContext = struct {
    const KeySlice = []const Key;

    pub fn hash(_: @This(), s: KeySlice) u64 {
        var wh = std.hash.Wyhash.init(0);

        for (s) |key| {
            wh.update(std.mem.asBytes(&key.kind));
            wh.update(std.mem.asBytes(&key.mod));
        }

        return wh.final();
    }

    pub fn eql(_: @This(), a: KeySlice, b: KeySlice) bool {
        return Key.eqlSlice(a, b);
    }
};
