const std = @import("std");
const fs = std.fs;
const print = std.debug.print;
const indexOf = std.mem.indexOf;

const imgui = @import("imgui");

const core = @import("core");
const dmodify = core.dmodify;

const focused = core.focused;
const Buffer = core.Buffer;
const Range = Buffer.Range;
const PointRange = Buffer.PointRange;
const Point = Buffer.Point;
const BufferHandle = core.BufferHandle;

const mapping_system = @import("mapping_system.zig");

const editor = core.editor;
const input = core.input;
const Key = input.Key;

const TextObjectKind = core.text_objects.TextObjectKind;
const MotionKind = core.motions.MotionKind;

const log = std.log.scoped(.vim_like);

const k = core.input.Key.create;

pub const default_bt = "";
const default_register = @tagName(core.Registers.Named.default);

const FindOperation = enum { find, till, bfind, btill };
const MotionOperation = enum { change, delete, yank, move };

var _state: ?State = null;
fn state() *State {
    return &(_state.?);
}

pub fn init() !void {
    try core.status_line.add("keys", .mid);

    _state = try State.init(core.getAllocator());

    createMappings();
}

pub fn deinit() void {
    state().deinit();
    _state = null;
}

fn createMappings() void {
    const all_modes_s: []const State.Mode = &.{ .normal, .insert, .visual, .replace };
    map(&.{k(.none, .left)}, .{ .container = fns.moveContainer(.prev_col), .modes = all_modes_s });
    map(&.{k(.none, .right)}, .{ .container = fns.moveContainer(.next_col), .modes = all_modes_s });
    map(&.{k(.none, .up)}, .{ .container = fns.moveContainer(.prev_row), .modes = all_modes_s });
    map(&.{k(.none, .down)}, .{ .container = fns.moveContainer(.next_row), .modes = all_modes_s });

    // normal - visual mappings
    const normal_visual_s: []const State.Mode = &.{ .normal, .visual };
    map(&.{k(.none, .j)}, .{ .container = fns.moveContainer(.next_row), .modes = normal_visual_s });

    map(&.{k(.none, .k)}, .{ .container = fns.moveContainer(.prev_row), .modes = normal_visual_s });
    map(&.{ k(.none, .g), k(.none, .g) }, .{ .container = fns.moveContainer(.top_of_buffer), .modes = normal_visual_s });
    map(&.{k(.shift, .g)}, .{ .container = fns.moveContainer(.bottom_of_buffer), .modes = normal_visual_s });

    map(&.{k(.none, .f)}, .{ .container = find_dialog.StartFindDialog(.find, .move), .modes = normal_visual_s });
    map(&.{k(.none, .t)}, .{ .container = find_dialog.StartFindDialog(.till, .move), .modes = normal_visual_s });
    map(&.{k(.shift, .f)}, .{ .container = find_dialog.StartFindDialog(.bfind, .move), .modes = normal_visual_s });
    map(&.{k(.shift, .t)}, .{ .container = find_dialog.StartFindDialog(.btill, .move), .modes = normal_visual_s });
    // movement end
    map(&.{k(.none, .n)}, .{ .container = wrapContainer(fns.search.nextSearchPoint), .modes = normal_visual_s });
    map(&.{k(.shift, .n)}, .{ .container = wrapContainer(fns.search.prevSearchPoint), .modes = normal_visual_s });
    map(&.{k(.control, .y)}, .{ .container = wrapContainer(fns.scrollUp), .modes = normal_visual_s });
    map(&.{k(.control, .e)}, .{ .container = wrapContainer(fns.scrollDown), .modes = normal_visual_s });
    map(&.{k(.control, .f)}, .{ .container = wrapContainer(fns.scrollDownFullPage), .modes = normal_visual_s });
    map(&.{k(.control, .b)}, .{ .container = wrapContainer(fns.scrollUpFullPage), .modes = normal_visual_s });

    // normal mapping
    const normal_s: []const State.Mode = &.{.normal};

    map(&.{k(.control, .i)}, .{ .container = jump_list.goto_next_jump, .modes = normal_s });
    map(&.{k(.control, .o)}, .{ .container = jump_list.goto_prev_jump, .modes = normal_s });

    map(&.{k(.none, .escape)}, .{ .container = setModeContainer(.normal), .modes = normal_s });
    map(&.{k(.shift, .semicolon)}, .{ .container = wrapContainer(fns.openCommandLine), .modes = normal_s });
    map(&.{k(.none, .dot)}, .{ .container = wrapContainer(fns.repeatLastKeys), .modes = normal_s });
    map(&.{ k(.none, .z), k(.none, .z) }, .{ .container = fns.center_screen, .modes = normal_s });
    map(&.{k(.control, .g)}, .{ .container = fns.show_current_file, .modes = normal_s });

    // change
    map(&.{k(.shift, .c)}, .{ .container = fns.MotionChange(.end_of_row), .modes = normal_s });

    map(&.{ k(.none, .c), k(.none, .f) }, .{ .container = find_dialog.StartFindDialog(.find, .change), .modes = normal_s });
    map(&.{ k(.none, .c), k(.none, .t) }, .{ .container = find_dialog.StartFindDialog(.till, .change), .modes = normal_s });
    map(&.{ k(.none, .c), k(.shift, .f) }, .{ .container = find_dialog.StartFindDialog(.bfind, .change), .modes = normal_s });
    map(&.{ k(.none, .c), k(.shift, .t) }, .{ .container = find_dialog.StartFindDialog(.btill, .change), .modes = normal_s });

    // change end

    // delete
    map(&.{ k(.none, .d), k(.none, .f) }, .{ .container = find_dialog.StartFindDialog(.find, .delete), .modes = normal_s });
    map(&.{ k(.none, .d), k(.none, .t) }, .{ .container = find_dialog.StartFindDialog(.till, .delete), .modes = normal_s });
    map(&.{ k(.none, .d), k(.shift, .f) }, .{ .container = find_dialog.StartFindDialog(.bfind, .delete), .modes = normal_s });
    map(&.{ k(.none, .d), k(.shift, .t) }, .{ .container = find_dialog.StartFindDialog(.btill, .delete), .modes = normal_s });

    map(&.{k(.shift, .d)}, .{ .container = fns.MotionDelete(.end_of_row), .modes = normal_s });
    // delete end

    // yand and past
    map(&.{k(.none, .p)}, .{ .container = wrapContainer(fns.pasteAfterCursor), .modes = normal_s });
    map(&.{k(.shift, .p)}, .{ .container = wrapContainer(fns.pasteBeforeCursor), .modes = normal_s });

    map(&.{ k(.none, .y), k(.none, .f) }, .{ .container = find_dialog.StartFindDialog(.find, .yank), .modes = normal_s });
    map(&.{ k(.none, .y), k(.none, .t) }, .{ .container = find_dialog.StartFindDialog(.till, .yank), .modes = normal_s });
    map(&.{ k(.none, .y), k(.shift, .f) }, .{ .container = find_dialog.StartFindDialog(.bfind, .yank), .modes = normal_s });
    map(&.{ k(.none, .y), k(.shift, .t) }, .{ .container = find_dialog.StartFindDialog(.btill, .yank), .modes = normal_s });
    // yand and past end

    // visual mode
    map(&.{k(.none, .v)}, .{ .container = visual.setVisualAndSelectionKindContainer(.regular), .modes = normal_s });
    map(&.{k(.shift, .v)}, .{ .container = visual.setVisualAndSelectionKindContainer(.line), .modes = normal_s });
    map(&.{k(.control, .v)}, .{ .container = visual.setVisualAndSelectionKindContainer(.block), .modes = normal_s });
    // visual mode end

    // insert mode
    map(&.{k(.none, .i)}, .{ .container = setModeContainer(.insert), .modes = normal_s });
    map(&.{k(.none, .a)}, .{ .container = wrapContainer(insert.insertAfter), .modes = normal_s });
    map(&.{k(.none, .o)}, .{ .container = wrapContainer(insert.insertNewLineAfter), .modes = normal_s });

    map(&.{k(.shift, .i)}, .{ .container = wrapContainer(insert.setInsertStartOfRow), .modes = normal_s });
    map(&.{k(.shift, .a)}, .{ .container = wrapContainer(insert.setInsertEndOfRow), .modes = normal_s });
    map(&.{k(.shift, .o)}, .{ .container = wrapContainer(insert.insertNewLineBefore), .modes = normal_s });
    // insert mode end

    // replace mode
    map(&.{k(.shift, .r)}, .{ .container = setModeContainer(.replace), .modes = normal_s });
    map(&.{k(.none, .r)}, .{ .container = replace_dialog.StartReplaceDialog, .modes = normal_s });
    // replace mode end

    map(&.{k(.none, .x)}, .{ .container = fns.delete_forward, .modes = normal_s });
    map(&.{k(.shift, .x)}, .{ .container = fns.delete_backward, .modes = normal_s });

    // history
    map(&.{k(.none, .u)}, .{ .container = wrapContainer(history.undo), .modes = normal_s });
    map(&.{k(.control, .r)}, .{ .container = wrapContainer(history.redo), .modes = normal_s });
    map(&.{k(.shift, .u)}, .{ .container = wrapContainer(history.pushHistory), .modes = normal_s });
    // history end

    map(&.{k(.none, .slash)}, .{ .container = wrapContainer(fns.search.openSearchCommand), .modes = normal_s });

    // insert mappings
    const insert_s: []const State.Mode = &.{.insert};

    map(&.{k(.none, .tab)}, .{ .container = insert_tab_character, .modes = insert_s });
    map(&.{k(.none, .escape)}, .{ .container = setModeContainer(.normal), .modes = insert_s });

    map(&.{k(.none, .page_up)}, .{ .container = wrapContainer(fns.scrollUp), .modes = insert_s });
    map(&.{k(.none, .page_down)}, .{ .container = wrapContainer(fns.scrollDown), .modes = insert_s });

    map(&.{k(.none, .enter)}, .{ .container = wrapContainer(fns.enterKey), .modes = insert_s });
    map(&.{k(.none, .backspace)}, .{ .container = fns.delete_backward, .modes = insert_s });
    map(&.{k(.none, .delete)}, .{ .container = fns.delete_forward, .modes = insert_s });
    map(&.{k(.control, .v)}, .{ .container = wrapContainer(fns.clipboardPaste), .modes = insert_s });

    // auto complete menu traverse
    map(&.{k(.control, .n)}, .{ .container = wrapContainer(fns.acSelectNext), .modes = insert_s });
    map(&.{k(.control, .p)}, .{ .container = wrapContainer(fns.acSelectPrev), .modes = insert_s });
    map(&.{k(.control, .y)}, .{ .container = wrapContainer(fns.acAccept), .modes = insert_s });
    map(&.{k(.control, .c)}, .{ .container = wrapContainer(fns.acUpdateEntries), .modes = insert_s });

    // visual mappings
    const visual_s: []const State.Mode = &.{.visual};
    map(&.{k(.none, .escape)}, .{ .container = setModeContainer(.normal), .modes = visual_s });
    map(&.{k(.shift, .i)}, .{ .container = visual.BlockVisualToInsert(.before), .modes = visual_s });
    map(&.{k(.shift, .a)}, .{ .container = visual.BlockVisualToInsert(.after), .modes = visual_s });
    map(&.{k(.none, .o)}, .{ .container = wrapContainer(fns.swapCursorAndAnchor), .modes = visual_s });
    map(&.{k(.none, .y)}, .{ .container = visual.yank_text, .modes = visual_s });
    map(&.{k(.none, .d)}, .{ .container = visual.delete_text, .modes = visual_s });
    map(&.{k(.none, .c)}, .{ .container = visual.change_text, .modes = visual_s });
    map(&.{k(.none, .v)}, .{ .container = visual.setSelectionKindContainer(.regular), .modes = visual_s });
    map(&.{k(.shift, .v)}, .{ .container = visual.setSelectionKindContainer(.line), .modes = visual_s });
    map(&.{k(.control, .v)}, .{ .container = visual.setSelectionKindContainer(.block), .modes = visual_s });

    // replace mappings
    const replace_s: []const State.Mode = &.{.replace};
    map(&.{k(.none, .escape)}, .{ .container = setModeContainer(.normal), .modes = replace_s });

    const generated_change_motions = fns.GenerateKeysAndMotionFor(.change);
    const generated_change_object = fns.GenerateKeysAndTextObjectFor(.change);

    const generated_delete_motions = fns.GenerateKeysAndMotionFor(.delete);
    const generated_delete_object = fns.GenerateKeysAndTextObjectFor(.delete);

    const generated_yank_motions = fns.GenerateKeysAndMotionFor(.yank);
    const generated_yank_object = fns.GenerateKeysAndTextObjectFor(.yank);

    const generated_movement_keys = fns.GenerateKeysAndMotionFor(.move);
    const generated_visual_select_keys = fns.GenerateKeysAndTextObjectFor(.select);

    @setEvalBranchQuota(2000);

    inline for (.{ generated_change_motions, generated_change_object }) |tuple| {
        inline for (tuple) |kc| {
            map([_]Key{k(.none, .c)} ++ kc.keys, .{ .container = kc.container, .modes = normal_s });
        }
    }

    inline for (.{ generated_delete_motions, generated_delete_object }) |tuple| {
        inline for (tuple) |kc| {
            map([_]Key{k(.none, .d)} ++ kc.keys, .{ .container = kc.container, .modes = normal_s });
        }
    }

    inline for (.{ generated_yank_motions, generated_yank_object }) |tuple| {
        inline for (tuple) |kc| {
            map([_]Key{k(.none, .y)} ++ kc.keys, .{ .container = kc.container, .modes = normal_s });
        }
    }

    inline for (generated_movement_keys) |kc| {
        map(kc.keys, .{ .container = kc.container, .modes = normal_visual_s });
    }

    inline for (generated_visual_select_keys) |kc| {
        map(kc.keys, .{ .container = kc.container, .modes = visual_s });
    }
}

pub fn handleInput(keys: *core.KeyQueue, chars: *core.CharQueue) void {
    { // handle text
        var text = std.ArrayList(u8).init(core.getAllocator());
        defer text.deinit();
        while (chars.popOrNull()) |cp| {
            var seq: [4]u8 = undefined;
            const bytes = std.unicode.utf8Encode(cp, &seq) catch unreachable;
            text.appendSlice(seq[0..bytes]) catch return;
        }

        characterInput(text.items);
    }

    { // handle keys
        if (keys.slice().len > 0)
            keyInput(keys.slice());

        keys.resize(0) catch unreachable;
    }

    { // handle status line
        switch (state().mode) {
            .insert, .replace => {
                state().mapping_system.clearCurrentKeys();
            },
            .normal, .visual => {},
        }
        var buf: [mapping_system.max_current_keys_string]u8 = undefined;
        const keys_str = state().mapping_system.getCurrentKeysString(&buf);
        core.status_line.set("keys", keys_str);
    }
}

pub fn keyInput(keys: []const Key) void {
    const clear_keys = blk: {
        for (keys) |key| {
            if (key.kind == .escape)
                break :blk true;
        }
        break :blk false;
    };

    const modes = State.Mode.modesToInt(&.{state().mode});
    const buffer_data = focused.getData();
    const condition_data = State.ConditionData{
        .buffer_type = buffer_data.buffer_type,
        .modes = modes,
    };

    const default_condition_data = State.ConditionData{
        .buffer_type = "",
        .modes = modes,
    };

    if (clear_keys) {
        state().mapping_system.clearCurrentKeys();
        state().mapping_system.accumulateKey(Key.create(.none, .escape));
        _ = state().mapping_system.run(default_condition_data) catch return;
        state().mapping_system.clearCurrentKeys();
    } else {
        var res: State.MappingSystem.RunResult = .need_more_keys;
        for (keys) |key| {
            state().mapping_system.accumulateKey(key);
            res = state().mapping_system.run(condition_data) catch return;

            switch (res) {
                .need_more_keys => continue,
                .done => setLastMapping(res),
                .mapping_does_not_exist => {
                    res = state().mapping_system.run(default_condition_data) catch return;
                    setLastMapping(res);
                },
            }
        } else if (res != .need_more_keys) {
            state().mapping_system.clearCurrentKeys();
        }
    }
}

fn setLastMapping(run_result: State.MappingSystem.RunResult) void {
    if (run_result != .done) return;

    if (run_result.done.vtable.repeat) |_| {
        state().last_mapping = run_result;
    }
}

pub fn characterInput(text_input: []const u8) void {
    if (text_input.len == 0) return;
    if (!std.unicode.utf8ValidateSlice(text_input)) {
        log.err("Received Invalid utf8 sequence", .{});
        return;
    }

    const cursor = focused.cursor();
    const bt = focused.getData().buffer_type;

    var to_insert: [258]u8 = undefined;
    var to_insert_len: usize = 0;

    const config = core.config.get(bt);
    const tab_count = std.mem.count(u8, text_input, "\t");
    if (tab_count > 0 and config.expand_tab) {
        var replacement: [128]u8 = undefined;
        for (0..config.tab_width) |i| {
            replacement[i] = ' ';
        }

        _ = std.mem.replace(
            u8,
            text_input,
            "\t",
            replacement[0..config.tab_width],
            &to_insert,
        );
        to_insert_len = std.mem.replacementSize(
            u8,
            text_input,
            "\t",
            replacement[0..config.tab_width],
        );
    } else {
        std.mem.copyForwards(u8, &to_insert, text_input);
        to_insert_len = text_input.len;
    }

    const string = to_insert[0..to_insert_len];
    switch (state().mode) {
        .normal, .visual => return,
        .insert => {
            dmodify.buffer.insertAt(focused.handle(), cursor, string) catch |err| {
                core.notify("Error:", .{}, "{!}", .{err}, 3);
                return;
            };
        },
        .replace => {
            dmodify.buffer.replaceRange(focused.handle(), .{ .start = cursor, .end = cursor.addCol(1) }, string) catch |err| {
                core.notify("Error:", .{}, "{!}", .{err}, 3);
                return;
            };
        },
    }

    state().change_recorder.appendInsert(state().gpa, cursor, string) catch log.err("OOM", .{});
    const new_cursor = calculateNewCursorFromString(cursor, string);
    dmodify.buffer_window.setCursor(focused.bufferWindow(), new_cursor) catch return;
}

fn calculateNewCursorFromString(cursor: Point, string: []const u8) Point {
    std.debug.assert(string.len > 0);

    const rows: u32 = @truncate(std.mem.count(u8, string, "\n"));
    if (rows > 0) {
        const index = std.mem.lastIndexOf(u8, string, "\n").?;
        const cols: u32 = @truncate(core.utf8.countCodepoints(string[index + 1 ..]));
        return cursor.addRow(rows).setCol(cols);
    } else {
        const cols: u32 = @truncate(core.utf8.countCodepoints(string));
        return cursor.addCol(cols);
    }
}

/// container is a sturct, enum or union and must have:
/// a function named `regular` of type `core.input.RegularCall`.
/// Optionally a function called `repeat` of the same type can be provided
///
/// A description can be declared as a public `description` variable.
pub fn map(comptime keys: []const Key, comptime mapping: anytype) void {
    const buffer_type = if (@hasField(@TypeOf(mapping), "buffer_type")) mapping.buffer_type else "";

    const interface = functionInterfaceFromContainer(mapping.container);

    const description = if (comptime @hasDecl(mapping.container, "description"))
        mapping.container.description
    else
        "No description";

    if (@hasField(@TypeOf(mapping), "modes")) {
        internalMap(mapping.modes, buffer_type, keys, interface, description);
    } else {
        internalMap(&.{.normal}, buffer_type, keys, interface, description);
    }
}

pub fn multiMap(comptime keys: []const Key, comptime tuple_of_mapping: anytype) void {
    inline for (tuple_of_mapping) |mapping| {
        map(keys, mapping);
    }
}

fn internalMap(modes: []const State.Mode, buffer_type: []const u8, keys: []const Key, vtable: input.RegularVTable, description: []const u8) void {
    state().mapping_system.put(keys, .{
        .vtable = vtable,
        .description = description,
        .condition_data = .{
            .modes = State.Mode.modesToInt(modes),
            .buffer_type = buffer_type,
        },
    }) catch |err| {
        log.err("Couldn't map {any} due to `{!}`", .{ keys, err });
    };
}

fn functionInterfaceFromContainer(container: anytype) core.input.RegularVTable {
    const has_regular = comptime @hasDecl(container, "regular");

    if (has_regular) {
        const vtable = comptime core.utils.vtableFromContainer(input.RegularVTable, container);
        return vtable;
    } else {
        const name = @typeName(container);
        @compileError("`" ++ name ++ "` Container has no `regular` function");
    }
}

fn getModeFunctions(mode: State.Mode, file_type: []const u8, keys: []const Key) ?mapping_system.FunctionInterface {
    var mapping = state.mappings.getPtr(mode);
    return mapping.getWithFallback(&.{ file_type, default_bt }, keys);
}

const State = struct {
    const WordDelimiterMap = std.StringHashMapUnmanaged(std.ArrayListUnmanaged(u21));

    const ChangeRecorder = struct {
        const Change = struct {
            point: Point,
            change: union(enum) {
                insert: struct {
                    /// the offset and len within `strings`
                    offset: usize,
                    len: usize,
                },
                delete_characters: usize,
            },
        };

        strings: std.ArrayListUnmanaged(u8) = .{},
        changes: std.ArrayListUnmanaged(Change) = .{},

        pub fn reset(cr: *ChangeRecorder) void {
            cr.strings.shrinkRetainingCapacity(0);
            cr.changes.shrinkRetainingCapacity(0);
        }

        pub fn appendInsert(cr: *ChangeRecorder, allocator: std.mem.Allocator, point: Point, string: []const u8) !void {
            try cr.changes.append(allocator, .{
                .point = point,
                .change = .{ .insert = .{ .offset = cr.strings.items.len, .len = string.len } },
            });
            errdefer _ = cr.changes.pop();

            try cr.strings.appendSlice(allocator, string);
        }

        pub fn appendDelete(cr: *ChangeRecorder, allocator: std.mem.Allocator, point: Point, deleted_chars: usize) !void {
            try cr.changes.append(allocator, .{
                .point = point,
                .change = .{ .delete_characters = deleted_chars },
            });
        }
    };

    pub const ConditionData = struct {
        buffer_type: []const u8,
        modes: u8,

        pub fn eql(required: ConditionData, stored: ConditionData) bool {
            // if the stored.buffer_type is an empty string assume the mapping
            // works on all buffer types
            if (stored.buffer_type.len == 0 and
                !std.mem.eql(u8, required.buffer_type, stored.buffer_type)) return false;

            // if the same bit in `a` and `b` is set this will not be zero
            if (required.modes & stored.modes == 0) return false;

            return true;
        }
    };
    const MappingSystem = mapping_system.MappingSystem(ConditionData);

    mode: Mode = .normal,
    mapping_system: MappingSystem,
    gpa: std.mem.Allocator,
    string_storage: core.slice_storage.StringStorageUnmanaged = .{},
    last_mapping: MappingSystem.RunResult = .need_more_keys,

    insertion_recording_anchor: ?Point = null,
    change_recorder: ChangeRecorder = .{},
    last_insertion: std.ArrayListUnmanaged(u8) = .{},

    last_visual_selection: ?Buffer.PointRange = null,
    last_selection_kind: Buffer.Selection.Kind = .regular,

    last_found_cp: ?u21 = null,
    last_find_and_motion_op: ?struct { fop: FindOperation, mop: MotionOperation } = null,

    one_time_calls: std.ArrayListUnmanaged(RequestOnce) = .{},

    pub fn init(gpa: std.mem.Allocator) !State {
        const s = State{
            .gpa = gpa,
            .mapping_system = MappingSystem.init(gpa),
        };

        return s;
    }

    pub fn deinit(self: *State) void {
        self.mapping_system.deinit();
        self.last_insertion.deinit(self.gpa);
        self.string_storage.deinit(self.gpa);
        self.one_time_calls.deinit(self.gpa);
        self.change_recorder.strings.deinit(state().gpa);
        self.change_recorder.changes.deinit(state().gpa);
    }

    // pub fn appendWordDelimiters(self: *State, buffer_type: []const u8, delimiters: []const u21) !void {
    //     const bt = try self.string_storage.getAndPut(self.gpa, buffer_type);
    //     var gop = try self.word_delimiters.getOrPut(self.gpa, bt);

    //     if (!gop.found_existing) gop.value_ptr.* = .{};

    //     try gop.value_ptr.appendSlice(self.gpa, delimiters);
    // }

    // pub fn setWordDelimiters(self: *State, buffer_type: []const u8, delimiters: []const u21) !void {
    //     const bt = try self.string_storage.getAndPut(self.gpa, buffer_type);
    //     const gop = try self.word_delimiters.getOrPut(self.gpa, bt);
    //     if (!gop.found_existing) gop.value_ptr.* = .{};

    //     var new_list = std.ArrayListUnmanaged(u21){};
    //     try new_list.appendSlice(delimiters);

    //     gop.value_ptr.*.deinit();
    //     gop.value_ptr.* = new_list;
    // }

    // pub fn removeWordDelimiters(self: *State, buffer_type: []const u8, delimiters: []const u21) !void {
    //     var list = try self.word_delimiters.getPtr(buffer_type) orelse return;
    //     for (delimiters) |delim| _ = list.orderedRemove(delim);
    // }

    pub const Mode = enum(u8) {
        normal = 1,
        insert = 1 << 1,
        visual = 1 << 2,
        replace = 1 << 3,

        pub fn modesToInt(modes: []const Mode) u8 {
            var int: u8 = 0;
            for (modes) |mode| {
                int |= @intFromEnum(mode);
            }

            return int;
        }
    };
};

fn wrap(callFn: anytype) input.RegularCall {
    const wrapper = struct {
        pub fn wrapper(_: ?*anyopaque, _: usize) anyerror!void {
            callFn();
        }
    }.wrapper;
    return wrapper;
}

fn wrapContainer(comptime callFn: anytype) type {
    return struct {
        pub fn regular(_: usize) anyerror!void {
            callFn();
        }
    };
}

fn changeText(range: Buffer.PointRange) !void {
    try deleteText(range);
    setMode(.insert);
}

fn deleteText(range: Buffer.PointRange) !void {
    try dmodify.buffer_window.setCursor(focused.bufferWindow(), range.start.min(range.end));
    try core.registers.copyToByRange(default_register, focused.handle(), range);
    try dmodify.buffer.deleteRange(focused.handle(), range);
}

fn yankText(range: Buffer.PointRange) !void {
    try core.registers.copyToByRange(default_register, focused.handle(), range);
}

pub fn setModeContainer(comptime mode: State.Mode) type {
    return struct {
        pub fn regular(_: usize) anyerror!void {
            setMode(mode);
        }
    };
}

pub fn setMode(mode: State.Mode) void {
    const old_mode = state().mode;
    state().mode = mode;

    { // set_last_selection
        if (old_mode == .visual) {
            var selection = focused.getSelection();
            const point_range = selection.get(focused.cursor());
            state().last_visual_selection = point_range;
            state().last_selection_kind = selection.kind;
        }

        if (old_mode == .insert) {
            history.pushHistory();

            handleRequestOnce(.insert_exit);
        }
    }

    switch (mode) {
        .normal => {
            fns.resetSelection();
        },
        .insert => {
            history.pushHistory();

            state().change_recorder.reset();

            fns.resetSelection();
        },
        .visual => {
            dmodify.buffer.setSelection(focused.handle(), .{ .set_anchor = focused.cursor() }) catch return;
        },
        .replace => {},
    }

    core.config.config.cursor.shape = switch (mode) {
        .insert => .thin,
        .visual, .normal => .thick,
        .replace => .underline,
    };
}

pub const fns = struct {
    pub fn scrollUp() void {
        dmodify.buffer_window.scroll(focused.bufferWindow(), .up, 1) catch return;
    }

    pub fn scrollDown() void {
        dmodify.buffer_window.scroll(focused.bufferWindow(), .down, 1) catch return;
    }

    pub fn scrollDownFullPage() void {
        dmodify.buffer_window.scroll(focused.bufferWindow(), .down, focused.visiableLines().count) catch return;
    }

    pub fn scrollUpFullPage() void {
        dmodify.buffer_window.scroll(focused.bufferWindow(), .up, focused.visiableLines().count) catch return;
    }

    pub const center_screen = struct {
        pub fn regular(_: usize) anyerror!void {
            try dmodify.buffer_window.centerWindow(focused.bufferWindow());
        }
    };

    pub const show_current_file = struct {
        pub fn regular(_: usize) anyerror!void {
            core.notifyMessage("{s}", .{focused.getData().path}, 1);
        }
    };

    pub fn repeatLastKeys() void {
        switch (state().last_mapping) {
            .done => |result| {
                result.vtable.repeat.?(result.repeat) catch return;
            },
            else => {},
        }
    }

    pub fn openCommandLine() void {
        if (core.command_line.open()) {
            setMode(.insert);
        }
    }

    pub fn closeCommandLine() void {
        setMode(.normal);
        core.command_line.close();
    }

    pub fn enterKey() void {
        if (core.command_line.isOpen()) {
            core.command_line.run();
            setMode(.normal);
        } else insertNewLineAtCursor();
    }

    pub fn insertNewLineAtCursor() void {
        characterInput("\n");
    }

    pub fn moveContainer(comptime motion: core.motions.MotionKind) type {
        return struct {
            pub fn regular(repeat: usize) anyerror!void {
                move(motion, repeat);
            }
        };
    }

    pub fn move(motion: core.motions.MotionKind, repeat: usize) void {
        const handle = focused.handle();
        const cursor = focused.cursor();
        const buffe_type = focused.getData().buffer_type;

        const delimiters: []const u21 = getWordDelimiters(buffe_type);
        const point = motion.getPointRepeat(state().gpa, handle, cursor, delimiters, repeat) orelse return;

        const row_diff = core.utils.diff(cursor.row, point.row);
        const should_jump = row_diff >= 5;
        pushJumpPoint(should_jump);
        dmodify.buffer_window.setCursor(focused.bufferWindow(), point) catch return;
        pushJumpPoint(should_jump);
    }

    pub fn clipboardPaste() void {
        // TODO: This
        // var clipboard = glfw.getClipboardString() orelse {
        //     core.notify("Clipboard Empty", .{}, "", .{}, 2);
        //     return;
        // };

        // const cursor = focused.cursor();
        // focused.insertAt(cursor, clipboard) catch return;
    }

    pub fn swapCursorAndAnchor() void {
        dmodify.buffer_window.swapCursorAndAnchor(focused.bufferWindow());
    }

    fn resetSelection() void {
        if (state().mode != .visual) dmodify.buffer.setSelection(focused.handle(), .reset) catch return;
    }

    pub const delete_backward = struct {
        pub fn regular(repeats: usize) anyerror!void {
            const cursor = focused.cursor();
            dmodify.buffer.deleteBefore(focused.handle(), cursor, @intCast(repeats)) catch |err| {
                print("input_layer.deleteBackward()\n\t{}\n", .{err});
            };

            if (cursor.col == 0 and cursor.row > 0) {
                const col = focused.lastColAtRow(cursor.row - 1);
                try dmodify.buffer_window.setCursor(focused.bufferWindow(), cursor.subRow(1).setCol(col));
            } else {
                try dmodify.buffer_window.setCursor(focused.bufferWindow(), cursor.subCol(1));
            }

            state().change_recorder.appendDelete(state().gpa, focused.cursor(), 1) catch log.err("OOM", .{});
        }
    };

    pub const delete_forward = struct {
        pub fn regular(repeats: usize) anyerror!void {
            const cursor = focused.cursor();
            dmodify.buffer.deleteAfter(focused.handle(), cursor, @intCast(repeats)) catch |err| {
                print("input_layer.deleteForward()\n\t{}\n", .{err});
            };

            state().change_recorder.appendDelete(state().gpa, cursor, 1) catch log.err("OOM", .{});
        }
    };

    pub fn toggleCommandLine() void {
        if (core.cliOpen())
            core.closeCLI(true, true)
        else
            core.openCLI();
    }

    pub fn MotionChange(comptime motion: MotionKind) type {
        return struct {
            const direction = if (motion.isForward()) "(forward)" else "(backward)";
            pub const description = "Change " ++ @tagName(motion) ++ " " ++ direction;

            pub fn regular(r: usize) anyerror!void {
                const range = getRange(r) catch return;
                changeText(range) catch return;
            }

            pub fn repeat(r: usize) anyerror!void {
                const range = getRange(r) catch return;
                deleteText(range) catch return;

                const cursor = focused.cursor();
                applyChangesFromRecorder(cursor.row);
            }

            pub fn getRange(r: usize) !Buffer.PointRange {
                const cursor = focused.cursor();
                const data = focused.getData();
                const delims = getWordDelimiters(data.buffer_type);
                const point = motion.getPointRepeat(state().gpa, data.handle, cursor, delims, r) orelse return error.NoRange;

                if (motion.isForward()) {
                    return .{ .start = cursor, .end = point };
                } else {
                    return .{ .start = point, .end = cursor };
                }
            }
        };
    }

    pub fn MotionDelete(comptime motion: MotionKind) type {
        return struct {
            const direction = if (motion.isForward()) "(forward)" else "(backward)";
            pub const description = "Delete " ++ @tagName(motion) ++ " " ++ direction;

            pub fn regular(r: usize) anyerror!void {
                const range = getRange(r) catch return;
                deleteText(range) catch return;
            }

            pub const repeat = regular;

            pub fn getRange(r: usize) !Buffer.PointRange {
                const cursor = focused.cursor();
                const data = focused.getData();
                const delims = getWordDelimiters(data.buffer_type);
                const point = motion.getPointRepeat(state().gpa, data.handle, cursor, delims, r) orelse return error.NoRange;

                if (motion.isForward()) {
                    return .{ .start = cursor, .end = point };
                } else {
                    return .{ .start = point, .end = cursor };
                }
            }
        };
    }

    pub fn MotionYank(comptime motion: MotionKind) type {
        return struct {
            const direction = if (motion.isForward()) "(forward)" else "(backward)";
            pub const description = "Yank " ++ @tagName(motion) ++ " " ++ direction;

            pub fn regular(repeat: usize) anyerror!void {
                const data = focused.getData();
                const cursor = focused.cursor();
                const delims = getWordDelimiters(data.buffer_type);

                const point = motion.getPointRepeat(state().gpa, data.handle, cursor, delims, repeat) orelse return;
                const prange = Buffer.PointRange{ .start = cursor.min(point), .end = cursor.max(point) };

                yankText(prange) catch return;
            }
        };
    }

    pub fn MotionMove(comptime motion: MotionKind) type {
        return struct {
            const direction = if (motion.isForward()) "(forward)" else "(backward)";
            pub const description = "Move the cursor " ++ @tagName(motion) ++ " " ++ direction;

            pub fn regular(repeat: usize) anyerror!void {
                const data = focused.getData();
                const cursor = focused.cursor();

                const delimiters: []const u21 = getWordDelimiters(data.buffer_type);
                const point = motion.getPointRepeat(state().gpa, data.handle, cursor, delimiters, repeat) orelse return;

                try dmodify.buffer_window.setCursor(focused.bufferWindow(), point);
            }
        };
    }

    pub fn TextObjectChange(comptime text_object: TextObjectKind) type {
        return struct {
            pub const description = "Change text object " ++ @tagName(text_object);

            pub fn regular(_: usize) anyerror!void {
                try @This().delete();
                setMode(.insert);
            }

            pub fn repeat(_: usize) anyerror!void {
                try @This().delete();

                const cursor = focused.cursor();
                applyChangesFromRecorder(cursor.row);
            }

            pub fn delete() !void {
                const cursor = focused.cursor();
                const data = focused.getData();
                const delims = getWordDelimiters(data.buffer_type);
                const range = text_object.getRange(state().gpa, data.handle, cursor, delims) orelse return error.NoRange;

                try deleteText(range);
            }
        };
    }

    pub fn TextObjectYank(comptime text_object: TextObjectKind) type {
        return struct {
            pub const description = "Yank " ++ @tagName(text_object);

            pub fn regular(_: usize) anyerror!void {
                const data = focused.getData();
                const cursor = focused.cursor();
                const delims = getWordDelimiters(data.buffer_type);

                const prange = text_object.getRange(state().gpa, data.handle, cursor, delims) orelse return;
                const string = (focused.getBufferRange(state().gpa, prange) catch return);
                defer state().gpa.free(string);

                core.registers.copyTo(default_register, string) catch return;
            }
        };
    }

    pub fn TextObjectSelect(comptime text_object: TextObjectKind) type {
        return struct {
            pub const description = "Yank " ++ @tagName(text_object);

            pub fn regular(_: usize) anyerror!void {
                const data = focused.getData();
                const cursor = focused.cursor();
                const delims = getWordDelimiters(data.buffer_type);

                const prange = text_object.getRange(state().gpa, data.handle, cursor, delims) orelse return;

                try dmodify.buffer.setSelection(focused.handle(), .{ .set_anchor = prange.start });

                const end = if (prange.end.col == 0)
                    prange.end.subRow(1).setCol(focused.lastColAtRow(prange.end.row -| 1))
                else
                    prange.end.subCol(1);

                try dmodify.buffer_window.setCursor(focused.bufferWindow(), end);
            }
        };
    }

    pub fn TextObjectDelete(comptime text_object: TextObjectKind) type {
        return struct {
            pub const description = "Delete text object " ++ @tagName(text_object);

            pub fn regular(_: usize) anyerror!void {
                @This().delete() catch return;
            }

            pub fn repeat(_: usize) anyerror!void {
                @This().delete() catch return;
            }

            pub fn delete() !void {
                const cursor = focused.cursor();
                const data = focused.getData();
                const delims = getWordDelimiters(data.buffer_type);
                const range = text_object.getRange(state().gpa, data.handle, cursor, delims) orelse return;

                try deleteText(range);
            }
        };
    }

    const KeysAndContainer = struct { keys: []const Key, container: type };

    const SuffixMotion = struct { suffix: []const Key, motion: MotionKind };

    const TextObjectOperation = enum { change, delete, yank, select };
    const SuffixTextObject = struct { suffix: []const Key, object: TextObjectKind };

    pub fn GenerateKeysAndMotionFor(comptime operation: MotionOperation) []const KeysAndContainer {
        const suffix_motion = GenerateKeySuffixAndMotion(operation);

        const GenerateContainer = switch (operation) {
            .change => fns.MotionChange,
            .delete => fns.MotionDelete,
            .yank => fns.MotionYank,
            .move => fns.MotionMove,
        };

        var result: []const KeysAndContainer = &[_]KeysAndContainer{};
        inline for (suffix_motion) |tuple| {
            result = result ++ [_]KeysAndContainer{
                .{ .keys = tuple.@"0", .container = GenerateContainer(tuple.@"1") },
            };
        }

        return result;
    }

    pub fn GenerateKeySuffixAndMotion(comptime operation: MotionOperation) []const core.utils.TupleFromStruct(SuffixMotion) {
        const word = switch (operation) {
            .move => MotionKind{ .word = .{ .stop_eol = false } },
            else => MotionKind{ .word = .{ .stop_eol = true } },
        };
        const word_end = switch (operation) {
            .move => MotionKind{ .word_end = .{ .stop_eol = false } },
            else => MotionKind{ .word_end = .{ .stop_eol = true } },
        };

        return &.{
            .{ &.{k(.none, .h)}, .prev_col },
            .{ &.{k(.none, .l)}, .next_col },

            .{ &.{k(.none, .e)}, word_end },
            .{ &.{k(.shift, .e)}, .{ .full_word_end = .{} } },

            .{ &.{k(.none, .b)}, .back_word },
            .{ &.{k(.shift, .b)}, .full_back_word },

            .{ &.{ k(.none, .g), k(.none, .e) }, .back_word_end },
            .{ &.{ k(.none, .g), k(.shift, .e) }, .full_back_word_end },

            .{ &.{k(.shift, .four)}, .end_of_row },
            .{ &.{k(.none, .zero)}, .start_of_row },

            .{ &.{k(.none, .w)}, switch (operation) {
                .change => word_end,
                else => word,
            } },
            .{ &.{k(.shift, .w)}, switch (operation) {
                .change => word_end,
                else => .{ .full_word = .{} },
            } },
        };
    }

    pub fn GenerateKeysAndTextObjectFor(comptime operation: TextObjectOperation) []const KeysAndContainer {
        const suffix_motion = GenerateKeySuffixAndTextObject(operation);

        const GenerateContainer = switch (operation) {
            .change => fns.TextObjectChange,
            .delete => fns.TextObjectDelete,
            .yank => fns.TextObjectYank,
            .select => fns.TextObjectSelect,
        };

        var result: []const KeysAndContainer = &[_]KeysAndContainer{};
        inline for (suffix_motion) |tuple| {
            result = result ++ [_]KeysAndContainer{
                .{ .keys = tuple.@"0", .container = GenerateContainer(tuple.@"1") },
            };
        }

        return result;
    }

    pub fn GenerateKeySuffixAndTextObject(comptime operation: TextObjectOperation) []const core.utils.TupleFromStruct(SuffixTextObject) {
        _ = operation;

        return &.{
            .{ &.{ k(.none, .i), k(.none, .w) }, .inner_word },
            .{ &.{ k(.none, .a), k(.none, .w) }, .around_word },

            .{ &.{ k(.none, .i), k(.none, .single_quote) }, .{ .inner_cp = '\'' } },
            .{ &.{ k(.none, .a), k(.none, .single_quote) }, .{ .around_cp = '\'' } },

            .{ &.{ k(.none, .i), k(.shift, .single_quote) }, .{ .inner_cp = '"' } },
            .{ &.{ k(.none, .a), k(.shift, .single_quote) }, .{ .around_cp = '"' } },

            .{ &.{ k(.none, .i), k(.none, .backtick) }, .{ .inner_cp = '`' } },
            .{ &.{ k(.none, .a), k(.none, .backtick) }, .{ .around_cp = '`' } },

            .{ &.{ k(.none, .i), k(.none, .comma) }, .{ .inner_cp = ',' } },
            .{ &.{ k(.none, .a), k(.none, .comma) }, .{ .around_cp = ',' } },

            .{ &.{ k(.none, .i), k(.none, .dot) }, .{ .inner_cp = '.' } },
            .{ &.{ k(.none, .a), k(.none, .dot) }, .{ .around_cp = '.' } },

            .{ &.{ k(.none, .i), k(.none, .dash) }, .{ .inner_cp = '-' } },
            .{ &.{ k(.none, .a), k(.none, .dash) }, .{ .around_cp = '-' } },

            .{ &.{ k(.none, .i), k(.shift, .dash) }, .{ .inner_cp = '_' } },
            .{ &.{ k(.none, .a), k(.shift, .dash) }, .{ .around_cp = '_' } },

            .{ &.{ k(.none, .i), k(.shift, .nine) }, .{ .inner_pair = core.text_objects.pairs.parens } },
            .{ &.{ k(.none, .a), k(.shift, .nine) }, .{ .around_pair = core.text_objects.pairs.parens } },
            .{ &.{ k(.none, .i), k(.shift, .zero) }, .{ .inner_pair = core.text_objects.pairs.parens } },
            .{ &.{ k(.none, .a), k(.shift, .zero) }, .{ .around_pair = core.text_objects.pairs.parens } },

            .{ &.{ k(.none, .i), k(.none, .left_bracket) }, .{ .inner_pair = core.text_objects.pairs.brackets } },
            .{ &.{ k(.none, .a), k(.none, .left_bracket) }, .{ .around_pair = core.text_objects.pairs.brackets } },
            .{ &.{ k(.none, .i), k(.none, .right_bracket) }, .{ .inner_pair = core.text_objects.pairs.brackets } },
            .{ &.{ k(.none, .a), k(.none, .right_bracket) }, .{ .around_pair = core.text_objects.pairs.brackets } },

            .{ &.{ k(.none, .i), k(.shift, .left_bracket) }, .{ .inner_pair = core.text_objects.pairs.braces } },
            .{ &.{ k(.none, .a), k(.shift, .left_bracket) }, .{ .around_pair = core.text_objects.pairs.braces } },
            .{ &.{ k(.none, .i), k(.shift, .right_bracket) }, .{ .inner_pair = core.text_objects.pairs.braces } },
            .{ &.{ k(.none, .a), k(.shift, .right_bracket) }, .{ .around_pair = core.text_objects.pairs.braces } },

            .{ &.{ k(.none, .i), k(.shift, .comma) }, .{ .inner_pair = core.text_objects.pairs.angle_brackets } },
            .{ &.{ k(.none, .a), k(.shift, .comma) }, .{ .around_pair = core.text_objects.pairs.angle_brackets } },
            .{ &.{ k(.none, .i), k(.shift, .dot) }, .{ .inner_pair = core.text_objects.pairs.angle_brackets } },
            .{ &.{ k(.none, .a), k(.shift, .dot) }, .{ .around_pair = core.text_objects.pairs.angle_brackets } },
        };
    }

    pub fn pasteAfterCursor() void {
        const string = core.registers.getFrom(default_register);
        var point = focused.cursor();
        if (string.len > 0 and string[string.len - 1] == '\n') {
            point = point.addRow(1).setCol(0);
        } else {
            point = point.addCol(1);
        }

        dmodify.registers.pasteAtFrom(
            default_register,
            focused.handle(),
            point,
        ) catch return;
    }

    pub fn pasteBeforeCursor() void {
        const string = core.registers.getFrom(default_register);
        var point = focused.cursor();
        if (string.len > 0 and string[string.len - 1] == '\n') {
            point = point.setCol(0);
        }

        dmodify.registers.pasteAtFrom(
            default_register,
            focused.handle(),
            point,
        ) catch return;

        const count = std.mem.count(u8, string, "\n");
        dmodify.buffer_window.setCursor(focused.bufferWindow(), point.addRow(@truncate(count))) catch return;
    }

    pub fn acSelectNext() void {
        core.builtin.auto_complete.selectNext();
    }

    pub fn acSelectPrev() void {
        core.builtin.auto_complete.selectPrev();
    }

    pub fn acAccept() void {
        core.builtin.auto_complete.acceptSelected() catch return;
    }

    pub fn acUpdateEntries() void {
        core.builtin.auto_complete.updateEntries();
    }

    pub const search = struct {
        pub fn openSearchCommand() void {
            if (core.command_line.openAndSet("search ")) {
                setMode(.insert);
            }
        }

        pub fn nextSearchPoint() void {
            const point = focused.nextSearchResultPoint(focused.cursor()) orelse return;
            dmodify.buffer_window.setCursor(focused.bufferWindow(), point) catch return;
        }

        pub fn prevSearchPoint() void {
            const point = focused.prevSearchResultPoint(focused.cursor()) orelse return;
            dmodify.buffer_window.setCursor(focused.bufferWindow(), point) catch return;
        }
    };

    fn keysToString(buf: []u8, keys: []const Key) []u8 {
        var len: usize = 0;
        for (keys) |key| len += key.toString(buf[len..]).len;
        return buf[0..len];
    }
};

const find_dialog = struct {
    pub fn FindDialog(comptime find_op: FindOperation, comptime motion_op: MotionOperation) type {
        return struct {
            repeats: usize = 1,
            pub fn init(repeats: usize) @This() {
                return .{ .repeats = repeats };
            }
            pub fn deinit(_: *anyopaque) void {}

            pub fn callback(ptr: *anyopaque, text: []const u8) core.Dialog.Result {
                const len = std.unicode.utf8ByteSequenceLength(text[0]) catch unreachable;
                const cp = std.unicode.utf8Decode(text[0..len]) catch unreachable;

                const repeats = @as(*@This(), @ptrCast(@alignCast(ptr))).repeats;
                applyFindAndMotionOperations(cp, find_op, motion_op, repeats) catch return .done;

                return .done;
            }
        };
    }

    pub fn StartFindDialog(comptime find_op: FindOperation, comptime motion_op: MotionOperation) type {
        return struct {
            pub fn regular(repeats: usize) anyerror!void {
                try core.dialog.textDialog(FindDialog(find_op, motion_op), .{repeats}, "Enter character:", .low);
            }

            pub fn repeat(repeats: usize) anyerror!void {
                if (state().last_found_cp != null and state().last_find_and_motion_op != null) {
                    const cp = state().last_found_cp.?;
                    const last = state().last_find_and_motion_op.?;
                    const result = getRange(cp, last.fop, repeats) orelse return;

                    switch (last.mop) {
                        .move, .yank => return,
                        .change => {
                            deleteText(result.range) catch return;
                            if (state().last_insertion.items.len != 0) {
                                try dmodify.buffer.insertAt(
                                    focused.handle(),
                                    focused.cursor(),
                                    state().last_insertion.items,
                                );
                            }
                        },
                        .delete => try deleteText(result.range),
                    }
                }
            }
        };
    }

    pub fn getRange(cp: u21, find_op: FindOperation, repeat: usize) ?struct { point: Point, range: PointRange } {
        const cursor = focused.cursor();
        const handle = focused.handle();

        var point = cursor;
        for (0..repeat) |_| {
            const args = .{ core.getAllocator(), handle, point, cp, 1 };
            point = switch (find_op) {
                .find => @call(.auto, core.motions.find, args),
                .till => @call(.auto, core.motions.till, args),
                .bfind => @call(.auto, core.motions.backFind, args),
                .btill => @call(.auto, core.motions.backTill, args),
            } orelse break;
        }

        var range = PointRange{
            .start = cursor.min(point),
            .end = cursor.max(point),
        };
        switch (find_op) {
            .find, .till => range.end.col +|= 1,
            else => {},
        }

        return .{ .point = point, .range = range };
    }

    pub fn applyFindAndMotionOperations(cp: u21, find_op: FindOperation, motion_op: MotionOperation, repeats: usize) !void {
        const result = getRange(cp, find_op, repeats) orelse return error.NoResult;

        state().last_found_cp = cp;
        state().last_find_and_motion_op = .{
            .fop = find_op,
            .mop = motion_op,
        };

        switch (motion_op) {
            .move => try dmodify.buffer_window.setCursor(focused.bufferWindow(), result.point),
            .yank => try yankText(result.range),
            .change => try changeText(result.range),
            .delete => try deleteText(result.range),
        }
    }
};

pub const replace_dialog = struct {
    pub const ReplaceDialog = struct {
        repeats: u32 = 1,
        pub fn init(repeats: usize) @This() {
            return .{ .repeats = @truncate(repeats) };
        }
        pub fn deinit(_: *anyopaque) void {}

        pub fn callback(ptr: *anyopaque, text: []const u8) core.Dialog.Result {
            const self: *@This() = @ptrCast(@alignCast(ptr));
            const cursor = focused.cursor();
            for (0..self.repeats) |offset| {
                dmodify.buffer.replaceRange(
                    focused.handle(),
                    .{ .start = cursor.addCol(@intCast(offset)), .end = cursor.addCol(@intCast(1 + offset)) },
                    text,
                ) catch return .done;
            }
            dmodify.buffer_window.setCursor(focused.bufferWindow(), cursor.addCol(self.repeats -| 1)) catch return .done;

            return .done;
        }
    };

    pub const StartReplaceDialog = struct {
        pub fn regular(repeats: usize) anyerror!void {
            core.dialog.textDialog(ReplaceDialog, .{repeats}, "Enter character:", .low) catch return;
        }

        pub fn repeat(repeats: usize) anyerror!void {
            _ = repeats;
        }
    };
};

const insert = struct {
    pub fn insertAfter() void {
        var cursor = focused.cursor();
        dmodify.buffer_window.setCursor(focused.bufferWindow(), cursor.addCol(1)) catch return;
        setMode(.insert);
    }

    pub fn setInsertStartOfRow() void {
        const cursor = focused.cursor();
        const handle = focused.handle();

        const p = core.motions.firstNonWhiteSpace(state().gpa, handle, cursor);
        dmodify.buffer_window.setCursor(focused.bufferWindow(), p) catch return;
        setMode(.insert);
    }

    pub fn setInsertEndOfRow() void {
        const h = focused.handle();
        const c = focused.cursor();
        dmodify.buffer_window.setCursor(focused.bufferWindow(), core.motions.endOfRow(h, c) orelse return) catch return;
        setMode(.insert);
    }

    pub fn insertNewLineAfter() void {
        const c = focused.cursor().addRow(1).setCol(0);
        dmodify.buffer.insertAt(focused.handle(), c, "\n") catch return;
        dmodify.buffer_window.setCursor(focused.bufferWindow(), c) catch return;
        setMode(.insert);
    }

    pub fn insertNewLineBefore() void {
        const c = focused.cursor().setCol(0);
        dmodify.buffer.insertAt(focused.handle(), c, "\n") catch return;
        dmodify.buffer_window.setCursor(focused.bufferWindow(), c) catch return;
        setMode(.insert);
    }
};

const visual = struct {
    pub fn offsetLastVisual(point: Point) ?Buffer.PointRange {
        const prange = state().last_visual_selection orelse return null;
        const prange_row_diff = prange.end.row - prange.start.row;
        const prange_col_diff = core.utils.diff(prange.end.col, prange.start.col);

        return switch (state().last_selection_kind) {
            .regular, .block => .{
                .start = point,
                .end = .{
                    .row = point.row + prange_row_diff,
                    .col = if (prange_row_diff == 0) point.col + prange_col_diff else prange.end.col,
                },
            },

            .line => .{
                .start = point.setCol(0),
                .end = .{
                    .row = point.row + prange_row_diff -| 1,
                    .col = focused.codePointCountAtLine(point.row + prange_row_diff -| 1) - 1,
                },
            },
        };
    }

    pub const yank_text = struct {
        pub fn regular(_: usize) anyerror!void {
            var selection = focused.getSelection();
            const cursor = focused.cursor();
            if (selection.selected()) {
                const range = selection.get(cursor);
                try yankText(range);
                setMode(.normal);
                try dmodify.buffer_window.setCursor(focused.bufferWindow(), range.start);
            }
        }
    };

    pub const delete_text = struct {
        pub fn regular(_: usize) anyerror!void {
            var selection = focused.getSelection();
            const cursor = focused.cursor();
            if (selection.selected()) {
                const point_range = selection.get(cursor);
                // set mode before setting cursor so we don't lose the selection when repeating
                setMode(.normal);

                deleteSelection(selection.kind, point_range) catch return;
                try dmodify.buffer_window.setCursor(focused.bufferWindow(), point_range.start);
            }
        }

        pub fn repeat(_: usize) anyerror!void {
            const cursor = focused.cursor();
            const point_range = offsetLastVisual(cursor) orelse return;

            deleteSelection(state().last_selection_kind, point_range) catch return;
            try dmodify.buffer_window.setCursor(focused.bufferWindow(), point_range.start);
        }
    };

    pub const change_text = struct {
        pub fn regular(_: usize) anyerror!void {
            var selection = focused.getSelection();
            const cursor = focused.cursor();

            if (selection.selected()) {
                const point_range = selection.get(cursor);
                // set mode before setting cursor so we don't lose the selection when repeating
                setMode(.insert);

                switch (selection.kind) {
                    .line => dmodify.buffer.replaceRange(focused.handle(), point_range, "\n") catch return,
                    .regular, .block => deleteSelection(selection.kind, point_range) catch return,
                }

                state().insertion_recording_anchor = point_range.start;
                try dmodify.buffer_window.setCursor(focused.bufferWindow(), point_range.start);

                if (selection.kind == .block) {
                    requestOnce(.insert_exit, .{ .insert_at_block_range = .{ .range = point_range, .exclude_first_row = true } });
                }
            }
        }

        pub fn repeat(_: usize) anyerror!void {
            const cursor = focused.cursor();
            const point_range = offsetLastVisual(cursor) orelse return;

            deleteText(point_range) catch return;
            applyChangesFromRecorderOffset(point_range.start.row, point_range.start.col);

            try dmodify.buffer_window.setCursor(focused.bufferWindow(), point_range.start);
        }
    };

    pub fn BlockVisualToInsert(comptime pos: enum { before, after }) type {
        return struct {
            const offset = switch (pos) {
                .before => 0,
                .after => 1,
            };

            pub fn regular(_: usize) anyerror!void {
                const selection = focused.getSelection();
                if (selection.kind != .block) return;

                const range = selection.get(focused.cursor());
                setMode(.insert);
                try dmodify.buffer_window.setCursor(focused.bufferWindow(), range.start.addCol(offset));
                requestOnce(.insert_exit, .{ .insert_at_block_range = .{ .range = range, .exclude_first_row = true } });
            }

            pub fn repeat(_: usize) anyerror!void {
                if (state().last_selection_kind != .block) return;
                const range = offsetLastVisual(focused.cursor()) orelse return;
                applyChangesAtBlockRange(range, false);
            }
        };
    }

    pub fn resetLastVisualSelection() void {
        const last_visual_selection = state().last_visual_selection orelse return;

        var bw = focused.bufferWindow();
        setMode(.visual);
        focused.setSelectionAnchor(last_visual_selection.start);
        bw.data.setCursor(last_visual_selection.end.subCol(1));
    }

    pub fn setVisualAndSelectionKindContainer(comptime kind: Buffer.Selection.Kind) type {
        return struct {
            pub fn regular(_: usize) anyerror!void {
                setMode(.visual);
                try dmodify.buffer.setSelection(focused.handle(), .{ .set_kind = kind });
            }
        };
    }

    pub fn setSelectionKindContainer(comptime kind: Buffer.Selection.Kind) type {
        return struct {
            pub fn regular(_: usize) anyerror!void {
                setSelectionKind(kind);
            }
        };
    }

    pub fn setSelectionKind(kind: Buffer.Selection.Kind) void {
        const selection = focused.getSelection();
        const current_kind = if (selection.selected()) selection.kind else return;

        if (current_kind == kind)
            setMode(.normal)
        else
            dmodify.buffer.setSelection(focused.handle(), .{ .set_kind = kind }) catch return;
    }
};

const history = struct {
    pub fn pushHistory() void {
        const h = focused.handle();
        dmodify.history.pushHistory(h) catch return;
    }

    pub fn undo() void {
        const h = focused.handle();
        dmodify.history.undo(h) catch return;
    }

    pub fn redo() void {
        const h = focused.handle();
        dmodify.history.redo(h) catch return;
    }
};

fn deleteSelection(selection_kind: Buffer.Selection.Kind, range: PointRange) !void {
    try replaceSelection(selection_kind, "", range);
}

fn replaceSelection(selection_kind: Buffer.Selection.Kind, string: []const u8, range: PointRange) !void {
    switch (selection_kind) {
        .block => {
            const scol = range.start.col;
            const ecol = range.end.col;
            var row = range.end.row + 1;
            while (row > range.start.row) {
                row -= 1;

                if (focused.codePointCountAtLine(@intCast(row)) > scol + 1) {
                    try dmodify.buffer.replaceRange(focused.handle(), .{
                        .start = .{ .row = row, .col = scol },
                        .end = .{ .row = row, .col = ecol },
                    }, string);
                }
            }
        },
        else => {
            try dmodify.buffer.replaceRange(focused.handle(), range, string);
        },
    }
}

const jump_list = struct {
    pub const goto_next_jump = struct {
        pub fn regular(_: usize) anyerror!void {
            try dmodify.jump_list.jumpNext(focused.bufferWindow());
        }
    };

    pub const goto_prev_jump = struct {
        pub fn regular(_: usize) anyerror!void {
            try dmodify.jump_list.jumpPrev(focused.bufferWindow());
        }
    };
};

const insert_tab_character = struct {
    pub fn regular(_: usize) anyerror!void {
        characterInput("\t");
    }
};

const RequestOnce = struct {
    const Event = enum {
        insert_exit,
    };
    const Request = union(enum) {
        insert_at_block_range: struct {
            range: PointRange,
            exclude_first_row: bool = true,
        },

        pub fn call(self: @This()) void {
            switch (self) {
                .insert_at_block_range => |v| applyChangesAtBlockRange(v.range, v.exclude_first_row),
            }
        }
    };

    event: Event,
    request: Request,
};

fn requestOnce(event: RequestOnce.Event, request: RequestOnce.Request) void {
    state().one_time_calls.append(state().gpa, .{
        .event = event,
        .request = request,
    }) catch return;
}

pub fn handleRequestOnce(event: RequestOnce.Event) void {
    var i: usize = 0;
    while (i < state().one_time_calls.items.len) {
        const call_once = state().one_time_calls.items[i];
        if (call_once.event == event) {
            state().one_time_calls.orderedRemove(i).request.call();
        } else {
            i += 1;
        }
    }
}

fn applyChangesAtBlockRange(range: PointRange, exclude_first_row: bool) void {
    switch (state().last_selection_kind) {
        .regular, .line => return,
        .block => {
            const col = range.start.col;
            var row = range.end.row;

            while (row > range.start.row) : (row -= 1) {
                if (focused.codePointCountAtLine(row) <= col) continue;
                applyChangesFromRecorder(row);
            }

            if (!exclude_first_row) {
                applyChangesFromRecorder(range.start.row);
            }
        },
    }
}

fn applyChangesFromRecorder(row: u32) void {
    for (state().change_recorder.changes.items) |change| {
        const col = change.point.col;
        switch (change.change) {
            .insert => |v| {
                const string = state().change_recorder.strings.items[v.offset .. v.offset + v.len];
                dmodify.buffer.insertAt(
                    focused.handle(),
                    .{ .row = row, .col = col },
                    string,
                ) catch return;
            },
            .delete_characters => |delete| {
                dmodify.buffer.deleteAfter(
                    focused.handle(),
                    .{ .row = row, .col = col },
                    @intCast(delete),
                ) catch return;
            },
        }
    }
}

fn applyChangesFromRecorderOffset(row: u32, arg_col: u32) void {
    for (state().change_recorder.changes.items) |change| {
        const offset = state().change_recorder.changes.items[0].point.col;
        const col = arg_col + core.utils.diff(change.point.col, offset);
        switch (change.change) {
            .insert => |v| {
                const string = state().change_recorder.strings.items[v.offset .. v.offset + v.len];
                dmodify.buffer.insertAt(
                    focused.handle(),
                    .{ .row = row, .col = col },
                    string,
                ) catch return;
            },
            .delete_characters => |delete| {
                dmodify.buffer.deleteAfter(
                    focused.handle(),
                    .{ .row = row, .col = col },
                    @intCast(delete),
                ) catch return;
            },
        }
    }
}

fn pushJumpPoint(should_jump: bool) void {
    if (should_jump)
        dmodify.jump_list.pushCurrentPoint() catch return;
}

fn getWordDelimiters(buffer_type: []const u8) []const u21 {
    return core.config.get(buffer_type).word_delimiters.slice();
}
