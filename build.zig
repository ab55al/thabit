const std = @import("std");
const print = std.debug.print;
const Build = std.Build;
const Module = std.Build.Module;
const join = std.fs.path.join;

const src_path = thisDir() ++ "/src";

inline fn thisDir() []const u8 {
    return comptime std.fs.path.dirname(@src().file) orelse ".";
}

pub const input_layers = struct {
    pub fn vimLike(bob: *Build) *Module {
        const vim_like_input_layer_root_path = thisDir() ++ "/src/input-layers/vim-like.zig";
        return bob.createModule(.{ .root_source_file = .{ .path = vim_like_input_layer_root_path } });
    }

    pub fn standard(bob: *Build) *Module {
        const standard_input_layer_root_path = thisDir() ++ "/src/input-layers/standard-input-layer/src/main.zig";
        return bob.createModule(.{ .root_source_file = .{ .path = standard_input_layer_root_path } });
    }
};

pub const BuildOptions = struct {
    input_layer_module: ?*Module = null,
    user_module: ?*Module = null,
    plugins_modules: []const *Module = &.{},
    use_freetype: bool = true,
};

pub var build_options = BuildOptions{};

pub fn createCore(bob: *Build, target: Build.ResolvedTarget, optimize: std.builtin.OptimizeMode) *Module {
    var input_layer_module = build_options.input_layer_module orelse input_layers.vimLike(bob);

    var options = bob.addOptions();
    options.addOption(bool, "init_user", build_options.user_module != null);
    options.addOption([]const u8, "assets_path", thisDir() ++ "/assets");
    options.addOption(bool, "use_freetype", build_options.use_freetype);

    const core_module = bob.addModule("core", .{
        .root_source_file = .{ .path = thisDir() ++ "/src/core.zig" },
        .target = target,
        .optimize = optimize,
        .imports = &.{
            .{ .name = "input_layer", .module = input_layer_module },
            .{ .name = "thabit_options", .module = options.createModule() },
        },
    });

    input_layer_module.addImport("core", core_module);

    if (build_options.user_module) |um| {
        core_module.addImport("user", um);
    }

    return core_module;
}

pub fn build(bob: *Build) void {
    const target = bob.standardTargetOptions(.{});
    const optimize = bob.standardOptimizeOption(.{});

    const core_module = createCore(bob, target, optimize);

    const exe = bob.addExecutable(.{
        .name = "thabit",
        .root_source_file = .{ .path = thisDir() ++ "/src/main.zig" },
        .optimize = optimize,
        .target = target,
        // .use_llvm = false,
        // .use_lld = false,
        .link_libc = true,
    });
    exe.root_module.addImport("core", core_module);

    exe.addIncludePath(.{ .path = thisDir() ++ "/glad/include" });
    exe.addCSourceFile(.{
        .file = .{ .path = thisDir() ++ "/glad/src/gl.c" },
        .flags = &.{},
    });

    if (build_options.use_freetype) {
        // adding to the core module automatically adds to the exe
        core_module.linkSystemLibrary("freetype2", .{});
    }
    exe.linkSystemLibrary("sdl2");

    bob.installArtifact(exe);

    addRunStep(bob, exe);

    // const tests: []const []const u8 = &.{
    //     "buffer",
    //     "vim-like",
    // };

    // inline for (tests) |test_name| {
    //     const t = bob.addTest(.{
    //         .name = test_name,
    //         .root_source_file = .{ .path = "tests/" ++ test_name ++ ".zig" },
    //         .optimize = optimize,
    //         .target = target,
    //     });
    //     // t.main_mod_path = .{ .path = thisDir() };
    //     t.root_module.addImport("core", core_module);

    //     const run_tests = bob.addRunArtifact(t);
    //     const test_step = bob.step("test-" ++ test_name, "Run tests for " ++ test_name);
    //     test_step.dependOn(&run_tests.step);

    //     const install_test = bob.addInstallArtifact(t, .{});
    //     const install_step = bob.step("itest-" ++ test_name, "Installs the binary of the " ++ test_name ++ " test");
    //     install_step.dependOn(&install_test.step);
    // }
}

pub fn addRunStep(bob: *std.Build, exe: *std.Build.Step.Compile) void {
    const run_cmd = bob.addRunArtifact(exe);
    run_cmd.step.dependOn(bob.getInstallStep());

    const run_step = bob.step("run", "Run the editor");
    run_step.dependOn(&run_cmd.step);
}
