const std = @import("std");
const expectEqualStrings = std.testing.expectEqualStrings;
const allocator = std.testing.allocator;

const core = @import("core");
const layer = core.input_layer;
const k = core.input.Key.create;

pub fn TomTestText(comptime strings: [3][]const u8) type {
    return struct {
        left: []const u8 = strings[0],
        target: []const u8 = strings[1],
        right: []const u8 = strings[2],
        text: []const u8 = strings[0] ++ strings[1] ++ strings[2],
        expected: []const u8 = strings[0] ++ strings[2],

        pub fn setCursor(test_text: @This(), h: core.BufferHandle) !void {
            core.focused.setCursor(
                try core.buffer.getPoint(
                    h,
                    std.mem.indexOf(u8, test_text.text, test_text.target).?,
                ),
            );
        }
    };
}

pub fn setupBuffer(text: []const u8) !core.BufferHandle {
    const h = try core.buffer.openFP("", .{});
    try core.edit.setContent(h, text);
    return h;
}

test "change tom" {
    try core.initThabit(std.testing.allocator);
    defer core.deinitThabit();

    const tt = TomTestText(.{ "hello ", "there", " extra text\n" }){};
    const h = try setupBuffer(tt.text);

    try tt.setCursor(h);

    const bline = try core.buffer.getLine(allocator, h, 0);
    defer allocator.free(bline);

    layer.keyInput(&.{ k(.none, .c), k(.none, .w) });

    const aline = try core.buffer.getLine(allocator, h, 0);
    defer allocator.free(aline);

    try expectEqualStrings(tt.expected, aline);
}

test "visual block change" {
    try core.initThabit(std.testing.allocator);
    defer core.deinitThabit();

    const text =
        \\hello there
        \\hello there
        \\hello there
        \\extra
        \\
    ;

    const expected =
        \\hey there
        \\hey there
        \\hey there
        \\extra
        \\
    ;
    _ = try setupBuffer(text);
    core.focused.setCursor(.{ .row = 0, .col = 0 });
    layer.keyInput(&.{k(.control, .v)});
    core.focused.setCursor(.{ .row = 2, .col = 4 });
    layer.keyInput(&.{k(.none, .c)});
    layer.characterInput("hey");
    layer.keyInput(&.{k(.none, .escape)});

    const lines = try core.focused.getLines(allocator, 0, std.math.maxInt(u32));
    defer allocator.free(lines);
    try expectEqualStrings(expected, lines);
}
